import React, { Component } from "react";
import * as log from "loglevel";

import authentication from "../src/assets/images/landing-page/something.png";
import logoImg from "../src/assets/images/landing-page/rocketreach-logo_bg.png";

/**
 * Parent error boundary to handle all errors occured in application and show fallback UI insted of crashing.
 * This must be a class component.
 * Error won't show up in production mode, it's just a development tool duplicating the normal browser console.
 * This won't work for:
        Event handlers 
        Asynchronous code (e.g. setTimeout or requestAnimationFrame callbacks)
        Server side rendering
        Errors thrown in the error boundary itself (rather than its children)    
 */

export default class ParentErrorBoundary extends Component {
  constructor(props) {
    log.setLevel(0, true); //setting this to the lowest level so that all msgs will be logged
    super(props);
    this.state = { error: null, errorInfo: null };
  }

  //this method makes this component a ErrorBoundary component
  componentDidCatch(error, errorInfo) {
    // Catch errors in any components below and re-render with error message
    this.setState({
      error: error,
      errorInfo: errorInfo,
    });
    //Log error message here
    log.info(`Error:: ${JSON.stringify(errorInfo)}`);
  }

  render() {
    if (this.state.errorInfo) {
      return (
        <div>
          <div class="container">
              <div class="row mb-3 logobgpgfound">
              <div class="col-sm-4 text-center mt-3"><img src={logoImg} alt="" class="img-fluid" /></div>
              </div>
          </div>
          <section className="pagenotfound pb-5">
            <div className="container">
              <div className="row">
               <div className="text-center col-sm-8 mx-auto">
                  <img src={authentication} className="img-fluid" alt="" />          
                  <p class="text-center lastlinepgfnd">
                    Something went wrong. Please click this 
                    </p>
                    <p class="text-center lastlinepgfnd">
                      <a href="/"
                      onClick={() => {
                        this.props.history.push("/");
                      }} className="btn_light_green pageerror_link">Go Back</a>
                    </p>      
                 
                </div>
              </div>
            </div>
          </section>          
        </div>
      );
    } else {
      // Normally, just render children
      return this.props.children;
    }
  }
}
