import React from "react";
import { Route } from "react-router-dom";

const PublicRTHandler = ({ component: Component, ...params }) => (
  <Route {...params} render={props => <Component {...props} {...params} />} />
);

export default PublicRTHandler;
