import React from "react";
import { Route, Redirect } from "react-router-dom";
import CommonLayout from "../components/Layout/index";
/**
 * PrivateRoute serves as a wrapper component to all of the routes that need authentication.
 * @param {boolean} authed if false then returns to loginpage else to the props component
 * @param {Object} component
 */

const PrivateRTHandler = ({
  component: Component,
  role,
  authed,
  ...params
}) => (
  <Route
    {...params}
    render={(props) =>
      authed === true ? (
        <CommonLayout role={role} {...props}>
          <Component {...props} {...params} />
        </CommonLayout>
      ) : (
        <Redirect
          to={{
            pathname: "/",
            state: {
              from: props.location,
              message: "Please Sign in to Continue",
            },
          }}
        />
      )
    }
  />
);

export default PrivateRTHandler;