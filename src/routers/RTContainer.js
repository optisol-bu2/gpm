import { connect } from "react-redux";

import Router from "./Router";
import {
  hideDisplayMessage,
  callRefreshTokenSaga,
} from "../Actions/actionContainer";

const mapStateToProps = (state) => {
  return {
    isUserAuthorize: state.loginReducer.isUserAuthorize,
    displayMessage: state.mainReducer.displayMessage,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    hideDisplayMessage: () => dispatch(hideDisplayMessage()),
    callRefreshTokenSaga: () => dispatch(callRefreshTokenSaga()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Router);
