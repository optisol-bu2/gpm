import React, { useEffect } from "react";
import {
  Switch,
  Route,
  BrowserRouter as Router,
} from "react-router-dom";

import PrivateRTHandler from "./PrivateRTHandler";
import ScrollToTop from "../utils/scroll";
import PublicRTHandler from "./PublicRTHandler";
import Profile from "../modules/Profile/Profile";
import Inbox from "../modules/Inbox/Inbox";
import About from "../modules/About/About";
import HelpAndSupport from "../modules/HelpAndSupport/HelpAndSupport";
import Blogger from "../modules/Blogger/Blogger";
import BloggerDetail from "../modules/Blogger/BloggerDetail";
import CartDetails from "../modules/MarketPlace/Container";
import Dashboard from "../modules/Dashboard/DashboardContainer";
import MainPage from "../modules/MainPage/MainPageContainer";
import SellerDashboard from "../modules/Dashboard/Seller/Container";
import Service from "../modules/Services/ServiceContainer";
import MarketPlaceParent from "../modules/MarketPlace/Master/Container";
import MarketPlaceDetail from "../modules/MarketPlace/Detail/Container";
import MyOrder from "../modules/MyOrder/MyOrderContainer";
import MyOrderDetail from "../modules/MyOrder/MyOrderDetailContainer";
import FavoriteWebsite from "../modules/FavoriteWebsite/FavoriteWebsite";
import Blog from "../modules/Blog/Blog";
import InviteFriend from "../modules/InviteFriend/InviteFriend";
import PageNotFound from "../components/core/PageNotFound";
import PrivacyPolicy from "../modules/MainPage/PrivacyPolicy";
import TermsCondition from "../modules/MainPage/TermsCondition";
/**
 * Parent App component with routing
 */
const AppRoute = (props) => {
  // window.history.pushState(null, null, window.location.href);
  useEffect(() => {
    try {
      if (props.isUserAuthorize) {
        props.callRefreshTokenSaga();
      }
    } catch (error) { }
    // window.onpopstate = function (e) {
    //   window.history.go(1);
    // };
  }, []);
  const NoMatchPage = () => {
    return (
      <h3 style={{ padding: 50 }}>
        Sorry, the page you are looking for could not be found.
      </h3>
    );
  };
  return (
    // <div>
    <Router basename="/">
      <ScrollToTop />
      <Switch>
        <PublicRTHandler
          {...props}
          exact
          path="/"
          component={MainPage}
        ></PublicRTHandler>
        <PublicRTHandler
          {...props}
          exact
          path="/resetpassword"
          component={MainPage}
        ></PublicRTHandler>
        <PublicRTHandler
          {...props}
          exact
          path="/signin"
          component={MainPage}
        ></PublicRTHandler>
        <PublicRTHandler
          {...props}
          exact
          path="/signup"
          component={MainPage}
        ></PublicRTHandler>
        <PublicRTHandler
          {...props}
          exact
          path="/seller"
          component={MainPage}
        />
        <PublicRTHandler
          {...props}
          exact
          path="/activeaccount/:token"
          component={MainPage}
        ></PublicRTHandler>
        <PrivateRTHandler
          exact
          path="/marketplace"
          authed={props.isUserAuthorize}
          role={2}
          component={MarketPlaceParent}
        />
        <PrivateRTHandler
          exact
          path="/dashboard"
          authed={props.isUserAuthorize}
          role={2}
          component={Dashboard}
        />
        <PrivateRTHandler
          exact
          path="/marketplace/:id"
          role={2}
          authed={props.isUserAuthorize}
          component={MarketPlaceDetail}
        />
        <PrivateRTHandler
          exact
          authed={props.isUserAuthorize}
          path="/profile/details"
          component={Profile}
        />
        <PrivateRTHandler
          exact
          authed={props.isUserAuthorize}
          path="/profile/info"
          component={Profile}
        />
        <PrivateRTHandler
          exact
          authed={props.isUserAuthorize}
          path="/profile/billing"
          component={Profile}
        />
        <PrivateRTHandler
          exact
          authed={props.isUserAuthorize}
          path="/profile/card"
          component={Profile}
        />
        <PrivateRTHandler
          exact
          authed={props.isUserAuthorize}
          path="/cartdetails"
          component={CartDetails}
        />
        <PrivateRTHandler
          exact
          path="/sellerDashboard"
          authed={props.isUserAuthorize}
          role={2}
          component={SellerDashboard}
        />
        <PrivateRTHandler
          exact
          path="/services"
          authed={props.isUserAuthorize}
          component={Service}
        />
        <PrivateRTHandler
          exact
          path="/myOrder"
          authed={props.isUserAuthorize}
          role={2}
          component={MyOrder}
        />
        <PrivateRTHandler
          exact
          path="/myOrder/:id"
          authed={props.isUserAuthorize}
          role={2}
          component={MyOrderDetail}
        />
        <PrivateRTHandler
          exact
          path="/inbox"
          authed={props.isUserAuthorize}
          role={2}
          component={Inbox}
        />
        <PrivateRTHandler
          exact
          path="/favorite"
          authed={props.isUserAuthorize}
          role={2}
          component={FavoriteWebsite}
        />
        <PrivateRTHandler
          exact
          path="/blogs"
          authed={props.isUserAuthorize}
          role={2}
          component={Blog}
        />
        <PrivateRTHandler
          exact
          path="/about"
          authed={props.isUserAuthorize}
          role={2}
          component={About}
        />
        <PrivateRTHandler
          exact
          path="/help"
          authed={props.isUserAuthorize}
          role={2}
          component={HelpAndSupport}
        />
        <PrivateRTHandler
          exact
          path="/contentWriter"
          authed={props.isUserAuthorize}
          role={2}
          component={Blogger}
        />
        <PrivateRTHandler
          exact
          path="/contentWriter/:id"
          authed={props.isUserAuthorize}
          role={2}
          component={BloggerDetail}
        />
        <PrivateRTHandler
          exact
          path="/invite"
          authed={props.isUserAuthorize}
          role={2}
          component={InviteFriend}
        />
         <PublicRTHandler
          {...props}
          exact
          path="/seller"
          component={MainPage}
        />
         <PublicRTHandler
          {...props}
          exact
          path="/privacypolicy"
          component={MainPage}
        />
         <PublicRTHandler
          {...props}
          exact
          path="/termscondition"
          component={MainPage}
        />
        <Route component={PageNotFound} />
      </Switch>
    </Router>
    // <DisplayMessage
    //   {...props.displayMessage}
    //   onClose={() => props.hideDisplayMessage()}
    // />
    // </div>
  );
};

export default AppRoute;
