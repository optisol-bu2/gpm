/**
 * This file contains all the global constants used across the application
 */
function backEndConstant() {
	var backendObject = {};
	if (process.env.REACT_APP_CUSTOM_NODE_ENV === 'develop') {
		//develop environment variables
		backendObject.backend_url = "https://gpm.service.optisolbusiness.com/api/v1/";
		backendObject.blog_backend_url = "https://gpm.dev.optisolbusiness.com/blog/wp-json/gpm/v1/";
		backendObject.frontend_url = "https://gpm.dev.optisolbusiness.com";
		backendObject.google_key = "1007818822993-jllg7s8b2hbnukraeh9hdg39n34aogfr.apps.googleusercontent.com";
		backendObject.facebook_key = "437270654256934";
	} else if (process.env.REACT_APP_CUSTOM_NODE_ENV === 'staging') {
		//develop environment variables
		backendObject.backend_url = "https://backend.gpmtestdevelopment.com/api/v1/";
		backendObject.blog_backend_url = "https://gpmtestdevelopment.com/blog/wp-json/gpm/v1/";
		backendObject.frontend_url = "https://gpmtestdevelopment.com";
		backendObject.google_key = "1007818822993-jllg7s8b2hbnukraeh9hdg39n34aogfr.apps.googleusercontent.com";
		backendObject.facebook_key = "437270654256934";
	} else if (process.env.REACT_APP_CUSTOM_NODE_ENV === 'production') {
		//develop environment variables
		backendObject.backend_url = "https://dragonfruitfly.enginescale.com/api/v1/";
		backendObject.blog_backend_url = "https://enginescale.com/blog/wp-json/gpm/v1/";
		backendObject.frontend_url = "https://enginescale.com";
		backendObject.google_key = "1007818822993-jllg7s8b2hbnukraeh9hdg39n34aogfr.apps.googleusercontent.com";
		backendObject.facebook_key = "437270654256934";
	} else {
		//local environment variables
		backendObject.backend_url = "https://gpm.service.optisolbusiness.com/api/v1/";
		backendObject.blog_backend_url = "https://gpm.dev.optisolbusiness.com/blog/wp-json/gpm/v1/";
		backendObject.frontend_url = "http://localhost:3000";
		backendObject.google_key = "1007818822993-jllg7s8b2hbnukraeh9hdg39n34aogfr.apps.googleusercontent.com";
		backendObject.facebook_key = "437270654256934";
	}
	return backendObject;
}

const BASE_URL = backEndConstant().backend_url;
const BLOG_BASE_URL = backEndConstant().blog_backend_url;
const FRONT_END_URL = backEndConstant().frontend_url;
const GOOGLE_KEY = backEndConstant().google_key;
const FACEBOOK_KEY = backEndConstant().facebook_key;
const PUSHER_KEY = process.env.REACT_APP_PUSHER_KEY;
const PUSHER_CLUSTER = process.env.REACT_APP_PUSHER_CLUSTER;
const STRIPE_KEY = process.env.REACT_APP_STRIPE_KEY;
const API = {
	USER_LOGIN: "login",
	USER_LOGOUT: "logOut",
	DASHBOARD: "dashboard",
	ANALYTICS_DETAILS: "analyticsdetails",
	GET_DATA_TOTALSPENT: "logOut",
	GET_DATA_STATISTICS: "logOut",
	GET_DATA_TRAFFICANALYTICS: "logOut",
	GET_DATA_DAILYTRAFFIC: "logOut",
	SWITCH_USER: "switchUser",
	USER_SOCIAL_LOGIN: "sociallogin",
	RE_LOGIN: "profiledetail",
	GET_SERVICE_CATEGORIES: "serviceCategory",
	GET_SERVICE_ESTIMATED_DELIVERY: "serviceEstimatedDelivery",
	GET_SERVICE_ESTIMATED_EXPERTISE: "expertise",
	SEARCH_SERVICE_USER: "serviceUser/search",
	ADD_SERVICE: "service",
	ADD_CUSTOM_ORDER: "customorder",
	VIEW_QUESTIONS: "questions",
	ADD_QUESTIONS:'questions/submit',
	REMOVE_SERVICE: "service",
	REMOVE_CUSTOM_SERVICE: "customorder",
	CHANGE_SERVICE_STATUS: "service/status",
	CHANGE_CUSTOM_STATUS: "customorder/status",
	REGISTER: "register",
	FORGOT_PASSWORD: "forgotpassword",
	VERIFY_TOKEN: "verifytoken",
	VERIFY_ACTIVATE_TOKEN: "activeaccount",
	RESET_PASSWORD: "resetpassword",
	CHECK_EMAIL: "isemailexist",
	PROFILE: "profile",
	USER_INFO: "userInfo",
	CHANGE_PASSWORD: "changePassword",
	COUNTRY: "country",
	STATE: "state",
	IS_NOTIFICATION: "isnotification",
	BILLING_INFO: "billingInfo",
	UPDATE_EMAIL_BILLING_INFO: "updateIsEmailBillingInfo",
	PROFILE_PIC: "profilepic",
	REASONS: "reasons",
	DEACTIVATE: "deactivate",
	BILLING_INFO_ACCOUNT_LINK: "billingInfoAccountLink",
	BILLING_INFO_STATUS: "getBillingInformationStatus",
	ADD_BILLING_TOKEN: "billingInfo/bankdetails",
	PAYPAL: "billingInfo/paypal",
	GET_PAYPAL_LINK: "getpaypalurl",
	CHANGE_DEFAULT_METHOD: "defaultPaymentMethod",
	USER_SERVICE: "service/user",
	USER_RECOMMENDATIONS: "service/recommendations",
	MARKETPLACE_SERVICE: "service/page",
	FILTER_MARKETPLACE: "service/filter",
	GET_SERVICE_DETAIL: "service",
	UPDATE_SERVICE: "service/edit",
	REFRESH_TOKEN: "refreshtoken",
	CART_REDIRECT: "cart",
	GET_CART_DATA: "cart",
	CART_BLOGGER: "cart/blogger",
	CART_CUSTOM: "cart/customorder",
	REMOVE_CART_ITEM: "cart",
	GET_SERVICEBACKLINK_DATA: "serviceBacklink",
	GET_SERVICEGUESTPOST_DATA: "serviceCost",
	GET_TOP_SERVICES_DATA: "getTopServices",
	GET_TOP_CONTENT_WRITERS: "getTopContentWriters",
	CREATE_ORDER: "order",
	GET_MYORDER_DATA: "currentorder/page",
	UPLOAD_ATTACHMENT: "order/attachment",
	REMOVE_ATTACHMENT: "order/attachment",
	START_ORDER: "order/start",
	GET_FILTER_ORDER_DATA: "currentorder/search",
	GET_SEARCH_ORDER_DATA: "currentorder/search",
	GET_ORDER_DATA: "order",
	GET_COMPLETE_ORDER_FILTER: "completedorder/search",
	GET_COMPLETE_ORDER_SEARCH: "completedorder/search",
	GET_CANCEL_ORDER_FILTER: "cancledorder/search",
	GET_CANCEL_ORDER_SEARCH: "cancledorder/search",
	GET_CANCEL_ORDER_DATA: "cancledorder/page",
	GET_COMPLETE_ORDER_DATA: "completedorder/page",
	APPROVE_ORDER: "order/approved",
	ASK_REVISION: "order/notapproved",
	GET_ATTACHMENTS: "order/update/attachment",
	DELETE_ATTACHMENT: "order/update/attachment",
	SEND_ORDER_UPDATE: "order/update",
	SUBMIT_FEEDBACK: "order/feedback",
	GET_FEEDBACK: "order/feedback",
	UPDATE_ATTACHMENT: "order/update/attachment",
	EXTEND_ORDER: "order/extend",
	CANCEL_ORDER: "order/cancel",
	SEND_MESSAGE: "message/send",
	DELETE_MESSAGE: "message",
	REJECT_CUSTOM_ORDER: "customorder/reject",
	CUSTOM_ORDER_DETAIL: "customorder",
	SEND_ATTACHMENT: "message/attachment",
	GET_CHATS: "message/users",
	HEADERINFO: "headerinfo",
	NOTIFICATIONS: "notifications",
	GET_CHAT_MESSAGES: "message",
	SEARCH_CHATS: "message/search/user",
	FAVORITE_WEBSITE: "favorite",
	FAVORITE_SERVICE: "favorite/service",
	FAVORITE_BLOGGER: "favorite/blogger",
	SEARCH_FAVORITE_WEBSITE: "favorite/search",
	PAYMENT_SESSION: "createPaymentSession",
	PAYMENT_SESSION_BLOGGER: "createPaymentSessionForBlogger",
	PAYMENT_SESSION_CUSTOM: "createPaymentSessionForCustomOrder",
	PLACE_ORDER: "order/place",
	CREATE_ORDER_PAYPAL: "createOrderForPaypal",
	SAVED_CARD: "card",
	CHECK_BILLING_INFO: "isBillingInfoAvailable",
	BLOGGER: "blogger",
	BLOGGERS: "bloggers/page",
	BLOGGERS_SEARCH: "bloggers/search",
	BLOGGER_PORTFOLIO: "blogger/portfolio",
	BLOG_LINK: "blogger/portfolio/blog",
	REMOVE_BLOG_LINK: "blogger/portfolio",
	BLOGGER_ABOUTME: "blogger/aboutme",
	ABOUT: "page",
	FAQs: "faqs",
	INVITE_FRIEND: "invite-friend",
	HELP_SUPPORT: "help-support",
	GET_BLOGS: "get_all_posts",
	KYCINFORMATION: "page/stripe_kyc"
};

const CONTENT_WRITER_DATA = [
	{id:1,expertise:"Technology, Design, and Engineering",rate:"15",aboutme:"Recent graduate in communications with a passion for technology, design, and engineering. Enjoy writing about mobile and web applications, artificial intelligence and Internet-of-Things (IoT), and the latest innovations and trends.",wordsperhour:500,review:4,username:"Bradford",totalsold:14},
	{id:2,expertise:"Household Appliances, Education, Sports",rate:"15",aboutme:"Passionate writer with a BA in English. Enjoy writing articles of all genres ranging from household appliances to educational products and services to sports. I write articles that will help you get noticed and stand out from the crowd.",wordsperhour:400,review:5,username:"Kendric",totalsold:10},
	{id:3,expertise:"Entertainment, TV Shows, Movies",rate:"18",aboutme:"Blogger with a background in the entertainment industry. I love to write articles about the latest TV shows, movies, and music. I follow up the latest news about celebrities and am ready to write spellbinding content about the latest news in Hollywood.",wordsperhour:350,review:4,username:"Jessica",totalsold:23},
	{id:4,expertise:"Legal",rate:"20",aboutme:"Blogger with a BA in Legal Studies. I help your content get noticed and can write about complex topics such as trusts, trademarks, and business law. I help law firms build their brand awareness by writing content to educate prospective clients and make a buying decision.",wordsperhour:550,review:5,username:"Orson",totalsold:12},
	{id:5,expertise:"E-Commerce",rate:"25",aboutme:"If you have a product that you want to sell, I am the best content writer who will write articles in your niche, whether you’re selling bicycles or scooters. I have helped numerous e-commerce businesses increase their organic traffic by writing informative articles on their products.",wordsperhour:450,review:4,username:"Suzanne",totalsold:7},
];

const PUBLIC_APIS = ["login", "isemailexist"];

module.exports = {
	BASE_URL,
	BLOG_BASE_URL,
	FRONT_END_URL,
	GOOGLE_KEY,
	FACEBOOK_KEY,
	API,
	PUBLIC_APIS,
	PUSHER_KEY,
	PUSHER_CLUSTER,
	STRIPE_KEY,
	CONTENT_WRITER_DATA,
};
