import { createStore, applyMiddleware } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";
import thunk from "redux-thunk";
import rootReducer from "../Reducers/RootReducer";
import createSagaMiddleware from "redux-saga";
import rootSaga from "../sagas/index";
import logger from "redux-logger";

const sagaMiddleware = createSagaMiddleware();
const middlewares = process.env.REACT_APP_CUSTOM_NODE_ENV === 'production' ? [thunk, sagaMiddleware] : [thunk, sagaMiddleware, logger] ;
const persistConfig = {
  key: "root",
  storage: storage,
  blacklist: ['inboxReducer']
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

/**
 * Store configuration by combining Reducers, Sagas and Middleware.
 * Added persistent storage to save the state when the page is refreshed/redirecte
 */
export default function configureStore() {
  let store = createStore(persistedReducer, applyMiddleware(...middlewares)); //sagaMiddleware
  sagaMiddleware.run(rootSaga);
  let persistor = persistStore(store);
  return { store, persistor };
}
