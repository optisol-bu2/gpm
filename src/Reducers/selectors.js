/**
 * This is a selector file created to access the store/central-state from saga directly.
 */

export const loginInfo = (state) => state.loginReducer.loginResponse;
export const token = (state) => state.loginReducer.token;
export const refreshInterval = (state) => state.loginReducer.refreshInteval;
