import {
  SHOW_DISPLAY_MESSAGE,
  HIDE_DISPLAY_MESSAGE,
  SERVICE_CATEGORIES_SUCCESS,
  SERVICE_ESTIMATED_DELIVERY_SUCCESS,
  SERVICE_ESTIMATED_EXPERTISE_SUCCESS,
  SET_TIMER_START,
  SET_24HR_TIMER,
} from "../Actions/actionType";
const initState = {
  serviceCategories: [],
  serviceEstimatedDelivery: [],
  serviceEstimatedExpertise: [],
  displayMessage: {},
  token: "",
  refreshInteval: 1000,
  isTimerEnd: false,
  lastHrTimer: false
};

const mainReducer = (state = initState, payload) => {
  switch (payload.type) {
    case SHOW_DISPLAY_MESSAGE:
      return {
        ...state,
        displayMessage: { open: true, ...payload.messageDetails },
      };

    case HIDE_DISPLAY_MESSAGE:
      return { ...state, displayMessage: { open: false } };
    case SERVICE_ESTIMATED_DELIVERY_SUCCESS:
      var deliveryArray = [];
      payload.data.forEach(data => {
        let deliveryObj = {
          label: data.name == 1 ? data.name+" day": data.name+" days",
          value: data.id.toString()
        }
        deliveryArray.push(deliveryObj);
      })
      return { ...state, serviceEstimatedDelivery: deliveryArray };
      case SERVICE_ESTIMATED_EXPERTISE_SUCCESS:
      var expertiseArray = [];
      payload.data.forEach(data => {
        let expertiseObj = {
          label: data.name,
          value: data.id
        }
        expertiseArray.push(expertiseObj);
      })
      return { ...state, serviceEstimatedExpertise: expertiseArray };
    case SERVICE_CATEGORIES_SUCCESS:
      var categoryArray = [];
      payload.data.forEach(data => {
        let categoryObj = {
          label: data.name,
          value: data.id.toString()
        }
        categoryArray.push(categoryObj);
      })
      return { ...state, serviceCategories: categoryArray };

      case SET_TIMER_START:
        return {
          ...state,
          isTimerEnd: payload.data,
        };
      case SET_24HR_TIMER:
        return {
          ...state,
          lastHrTimer: payload.data,
        };
    default:
      return state;
  }
};

export default mainReducer;
