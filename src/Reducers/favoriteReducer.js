import { FAVORITE_WEBSITE_SUCCESS } from "../Actions/actionType";
const initState = {
	favoriteWebsiteList: [],
};
/**
 * Assigns api response to reducer state
 * @param {*} state 
 * @param {*} object 
 * @returns 
 */
export const favoriteReducer = (state = initState, action ) => {
	var payload = action.data;
	switch (action.type) {
		case FAVORITE_WEBSITE_SUCCESS:
			return {
				...state,
				favoriteWebsiteList: payload.records,
			};
		default:
			return state;
	}
};