import {
  SUCCESS_FETCH_PROFILE,
  SUCCESS_FETCH_USERINFO,
  SUCCESS_FETCH_NOTIFICATION,
  SUCCESS_FETCH_BILLING,
  SUCCESS_FETCH_PROFILEPIC,
  SUCCESS_FETCH_REASONS,
  SUCCESS_BILLING_INFO_STATUS,
} from "../Actions/actionType";

const initState = {
  profile: {},
  userInfo: {},
  notification: {},
  billingInfo: {},
  profilePic: {},
  reasonsData: [],
  bankInfo: {},
};

export const profileReducer = (state = initState, action) => {
  var payload = action.data;
  switch (action.type) {
    case SUCCESS_FETCH_PROFILE:
      return {
        ...state,
        profile: payload,
      };
    case SUCCESS_FETCH_USERINFO:
      return {
        ...state,
        userInfo: payload,
      };
    case SUCCESS_FETCH_NOTIFICATION:
      return {
        ...state,
        notification: payload,
      };
    case SUCCESS_FETCH_BILLING:
      return {
        ...state,
        billingInfo: payload,
      };
    case SUCCESS_FETCH_PROFILEPIC:
      return {
        ...state,
        profilePic: payload,
      };
    case SUCCESS_BILLING_INFO_STATUS:
      return {
        ...state,
        bankInfo: payload,
      };
    case SUCCESS_FETCH_REASONS:
      var reasonArray = [];
      payload.forEach(data => {
        let reasonObj = {
          label: data.name,
          value: data.id.toString()
        }
        reasonArray.push(reasonObj);
      })
      return {
        ...state,
        reasonsData: reasonArray,
      };
    default:
      return state;
  }
};
