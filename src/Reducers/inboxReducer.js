import { 
	SEND_MESSAGE_SUCCESS,
	GET_CHATS_SUCCESS,
	GET_CHAT_MESSAGES_SUCCESS,
	UPDATE_CHAT_MESSAGE_SUCCESS,
	SEARCH_CHAT_LIST_SUCCESS,
	UPDATE_MESSAGE_SUCCESS,
	DELETE_MESSAGE_SUCCESS,
	GET_HEADERINFO_SUCCESS,
	CHANGE_MESSAGE,
} from "../Actions/actionType";
const initState = {
	chatList: [],
	chatMessages: [],
	chatHistoryDetails: {},
	headerInfo: {},
};
/**
 * Assigns api response to reducer state
 * @param {*} state 
 * @param {*} object 
 * @returns 
 */
export const inboxReducer = (state = initState, action ) => {
	var payload = action.data;
	switch (action.type) {
		case SEND_MESSAGE_SUCCESS:
			var messages = [...state.chatMessages];
			messages.push(payload);
			return {
				...state,
				chatMessages: messages,
			};
		case GET_CHATS_SUCCESS:
			return {
				...state,
				chatList: payload,
			};
		case GET_CHAT_MESSAGES_SUCCESS:
			return {
				...state,
				chatHistoryDetails: payload,
				chatMessages: payload.messages,
			};
		case SEARCH_CHAT_LIST_SUCCESS:
			return {
				...state,
				chatList: payload,
			};
		case UPDATE_MESSAGE_SUCCESS:
			var messages = [...state.chatMessages];
			messages.push(payload);
			return {
				...state,
				chatMessages: messages,
			};
		case DELETE_MESSAGE_SUCCESS:
			var messages = [...state.chatMessages];
			messages.forEach((message, index) => {
				if (message.id === payload.id) {
					messages.splice(index, 1);
				}
			})
			return {
				...state,
				chatMessages: messages,
			};
		case CHANGE_MESSAGE:
			var messages = [...state.chatMessages];
			messages.forEach((message, index) => {
				if (message.id === payload.id) {
					messages[index]= payload;
				}
			})
			return {
				...state,
				chatMessages: messages,
			};
		case UPDATE_CHAT_MESSAGE_SUCCESS:
			var messages = [...payload.messages, ...state.chatMessages];
			return {
				...state,
				chatMessages: messages,
			};
		case GET_HEADERINFO_SUCCESS:
			return {
				...state,
				headerInfo: payload,
			};
		default:
			return state;
	}
};
