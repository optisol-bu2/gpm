import {
  USER_LOGIN_SUCCESS,
  REGISTRATION_SUCCESS,
  SUCCESS_RELOGIN,
  ANALYTICS_SUCCESS,
  UPDATE_ANALYTICS,
  SWITCH_USER_SUCCESS,
  GET_REFRESH_TOKEN_SUCCESS,
  REMOVE_AUTHORIZATION,
  FORCE_LOGOUT,
  SET_TOTAL_ORGANIC,
  CHECK_CONGRATULATION
} from "../Actions/actionType";
const initState = {
  isLogin: false,
  userData: {},
  profilePicture: "",
  socialRegistration: false,
  roleChanged: false,
  loginResponse: {},
  isUserAuthorize: false,
  token: "",
  refreshInteval: 1000,
  clientId: '',
  viewId: '',
  organicValue: '0',
};
const loginReducer = (state = initState, payload) => {
  switch (payload.type) {
    case USER_LOGIN_SUCCESS:
      return {
        ...state,
        loginResponse: payload.data,
        isLogin: payload.data.isregistered != undefined ? payload.data.isregistered : true,
        userData: payload.data,
        profilePicture: payload.data,
        roleChanged: false,
        isUserAuthorize: true,
        token: payload.data.token,
        refreshInteval: payload.data.refresh_time,
        clientId: (payload.data.analytics_clientid && payload.data.analytics_clientid !== undefined)  ? payload.data.analytics_clientid : null,
        viewId: (payload.data.analytics_viewid && payload.data.analytics_viewid !== undefined) ? payload.data.analytics_viewid: null,
      };
    case REGISTRATION_SUCCESS:
      return {
        ...state,
        loginResponse: payload.data,
        isLogin: false,
        userData: payload.data,
        profilePicture: payload.data,
        roleChanged: false,
        isUserAuthorize: true,
        token: payload.data.token,
        refreshInteval: payload.data.refresh_time,
        clientId: (payload.data.analytics_clientid && payload.data.analytics_clientid !== undefined)  ? payload.data.analytics_clientid : null,
        viewId: (payload.data.analytics_viewid && payload.data.analytics_viewid !== undefined) ? payload.data.analytics_viewid: null,
      };
    case SWITCH_USER_SUCCESS:
      return {
        ...state,
        userData: payload.data,
      };
    case SUCCESS_RELOGIN:
      let userDataChange = {}
      userDataChange.id = state.userData.id
      userDataChange.email = state.userData.email
      userDataChange.username = payload.data.username
      userDataChange.profilepic = payload.data.profilepic
      userDataChange.usertype = state.userData.usertype
      userDataChange.activeusertype = state.userData.activeusertype
      userDataChange.onlinestatus = payload.data.onlinestatus
      userDataChange.status = state.userData.status
      userDataChange.token = state.userData.token
      userDataChange.expiry_time = state.userData.expiry_time
      userDataChange.refresh_time = state.userData.refresh_time
      userDataChange.isfirstimelogin = false
      return {
        ...state,
        isLogin: true,
        userData: userDataChange,
        profilePicture: payload.data.profilepic,
        roleChanged: false,
      };
    case GET_REFRESH_TOKEN_SUCCESS:
      return {
        ...state,
        token: payload.data.token,
        refreshInteval: payload.data.refresh_time,
      };
    case ANALYTICS_SUCCESS:
      return {
        ...state,
        clientId: (payload.data.analytics_clientid && payload.data.analytics_clientid !== undefined)  ? payload.data.analytics_clientid : null,
        viewId: (payload.data.analytics_viewid && payload.data.analytics_viewid !== undefined) ? payload.data.analytics_viewid: null,
      };
    case UPDATE_ANALYTICS:
      return {
        ...state,
        clientId: payload.data.clientId,
        viewId: payload.data.viewId,
      };
    case REMOVE_AUTHORIZATION:
      return {
        ...state,
        isLogin: false,
        userData: {},
        profilePicture: "",
        socialRegistration: false,
        roleChanged: false,
        loginResponse: {},
        isUserAuthorize: false,
        token: "",
        refreshInteval: 1000,
        clientId: '',
        viewId: '',
        organicValue:'0'
      };
    case FORCE_LOGOUT:
      return { ...initState };
    case SET_TOTAL_ORGANIC:
      return {
        ...state,
        organicValue: payload.data,
      };
      case CHECK_CONGRATULATION:
        var iscongratulationmodel=payload.data
        return {
          ...state,
          userData:{
            ...state.userData,iscongratulationmodel:iscongratulationmodel
          }
        };
    default:
      return state;
  }
};
export default loginReducer;
