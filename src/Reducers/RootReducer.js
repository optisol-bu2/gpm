import { combineReducers } from "redux";
import loginReducer from "./loginReducer";
import { profileReducer } from "./profileReducer";
import { inboxReducer } from "./inboxReducer";
import mainReducer from "./mainReducer";
import { favoriteReducer } from "./favoriteReducer";
import { FORCE_LOGOUT } from "../Actions/actionType"

const appReducer = combineReducers({
  loginReducer,
  profileReducer,
  inboxReducer,
  mainReducer,
  favoriteReducer,
});

const rootReducer = (state, action) => {
  if (action.type === FORCE_LOGOUT) {
    state = undefined;
  }

  return appReducer(state, action);
};

export default rootReducer;