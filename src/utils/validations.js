export function isValidMail(email) {
    let reg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (reg.test(String(email).toLowerCase())) {
      return true;
    }
    return false;
  }
  
  export function isNotEmpty(val) {
    if (val) {
      return true;
    }
    return false;
  }
  
  export const validatePassword = password => {
    if (!isNotEmpty(password)) {
      return "Please Enter The Password.";
    } else if (!/^.{8,}$/.test(password)) {
      return "Password Should Have Minimum Eight Characters.";
    } else if (
      !/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d)[A-Za-z\d@$!~%*#?&]{8,}$/.test(
        password
      )
    ) {
      return "Password Should Have Combination Of Alphanumeric and One upper character.";
    }
  };
  