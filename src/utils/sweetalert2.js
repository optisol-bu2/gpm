import Swal from "sweetalert2";
import like from "../assets/music/notification.mp3";

const likeAudio = new Audio(like);
let audio =false;

const Toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  showCloseButton: false,
  customClass: {
    popup: 'colored-toast',
    icon: 'white-toast-icon'
  },
  timer: 7000,
  onOpen: (toast) => {
    toast.addEventListener("mouseenter", Swal.stopTimer);
    toast.addEventListener("mouseleave", Swal.resumeTimer);
  },
});

const playSound = audioFile => {
  if(audio){
    audioFile.play();
  }/* else{
    audioFile.stop();
  } */
}

export function PlayAudio(sound){
     if(sound){
       audio = true;
     }
     else{
       audio = false;
     }     
}

export function PlaySound(){  
    playSound(likeAudio);
}

export function ToastAlert(icon, title) {
  return Toast.fire({
    target: document.getElementById("form-modal"),
    icon: `${icon}`,
    title:`${icon}`=="success" ? `${title} &nbsp <i class="fa fa-check"></i>`: `${title} &nbsp <i class="fa fa-times"></i>`,
  });
}

export function ToastModal(icon, title, text, data) {
  return Swal.fire({
    ...data,
    icon: `${icon}`,
    title: `${title}`,
    text: `${text}`,
  });
}

export async function WarningModal(message) {
  return Swal.fire({
    ...message,
    title: "Are you sure",
    showCancelButton: true,
    confirmButtonColor: "#3085d6",
    cancelButtonColor: "#d33",
    confirmButtonText: "Sure",
  });
}
