import { takeLatest, call } from "redux-saga/effects";
import { HELP_SUPPORT } from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

/**
 * invites a friend
 * @param {*} action 
 */
function* helpSupport(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.HELP_SUPPORT,
        requestBody: action.payload.params,
        requestType: "POST",
    });
}

function* dataSaga() {
  yield takeLatest(HELP_SUPPORT, helpSupport);
}

export default dataSaga;
