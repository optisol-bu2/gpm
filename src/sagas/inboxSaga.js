import { takeLatest, takeEvery, call } from "redux-saga/effects";

import {
    SEND_MESSAGE_SUCCESS,
    GET_CHATS_SUCCESS,
    GET_CHAT_MESSAGES_SUCCESS,
    SEARCH_CHAT_LIST_SUCCESS,
    UPDATE_CHAT_MESSAGE_SUCCESS,
    SEND_MESSAGE,
    SEND_ATTACHMENT,
    GET_CHATS,
    GET_HEADERINFO,
    GET_HEADERINFO_SUCCESS,
    GET_CHAT_MESSAGES,
    SEARCH_CHAT_LIST,
    UPDATE_CHAT_MESSAGE,
    READ_NOTIFICATION,
    READ_ALL_NOTIFICATION,
    DELETE_MESSAGE,
    REJECT_CUSTOM_ORDER,
    CUSTOM_ORDER_DETAIL,
} from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

/**
 * Sends text message 
 * @param {*} action 
 */
function* sendMessage(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.SEND_MESSAGE,
        requestBody: action.payload.params,
        requestType: "POST",
        successAction: SEND_MESSAGE_SUCCESS
    });
}

/**
 * Sends attachment message
 * @param {*} action 
 */
function* sendAttachment(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.SEND_ATTACHMENT,
        requestBody: action.payload.params,
        requestType: "POST",
        isMultiPart: true,
        successAction: SEND_MESSAGE_SUCCESS
    });
}

/**
 * Gets chat list
 * @param {*} action 
 */
function* getChats(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.GET_CHATS,
        requestType: "GET",
        successAction: GET_CHATS_SUCCESS
    });
}

/**
 * Gets chat messages of a particular chat
 * @param {*} action 
 */
function* getChatMessages(action) {
    yield call(MasterSaga, {
      action,
      apiURL:
        API.GET_CHAT_MESSAGES +
        "/" +
        action.payload.params.receiverId +
        "/" +
        action.payload.params.pageIndex,
      requestType: "GET",
      successAction: GET_CHAT_MESSAGES_SUCCESS
    });
}

/**
 * Gets chat messages of a particular chat and updates it to current messages
 * @param {*} action 
 */
function* updateChatMessages(action) {
    yield call(MasterSaga, {
      action,
      apiURL:
        API.GET_CHAT_MESSAGES +
        "/" +
        action.payload.params.receiverId +
        "/" +
        action.payload.params.pageIndex,
      requestType: "GET",
      successAction: UPDATE_CHAT_MESSAGE_SUCCESS
    });
}

/**
 * Gets chat list of users matching the given searchText
 * @param {*} action 
 */
function* searchChatList(action) {
    yield call(MasterSaga, {
      action,
      apiURL: API.SEARCH_CHATS + "/" + action.payload.params.searchText,
      requestType: "GET",
      successAction: SEARCH_CHAT_LIST_SUCCESS
    });
}

/**
 * get header info for the user
 * @param {*} action 
 */
 function* getHeaderInfo(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.HEADERINFO,
      requestType: "GET",
      successAction: GET_HEADERINFO_SUCCESS
  });
}

/**
 * deletes message
 * @param {*} action 
 */
 function* deleteMessage(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.DELETE_MESSAGE + "/" + action.payload.id,
      requestType: "DELETE",
  });
}

/**
 * deletes message
 * @param {*} action 
 */
 function* customOrderDetail(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.CUSTOM_ORDER_DETAIL + "/" + action.payload.id,
      requestType: "GET",
  });
}

/**
 * deletes message
 * @param {*} action 
 */
 function* rejectCustomOrder(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.REJECT_CUSTOM_ORDER,
      requestBody: action.payload.params,
      requestType: "PUT",
  });
}

/**
 * reads a notification for the user
 * @param {*} action 
 */
 function* readNotification(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.NOTIFICATIONS + "/" + action.payload.id,
      requestType: "PUT",
  });
}

/**
 * reads all notifications for the user
 * @param {*} action 
 */
 function* readAllNotification(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.NOTIFICATIONS + "/read",
      requestType: "PUT",
  });
}

function* dataSaga() {
  yield takeLatest(SEND_MESSAGE, sendMessage);
  yield takeEvery(SEND_ATTACHMENT, sendAttachment);
  yield takeLatest(GET_CHATS, getChats);
  yield takeLatest(GET_HEADERINFO, getHeaderInfo);
  yield takeLatest(GET_CHAT_MESSAGES, getChatMessages);
  yield takeLatest(UPDATE_CHAT_MESSAGE, updateChatMessages);
  yield takeLatest(SEARCH_CHAT_LIST, searchChatList);
  yield takeLatest(DELETE_MESSAGE, deleteMessage);
  yield takeLatest(CUSTOM_ORDER_DETAIL, customOrderDetail);
  yield takeLatest(REJECT_CUSTOM_ORDER, rejectCustomOrder);
  yield takeLatest(READ_NOTIFICATION, readNotification);
  yield takeLatest(READ_ALL_NOTIFICATION, readAllNotification);
}

export default dataSaga;
