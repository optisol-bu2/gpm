import { takeLatest, call } from "redux-saga/effects";

import {
  GET_MYORDER_DATA,
  GET_FILTER_ORDER_DATA,
  GET_CANCEL_ORDER_DATA,
  GET_CANCEL_ORDER_FILTER,
  GET_COMPLETE_ORDER_DATA,
  GET_COMPLETE_ORDER_FILTER,
  GET_SEARCH_ORDER_DATA,
  GET_COMPLETE_ORDER_SEARCH,
  GET_CANCEL_ORDER_SEARCH,
  ORDER_APPROVED,
  ORDER_CANCEL,
  ORDER_NOT_APPROVED,
  ORDER_EXTENTION,
} from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

function* getMyOrderDetails(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_MYORDER_DATA + "/" + action.payload.id,
    requestType: "GET",
  });
}

function* filterOrderData(action) {
  yield call(MasterSaga, {
    action,
    apiURL:
      API.GET_FILTER_ORDER_DATA +
      "/" +
      action.payload.params.search +
      "/" +
      action.payload.params.pageindex,
    requestType: "GET",
  });
}

function* SearchOrderData(action) {
  yield call(MasterSaga, {
    action,
    apiURL:
      API.GET_SEARCH_ORDER_DATA +
      "/" + action.payload.params.searchfield + 
      "/" + action.payload.params.search +
      "/" + action.payload.params.pageindex,
    requestType: "GET",
  });
}

function* getCompleteOrderData(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_COMPLETE_ORDER_DATA + "/" + action.payload.id,
    requestType: "GET",
  });
}

function* filterCompleteOrderData(action) {
  yield call(MasterSaga, {
    action,
    apiURL:
      API.GET_COMPLETE_ORDER_FILTER +
      "/" +
      action.payload.params.search +
      "/" +
      action.payload.params.pageindex,
    requestType: "GET",
  });
}

function* SearchCompletedOrderData(action) {
  yield call(MasterSaga, {
    action,
    apiURL:
      API.GET_COMPLETE_ORDER_SEARCH +
      "/" + action.payload.params.searchfield + 
      "/" + action.payload.params.search +
      "/" + action.payload.params.pageindex,
    requestType: "GET",
  });
}

function* getCancelOrderData(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_CANCEL_ORDER_DATA + "/" + action.payload.id,
    requestType: "GET",
  });
}

function* filterCancelOrderData(action) {
  yield call(MasterSaga, {
    action,
    apiURL:
      API.GET_CANCEL_ORDER_FILTER +
      "/" +
      action.payload.params.search +
      "/" +
      action.payload.params.pageindex,
    requestType: "GET",
  });
}

function* SearchCancelOrderData(action) {
  yield call(MasterSaga, {
    action,
    apiURL:
      API.GET_CANCEL_ORDER_SEARCH +
      "/" + action.payload.params.searchfield + 
      "/" + action.payload.params.search +
      "/" + action.payload.params.pageindex,
    requestType: "GET",
  });
}

function* orderApproved(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.APPROVE_ORDER,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

function* orderNotApproved(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.ASK_REVISION,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

function* orderExtention(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.EXTEND_ORDER,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

function* orderCancel(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.CANCEL_ORDER,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

function* dataSaga() {
  yield takeLatest(GET_MYORDER_DATA, getMyOrderDetails);
  yield takeLatest(GET_FILTER_ORDER_DATA, filterOrderData);
  yield takeLatest(GET_SEARCH_ORDER_DATA, SearchOrderData);
  yield takeLatest(GET_COMPLETE_ORDER_DATA, getCompleteOrderData);
  yield takeLatest(GET_COMPLETE_ORDER_FILTER, filterCompleteOrderData);
  yield takeLatest(GET_COMPLETE_ORDER_SEARCH, SearchCompletedOrderData);
  yield takeLatest(GET_CANCEL_ORDER_DATA, getCancelOrderData);
  yield takeLatest(GET_CANCEL_ORDER_FILTER, filterCancelOrderData);
  yield takeLatest(GET_CANCEL_ORDER_SEARCH, SearchCancelOrderData);
  yield takeLatest(ORDER_APPROVED, orderApproved);
  yield takeLatest(ORDER_CANCEL, orderCancel);
  yield takeLatest(ORDER_NOT_APPROVED, orderNotApproved);
  yield takeLatest(ORDER_EXTENTION, orderExtention);
}

export default dataSaga;
