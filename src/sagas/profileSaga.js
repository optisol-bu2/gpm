import { takeLatest, takeEvery, call } from "redux-saga/effects";

import {
    FETCH_PROFILE,
    PROFILE_UPDATE,
    FETCH_USERINFO,
    USERINFO_UPDATE,
    PASSWORD_UPDATE,
    FETCH_COUNTRY,
    FETCH_STATE,
    FETCH_NOTIFICATION,
    UPDATE_NOTIFICATION,
    FETCH_BILLING,
    UPDATE_BILLING,
    FETCH_PROFILEPIC,
    UPDATE_PROFILEPIC,
    REMOVE_PROFILEPIC,
    FETCH_REASONS,
    DEACTIVATE,
    SUCCESS_FETCH_PROFILE,
    SUCCESS_FETCH_USERINFO,
    SUCCESS_FETCH_NOTIFICATION,
    SUCCESS_FETCH_BILLING,
    SUCCESS_FETCH_PROFILEPIC,
    SUCCESS_FETCH_REASONS,
    BILLING_INFO_ACCOUNT_LINK,
    BILLING_INFO_STATUS,
    SUCCESS_BILLING_INFO_STATUS,
    ADD_BILLING_TOKEN,
    ADD_PAYPAL_CODE,
    GET_PAYPAL_LINK,
    CHANGE_DEFAULT_METHOD,
    FETCH_KYCINFORMATION

} from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";


/**
 * Gets profile data
 * @param {*} action 
 */
function* myprofileAction(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.PROFILE,
        requestType: "GET",
        successAction: SUCCESS_FETCH_PROFILE
    });
}

/**
 * Updates profile data
 * @param {*} action 
 */
function* myprofileUpdateAction(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.PROFILE,
        requestBody: action.payload.params,
        requestType: "PUT",
    });
}

/**
 * Gets user info data
 * @param {*} action 
 */
 function* userInfoAction(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.USER_INFO,
      requestType: "GET",
      successAction: SUCCESS_FETCH_USERINFO
  });
}

/**
 * Updates user info data
 * @param {*} action 
 */
 function* userInfoUpdateAction(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.USER_INFO,
      requestBody: action.payload.params,
      requestType: "PUT",
  });
}

/**
 * Updates password
 * @param {*} action 
 */
 function* passwordChangeAction(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.CHANGE_PASSWORD,
      requestBody: action.payload.params,
      requestType: "PUT",
  });
}

/**
 * Gets country data
 * @param {*} action 
 */
 function* countryAction(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.COUNTRY,
      requestType: "GET",
  });
}

/**
 * Gets state data
 * @param {*} action 
 */
 function* stateAction(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.STATE + "/" + action.payload.id,
      requestType: "GET",
  });
}

/**
 * Gets notification action data
 * @param {*} action 
 */
 function* notificationAction(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.IS_NOTIFICATION,
      requestType: "GET",
      successAction: SUCCESS_FETCH_NOTIFICATION
  });
}

/**
 * Updates notification action data
 * @param {*} action 
 */
 function* notificationPutAction(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.IS_NOTIFICATION,
      requestBody: action.payload.params,
      requestType: "PUT",
  });
}

/**
 * Gets billing info data
 * @param {*} action 
 */
 function* getBillingAction(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.BILLING_INFO,
      requestType: "GET",
      successAction: SUCCESS_FETCH_BILLING
  });
}

/**
 * Updates billing info data
 * @param {*} action 
 */
 function* billingAction(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.UPDATE_EMAIL_BILLING_INFO,
      requestBody: action.payload.params,
      requestType: "PUT",
  });
}

/**
 * Gets profile picture
 * @param {*} action 
 */
 function* getProfilePic(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.PROFILE_PIC,
      requestType: "GET",
      successAction: SUCCESS_FETCH_PROFILEPIC
  });
}

/**
 * Updates profile picture
 * @param {*} action 
 */
 function* updateProfilePic(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.PROFILE_PIC,
      requestBody: action.payload.params,
      requestType: "POST",
  });
}

/**
 * Removes profile picture
 * @param {*} action 
 */
 function* removeProfilePic(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.PROFILE_PIC,
      requestType: "DELETE",
  });
}

/**
 * Gets reasons
 * @param {*} action 
 */
 function* getReasonAction(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.REASONS,
      requestType: "GET",
      successAction: SUCCESS_FETCH_REASONS
  });
}

/**
 * Deactivates user account
 * @param {*} action 
 */
 function* deactivateAccountAction(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.DEACTIVATE,
      requestBody: action.payload.params,
      requestType: "POST",
  });
}

/**
 * gets Billing Info Account Link
 * @param {*} action 
 */
 function* billingInfoAccountLink(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.BILLING_INFO_ACCOUNT_LINK,
      requestBody: action.payload.params,
      requestType: "POST",
  });
}

/**
 * gets Billing Information Status
 * @param {*} action 
 */
 function* getBillingInformationStatus(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.BILLING_INFO_STATUS,
      requestType: "GET",
      successAction: SUCCESS_BILLING_INFO_STATUS
  });
}

/**
 * adds biliing bank/card token
 * @param {*} action 
 */
 function* addBillingToken(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.ADD_BILLING_TOKEN,
        requestBody: action.payload.params,
        requestType: "PUT",
    });
  }

/**
 * adds paypal code
 * @param {*} action 
 */
 function* addPayPalCode(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.PAYPAL,
        requestBody: action.payload.params,
        requestType: "PUT",
    });
  }
  
/**
 * gets paypal redirection link
 * @param {*} action 
 */
 function* getPayPalLink(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.GET_PAYPAL_LINK,
        requestType: "GET",
    });
  }

/**
 * changes default payment method
 * @param {*} action 
 */
 function* changeDefaultMethod(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.CHANGE_DEFAULT_METHOD,
        requestBody: action.payload.params,
        requestType: "PUT",
    });
  }

  /**
 * Gets kycinformation data
 * @param {*} action 
 */
 function* KycInformationAction(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.KYCINFORMATION,
      requestType: "GET",
  });
}

function* dataSaga() {
  yield takeLatest(FETCH_PROFILE, myprofileAction);
  yield takeLatest(PROFILE_UPDATE, myprofileUpdateAction);
  yield takeLatest(FETCH_USERINFO, userInfoAction);
  yield takeLatest(USERINFO_UPDATE, userInfoUpdateAction);
  yield takeLatest(PASSWORD_UPDATE, passwordChangeAction);
  yield takeLatest(FETCH_COUNTRY, countryAction);
  yield takeLatest(FETCH_STATE, stateAction);
  yield takeLatest(FETCH_NOTIFICATION, notificationAction);
  yield takeLatest(UPDATE_NOTIFICATION, notificationPutAction);
  yield takeLatest(FETCH_BILLING, getBillingAction);
  yield takeLatest(UPDATE_BILLING, billingAction);
  yield takeLatest(FETCH_PROFILEPIC, getProfilePic);
  yield takeLatest(UPDATE_PROFILEPIC, updateProfilePic);
  yield takeLatest(REMOVE_PROFILEPIC, removeProfilePic);
  yield takeLatest(FETCH_REASONS, getReasonAction);
  yield takeLatest(DEACTIVATE, deactivateAccountAction);
  yield takeLatest(BILLING_INFO_ACCOUNT_LINK, billingInfoAccountLink);
  yield takeLatest(BILLING_INFO_STATUS, getBillingInformationStatus);
  yield takeLatest(ADD_BILLING_TOKEN, addBillingToken);
  yield takeLatest(ADD_PAYPAL_CODE, addPayPalCode);
  yield takeLatest(GET_PAYPAL_LINK, getPayPalLink);
  yield takeLatest(CHANGE_DEFAULT_METHOD, changeDefaultMethod);
  yield takeLatest(FETCH_KYCINFORMATION, KycInformationAction);
}

export default dataSaga;
