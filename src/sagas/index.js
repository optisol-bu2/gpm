import { all } from "redux-saga/effects";
/**
 * This root Saga aggregates multiple Sagas to a single entry point for the sagaMiddleware to run.
 */
import loginSaga from "./loginSaga";
import dashboardSaga from "./dashboardSaga";
import switchusersaga from "./switchusersaga";
import lookUp from "./lookUp";
import servicesaga from "./servicesaga";
import registerationsaga from "./registerationsaga";
import refreshTokenSaga from "./refreshTokenSaga";
import ordersaga from "./ordersaga";
import myOrderSaga from "./myOrderSaga";
import feedbackSaga from "./feedbackSaga";
import inboxSaga from "./inboxSaga";
import bloggerSaga from "./bloggerSaga";
import favoriteSaga from "./favoriteSaga";
import aboutSaga from "./aboutSaga";
import profileSaga from "./profileSaga";
import inviteSaga from "./inviteSaga";
import helpSaga from "./helpSaga";
import blogSaga from "./blogSaga";

function* rootSaga() {
  yield all([
    loginSaga(),
    dashboardSaga(),
    switchusersaga(),
    lookUp(),
    servicesaga(),
    registerationsaga(),
    refreshTokenSaga(),
    ordersaga(),
    myOrderSaga(),
    feedbackSaga(),
    inboxSaga(),
    bloggerSaga(),
    favoriteSaga(),
    aboutSaga(),
    profileSaga(),
    inviteSaga(),
    helpSaga(),
    blogSaga(),
  ]);
}
export default rootSaga;
