import { takeLatest, call } from "redux-saga/effects";

import {
  ADD_SERVICE,
  ADD_CUSTOM_ORDER,
  VIEW_QUESTIONS,
  ADD_QUESTIONS,
  GET_SERVICE_LIST,
  GET_RECOMMENDATIONS,
  CHECK_BILLING_INFO,
  REMOVE_SERVICE,
  REMOVE_CUSTOM_SERVICE,
  MARKETPLACE_SERVICES,
  MARKETPLACE_FILTER,
  GET_SERVICE_DETAIL,
  UPDATE_SERVICE,
  CHANGE_SERVICE_STATUS,
  CHANGE_CUSTOM_STATUS,
} from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

function* addService(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.ADD_SERVICE,
    requestBody: action.payload.params,
    requestType: "POST",
    isMultiPart: true,
  });
}

/**
 * adds custom order
 * @param {*} action 
 */
function* addCustomOrder(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.ADD_CUSTOM_ORDER,
    requestBody: action.payload.params,
    requestType: "POST",
    isMultiPart: true,
  });
}

/**
 * adds custom order
 * @param {*} action 
 */
 function* addQuestions(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.ADD_QUESTIONS,
    requestBody: action.payload.params,
    requestType: "POST",
    isMultiPart: true,
  });
}

/**
 * adds custom order
 * @param {*} action 
 */
 function* getViewQuestions(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.VIEW_QUESTIONS,
    requestBody: action.payload.params,
    requestType: "GET",
    isMultiPart: true,
  });
}

function* getServiceList(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.USER_SERVICE,
    requestBody: action.payload.params,
    requestType: "GET",
  });
}
/**
 * gets recommendations for the service
 * @param {*} action 
 */
function* getRecommendations(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.USER_RECOMMENDATIONS + "/" + action.payload.id,
    requestType: "GET",
  });
}

/**
 * checks whether the seller already added billing information or not
 * @param {*} action 
 */
function* checkBillingInfo(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.CHECK_BILLING_INFO,
    requestBody: action.payload.params,
    requestType: "GET",
  });
}

function* removeService(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.REMOVE_SERVICE + "/" + action.payload.id,
    requestType: "DELETE",
  });
}

/**
 * removes custom order
 * @param {*} action 
 */
function* removeCustomService(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.REMOVE_CUSTOM_SERVICE + "/" + action.payload.id,
    requestType: "DELETE",
  });
}

/**
 * changes service status
 * @param {*} action 
 */
function* changeServiceStatus(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.CHANGE_SERVICE_STATUS,
    requestBody: action.payload.params,
    requestType: "PUT",
  });
}

/**
 * changes service status
 * @param {*} action 
 */
function* changeCustomStatus(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.CHANGE_CUSTOM_STATUS,
    requestBody: action.payload.params,
    requestType: "PUT",
  });
}

function* getMarketPlaceService(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.MARKETPLACE_SERVICE + "/" + action.payload.id,
    requestType: "GET",
  });
}

function* filterMarketPlaceList(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.FILTER_MARKETPLACE,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

function* getServiceDetail(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_SERVICE_DETAIL + "/" + action.payload.id,
    requestType: "GET",
  });
}

function* updateService(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.UPDATE_SERVICE,
    requestBody: action.payload.params,
    requestType: "POST",
    isMultiPart: true,
  });
}

function* dataSaga() {
  yield takeLatest(ADD_SERVICE, addService);
  yield takeLatest(ADD_CUSTOM_ORDER, addCustomOrder);
  yield takeLatest(ADD_QUESTIONS, addQuestions);
  yield takeLatest(VIEW_QUESTIONS, getViewQuestions);
  yield takeLatest(GET_SERVICE_LIST, getServiceList);
  yield takeLatest(GET_RECOMMENDATIONS, getRecommendations);
  yield takeLatest(CHECK_BILLING_INFO, checkBillingInfo);
  yield takeLatest(REMOVE_SERVICE, removeService);
  yield takeLatest(REMOVE_CUSTOM_SERVICE, removeCustomService);
  yield takeLatest(CHANGE_SERVICE_STATUS, changeServiceStatus);
  yield takeLatest(CHANGE_CUSTOM_STATUS, changeCustomStatus);
  yield takeLatest(MARKETPLACE_SERVICES, getMarketPlaceService);
  yield takeLatest(MARKETPLACE_FILTER, filterMarketPlaceList);
  yield takeLatest(GET_SERVICE_DETAIL, getServiceDetail);
  yield takeLatest(UPDATE_SERVICE, updateService);
}

export default dataSaga;
