import { takeLatest, call, takeEvery } from "redux-saga/effects";

import {
  CART_REDIRECT,
  GET_CART_DATA,
  CART_BLOGGER,
  GET_CART_BLOGGER,
  CART_CUSTOM,
  GET_CART_CUSTOM,
  PAYMENT_SESSION_CUSTOM,
  PAYMENT_SESSION_BLOGGER,
  REMOVE_CART_ITEM,
  CREATE_ORDER,
  UPLOAD_ATTACHMENT,
  REMOVE_ATTACHMENT,
  START_ORDER,
  GET_ORDER_DATA,
  GET_ATTACHMENTS,
  DELETE_ATTACHMENT,
  SEND_ORDER_UPDATE,
  UPDATE_ATTACHMENT,
  PAYMENT_SESSION,
  PLACE_ORDER,
  CREATE_ORDER_PAYPAL,
  GET_SAVED_CARDS,
  ADD_CARD,
  REMOVE_CARD,
} from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

function* getCartData(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_CART_DATA,
    requestType: "GET",
  });
}

function* removeCartItem(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.REMOVE_CART_ITEM + "/" + action.payload.id,
    requestType: "DELETE",
  });
}

function* verifyCartRedirect(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.CART_REDIRECT,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

/**
 * adds blogger to cart
 * @param {*} action 
 */
function* addCartBlogger(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.CART_BLOGGER,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

/**
 * gets blogger cart data
 * @param {*} action 
 */
function* getCartBlogger(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.CART_BLOGGER,
    requestType: "GET",
  });
}

/**
 * adds custom order to cart
 * @param {*} action 
 */
function* addCartCustom(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.CART_CUSTOM,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

/**
 * gets custom order cart data
 * @param {*} action 
 */
function* getCartCustom(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.CART_CUSTOM,
    requestType: "GET",
  });
}

/**
 * creates custom order payment session for stripe
 * @param {*} action 
 */
 function* paymentSessionCustom(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.PAYMENT_SESSION_CUSTOM,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

/**
 * creates blogger payment session for stripe
 * @param {*} action 
 */
 function* paymentSessionBlogger(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.PAYMENT_SESSION_BLOGGER,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

/**
 * creates payment session for stripe
 * @param {*} action 
 */
function* paymentSession(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.PAYMENT_SESSION,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

/**
 * places order on successful payment
 * @param {*} action 
 */
function* placeOrder(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.PLACE_ORDER,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

/**
 * create order for paypal payment
 * @param {*} action 
 */
function* createOrderForPaypal(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.CREATE_ORDER_PAYPAL,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

/**
 * Gets saved cards of the user
 * @param {*} action 
 */
function* getSavedCards(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.SAVED_CARD,
    requestType: "GET",
  });
}

/**
 * Adds saved cards of the user
 * @param {*} action 
 */
function* addCard(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.SAVED_CARD,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

/**
 * Removes saved cards of the user
 * @param {*} action 
 */
function* removeCard(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.SAVED_CARD + "/" + action.payload.id,
    requestType: "DELETE",
  });
}

function* createOrder(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.CREATE_ORDER,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

function* uploadAttachment(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.UPLOAD_ATTACHMENT,
    requestBody: action.payload.params,
    requestType: "POST",
    isMultiPart: true,
  });
}

function* removeAttachment(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.REMOVE_ATTACHMENT + "/" + action.payload.id,
    requestType: "DELETE",
  });
}

function* startOrder(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.START_ORDER,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

function* getOrderDetail(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_ORDER_DATA + "/" + action.payload.id,
    requestType: "GET",
  });
}

function* getAttachments(action) {
  yield call(MasterSaga, {
    action,
    apiURL:
      API.GET_ATTACHMENTS +
      "/" +
      action.payload.order_id +
      "/" +
      action.payload.revision,
    requestType: "GET",
  });
}

function* deleteAttachment(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.DELETE_ATTACHMENT + "/" + action.payload.id,
    requestType: "DELETE",
  });
}

function* sendOrderUpdate(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.SEND_ORDER_UPDATE,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

function* updateAttachment(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.UPDATE_ATTACHMENT,
    requestBody: action.payload.params,
    requestType: "POST",
    isMultiPart: true,
  });
}

function* dataSaga() {
  yield takeEvery(UPLOAD_ATTACHMENT, uploadAttachment);
  yield takeLatest(CART_REDIRECT, verifyCartRedirect);
  yield takeEvery(GET_CART_DATA, getCartData);
  yield takeLatest(CART_BLOGGER, addCartBlogger);
  yield takeLatest(GET_CART_BLOGGER, getCartBlogger);
  yield takeLatest(CART_CUSTOM, addCartCustom);
  yield takeLatest(GET_CART_CUSTOM, getCartCustom);
  yield takeLatest(PAYMENT_SESSION_CUSTOM, paymentSessionCustom);
  yield takeLatest(PAYMENT_SESSION_BLOGGER, paymentSessionBlogger);
  yield takeLatest(REMOVE_CART_ITEM, removeCartItem);
  yield takeLatest(PAYMENT_SESSION, paymentSession);
  yield takeLatest(PLACE_ORDER, placeOrder);
  yield takeLatest(CREATE_ORDER_PAYPAL, createOrderForPaypal);
  yield takeLatest(GET_SAVED_CARDS, getSavedCards);
  yield takeLatest(ADD_CARD, addCard);
  yield takeLatest(REMOVE_CARD, removeCard);
  yield takeLatest(CREATE_ORDER, createOrder);
  yield takeLatest(REMOVE_ATTACHMENT, removeAttachment);
  yield takeLatest(START_ORDER, startOrder);
  yield takeEvery(GET_ORDER_DATA, getOrderDetail);
  yield takeLatest(GET_ATTACHMENTS, getAttachments);
  yield takeLatest(DELETE_ATTACHMENT, deleteAttachment);
  yield takeLatest(SEND_ORDER_UPDATE, sendOrderUpdate);
  yield takeEvery(UPDATE_ATTACHMENT, updateAttachment);
}

export default dataSaga;
