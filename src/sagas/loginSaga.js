import { takeLatest, call } from "redux-saga/effects";

import {
  USER_LOGIN,
  USER_LOGIN_SUCCESS,
  SOCIAL_LOGIN,
  FETCH_LOGIN,
  SUCCESS_RELOGIN,
} from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

function* userLogin(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.USER_LOGIN,
    requestBody: action.payload.params,
    successAction: USER_LOGIN_SUCCESS,
    requestType: "POST",
  });
}

function* userSocialLogin(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.USER_SOCIAL_LOGIN,
    requestBody: action.payload.params,
    successAction: USER_LOGIN_SUCCESS,
    requestType: "POST",
  });
}

/**
 * fetch login data
 * @param {*} action 
 */
function* reLoginAction(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.RE_LOGIN,
    successAction: SUCCESS_RELOGIN,
    requestType: "GET",
  });
}

/**
 * This saga is used to call the login & forgot APIs.
 * It calls callback event to save the login-response in the state on API success.
 */
function* dataSaga() {
  yield takeLatest(USER_LOGIN, userLogin);
  yield takeLatest(SOCIAL_LOGIN, userSocialLogin);
  yield takeLatest(FETCH_LOGIN, reLoginAction);
}

export default dataSaga;
