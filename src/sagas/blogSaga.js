import { takeLatest, call } from "redux-saga/effects";

import {
  GET_BLOGS,
} from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

/**
 * Gets blog list
 * @param {*} action 
 */
function* getBlogs(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_BLOGS,
    requestBody: action.payload.params,
    requestType: "POST",
    isBlogURL: true,
  });
}

function* dataSaga() {
  yield takeLatest(GET_BLOGS, getBlogs);
}

export default dataSaga;
