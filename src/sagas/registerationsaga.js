import { takeLatest, call } from "redux-saga/effects";

import {
  REGISTERATION,
  FORGOT_PASSWORD,
  VERIFY_TOKEN,
  VERIFY_ACTIVATE_TOKEN,
  RESET_PASSWORD,
  EMAIL_VALIDATE,
  USER_LOGIN_SUCCESS,
  REGISTRATION_SUCCESS,
} from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

function* registration(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.REGISTER,
    requestBody: action.payload.params,
    requestType: "POST",
    successAction: REGISTRATION_SUCCESS,
  });
}

/**
 * sends forgot password email
 * @param {*} action 
 */
function* forgotPassword(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.FORGOT_PASSWORD,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

/**
 * verifies forgot password token
 * @param {*} action 
 */
function* verifyToken(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.VERIFY_TOKEN,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

/**
 * verifies activate account token
 * @param {*} action 
 */
function* verifyActivateToken(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.VERIFY_ACTIVATE_TOKEN,
    requestBody: action.payload.params,
    requestType: "POST",
    successAction: USER_LOGIN_SUCCESS,
  });
}

/**
 * resets password
 * @param {*} action 
 */
function* resetPassword(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.RESET_PASSWORD,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

function* checkEmail(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.CHECK_EMAIL,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

function* dataSaga() {
  yield takeLatest(REGISTERATION, registration);
  yield takeLatest(FORGOT_PASSWORD, forgotPassword);
  yield takeLatest(VERIFY_TOKEN, verifyToken);
  yield takeLatest(VERIFY_ACTIVATE_TOKEN, verifyActivateToken);
  yield takeLatest(RESET_PASSWORD, resetPassword);
  yield takeLatest(EMAIL_VALIDATE, checkEmail);
}

export default dataSaga;
