import React from "react";
import { put, select } from "redux-saga/effects";
import axios from "axios";

import { BASE_URL, BLOG_BASE_URL, PUBLIC_APIS } from "../common/constants";
import * as selectors from "../Reducers/selectors";
import { FORCE_LOGOUT, SHOW_DISPLAY_MESSAGE } from "../Actions/actionType";
import { appConfig } from "../Config.json";

/**
 * This master-saga is used to call any API and to validate the API-response.
 * It calls callback events for the response if provided.
 * @param {Object} action It should include the action TYPE
 * @param {String} apiURL
 * @param {Object} requestBody
 * @param {String} successAction Optional callback function on API success
 * @param {String} validationFailureAction Optional callback function on API validation failure
 * @param {String} failureAction Optional callback function on API failure
 */

const fetchData = function* ({
  action,
  apiURL,
  requestBody,
  successAction,
  failureAction,
  requestType,
  isMultiPart = false,
  isBlogURL = false,
}) {
  try {
    let response = {};
    const {
      APITimeout = 0,
      APITimeoutMessage = "Server not reachable. Please contact Admin.",
    } = appConfig;

    let headers;
    isMultiPart
      ? (headers = {
          "Content-Type": "multipart/form-data",
        })
      : (headers = {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
        });
    let backend_url = '';
    isBlogURL ? backend_url = BLOG_BASE_URL : backend_url = BASE_URL;
    if (!PUBLIC_APIS.includes(apiURL)) {
      const token = yield select(selectors.token);
      headers.Authorization = token != "" ? "Bearer " + `${token}` : "";
    }
    if (requestType == "POST") {
      response = yield axios
        .post(backend_url + apiURL, requestBody, {
          headers,
          timeout: APITimeout,
        })
        .catch((error) => {
          if (error.code === "ECONNABORTED") {
            action.payload.onFailure({
              message: APITimeoutMessage,
              isError: false,
            });
            return;
          } else throw error;
        });
    } else if (requestType == "PUT") {
      response = yield axios
        .put(backend_url + apiURL, requestBody, {
          headers,
          timeout: APITimeout,
        })
        .catch((error) => {
          if (error.code === "ECONNABORTED") {
            action.payload.onFailure({
              message: APITimeoutMessage,
              isError: false,
            });
            return;
          } else throw error;
        });
    } else if (requestType == "GET") {
      response = yield axios
        .get(backend_url + apiURL, {
          headers,
          timeout: APITimeout,
        })
        .catch((error) => {
          if (error.code === "ECONNABORTED") {
            action.payload.onFailure({
              message: APITimeoutMessage,
              isError: false,
            });
            return;
          } else throw error;
        });
    } else {
      response = yield axios
        .delete(backend_url + apiURL, {
          headers,
          timeout: APITimeout,
        })
        .catch((error) => {
          if (error.code === "ECONNABORTED") {
            action.payload.onFailure({
              message: APITimeoutMessage,
              isError: false,
            });
            return;
          } else throw error;
        });
    }
    const { data = {} } = response;
    if (data && (data.data !== undefined ? data.data !== null : true) && (data.stausCode === 200 || data.status === 200)) {
      if (successAction && data.data != null)
        yield put({
          type: successAction,
          data: data.data,
        });

      if (action.payload && typeof action.payload.onSuccess === "function")
        action.payload.onSuccess({
          data: data.data,
          message: data.message,
        });
    } else {
      if (action.payload && typeof action.payload.onFailure === "function") {
        if (failureAction)
          yield put({
            type: failureAction,
            data: response,
          });
        action.payload.onFailure({
          message: response.data.message,
          isError: false,
        });
      } else {
        yield put({
          type: SHOW_DISPLAY_MESSAGE,
          messageDetails: {
            displayMessage: response.data.message,
            severity: "error",
          },
        });
      }
    }
  } catch (e) {
    try {
      if (e.response.status === 401) {
        yield put({
          type: FORCE_LOGOUT,
        });
        if (!window.location.pathname == "/") {
          window.history.go("/");
        }
      } else {
        if (e.response && e.response.data) {
          const data = e.response.data;
          if (action.payload && typeof action.payload.onFailure === "function")
            action.payload.onFailure({
              message: data.message,
              isError: true,
              statusCode: data.statusCode,
            });
          else {
            yield put({
              type: SHOW_DISPLAY_MESSAGE,
              messageDetails: {
                displayMessage: data.message,
                severity: "error",
              },
            });
          }
        }
      }
    } catch (error) {}
  }
};

export default fetchData;
