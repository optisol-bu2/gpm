import { takeLatest, takeEvery, call } from "redux-saga/effects";

import {
  SERVICE_CATEGORIES,
  SERVICE_CATEGORIES_SUCCESS,
  SERVICE_ESTIMATED_DELIVERY,
  SERVICE_ESTIMATED_EXPERTISE,
  SERVICE_ESTIMATED_DELIVERY_SUCCESS,
  SERVICE_ESTIMATED_EXPERTISE_SUCCESS,
  SEARCH_SERVICE_USER,
  GET_SERVICE_COST,
  GET_SERVICEBACKLINK_DATA,
  GET_SERVICEGUESTPOST_DATA,
  GET_TOP_SERVICES_DATA,
  GET_TOP_CONTENT_WRITERS,
} from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

function* getServiceCategories(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_SERVICE_CATEGORIES,
    requestBody: action.payload.params,
    successAction: SERVICE_CATEGORIES_SUCCESS,
    requestType: "GET",
  });
}

function* getServiceEstimatedDelivey(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_SERVICE_ESTIMATED_DELIVERY,
    requestBody: action.payload.params,
    successAction: SERVICE_ESTIMATED_DELIVERY_SUCCESS,
    requestType: "GET",
  });
}

function* getServiceEstimatedExpertise(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_SERVICE_ESTIMATED_EXPERTISE,
    requestBody: action.payload.params,
    successAction: SERVICE_ESTIMATED_EXPERTISE_SUCCESS,
    requestType: "GET",
  });
}

/**
 * gets user matching searchText
 * @param {*} action 
 */
function* searchServiceUsers(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.SEARCH_SERVICE_USER + (action.payload.searchText == "" ? "" : "/" + action.payload.searchText),
    requestType: "GET",
  });
}

function* getServiceBackLinkData(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_SERVICEBACKLINK_DATA,
    requestType: "GET",
  });
}

function* getServiceGuestPostData(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_SERVICEGUESTPOST_DATA,
    requestType: "GET",
  });
}

/**
 * gets top services
 * @param {*} action 
 */
function* getTopServices(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_TOP_SERVICES_DATA,
    requestType: "GET",
  });
}

/**
 * gets top content writers
 * @param {*} action 
 */
function* getTopContentWriters(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_TOP_CONTENT_WRITERS,
    requestType: "GET",
  });
}

function* dataSaga() {
  yield takeLatest(SERVICE_CATEGORIES, getServiceCategories);
  yield takeLatest(SERVICE_ESTIMATED_DELIVERY, getServiceEstimatedDelivey);
  yield takeLatest(SERVICE_ESTIMATED_EXPERTISE, getServiceEstimatedExpertise);
  yield takeLatest(GET_SERVICEBACKLINK_DATA, getServiceBackLinkData);
  yield takeLatest(GET_SERVICEGUESTPOST_DATA, getServiceGuestPostData);
  yield takeEvery(SEARCH_SERVICE_USER, searchServiceUsers);
  yield takeEvery(GET_TOP_SERVICES_DATA, getTopServices);
  yield takeEvery(GET_TOP_CONTENT_WRITERS, getTopContentWriters);
}

export default dataSaga;
