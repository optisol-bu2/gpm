import { takeLatest, call } from "redux-saga/effects";

import { SUBMIT_FEEDBACK, GET_FEEDBACK } from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

function* submitFeedback(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.SUBMIT_FEEDBACK,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

function* getFeedback(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.GET_FEEDBACK + "/" + action.payload.id,
    requestType: "GET",
  });
}

/**
 * This saga is used to call the login & forgot APIs.
 * It calls callback event to save the login-response in the state on API success.
 */
function* dataSaga() {
  yield takeLatest(SUBMIT_FEEDBACK, submitFeedback);
  yield takeLatest(GET_FEEDBACK, getFeedback);
}

export default dataSaga;
