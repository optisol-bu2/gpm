import { takeLatest, call } from "redux-saga/effects";

import {
    GET_DASHBOARD,
    GET_DATA_TOTALSPENT,
    GET_STATISTICS,
    ANALYTICS_DETAILS,
    ANALYTICS_SUCCESS,
    GET_DATA_TRAFFICANALYTICS,
    GET_DATA_DAILYTRAFFIC,
} from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

/**
 * gets dashboard data
 * @param {*} action 
 */
function* getDashboard(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.DASHBOARD,
        requestType: "GET"
    });
}

function* getDataTotalSpent(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.GET_DATA_TOTALSPENT,
        requestBody: action.payload.params,
        requestType: "POST"
    });
}

/**
 * gets statistics data
 * @param {*} action 
 */
function* getStatistics(action) {
    yield call(MasterSaga, {
        action,
        apiURL: action.payload.id != "" ? API.DASHBOARD + "/" + action.payload.id : API.DASHBOARD,
        requestType: "GET"
    });
}

/**
 * saves analytics details
 * @param {*} action 
 */
function* analyticsDetails(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.ANALYTICS_DETAILS,
        requestBody: action.payload.params,
        successAction: ANALYTICS_SUCCESS,
        requestType: "PUT"
    });
}

function* getDataTrafficAnalytics(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.GET_DATA_TRAFFICANALYTICS,
        requestBody: action.payload.params,
        requestType: "POST"
    });
}

function* getDataDailyTraffic(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.GET_DATA_DAILYTRAFFIC,
        requestBody: action.payload.params,
        requestType: "POST"
    });
}

/**
 * This saga is used to call the login & forgot APIs.
 * It calls callback event to save the login-response in the state on API success.
 */
function* dataSaga() {
    yield takeLatest(GET_DASHBOARD, getDashboard);
    yield takeLatest(GET_DATA_TOTALSPENT, getDataTotalSpent);
    yield takeLatest(GET_STATISTICS, getStatistics);
    yield takeLatest(ANALYTICS_DETAILS, analyticsDetails);
    yield takeLatest(GET_DATA_TRAFFICANALYTICS, getDataTrafficAnalytics);
    yield takeLatest(GET_DATA_DAILYTRAFFIC, getDataDailyTraffic);
}

export default dataSaga;
