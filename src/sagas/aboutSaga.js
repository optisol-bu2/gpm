import { takeLatest, call } from "redux-saga/effects";
import { GET_ABOUT,GET_FAQ } from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

/**
 * get about data
 * @param {*} action 
 */
function* getAbout(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.ABOUT+ "/" + action.payload.id,
        requestType: "GET",
    });
}

/**
 * get about data
 * @param {*} action 
 */
 function* getFaq(action) {
  yield call(MasterSaga, {
      action,
      apiURL: API.FAQs,
      requestType: "GET",
  });
}

function* dataSaga() {
  yield takeLatest(GET_ABOUT, getAbout);
  yield takeLatest(GET_FAQ, getFaq);
}

export default dataSaga;
