import { takeLatest, takeEvery, call } from "redux-saga/effects";

import {
    GET_BLOGGER_LIST,
    SEARCH_BLOGGER_LIST,
    FILTER_BLOGGER_LIST,
    GET_BLOGGER_PROFILE,
    GET_BLOGGER_DETAILS,
    ADD_BLOGGER_DETAILS,
    ADD_BLOGGER_PORTFOLIO,
    ADD_BLOG_LINK,
    REMOVE_BLOG_LINK,
    ADD_BLOGGER_ABOUTME,
    REMOVE_BLOGGER_ATTACHMENT,
} from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

/**
 * get blogger list data
 * @param {*} action 
 */
function* getBloggerList(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.BLOGGERS + "/" + action.payload.id,
        requestType: "GET",
    });
}

/**
 * search blogger list data
 * @param {*} action 
 */
function* searchBloggerList(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.BLOGGERS_SEARCH + "/" + action.payload.params.searchText + "/" + action.payload.params.pageIndex,
        requestType: "GET",
    });
}

/**
 * filter blogger list data
 * @param {*} action 
 */
function* filterBloggerList(action) {
    yield call(MasterSaga, {
        action,
        apiURL: 
        API.BLOGGERS_SEARCH + "/" + 
        action.payload.params.searchfield + "/" +
        action.payload.params.searchText + "/" + 
        action.payload.params.pageIndex,
        requestType: "GET",
    });
}

/**
 * get blogger profile
 * @param {*} action 
 */
function* getBloggerProfile(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.BLOGGER + "/" + action.payload.id,
        requestType: "GET",
    });
}

/**
 * get blogger details
 * @param {*} action 
 */
function* getBloggerDetails(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.BLOGGER,
        requestType: "GET",
    });
}

/**
 * add blogger details
 * @param {*} action
 */
function* addBloggerDetails(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.BLOGGER,
        requestBody: action.payload.params,
        requestType: "PUT",
    });
}

/**
 * add blogger portfolio attachment
 * @param {*} action 
 */
 function* addBloggerPortfolio(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.BLOGGER_PORTFOLIO,
        requestBody: action.payload.params,
        requestType: "POST",
        isMultiPart: true,
    });
}

/**
 * add blog link
 * @param {*} action 
 */
 function* addBlogLink(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.BLOG_LINK,
        requestBody: action.payload.params,
        requestType: "POST",
    });
}

/**
 * remove blog link
 * @param {*} action 
 */
 function* removeBlogLink(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.REMOVE_BLOG_LINK + "/" + action.payload.id,
        requestType: "DELETE",
    });
}

/**
 * add blogger about me video attachment
 * @param {*} action 
 */
 function* addBloggerAboutMe(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.BLOGGER_ABOUTME,
        requestBody: action.payload.params,
        requestType: "POST",
        isMultiPart: true,
    });
}

/**
 * remove blogger attachment
 * @param {*} action 
 */
function* removeBloggerAttachment(action) {
    yield call(MasterSaga, {
      action,
      apiURL: API.BLOGGER + "/" + action.payload.id,
      requestType: "DELETE",
    });
}

function* dataSaga() {
  yield takeLatest(GET_BLOGGER_LIST, getBloggerList);
  yield takeLatest(SEARCH_BLOGGER_LIST, searchBloggerList);
  yield takeLatest(FILTER_BLOGGER_LIST, filterBloggerList);
  yield takeLatest(GET_BLOGGER_PROFILE, getBloggerProfile);
  yield takeLatest(GET_BLOGGER_DETAILS, getBloggerDetails);
  yield takeLatest(ADD_BLOGGER_DETAILS, addBloggerDetails);
  yield takeEvery(ADD_BLOGGER_PORTFOLIO, addBloggerPortfolio);
  yield takeEvery(ADD_BLOG_LINK, addBlogLink);
  yield takeEvery(REMOVE_BLOG_LINK, removeBlogLink);
  yield takeLatest(ADD_BLOGGER_ABOUTME, addBloggerAboutMe);
  yield takeEvery(REMOVE_BLOGGER_ATTACHMENT, removeBloggerAttachment);
}

export default dataSaga;
