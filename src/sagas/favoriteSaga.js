import { takeLatest, call } from "redux-saga/effects";

import {
  FAVORITE_WEBSITE,
  FAVORITE_WEBSITE_SUCCESS,
  SEARCH_FAVORITE,
  ADD_FAVORITE,
  ADD_FAVORITE_BLOGGER,
  REMOVE_FAVORITE,
  REMOVE_FAVORITE_SERVICE,
  REMOVE_FAVORITE_BLOGGER,
} from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

/**
 * Gets favorites list
 * @param {*} action 
 */
function* getFavoriteWebsite(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.FAVORITE_WEBSITE + "/" + action.payload.id,
    requestType: "GET",
    successAction: FAVORITE_WEBSITE_SUCCESS,
  });
}

/**
 * Searches favorites list
 * @param {*} action 
 */
 function* searchFavorite(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.SEARCH_FAVORITE_WEBSITE,
    requestBody: action.payload.params,
    requestType: "POST",
    successAction: FAVORITE_WEBSITE_SUCCESS,
  });
}

/**
 * Adds favorite website
 * @param {*} action 
 */
 function* addFavorite(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.FAVORITE_WEBSITE,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

/**
 * Adds favorite blogger
 * @param {*} action 
 */
 function* addFavoriteBlogger(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.FAVORITE_BLOGGER,
    requestBody: action.payload.params,
    requestType: "POST",
  });
}

/**
 * Removes favorite
 * @param {*} action 
 */
function* removeFavorite(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.FAVORITE_WEBSITE + "/" + action.payload.id,
    requestType: "DELETE",
  });
}

/**
 * Removes favorite website
 * @param {*} action 
 */
function* removeFavoriteService(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.FAVORITE_SERVICE + "/" + action.payload.id,
    requestType: "DELETE",
  });
}

/**
 * Removes favorite blogger
 * @param {*} action 
 */
function* removeFavoriteBlogger(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.FAVORITE_BLOGGER + "/" + action.payload.id,
    requestType: "DELETE",
  });
}

function* dataSaga() {
  yield takeLatest(FAVORITE_WEBSITE, getFavoriteWebsite);
  yield takeLatest(SEARCH_FAVORITE, searchFavorite);
  yield takeLatest(ADD_FAVORITE, addFavorite);
  yield takeLatest(ADD_FAVORITE_BLOGGER, addFavoriteBlogger);
  yield takeLatest(REMOVE_FAVORITE, removeFavorite);
  yield takeLatest(REMOVE_FAVORITE_SERVICE, removeFavoriteService);
  yield takeLatest(REMOVE_FAVORITE_BLOGGER, removeFavoriteBlogger);
}

export default dataSaga;
