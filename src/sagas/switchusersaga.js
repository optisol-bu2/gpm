import { takeLatest, call } from "redux-saga/effects";

import { SWITCH_USER, SWITCH_USER_SUCCESS } from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

function* switchUser(action) {
  yield call(MasterSaga, {
    action,
    apiURL: API.SWITCH_USER,
    requestBody: action.payload.params,
    successAction: SWITCH_USER_SUCCESS,
    requestType: "GET",
  });
}

function* dataSaga() {
  yield takeLatest(SWITCH_USER, switchUser);
}

export default dataSaga;
