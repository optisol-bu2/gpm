import { takeLatest, call, delay, cancel, select } from "redux-saga/effects";

import {
  START_REFRESH_TOKEN_SAGA,
  GET_REFRESH_TOKEN,
  GET_REFRESH_TOKEN_SUCCESS,
  USER_LOGOUT,
  FORCE_LOGOUT,
} from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";
import * as selectors from "../Reducers/selectors";

function* fetchData() {
  try {
    const action = {
      type: GET_REFRESH_TOKEN,
    };

    yield call(MasterSaga, {
      action,
      apiURL: API.REFRESH_TOKEN,
      requestType: "GET",
      successAction: GET_REFRESH_TOKEN_SUCCESS,
    });
  } catch (e) {}
}

const initInteval = 10000;
function* fetchSagaPeriodically() {
  try {
    while (true) {
      const refreshInterval = yield select(selectors.refreshInterval);
      const refrshIntervalInMs =
        refreshInterval === undefined || refreshInterval === null
          ? initInteval
          : refreshInterval;

      yield delay(parseInt(refrshIntervalInMs));
      yield call(fetchData);
    }
  } catch (e) {}
}

function* cancelFetchSagaPeriodically(task) {
  try {
    yield cancel(task);
  } catch (error) {}
}

function* dataSaga() {
  const bgRefreshTask = yield takeLatest(
    START_REFRESH_TOKEN_SAGA,
    fetchSagaPeriodically
  );
  yield takeLatest(START_REFRESH_TOKEN_SAGA, fetchData);
  yield takeLatest(USER_LOGOUT, cancelFetchSagaPeriodically, bgRefreshTask);
  yield takeLatest(FORCE_LOGOUT, cancelFetchSagaPeriodically, bgRefreshTask);
}

export default dataSaga;
