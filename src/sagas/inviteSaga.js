import { takeLatest, call } from "redux-saga/effects";
import { INVITE_FRIEND } from "../Actions/actionType";
import MasterSaga from "./masterSaga";
import { API } from "../common/constants";

/**
 * invites a friend
 * @param {*} action 
 */
function* inviteFriend(action) {
    yield call(MasterSaga, {
        action,
        apiURL: API.INVITE_FRIEND,
        requestBody: action.payload.params,
        requestType: "POST",
    });
}

function* dataSaga() {
  yield takeLatest(INVITE_FRIEND, inviteFriend);
}

export default dataSaga;
