import { 
  GET_BLOGS,
} from "../Actions/actionType";

/**
 * Gets blog list
 * @returns 
 */
export const getBlogs = (params, onSuccess, onFailure) => {
    return {
        type: GET_BLOGS,
        payload: {
          params,
          onSuccess,
          onFailure,
        },
    };
};