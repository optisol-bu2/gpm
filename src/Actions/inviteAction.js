import { INVITE_FRIEND } from "./actionType";

/**
 * invites a friend
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const inviteFriend = (params, onSuccess, onFailure) => {
  return {
      type: INVITE_FRIEND,
      payload: {
        params,
        onSuccess,
        onFailure,
      },
  };
};