import { GET_ABOUT,GET_FAQ } from "./actionType";

/**
 * gets about data
 * @returns 
 */
export const getAbout = (id, onSuccess, onFailure) => {
  return {
      type: GET_ABOUT,
      payload: {
        id,
        onSuccess,
        onFailure,
      },
  };
};

/**
 * gets faq data
 * @returns 
 */
export const getFaq = (onSuccess, onFailure) => {
  return {
      type: GET_FAQ,
      payload: {
        onSuccess,
        onFailure,
      },
  };
};