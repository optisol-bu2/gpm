import { HELP_SUPPORT } from "./actionType";

/**
 * sends help & support email
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const helpSupport = (params, onSuccess, onFailure) => {
  return {
      type: HELP_SUPPORT,
      payload: {
        params,
        onSuccess,
        onFailure,
      },
  };
};