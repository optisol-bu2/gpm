import { 
  FAVORITE_WEBSITE,
  SEARCH_FAVORITE,
  ADD_FAVORITE,
  ADD_FAVORITE_BLOGGER,
  REMOVE_FAVORITE,
  REMOVE_FAVORITE_SERVICE,
  REMOVE_FAVORITE_BLOGGER,
} from "../Actions/actionType";

/**
 * Gets favorites list for a user
 * @returns 
 */
export const getFavoriteWebsite = (id, onSuccess, onFailure) => {
    return {
        type: FAVORITE_WEBSITE,
        payload: {
          id,
          onSuccess,
          onFailure,
        },
    };
};

/**
 * searches favorites list
 * @returns 
 */
 export const searchFavorite = (params, onSuccess, onFailure) => {
  return {
      type: SEARCH_FAVORITE,
      payload: {
        params,
        onSuccess,
        onFailure,
      },
  };
};

/**
 * adds favorite website for the user
 * @returns 
 */
export const addFavorite = (params, onSuccess, onFailure) => {
    return {
        type: ADD_FAVORITE,
        payload: {
          params,
          onSuccess,
          onFailure,
        },
    };
};

/**
 * adds favorite blogger for the user
 * @returns 
 */
export const addFavoriteBlogger = (params, onSuccess, onFailure) => {
    return {
        type: ADD_FAVORITE_BLOGGER,
        payload: {
          params,
          onSuccess,
          onFailure,
        },
    };
};

/**
 * removes favorites for the user
 * @returns 
 */
export const removeFavorite = (id, onSuccess, onFailure) => {
    return {
        type: REMOVE_FAVORITE,
        payload: {
          id,
          onSuccess,
          onFailure,
        },
    };
};

/**
 * removes favorite website for the user
 * @returns 
 */
export const removeFavoriteService = (id, onSuccess, onFailure) => {
    return {
        type: REMOVE_FAVORITE_SERVICE,
        payload: {
          id,
          onSuccess,
          onFailure,
        },
    };
};

/**
 * removes favorite blogger for the user
 * @returns 
 */
export const removeFavoriteBlogger = (id, onSuccess, onFailure) => {
    return {
        type: REMOVE_FAVORITE_BLOGGER,
        payload: {
          id,
          onSuccess,
          onFailure,
        },
    };
};