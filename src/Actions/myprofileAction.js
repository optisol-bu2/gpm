import {
    FETCH_PROFILE,
    PROFILE_UPDATE,
    FETCH_USERINFO,
    USERINFO_UPDATE,
    PASSWORD_UPDATE,
    FETCH_COUNTRY,
    FETCH_STATE,
    FETCH_NOTIFICATION,
    UPDATE_NOTIFICATION,
    FETCH_BILLING,
    UPDATE_BILLING,
    FETCH_PROFILEPIC,
    UPDATE_PROFILEPIC,
    REMOVE_PROFILEPIC,
    FETCH_REASONS,
    DEACTIVATE,
    BILLING_INFO_ACCOUNT_LINK,
    BILLING_INFO_STATUS,
    ADD_BILLING_TOKEN,
    ADD_PAYPAL_CODE,
    GET_PAYPAL_LINK,
    CHANGE_DEFAULT_METHOD,
    FETCH_KYCINFORMATION,
  } from "../Actions/actionType";

/**
 * gets profile data
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const myprofileAction = (onSuccess, onFailure) => {
    return {
        type: FETCH_PROFILE,
        payload: {
          onSuccess,
          onFailure,
        },
    };
}

/**
 * updates profile data
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const myprofileUpdateAction = (params, onSuccess, onFailure) => {
    return {
        type: PROFILE_UPDATE,
        payload: {
          params,
          onSuccess,
          onFailure,
        },
    };
}

/**
 * gets user info data
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const userInfoAction = (onSuccess, onFailure) => {
    return {
        type: FETCH_USERINFO,
        payload: {
          onSuccess,
          onFailure,
        },
    };
}

/**
 * updates user info data
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const userInfoUpdateAction = (params, onSuccess, onFailure) => {
    return {
        type: USERINFO_UPDATE,
        payload: {
          params,
          onSuccess,
          onFailure,
        },
    };
}

/**
 * changes password
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const passwordChangeAction = (params, onSuccess, onFailure) => {
    return {
        type: PASSWORD_UPDATE,
        payload: {
          params,
          onSuccess,
          onFailure,
        },
    };
}

/**
 * gets country data
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const countryAction = (onSuccess, onFailure) => {
    return {
        type: FETCH_COUNTRY,
        payload: {
          onSuccess,
          onFailure,
        },
    };
}

   /**
 * gets kyc information
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
    export const KycInformationAction = (onSuccess, onFailure) => {
        return {
            type: FETCH_KYCINFORMATION,
            payload: {
              onSuccess,
              onFailure,
            },
        };
    }

/**
 * gets state data
 * @param {*} id 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const stateAction = (id, onSuccess, onFailure) => {
    return {
        type: FETCH_STATE,
        payload: {
          id,
          onSuccess,
          onFailure,
        },
    };
}

/**
 * gets notification action data
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const notificationAction = (onSuccess, onFailure) => {
    return {
        type: FETCH_NOTIFICATION,
        payload: {
          onSuccess,
          onFailure,
        },
    };
}

/**
 * updates notification action data
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const notificationPutAction = (params, onSuccess, onFailure) => {
    return {
        type: UPDATE_NOTIFICATION,
        payload: {
            params,
            onSuccess,
            onFailure,
        },
    };
}

/**
 * gets billing info data
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getBillingAction = (onSuccess, onFailure) => {
    return {
        type: FETCH_BILLING,
        payload: {
          onSuccess,
          onFailure,
        },
    };
}

/**
 * updates billing info data
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const billingAction = (params, onSuccess, onFailure) => {
    return {
        type: UPDATE_BILLING,
        payload: {
            params,
            onSuccess,
            onFailure,
        },
    };
}

/**
 * gets profile picture
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getProfilePic = (onSuccess, onFailure) => {
    return {
        type: FETCH_PROFILEPIC,
        payload: {
          onSuccess,
          onFailure,
        },
    };
}

/**
 * updates profile picture
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const updateProfilePic = (params, onSuccess, onFailure) => {
    return {
        type: UPDATE_PROFILEPIC,
        payload: {
            params,
            onSuccess,
            onFailure,
        },
    };
}

/**
 * removes profile picture
 * @returns 
 */
export const removeProfilePic = (onSuccess, onFailure) => {
    return {
        type: REMOVE_PROFILEPIC,
        payload: {
          onSuccess,
          onFailure,
        },
    };
}

/**
 * gets reason data
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getReasonAction = (onSuccess, onFailure) => {
    return {
        type: FETCH_REASONS,
        payload: {
          onSuccess,
          onFailure,
        },
    };
}

/**
 * deactivates user account
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const deactivateAccountAction = (params, onSuccess, onFailure) => {
    return {
        type: DEACTIVATE,
        payload: {
            params,
            onSuccess,
            onFailure,
        },
    };
}

/**
 * gets billing Info Account Link
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
 export const billingInfoAccountLink = (params, onSuccess, onFailure) => {
    return {
        type: BILLING_INFO_ACCOUNT_LINK,
        payload: {
            params,
            onSuccess,
            onFailure,
        },
    };
}

/**
 * gets Billing Information Status
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
 export const getBillingInformationStatus = (onSuccess, onFailure) => {
    return {
        type: BILLING_INFO_STATUS,
        payload: {
            onSuccess,
            onFailure,
        },
    };
}

/**
 * add billing bank/card token
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
 export const addBillingToken = (params, onSuccess, onFailure) => {
    return {
        type: ADD_BILLING_TOKEN,
        payload: {
            params,
            onSuccess,
            onFailure,
        },
    };
}

/**
 * add paypal code
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
 export const addPayPalCode = (params, onSuccess, onFailure) => {
    return {
        type: ADD_PAYPAL_CODE,
        payload: {
            params,
            onSuccess,
            onFailure,
        },
    };
}

/**
 * get paypal redirection link
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
 export const getPayPalLink = (onSuccess, onFailure) => {
    return {
        type: GET_PAYPAL_LINK,
        payload: {
            onSuccess,
            onFailure,
        },
    };
}

/**
 * changes default payment method
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
 export const changeDefaultMethod = (params, onSuccess, onFailure) => {
    return {
        type: CHANGE_DEFAULT_METHOD,
        payload: {
            params,
            onSuccess,
            onFailure,
        },
    };
}