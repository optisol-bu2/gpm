import {
  SEND_MESSAGE,
  SEND_ATTACHMENT,
  GET_CHATS,
  GET_HEADERINFO,
  GET_CHAT_MESSAGES,
  UPDATE_CHAT_MESSAGE,
  SEARCH_CHAT_LIST,
  UPDATE_MESSAGE_SUCCESS,
  DELETE_MESSAGE_SUCCESS,
  READ_NOTIFICATION,
  READ_ALL_NOTIFICATION,
  DELETE_MESSAGE,
  REJECT_CUSTOM_ORDER,
  CUSTOM_ORDER_DETAIL,
  CHANGE_MESSAGE,
} from "../Actions/actionType";

/**
 * Sends text message
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const sendMessage = (params, onSuccess, onFailure) => {
  return {
    type: SEND_MESSAGE,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * Sends attachment message 
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const sendAttachment = (params, onSuccess, onFailure) => {
  return {
    type: SEND_ATTACHMENT,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * Gets chat list for the user
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getChats = (onSuccess, onFailure) => {
  return {
      type: GET_CHATS,
      payload: {
        onSuccess,
        onFailure,
      },
  };
};

/**
 * Gets messages of a particular user for the given pageIndex
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getChatMessages = (params, onSuccess, onFailure) => {
  return {
    type: GET_CHAT_MESSAGES,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * Gets messages of a particular user for the given pageIndex and update it to current messages
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const updateChatMessages = (params, onSuccess, onFailure) => {
  return {
    type: UPDATE_CHAT_MESSAGE,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * Gets chat list of users matching the given searchText
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const searchChatList = (params, onSuccess, onFailure) => {
  return {
    type: SEARCH_CHAT_LIST,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * Updates message data in the chat messages
 * @param {*} data 
 * @returns 
 */
export const updateMessages = (data) => {
  return async dispatch => {
      dispatch({
        type: UPDATE_MESSAGE_SUCCESS,
        data: data 
      });
      return data
  };
};

/**
 * removes message data in the chat messages
 * @param {*} data 
 * @returns 
 */
export const removeMessage = (data) => {
  return async dispatch => {
      dispatch({
        type: DELETE_MESSAGE_SUCCESS,
        data: data 
      });
      return data
  };
};

/**
 * changes message data in the chat messages
 * @param {*} data 
 * @returns 
 */
export const changeMessage = (data) => {
  return async dispatch => {
      dispatch({
        type: CHANGE_MESSAGE,
        data: data 
      });
      return data
  };
};

/**
 * get header info for the user
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getHeaderInfo = (onSuccess, onFailure) => {
  return {
      type: GET_HEADERINFO,
      payload: {
        onSuccess,
        onFailure,
      },
  };
};

/**
 * deletes message
 * @param {*} id 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const deleteMessage = (id, onSuccess, onFailure) => {
  return {
    type: DELETE_MESSAGE,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * get custom order details
 * @param {*} id 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
 export const customOrderDetail = (id, onSuccess, onFailure) => {
  return {
    type: CUSTOM_ORDER_DETAIL,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * reject custom order
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const rejectCustomOrder = (params, onSuccess, onFailure) => {
  return {
    type: REJECT_CUSTOM_ORDER,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * reads a notification for the user
 * @param {*} id 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const readNotification = (id, onSuccess, onFailure) => {
  return {
    type: READ_NOTIFICATION,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * reads all notifications for the user
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const readAllNotification = (onSuccess, onFailure) => {
  return {
    type: READ_ALL_NOTIFICATION,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};