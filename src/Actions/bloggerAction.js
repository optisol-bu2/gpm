import {
  GET_BLOGGER_LIST,
  SEARCH_BLOGGER_LIST,
  GET_BLOGGER_PROFILE,
  GET_BLOGGER_DETAILS,
  ADD_BLOGGER_DETAILS,
  ADD_BLOGGER_PORTFOLIO,
  ADD_BLOG_LINK,
  REMOVE_BLOG_LINK,
  ADD_BLOGGER_ABOUTME,
  REMOVE_BLOGGER_ATTACHMENT,
  CART_BLOGGER,
  GET_CART_BLOGGER,
  PAYMENT_SESSION_BLOGGER,
  FILTER_BLOGGER_LIST,
} from "./actionType";

/**
 * gets blogger list data
 * @returns 
 */
export const getBloggerList = (id, onSuccess, onFailure) => {
  return {
      type: GET_BLOGGER_LIST,
      payload: {
        id,
        onSuccess,
        onFailure,
      },
  };
};

/**
 * search blogger list data
 * @returns 
 */
export const searchBloggerList = (params, onSuccess, onFailure) => {
  return {
      type: SEARCH_BLOGGER_LIST,
      payload: {
        params,
        onSuccess,
        onFailure,
      },
  };
};


export const filterBloggerList = (params, onSuccess, onFailure) => {
  return {
      type: FILTER_BLOGGER_LIST,
      payload: {
        params,
        onSuccess,
        onFailure,
      },
  };
};

/**
 * gets blogger profile
 * @returns 
 */
export const getBloggerProfile = (id, onSuccess, onFailure) => {
  return {
      type: GET_BLOGGER_PROFILE,
      payload: {
        id,
        onSuccess,
        onFailure,
      },
  };
};

/**
 * gets blogger details
 * @returns 
 */
export const getBloggerDetails = (onSuccess, onFailure) => {
  return {
      type: GET_BLOGGER_DETAILS,
      payload: {
        onSuccess,
        onFailure,
      },
  };
};

/**
 * add blogger details
 * @param {*} params 
 * @returns 
 */
export const addBloggerDetails = (params, onSuccess, onFailure) => {
  return {
    type: ADD_BLOGGER_DETAILS,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * add blogger portfolio attachment
 * @param {*} params 
 * @returns 
 */
export const addBloggerPortfolio = (params, onSuccess, onFailure) => {
  return {
    type: ADD_BLOGGER_PORTFOLIO,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * add blog link
 * @param {*} params 
 * @returns 
 */
export const addBlogLink = (params, onSuccess, onFailure) => {
  return {
    type: ADD_BLOG_LINK,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * remove blog link
 * @param {*} params 
 * @returns 
 */
export const removeBlogLink = (id, onSuccess, onFailure) => {
  return {
    type: REMOVE_BLOG_LINK,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * add blogger about me video attachment
 * @param {*} params 
 * @returns 
 */
export const addBloggerAboutMe = (params, onSuccess, onFailure) => {
  return {
    type: ADD_BLOGGER_ABOUTME,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * remove blogger attachment
 * @param {*} id 
 * @returns 
 */
export const removeBloggerAttachment = (id, onSuccess, onFailure) => {
  return {
    type: REMOVE_BLOGGER_ATTACHMENT,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * adds blogger to cart
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
 export const addCartBlogger = (params, onSuccess, onFailure) => {
  return {
    type: CART_BLOGGER,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * gets blogger cart data
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getCartBlogger = (onSuccess, onFailure) => {
  return {
    type: GET_CART_BLOGGER,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

/**
 * creates a session for blogger payment
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
 export const paymentSessionBlogger = (params, onSuccess, onFailure) => {
  return {
    type: PAYMENT_SESSION_BLOGGER,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};