import {
  USER_LOGIN,
  FETCH_LOGIN,
  SHOW_DISPLAY_MESSAGE,
  HIDE_DISPLAY_MESSAGE,
  GET_DASHBOARD,
  GET_DATA_TOTALSPENT,
  GET_STATISTICS,
  ANALYTICS_DETAILS,
  UPDATE_ANALYTICS,
  GET_DATA_TRAFFICANALYTICS,
  GET_DATA_DAILYTRAFFIC,
  SWITCH_USER,
  REMOVE_AUTHORIZATION,
  SOCIAL_LOGIN,
  SERVICE_CATEGORIES,
  SERVICE_ESTIMATED_DELIVERY,
  SEARCH_SERVICE_USER,
  ADD_SERVICE,
  ADD_CUSTOM_ORDER,
  VIEW_QUESTIONS,
  ADD_QUESTIONS,
  GET_SERVICE_LIST,
  GET_RECOMMENDATIONS,
  CHECK_BILLING_INFO,
  REMOVE_SERVICE,
  REMOVE_CUSTOM_SERVICE,
  REGISTERATION,
  FORGOT_PASSWORD,
  VERIFY_TOKEN,
  VERIFY_ACTIVATE_TOKEN,
  RESET_PASSWORD,
  EMAIL_VALIDATE,
  MARKETPLACE_FILTER,
  MARKETPLACE_SERVICES,
  GET_SERVICE_COST,
  GET_SERVICE_DETAIL,
  UPDATE_SERVICE,
  CHANGE_SERVICE_STATUS,
  CHANGE_CUSTOM_STATUS,
  START_REFRESH_TOKEN_SAGA,
  CART_REDIRECT,
  GET_CART_DATA,
  CART_CUSTOM,
  GET_CART_CUSTOM,
  PAYMENT_SESSION_CUSTOM,
  REMOVE_CART_ITEM,
  GET_SERVICEBACKLINK_DATA,
  GET_SERVICEGUESTPOST_DATA,
  GET_TOP_SERVICES_DATA,
  GET_SEARCH_ORDER_DATA,
  GET_COMPLETE_ORDER_SEARCH,
  GET_CANCEL_ORDER_SEARCH,
  GET_TOP_CONTENT_WRITERS,
  CREATE_ORDER,
  GET_MYORDER_DATA,
  START_ORDER,
  REMOVE_ATTACHMENT,
  UPLOAD_ATTACHMENT,
  FORCE_LOGOUT,
  GET_FILTER_ORDER_DATA,
  GET_ORDER_DATA,
  GET_CANCEL_ORDER_DATA,
  GET_CANCEL_ORDER_FILTER,
  GET_COMPLETE_ORDER_DATA,
  GET_COMPLETE_ORDER_FILTER,
  ORDER_APPROVED,
  ORDER_NOT_APPROVED,
  GET_ATTACHMENTS,
  DELETE_ATTACHMENT,
  SEND_ORDER_UPDATE,
  SUBMIT_FEEDBACK,
  GET_FEEDBACK,
  UPDATE_ATTACHMENT,
  ORDER_EXTENTION,
  PAYMENT_SESSION,
  PLACE_ORDER,
  CREATE_ORDER_PAYPAL,
  GET_SAVED_CARDS,
  ADD_CARD,
  REMOVE_CARD,
  SET_TOTAL_ORGANIC,
  ORDER_CANCEL,
  SET_TIMER_START,
  SET_24HR_TIMER,
  CHECK_CONGRATULATION
} from "./actionType";

export const userLogin = (params, onSuccess, onFailure) => {
  return {
    type: USER_LOGIN,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const checkCongratulation = (params) => {
  return {
    type: CHECK_CONGRATULATION,
    data:params
  };
};

/**
 * fetch login data
 * @returns 
 */
export const reLoginAction = (onSuccess, onFailure) => {
  return {
    type: FETCH_LOGIN,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

export const showDisplayMessage = (messageDetails) => {
  return {
    type: SHOW_DISPLAY_MESSAGE,
    payload: { messageDetails },
  };
};

export const hideDisplayMessage = () => {
  return {
    type: HIDE_DISPLAY_MESSAGE,
    payload: {},
  };
};

/**
 * gets dashboard data
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getDashboard = (onSuccess, onFailure) => {
  return {
    type: GET_DASHBOARD,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

export const getDataTotalSpent = (params, onSuccess, onFailure) => {
  return {
    type: GET_DATA_TOTALSPENT,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * gets statistics data
 * @param {*} id 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getStatistics = (id, onSuccess, onFailure) => {
  return {
    type: GET_STATISTICS,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * saves google analytics details
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const analyticsDetails = (params, onSuccess, onFailure) => {
  return {
    type: ANALYTICS_DETAILS,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * updates analytics details
 * @param {*} data 
 * @returns 
 */
export const updateAnalytics = (data) => {
  return async dispatch => {
    dispatch({
      type: UPDATE_ANALYTICS,
      data: data 
    });
  };
};

export const getDataTrafficAnalytics = (params, onSuccess, onFailure) => {
  return {
    type: GET_DATA_TRAFFICANALYTICS,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const getDataDailyTraffic = (params, onSuccess, onFailure) => {
  return {
    type: GET_DATA_DAILYTRAFFIC,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const switchUser = (onSuccess, onFailure) => {
  return {
    type: SWITCH_USER,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

/**
 * removes authorization
 * @returns 
 */
 export const removeAuthorization = () => {
  return async dispatch => {
      dispatch({
        type: REMOVE_AUTHORIZATION,
      });
  };
};

export const userSocialLogin = (params, onSuccess, onFailure) => {
  return {
    type: SOCIAL_LOGIN,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const getServiceCategories = () => {
  return {
    type: SERVICE_CATEGORIES,
    payload: {},
  };
};

export const getServiceEstimatedDelivey = () => {
  return {
    type: SERVICE_ESTIMATED_DELIVERY,
    payload: {},
  };
};

/**
 * searches user matching searchText
 * @param {*} searchText 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const searchServiceUsers = (searchText, onSuccess, onFailure) => {
  return {
    type: SEARCH_SERVICE_USER,
    payload: {
      searchText,
      onSuccess,
      onFailure,
    },
  };
};

export const addService = (params, onSuccess, onFailure) => {
  return {
    type: ADD_SERVICE,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * adds custom order
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const addCustomOrder = (params, onSuccess, onFailure) => {
  return {
    type: ADD_CUSTOM_ORDER,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * adds custom order
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
 export const addQuestions = (params, onSuccess, onFailure) => {
  return {
    type: ADD_QUESTIONS,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * adds custom order
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
 export const getViewQuestions = (onSuccess, onFailure) => {
  return {
    type: VIEW_QUESTIONS,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

export const getServiceList = (onSuccess, onFailure) => {
  return {
    type: GET_SERVICE_LIST,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

/**
 * gets recommendations data for the service
 * @param {*} id 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getRecommendations = (id, onSuccess, onFailure) => {
  return {
    type: GET_RECOMMENDATIONS,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

export const removeService = (id, onSuccess, onFailure) => {
  return {
    type: REMOVE_SERVICE,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * removes custom order
 * @param {*} id 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const removeCustomService = (id, onSuccess, onFailure) => {
  return {
    type: REMOVE_CUSTOM_SERVICE,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * changes service status
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const changeServiceStatus = (params, onSuccess, onFailure) => {
  return {
    type: CHANGE_SERVICE_STATUS,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * changes custom order status
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const changeCustomStatus = (params, onSuccess, onFailure) => {
  return {
    type: CHANGE_CUSTOM_STATUS,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const registrationCall = (params, onSuccess, onFailure) => {
  return {
    type: REGISTERATION,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * sends forgot password email
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const forgotPassword = (params, onSuccess, onFailure) => {
  return {
    type: FORGOT_PASSWORD,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * verifies forgot password token
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const verifyToken = (params, onSuccess, onFailure) => {
  return {
    type: VERIFY_TOKEN,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * verifies activate account token
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const verifyActivateToken = (params, onSuccess, onFailure) => {
  return {
    type: VERIFY_ACTIVATE_TOKEN,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * resets password
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const resetPassword = (params, onSuccess, onFailure) => {
  return {
    type: RESET_PASSWORD,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const checkEmail = (params, onSuccess, onFailure) => {
  return {
    type: EMAIL_VALIDATE,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const getMarketPlaceList = (id, onSuccess, onFailure) => {
  return {
    type: MARKETPLACE_SERVICES,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

export const filterMarketPlace = (params, onSuccess, onFailure) => {
  return {
    type: MARKETPLACE_FILTER,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const getServiceCost = (onSuccess, onFailure) => {
  return {
    type: GET_SERVICE_COST,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

export const getServiceDetail = (id, onSuccess, onFailure) => {
  return {
    type: GET_SERVICE_DETAIL,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

export const updateService = (params, onSuccess, onFailure) => {
  return {
    type: UPDATE_SERVICE,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const callRefreshTokenSaga = () => {
  return {
    type: START_REFRESH_TOKEN_SAGA,
  };
};

export const verifyCartRedirect = (params, onSuccess, onFailure) => {
  return {
    type: CART_REDIRECT,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const getCartData = (onSuccess, onFailure) => {
  return {
    type: GET_CART_DATA,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

/**
 * adds custom order to cart
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const addCartCustom = (params, onSuccess, onFailure) => {
  return {
    type: CART_CUSTOM,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * gets custom order cart data
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getCartCustom = (onSuccess, onFailure) => {
  return {
    type: GET_CART_CUSTOM,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

/**
 * creates a session for custom order payment
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const paymentSessionCustom = (params, onSuccess, onFailure) => {
  return {
    type: PAYMENT_SESSION_CUSTOM,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const removeCartItem = (id, onSuccess, onFailure) => {
  return {
    type: REMOVE_CART_ITEM,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

export const getServiceBackLinkData = (onSuccess, onFailure) => {
  return {
    type: GET_SERVICEBACKLINK_DATA,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

export const getServiceGuestPostData = (onSuccess, onFailure) => {
  return {
    type: GET_SERVICEGUESTPOST_DATA,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

/**
 * gets top services
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getTopServices = (onSuccess, onFailure) => {
  return {
    type: GET_TOP_SERVICES_DATA,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

/**
 * gets top content writers
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getTopContentWriters = (onSuccess, onFailure) => {
  return {
    type: GET_TOP_CONTENT_WRITERS,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

/**
 * checks whether the seller already added billing information or not
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const checkBillingInfo = (onSuccess, onFailure) => {
  return {
    type: CHECK_BILLING_INFO,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

/**
 * creates a session for payment
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const paymentSession = (params, onSuccess, onFailure) => {
  return {
    type: PAYMENT_SESSION,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * Places order when payment session success
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const placeOrder = (params, onSuccess, onFailure) => {
  return {
    type: PLACE_ORDER,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * create order for paypal payment
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const createOrderForPaypal = (params, onSuccess, onFailure) => {
  return {
    type: CREATE_ORDER_PAYPAL,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * Gets all saved cards of the user
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const getSavedCards = (onSuccess, onFailure) => {
  return {
    type: GET_SAVED_CARDS,
    payload: {
      onSuccess,
      onFailure,
    },
  };
};

/**
 * Adds to saved cards of the user
 * @param {*} params 
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const addCard = (params, onSuccess, onFailure) => {
  return {
    type: ADD_CARD,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * Removes saved cards of the user
 * @param {*} onSuccess 
 * @param {*} onFailure 
 * @returns 
 */
export const removeCard = (id, onSuccess, onFailure) => {
  return {
    type: REMOVE_CARD,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

export const createOrder = (params, onSuccess, onFailure) => {
  return {
    type: CREATE_ORDER,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const getMyOrderDetails = (id, onSuccess, onFailure) => {
  return {
    type: GET_MYORDER_DATA,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

export const uploadAttachment = (params, onSuccess, onFailure) => {
  return {
    type: UPLOAD_ATTACHMENT,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const removeAttachment = (id, onSuccess, onFailure) => {
  return {
    type: REMOVE_ATTACHMENT,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

export const startOrder = (params, onSuccess, onFailure) => {
  return {
    type: START_ORDER,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const setEndTimer = (param) => {
  return{
    type: SET_TIMER_START,
    data: param
  }
};

export const last24Hours = (param) => {
  return{
    type: SET_24HR_TIMER,
    data: param
  }
};

export const forceLogOut = () => {
  return {
    type: FORCE_LOGOUT,
  };
};

export const filterOrderData = (params, onSuccess, onFailure) => {
  return {
    type: GET_FILTER_ORDER_DATA,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const SearchOrderData = (params, onSuccess, onFailure) => {
  return {
    type: GET_SEARCH_ORDER_DATA,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const getOrderDetail = (id, onSuccess, onFailure) => {
  return {
    type: GET_ORDER_DATA,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

export const filterCompletedOrderData = (params, onSuccess, onFailure) => {
  return {
    type: GET_COMPLETE_ORDER_FILTER,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const SearchCompletedOrderData = (params, onSuccess, onFailure) => {
  return {
    type: GET_COMPLETE_ORDER_SEARCH,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const getCompletedOrderData = (id, onSuccess, onFailure) => {
  return {
    type: GET_COMPLETE_ORDER_DATA,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

export const filterCancelOrderData = (params, onSuccess, onFailure) => {
  return {
    type: GET_CANCEL_ORDER_FILTER,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const SearchCancelOrderData = (params, onSuccess, onFailure) => {
  return {
    type: GET_CANCEL_ORDER_SEARCH,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};


export const getCancelOrderData = (id, onSuccess, onFailure) => {
  return {
    type: GET_CANCEL_ORDER_DATA,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

export const orderApproved = (params, onSuccess, onFailure) => {
  return {
    type: ORDER_APPROVED,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const orderNotApproved = (params, onSuccess, onFailure) => {
  return {
    type: ORDER_NOT_APPROVED,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const orderCancel = (params, onSuccess, onFailure) => {
  return {
    type: ORDER_CANCEL,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const deleteAttachment = (id, onSuccess, onFailure) => {
  return {
    type: DELETE_ATTACHMENT,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

export const getAttachments = (order_id, revision, onSuccess, onFailure) => {
  return {
    type: GET_ATTACHMENTS,
    payload: {
      order_id,
      revision,
      onSuccess,
      onFailure,
    },
  };
};

export const sendOrderUpdate = (params, onSuccess, onFailure) => {
  return {
    type: SEND_ORDER_UPDATE,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const submitFeedback = (params, onSuccess, onFailure) => {
  return {
    type: SUBMIT_FEEDBACK,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const getFeedback = (id, onSuccess, onFailure) => {
  return {
    type: GET_FEEDBACK,
    payload: {
      id,
      onSuccess,
      onFailure,
    },
  };
};

export const updateAttachment = (params, onSuccess, onFailure) => {
  return {
    type: UPDATE_ATTACHMENT,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

export const orderExtention = (params, onSuccess, onFailure) => {
  return {
    type: ORDER_EXTENTION,
    payload: {
      params,
      onSuccess,
      onFailure,
    },
  };
};

/**
 * updates analytics details
 * @param {*} data 
 * @returns 
 */
 export const setTotalOrganicValue = (data) => {
  return async dispatch => {
    dispatch({
      type: SET_TOTAL_ORGANIC,
      data: data 
    });
  };
};
