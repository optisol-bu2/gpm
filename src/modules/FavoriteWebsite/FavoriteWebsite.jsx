import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Loading from "../../components/core/Loading";
import Pagination from "@material-ui/lab/Pagination";
import { ToastAlert, PlaySound } from "../../utils/sweetalert2";
import {
    getFavoriteWebsite,
    searchFavorite,
    addFavorite,
    removeFavorite
} from "../../Actions/favoriteAction";
import defaultImage from "../../assets/images/placeholder.jpeg";
import avatar from '../../assets/images/avatar.png';
import { Image, Popup } from 'semantic-ui-react'
import moment from 'moment';

/**
 * FavoriteWebsite Component
 */
class FavoriteWebsite extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            totalRecord: 0,
            searchText: "",
            pageIndex: 1,
            isLoading: null,
            isSearch: false,
        };

    }

    /**
     * changes the given time to chat time format
     * @param {*} dateTime
     * @returns
     */ 
    chatTimeFormat = (dateTime) => {
        if(!dateTime) return "" ; 
        var utc = moment.utc(dateTime).toDate();
        var utcDate = moment.utc(dateTime).format('YYYY-MM-DD');
        var todayDate = moment.utc().format('YYYY-MM-DD');
        var utcTime = moment(utc).local().format("DD-MM-YYYY HH:mm:ss");
        var todayTime = moment().local().format("DD-MM-YYYY HH:mm:ss");
        var ms = moment(todayTime,"DD/MM/YYYY HH:mm:ss").diff(moment(utcTime,"DD/MM/YYYY HH:mm:ss"));
        var duration = moment.duration(ms);
        if (utcDate === todayDate) {
            var getMinutes = parseInt(duration.as('minutes'));
            var chatTime = getMinutes + " minutes ago"; 
            if(parseInt(getMinutes) == 1) {
                var chatTime = getMinutes + " minute ago";
            }
            if(parseInt(getMinutes) >= 60) {
                var getHours = parseInt(duration.as('hours'));
                var chatTime = getHours + " hours ago"; 
                if(parseInt(getHours) == 1) {
                    var chatTime = getHours + " hour ago";
                }
            }
        } else {
            var getDays = parseInt(duration.as('days'));
            var chatTime = getDays + " days ago";
            if(parseInt(getDays) == 1) {
                var chatTime = getDays + " day ago";
            }
        } 
        return chatTime
    }

    componentDidMount() {
        this.getFavoriteWebsite();
    }

    /**
     * function to get favorites for the user 
     */
    getFavoriteWebsite = (loading) => {
        if (loading === undefined) {
            this.setState({ isLoading: true });
        }
        this.props.getFavoriteWebsite(this.state.pageIndex, (favorite) => {
            if (favorite.data) {
                this.setState({ totalRecord: Math.ceil(favorite.data.total / favorite.data.noofrecords) == 1 ? 0 : Math.ceil(favorite.data.total / favorite.data.noofrecords), isLoading: false });
                if (loading === undefined) {
                    window.scrollTo({ top: 0, behavior: "smooth" });
                }
            } else {
                this.setState({ totalRecord: 0, isLoading: false });
            }
        },
            () => {
                this.setState({ totalRecord: 0, isLoading: false });
            })
    }

    /**
     * gets favorites list for given pageIndex
     * @param {*} pageIndex 
     */
    onPageChange = (pageIndex) => {
        this.setState({ pageIndex: pageIndex }, () => {
            if (this.state.isSearch) {
                this.searchFavoriteWebsite();
            } else {
                this.getFavoriteWebsite();
            }
        });
    }

    /**
     * removes favorite for the user
     * @param {*} id 
     */
    removeFavorite = (id) => {
        this.setState({ isLoading: true });
        this.props.removeFavorite(id,
            (response) => {
                this.getFavoriteWebsite();
            },
            (error) => {
                this.setState({ isLoading: false });
                ToastAlert("error", error.message);
                PlaySound();
            }
        )
    }

    /**
     * handles changes in the event and updates searchText state
     * @param {*} e 
     */
    onChangeSearchText = (e) => {
        if (e.target.value.length >= 3) {
            this.setState({ searchText: e.target.value, pageIndex: 1, isSearch: true }, () => {
                this.searchFavoriteWebsite(true);
            });
        } else {
            this.setState({ searchText: e.target.value, pageIndex: 1, isSearch: false }, () => {
                if (!this.state.searchText)
                    this.getFavoriteWebsite(true);
            });
        }

    }

    /**
     * searches favorites matching searchText in the list
     */
    searchFavoriteWebsite = (load) => {
        if (load === undefined) {
            this.setState({ isLoading: true });
        }
        var params = {
            pageindex: this.state.pageIndex,
            search: this.state.searchText,
        }
        this.props.searchFavorite(params, (favorite) => {
            if (favorite.data) {
                this.setState({ totalRecord: Math.ceil(favorite.data.total / favorite.data.noofrecords) == 1 ? 0 : Math.ceil(favorite.data.total / favorite.data.noofrecords), isLoading: false });
            } else {
                this.setState({ totalRecord: 0, isLoading: false });
            }
        },
            () => {
                this.setState({ totalRecord: 0, isLoading: false });
            })
    }

    /**
     * renders favorites list
     * @returns 
     */
    renderFavoriteWebsite1 = () => {
        const { favoriteWebsiteList } = this.props;
        return (
            <div class="row productscontainer-box">
                {favoriteWebsiteList && favoriteWebsiteList.length === 0 &&
                    <h3 className="norec_found">No Record Found</h3>
                }
                {favoriteWebsiteList && favoriteWebsiteList.length !== 0 && favoriteWebsiteList.map((favorite) => {
                    if (favorite.servicetype === 1) {
                        return (
                            <div class="col-lg-3 col-md-6 mb-4 item grid-view-products">
                                <div className="card h-100 bg-white">
                                    <div className="position-relative">
                                        <Link to={`/marketplace/${favorite.id}`} className="position-relative">
                                            <img
                                                src={favorite.servicepic}
                                                onError={(e) => { e.target.onerror = null; e.target.src = defaultImage }}
                                                class="img_crop img-fluid"
                                                alt="..."
                                            />
                                        </Link>
                                        <div className="new_seller">
                                            {favorite.rating === 0 &&
                                                <span className="new_seller_btn">New Content Writer</span>
                                            }
                                        </div>
                                    </div>
                                    <div class="card-body w-100 p-2">
                                        <h5 className="product_title" title={favorite.title}>{favorite.title}</h5>
                                        <div className="rating_star d-flex mt-1"> 
                                            {favorite.rating !== 0 && Array.from(Array(Math.round(favorite.rating)), () => {
                                                return (
                                                    <span className="fa fa-star star"></span>
                                                )
                                            })}
                                        </div>
                                        <div class="row m-0 mt-3 mb-3 align-items-center">
                                            <div class="col-sm-12 col-4 p-0 mb-2 text-left">
                                                <Popup
                                                    key={favorite.id}
                                                    trigger={<span className="sold_count_text link-cursor">By: {favorite.seller.name}</span>}
                                                >
                                                    <Popup.Header>
                                                        <Image
                                                            src={favorite.seller.image}
                                                            onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                                                            className="rounded-circle mr-2"
                                                            width="50"
                                                            height="50"
                                                            alt="..."
                                                        />
                                                        {favorite.seller.name}
                                                    </Popup.Header>
                                                    <Popup.Content>
                                                        {favorite.seller.bio}
                                                    </Popup.Content>
                                                </Popup>
                                            </div>
                                            <div class="col-sm-6 col-8 p-0 text-left"><span className="price-info mt-2 mb-2">${favorite.price}</span></div>
                                            <div class="col-sm-6 col-4 p-0 text-right"><p className="sold_count_text">{favorite.sold} Sold</p></div>
                                        </div>
                                        <div className="btn-groups d-flex justify-content-center mb-3">
                                            <a class="favorite_btn mr-2" onClick={() => { this.removeFavorite(favorite.favid) }}>
                                                <i class="fa fa-heart"></i> Favorite</a>
                                            <Link class="view_btn" to={`/marketplace/${favorite.id}`}>
                                                <span class="fa fa-eye"></span> View
                                            </Link>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    } else if (favorite.servicetype === 2) {
                        return (
                            <div class="col-lg-3 col-md-6 mb-4 item grid-view-products">
                                <div className="card h-100 bg-white blogger_card">
                                    <div class="row m-0">
                                        <div className="products_image bloggerimg fav_img_blogger">
                                            <Link to={`/contentWriter/${favorite.user}`}>
                                                <img
                                                    src={favorite.profilepic ? favorite.profilepic : avatar}
                                                    onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                                                    class="img_crop"
                                                    alt=""
                                                />
                                            </Link>
                                        </div>
                                        <div class="card-body w-100 p-2">
                                            <h5 className="product_title" title={favorite.username}>{favorite.username}</h5>
                                            <div className="rating_star d-flex mt-1">
                                                {favorite.rating !== 0 && Array.from(Array(Math.round(favorite.rating)), () => {
                                                    return (
                                                        <span className="fa fa-star star"></span>
                                                    )
                                                })}
                                                {favorite.rating === 0 &&
                                                    <span className="new_seller_btn">New Content Writer</span>
                                                }
                                            </div>
                                            <div class="row m-0 mt-3 mb-3 align-items-center">
                                                <div class="col-sm-12 col-4 p-0 text-right float-right"><p className="sold_count_text">{favorite.expertise}</p></div>
                                                <div class="col-sm-12 col-8 p-0 text-left"><span className="price-info mt-2 mb-2">${favorite.rate}/<span className="perhr_span">Hr</span></span></div>

                                            </div>
                                            {/* <p class="card-text">{favorite.expertise}</p> */}
                                            <div className="btn-groups d-flex justify-content-center mb-3">
                                                <a class="favorite_btn mr-2" onClick={() => { this.removeFavorite(favorite.favid) }}><i class="fa fa-heart"></i> Favorite</a>
                                                <Link class="view_btn" to={`/contentWriter/${favorite.user}`}>
                                                    <span class="fa fa-eye"></span> View
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                })}
            </div>
        )
    }

    renderFavoriteWebsite = () => {
        const { favoriteWebsiteList } = this.props;
        if (favoriteWebsiteList && favoriteWebsiteList.length === 0) {
            return (
                <div class="row" data-tour="content-writer">
                    <h3 className="norec_found">No Record Found</h3>
                </div>
            );
        } else {
            return (
                <div class="row pt-3" data-tour="content-writer">
                    {favoriteWebsiteList && favoriteWebsiteList.length !== 0 && favoriteWebsiteList.map((favorite, index) => {
                        if (favorite.servicetype === 1) {
                            return (
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-4 blogger-items">
                                    <div className={index % 4 == 0 ? "card h-100 blogger_green" : index % 4 == 1 ? "card h-100 blogger_brown" : index % 4 == 2 ? "card h-100 blogger_blue" : "card h-100 blogger_red"}>
                                        <div class="row m-0">
                                            <div class="col-sm-12 p-0 text-left"><span className="price-info mt-2 mb-2">${favorite.rate}/<span className="perhr_span">Hr</span></span></div>
                                            <div className="products_image">
                                                <Link to={`/marketplace/${favorite.id}`}>
                                                    <div class="blogger_img">
                                                        <img
                                                            src={favorite.servicepic}
                                                            onError={(e) => { e.target.onerror = null; e.target.src = defaultImage }}
                                                            class="rounded-circle img-fluid"
                                                            alt="" />
                                                        <span class="user-status on"></span>
                                                    </div>
                                                </Link>
                                            </div>
                                            <h5 className="product_title" title={favorite.title}>{favorite.title}</h5>
                                            <div className="new_seller">
                                                {favorite.review === 0 &&
                                                    <span className="new_seller_btn">New Content Write</span>
                                                }
                                            </div>
                                            <div className="rating_star d-flex mt-1">
                                                {favorite.rating !== 0 && Array.from(Array(Math.round(favorite.rating)), () => {
                                                    return (
                                                        <span className="fa fa-star star"></span>
                                                    )
                                                })}
                                            </div>
                                            <div class="row m-0 mt-3 mb-3 align-items-center">
                                                <div class="col-sm-12 col-4 p-0 mb-2 text-left">
                                                    <Popup
                                                        key={favorite.id}
                                                        trigger={<span className="sold_count_text link-cursor">By: {favorite.seller.name}</span>}
                                                    >
                                                        <Popup.Header>
                                                            <Image
                                                                src={favorite.seller.image}
                                                                onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                                                                className="rounded-circle mr-2"
                                                                width="50"
                                                                height="50"
                                                                alt="..."
                                                            />
                                                            {favorite.seller.name}
                                                        </Popup.Header>
                                                        <Popup.Content>
                                                            {favorite.seller.bio}
                                                        </Popup.Content>
                                                    </Popup>
                                                </div>
                                                <div class="col-sm-6 col-8 p-0 text-left"><span className="price-info mt-2 mb-2">${favorite.price}</span></div>
                                                <div class="col-sm-6 col-4 p-0 text-right"><p className="sold_count_text">{favorite.sold} Sold</p></div>
                                            </div>
                                            <div className="btn-groups d-flex justify-content-center mt-3 mb-3 w-100">
                                                <a class="favorite_btn mr-2" onClick={() => { this.removeFavorite(favorite.favid) }}><i class="fa fa-heart"></i> Favorite</a>
                                                <Link class="view_btn" to={`/marketplace/${favorite.user}`}>
                                                    <span class="fa fa-eye"></span> View
                                                </Link>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                        else if (favorite.servicetype === 2) {
                            return (
                                <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-4 blogger-items">
                                    <div className={index % 4 == 0 ? "card h-100 blogger_green" : index % 4 == 1 ? "card h-100 blogger_brown" : index % 4 == 2 ? "card h-100 blogger_blue" : "card h-100 blogger_red"}>
                                        <div class="row m-0">
                                            <div class="col-sm-12 p-0 text-left"><span className="price-info mt-2 mb-2">${favorite.rate}/<span className="perhr_span">Hr</span></span></div>
                                            <div className="products_image">
                                                <Link to={`/contentWriter/${favorite.user}`}>
                                                    <div class="blogger_img">
                                                        <img
                                                            src={favorite.profilepic ? favorite.profilepic : avatar}
                                                            onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                                                            class="rounded-circle img-fluid"

                                                            alt="" />
                                                        <span class="user-status on"></span>
                                                    </div>
                                                </Link>
                                            </div>
                                            <h5 className="product_title" title={favorite.username}>{favorite.username ? favorite.username : "User"}</h5>
                                            <div className="new_seller">
                                                {favorite.rating === 0 &&
                                                    <span className="new_seller_btn">New Content Writer</span>
                                                }
                                            </div>
                                            <div className="rating_star d-flex mt-1">
                                                {favorite.rating !== 0 && Array.from(Array(Math.round(favorite.rating)), () => {
                                                    return (
                                                        <span className="fa fa-star"></span>
                                                    )
                                                })}
                                            </div>
                                            <div class="sellers_list">
                                                {favorite && favorite.lastlogin_at != "" && 
                                                    <span class="on-date small timeFont-Right">Last Login: {this.chatTimeFormat(favorite.lastlogin_at)}</span>
                                                }   
                                            </div>   
                                            <div class="sellers_list">
                                                {favorite && favorite.lastlogin_at == "" && 
                                                    <span class="on-date small timeFont-Right">Last Login: Hasn’t logged in</span>
                                                }   
                                            </div>   
                                            <div className="btn-groups d-flex justify-content-center mt-3 mb-3 w-100">
                                                <a class="favorite_btn mr-2" onClick={() => { this.removeFavorite(favorite.favid) }}><i class="fa fa-heart"></i> Favorite</a>
                                                <Link class="view_btn" to={`/contentWriter/${favorite.user}`}>
                                                    <span class="fa fa-eye"></span> View
                                                </Link>
                                            </div>
                                            <div class="col-sm-12 col-4 p-0 text-center">
                                            <p className="blogger_btn px-2">{favorite.expertise && favorite.expertise.length > 0 && favorite.expertise.map((item, index) => {
                                                return <span key={`demo_snap_${index}`}>{ (index ? ', ' : '') + item.name }</span>;
                                            })}</p>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                            )
                        }
                    })}
                </div>
            );
        }
    }

    /**
     * main render
     * @returns 
     */
    render() {
        const { searchText, pageIndex, totalRecord, isLoading } = this.state;
        return (
            <div class="page-wrapper">
                <Loading isVisible={isLoading} />
                <div className="page_title row m-0 mb-4 pb-2">
                    <div class="col-lg-1 col-md-2 col-3 p-0">
                        <span className="left_icon_title"><span class="fa fa-heart-o fav_icon"></span></span>
                    </div>
                    <div class="col-lg-11 col-md-10 col-9 p-0">
                        <h2 className="right_title row m-0">Favorites</h2>
                        <span className="title_tagline m-0 row">All of your favorite content writers in one place.</span>
                    </div>
                </div>
                <div class="form-group search-box search_boxes mb-3">
                    <input type="text" class="form-control mb-3" placeholder="Search for Favorites..." name="searchText" value={searchText} onChange={this.onChangeSearchText} />
                    <span class="fa fa-search bg_icon_green"></span>
                </div>
                <div class="grey_box_bg p-3">
                    <div class="content-wrapper">
                        <div class="row m-0">
                            <div class="col-12 p-0">
                                <div class="products-sec product-grid mt-0">
                                    {this.renderFavoriteWebsite()}
                                </div>
                            </div>
                        </div>
                    </div>
                    {totalRecord !== 0 &&
                        <div className="favorite-pagination">
                            <Pagination
                                count={totalRecord} 
                                onChange={(event, val) => {
                                    if (pageIndex !== val) {
                                        this.onPageChange(val);
                                    }
                                }}
                                page={pageIndex}
                                variant="outlined"
                                color="primary"
                            />
                        </div>
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ favoriteReducer }) => {
    return {
        favoriteWebsiteList: favoriteReducer.favoriteWebsiteList
    }
}


export default connect(mapStateToProps, {
    getFavoriteWebsite,
    searchFavorite,
    addFavorite,
    removeFavorite,
})(FavoriteWebsite);