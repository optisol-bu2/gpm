import React from "react";
import { connect } from "react-redux";
import helpandsupport from "../../assets/images/menu-icons/help-support-white.svg";
import helpandsupport_pg from "../../assets/images/other-pages/modern-woman-using-laptop.png";
import PhoneInput from "react-phone-input-2";
import { ToastAlert,PlaySound } from "../../utils/sweetalert2";
import Loading from "../../components/core/Loading";
import { getAbout } from "../../Actions/aboutAction";
import { helpSupport } from "../../Actions/helpAction";

const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
/**
 * HelpAndSupport Component
 */
class HelpAndSupport extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
            name: '',
            email: '',
            phone_number: '',
            phoneValid: true,
            subject: '',
            message: '',
            helpData: '',
            isLoading: null,
            disable: false
		};

	}

    componentDidMount() {
        this.getAbout('helpandsupport');
	}

    /**
     * gets about data
     */
     getAbout = (type) => {
        this.setState({ isLoading: true, helpData: '' });
        this.props.getAbout(type,
            (about) => {
                if (about.data) {
                    this.setState({ helpData: about.data, isLoading: false });
                }
            },
            (error) => {
                this.setState({ isLoading: false, helpData: '' });
            }
        ); 
    }

    onFieldChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    /**
     * sends help & support email
     */
     helpSupport = () => {
        const {name, email, phone_number, phoneValid, subject, message } = this.state;
        if (!name || name == '') {
            this.setState({nameError: "Name is required!"})
            // ToastAlert("error", "Please enter name")
            PlaySound()
        } else if (!email || email == '') {
            this.setState({emailError: "Email is required!"})
            // ToastAlert("error", "Please enter email")
            PlaySound()
        } else if (email && !validEmailRegex.test(email)) {
            this.setState({emailError: "Please enter a valid email"})
            // ToastAlert('error', "Please enter a valid email");
            PlaySound()
        } else if (!phone_number || phone_number == '') {
            this.setState({phoneError: "Please enter phone number"})
            // ToastAlert("error", "Please enter phone number")
            PlaySound()
        } else if (phone_number && !phoneValid) {
            this.setState({phoneError: "Please enter valid phone number"})
            // ToastAlert("error", "Please enter valid phone number")
            PlaySound()
        } else if (!subject || subject == '') {
            this.setState({subjectError: "Subject should not be blank."})
            // ToastAlert("error", "Subject should not be blank.")
            PlaySound()
        } else if (!message || message == '') {
            this.setState({messageError: "Message should not be blank."})
            // ToastAlert("error", "Message should not be blank.")
            PlaySound()
        } else {
            var params = {
                name: name,
                email: email,
                subject: subject,
                message: message,
                phone_number: phone_number,
            }
            this.setState({ isLoading: true, disable: true, nameError:'', emailError:'', phoneError:'', subjectError:'', messageError:''});
            this.props.helpSupport(params,
                (help) => {
                    this.setState({ name: '', email: '', subject: '', message: '', phone_number:'+1',  isLoading: false, disable: false });
                    ToastAlert("success", help.message)
                    PlaySound()
                },
                (error) => {
                    this.setState({ isLoading: false, disable: false });
                    ToastAlert("error", error.message)
                    PlaySound()
                }
            ); 
        }
    }


    /**
     * main render
     * @returns 
     */
	render() {
		return (
            <div class="page-wrapper">
                <Loading isVisible={this.state.isLoading} />
                <div className="page_title row m-0 mb-4 pb-2">
                    <div class="col-lg-1 col-md-2 col-3 p-0">
                        <span className="left_icon_title">
                            {/* <img src={helpandsupport} alt="" /> */}
                            <i class="fa fa-headset"></i>
                        </span>
                    </div>
                    <div class="col-lg-11 col-md-10 col-9 p-0 d-flex align-items-center">
                        <h2 className="right_title row m-0">Help &amp; Support</h2>
                    </div>
                </div>
                <div class="grey_box_bg">
                    <div class="row m-0">
                        <div class="col-md-12 col-lg-6 col-xl-6 pl-5 pr-5 pt-4 pb-0 pad_sets_zero order-2 order-sm-1 order-md-1 order-lg-1">                            
                            <h4 class="sub_sub_title mb-3 mt-3">Marketplace for Help &amp; Support</h4>
                            <p class="text-justify" dangerouslySetInnerHTML={{ __html: this.state.helpData}}></p>
                            <div className="form_fields">
                                <div className="form-group">
                                    <input type="text" name="name" value={this.state.name} onChange={this.onFieldChange} className="input_field" placeHolder="Your Name" />
                                    <div class="error-text">{this.state.nameError}</div> 
                                </div>
                                <div className="form-group">
                                    <input type="text" name="email" value={this.state.email} onChange={this.onFieldChange}className="input_field" placeHolder="Email Address" />
                                    <div class="error-text">{this.state.emailError}</div> 
                                </div>
                                <div className="form-group phone_field phone_field_help" title="Phone Number">
                                    <PhoneInput
                                        country={'us'}
                                        countryCodeEditable={false}
                                        value={this.state.phone_number}
                                        onChange={(value, country) => {
                                            if (value.length == (country.format.match(/\./g) || []).length) {
                                                this.setState({ phone_number: value, phoneValid: true })
                                            } else if (value.length < (country.format.match(/\./g) || []).length) {
                                                this.setState({ phone_number: value, phoneValid: false })
                                            } else {
                                                this.setState({ phoneValid: false })
                                            }
                                        }}
                                    />
                                    <div class="error-text">{this.state.phoneError}</div> 
                                </div>
                                <div className="form-group">
                                    <input type="text" name="subject" value={this.state.subject} onChange={this.onFieldChange} className="input_field" placeHolder="Subject" />
                                    <div class="error-text">{this.state.subjectError}</div> 
                                </div>
                                <div className="form-group">
                                    <textarea name="message" value={this.state.message} onChange={this.onFieldChange} className="input_field" placeHolder="Message"></textarea>
                                    <div class="error-text">{this.state.messageError}</div> 
                                </div>
                                <div className="form-group text-right">
                                    <button type="submit" disabled={this.state.disable} className="btn_light_green effect effect-2" onClick={() => this.helpSupport()}>Submit</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-6 p-0 order-1 order-sm-2 order-md-2 order-lg-2">
                            <img src={helpandsupport_pg} alt="" className="img-fluid bor_rad_set" />
                        </div>
                    </div>
                </div>
			</div>
		);
	}
}

const mapStateToProps = ({ }) => {
	return {}
}


export default connect(mapStateToProps, {
    getAbout,
    helpSupport,
})(HelpAndSupport);
