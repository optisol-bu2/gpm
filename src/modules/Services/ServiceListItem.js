import React from "react";
import defaultImage from "../../assets/images/placeholder.jpeg";
import avatar from "../../assets/images/avatar.png";


const ServiceListItem = (props) => {
  const { data } = props;
  return (
      <div className="service_orange_gird">
        <div className="card-header  d-flex flex-wrap justify-content-between align-content-center">
          <h3 className="header_title_srvice d-flex align-content-center">
            {data.title}
            <a
              data-toggle="modal"
              data-target="#addservice"
              className="text-white ml-2 link-cursor"
              onClick={!data.iscustomorder ? props.onEditClick : props.onCustomEditClick}
            >
              {!data.iscustomorder ? (
                <i className="fa fa-edit bg_icon_set" title="Edit"></i>
                ) : (
                <i className="fa fa-clone bg_icon_set" title="Duplicate"></i>
                )
              }
            </a>
            <a
              data-toggle="modal"
              data-target="#addservice"
              className="text-white ml-2 link-cursor"
              onClick={() => !data.iscustomorder ? props.onDeleteClick(data.id) : props.onCustomDeleteClick(data.id)}
            >
              <i className="far fa-trash-alt bg_icon_set" title="Delete"></i>
            </a>
            {/* {data.status ? (
              <a className="text-white ml-2 mt-1 link-cursor" onClick={() => props.onStatusClick(data.id, 0, data.iscustomorder)}><i class="fa fa-toggle-on" title="Disable"></i></a>
            ) : (
              <a className="text-white ml-2 mt-1 link-cursor" onClick={() => props.onStatusClick(data.id, 1, data.iscustomorder)}><i class="fa fa-toggle-off" title="Enable"></i></a>
            )} */}
          </h3>
          {!data.iscustomorder && 
            <a class="link_service" href={data.websitelink.includes("www.") ? "//"+data.websitelink: data.websitelink} rel="noreferrer noopener" target="_blank">{data.websitelink}</a>
          }
        </div>
        <div className="card-body">
          <div className="row">
            <div className="col-lg-2 col-md-6 d-flex align-items-center">
              <div className="card border_radius">
                <div className="position-relative">
                  <img                
                    src={data.servicepic}
                    onError={(e)=>{e.target.onerror = null; e.target.src=defaultImage}}
                    className="img_crop img-fluid"
                    alt="new"
                  />
                </div>
              </div>
            </div>
            {!data.iscustomorder ? (
              <div className="col-lg-8 col-md-6 col-sm-12 service_listtms">
                <div className="row m-0 d-flex align-items-center">
                  <div className="col-lg-7 col-md-12 col-sm-12 pl-0 text-left">
                      <h3 className="orange_txt">                      
                          {props.serviceCategories && props.serviceCategories.length != 0 &&
                          props.serviceCategories.map((category) => {
                            if (category.value == data.category){
                              return category.label
                            }
                          })}
                        
                      </h3>
                      <div className="border_set_round">
                        <p className="tag_line_span">Estimated Delivery</p>
                        <h3 className="rate_dollar_price">
                          {props.serviceEstimatedDelivery && props.serviceEstimatedDelivery.length != 0 &&
                            props.serviceEstimatedDelivery.map((estimatedDelivery) => {
                              if (estimatedDelivery.value == data.estimatedelivery)
                              return estimatedDelivery.label + (estimatedDelivery.label == 1 ? " Day" : " Days")
                            })}
                        </h3>
                        
                      </div>
                  </div>
                  <div className="col-lg-5 col-md-12 col-sm-12 pl-0 text-center">
                    <div className="row m-0 d-flex align-items-center">
                      <div className="border_set_round">
                        <p className="tag_line_span">Per Guest Post</p>
                        <h3 className="rate_dollar_price">${data.price}</h3>                        
                      </div>

                      <div className="border_set_round">
                        <p className="tag_line_span">Per Backlink</p>
                        <h3 className="rate_dollar_price">
                          ${data.backlinkprice}
                        </h3>                        
                      </div>                      
                    </div>
                  </div>                  
                </div>
              </div>
            ) : (
              <div className="col-lg-8 col-md-6 col-sm-12 service_listtms">
                <div className="row m-0 d-flex align-items-center h-100">                  
                  <div className="col-lg-7 col-md-12 pl-0 text-left mb-2">
                    <div class="user-profile d-flex align-items-center">
                      <div class="mr-3">
                        <img
                          src={data.user.image ? data.user.image : avatar}
                          onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                          alt="..."
                          width="60"
                          height="60"
                          class="rounded-circle"
                          data-pagespeed-url-hash="3993073969"
                          onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                        />     
                      </div>
                      <div class="">
                        <a class="orange_txt">
                          {data.user.name}
                        </a>
                        {/* <p className="tagleftspan">User</p> */}
                      </div>
                    </div>                   
                  </div>
                  <div className="col-lg-5 col-md-12 pl-0 text-center">
                      <div className="row m-0 d-flex align-items-center">
                        <div className="border_set_round">
                          <p className="tag_line_span">Price</p>
                          <h3 className="rate_dollar_price">${data.price}</h3>                          
                        </div>
                        <div className="border_set_round">
                          <p className="tag_line_span">Estimated Delivery</p>
                          <h3 className="rate_dollar_price">
                            {props.serviceEstimatedDelivery && props.serviceEstimatedDelivery.length != 0 &&
                              props.serviceEstimatedDelivery.map((estimatedDelivery) => {
                                if (estimatedDelivery.value == data.estimatedelivery)
                                return estimatedDelivery.label + (estimatedDelivery.label == 1 ? " Day" : " Days")
                              })}
                          </h3>                          
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            )}
            <div className="col-lg-2 col-md-4 col-sm-12 view-options text-center">
              <a
                href="#!"
                className="btn_light_green"
                onClick={(e) => {
                  e.currentTarget.blur();
                  props.onViewClick();
                }}
              >
                {data.isViewMore ? "View Less" : "View More"}
              </a>
            </div>
            {(data.isViewMore && !data.iscustomorder) ? (
              <div className="more-details">
                <label className="label_title">
                  Service Description
                </label>
                <p className="para_content_sd">{data.description} </p>

                <label className="label_title">
                  Service Guideline and Restrictions
                </label>
                <p className="para_content_sd">{data.guideline} </p>

                <label className="label_title">Alert</label>
                <p className="para_content_sd">{data.alert}</p>
              </div>
            ) : (data.isViewMore && data.iscustomorder) ? (
              <div className="more-details">
                <label className="label_title">
                  Description
                </label>
                <p className="para_content_sd">{data.description} </p>
              </div>
            ) : (
              <div />
            )}
          </div>
        </div>
      </div>
  );
};

export default ServiceListItem;
