import React, { useState, useEffect } from "react";
import Modal from "@material-ui/core/Modal";
import Loading from "../../components/core/Loading";

const ContentWriterThankYou = (props) => {
    const [isLoading, setLoading] = useState(null);

    useEffect(() => {
        if (props.isVisible) {
        }
    }, [props.isVisible]);

    useEffect(() => {
    }, [props.isVisible]);

    return (
        <div>
            <Loading isVisible={isLoading} />
            {
                <Modal
                    open={props.isVisible}
                    disableBackdropClick
                    onClose={props.onClose}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description">
                    <div className="modal-dialog modal_popup" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="popup_title" id="exampleModalLabel">Blog Submitted</h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                    onClick={props.onClose}
                                >
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <br />
                            <div className="modal-body justify-content-center pt-3 pb-3 d-flex flex-column align-items-center text-center">
                                {/* <i className="fa fa-times-circle-o fa-2x" aria-hidden="true"></i> */}
                                <h4 className="sub_title_dash mt-3 mb-3 col-lg-10 mx-auto">Thank you for submitting to EngineScale. We will notify you of your status to become a content writer on our platform within 24 hours.</h4>
                            </div>
                            <br />
                            {/* <div className="modal-footer justify-content-center"> */}
                                {/* <Link
                           // to={"/profile/billing"}
                           onClick={() => { this.setState({ isModalOpen: false }); this.onContentWriterCreateCheck(); }}
                           className="btn_light_green"
                       >
                           Continue
                       </Link> */}
                            {/* </div> */}
                        </div>
                    </div>
                </Modal>

            }
        </div>
    );
};
export default ContentWriterThankYou;
