import React, { useState, useEffect } from "react";
import { Button, Form, InputGroup } from "react-bootstrap";
import Modal from "@material-ui/core/Modal";
import { ToastAlert,PlaySound } from "../../utils/sweetalert2";
import Loading from "../../components/core/Loading";
import { IconButton, Tooltip } from "@material-ui/core";
import { Close } from "@material-ui/icons";
import AsyncSelect from 'react-select/async';
import Select from 'react-select';
import upload_icon_image from "../../assets/images/other-pages/upload_icon_image.svg";
import ImageCropperComponent from "../../components/core/ImageCropperComponent";

const AddNewCustom = (props) => {
  const [validated, setValidated] = useState(false);
  const [imgUrl, setImgUrl] = useState("");
  const [imgUrlFile, setImgUrlFile] = useState("");
  const [price, setPrice] = useState("");
  const [user, setUser] = useState("");
  const [userOptions, setUserOptions] = useState([]);
  const [estimatedDelivery, setEstimatedDelivery] = useState(null);
  const [description, setDescription] = useState("");
  const [descriptionCount, setDescriptionCount] = useState(0);
  const [isLoading, setLoading] = useState(null);
  const [imgUrlToDisplay, setDisplayImgUrl] = useState("");
  const [buttonLabel, setButtonLabel] = useState("Create Custom Order");
  const [openImageCrop, setOpenImageCrop] = useState(false);
  const [imgUrlToCrop, setCropImgUrl] = useState("");
  const [disable, setDisabled ] = useState(false);

  const priceRegex = RegExp(/^([1-9]\d{0,4}\.{0,1}\d{0,2})$/i);

  useEffect(() => {
    if(props.isVisible) {
      loadUserOptions("");
    }
    setValidated(false);
    cancel();
  }, [props.isVisible]);

  useEffect(() => {
    if (props.isEdit) {
      setButtonLabel("Duplicate Custom Order");
      setUser("");
      setPrice(props.editData.price);
      props.serviceEstimatedDelivery.forEach(estimatedDelivery => {
        if (estimatedDelivery.value == props.editData.estimatedelivery) {
          setEstimatedDelivery(estimatedDelivery);
        }
      })
      setDescription(props.editData.description);
      setDisplayImgUrl(props.editData.servicepic);
      setImgUrl("");
      setImgUrlFile(props.editData.servicepic);
      setDescriptionCount(props.editData.description.length);
    }
  }, [props.isEdit]);

  /**
   * clears modal values on close
   */
  const cancel = () => {
    setUser("");
    setUserOptions([]);
    setPrice("");
    setEstimatedDelivery(null);
    setDescription("");
    setDisplayImgUrl("");
    setImgUrl("");
    setImgUrlFile("");
    setDescriptionCount(0);
  }

  /**
   * loads user options
   */
  const loadUserOptions = (searchText, results) => {
      props.searchServiceUsers(
        searchText,
        (user) => {
          if (user.data.length !== 0) {
            var userArray = [];
            user.data.forEach(userData => {
              let userObj = {
                label: userData.username,
                value: userData.id
              }
              userArray.push(userObj);
            })
            setUserOptions(userArray);
            results(userArray);
          } else {
            setUserOptions([]);
            results([]);
          }
        },
        (error) => {
          ToastAlert('error', error.message);
          PlaySound()
          setUserOptions([]);
          results([]);
        }
      );
  };

  /**
   * sets selected user in state
   * @param {*} item 
   */
  const handleOnSelect = (item, type) => {
    if (item != null) {
      setUser(item.value);
    } else {
      setUser("");
    }
  }

  /**
   * handles file change
   * @param {*} e 
   */
  const onFileChange = (e) => {
    try {
      if (e.target.files[0].size <= 1048576) {
        setImgUrlFile("");
        setDisplayImgUrl("");
        // setImgUrl(e.target.files[0]);
        // setDisplayImgUrl(URL.createObjectURL(e.target.files[0]));
        setCropImgUrl(URL.createObjectURL(e.target.files[0]));
        setOpenImageCrop(true);
      } else {
        ToastAlert('info', "Max size of 1MB allowed")
        PlaySound()
      }
    } catch (error) { }
  };

  /**
   * handles submit form
   * @param {*} e 
   */
  const handlesubmit = (e) => {
    const form = e.currentTarget;
    //  setValidated(true);
    if (price && price<15) {
      return (ToastAlert('error', 'Price allow minimum 15$'),PlaySound())
    }
    if (price == "" || user == "" || !priceRegex.test(price) || estimatedDelivery === null || description === "" || (imgUrl === "" && imgUrlFile === "")) {
      setValidated(true);
      e.preventDefault();
      e.stopPropagation();
    } else {
      let formData = new FormData();
      formData.set("user", user);
      formData.set("price", price);
      formData.set("estimatedelivery", estimatedDelivery.value);
      formData.set("description", description);
      if (imgUrl !== "") {
        formData.append("image", imgUrl);
      } else {
        formData.append("imageurl", imgUrlFile);
      }
      setLoading(true);
      setDisabled(true);
      props.addCustomOrder(
        formData,
        () => {
          setLoading(false);
          setDisabled(false);
          props.onSuccess();
        },
        (error) => {
          setLoading(false);
          setDisabled(false);
          ToastAlert('error', error.message);
          PlaySound()
        }
      );
    }
  };

  return (
    <div>
      <Loading isVisible={isLoading} />
      <Modal
        open={props.isVisible}
        disableBackdropClick
        style={{ overflow: "scroll" }}
        className="modal_popup"
        onClose={props.onClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div class="modal-dialog modal-xl" role="document">
          <div class="modal-content add_new_service">
            <div class="modal-header">
              <h5 class="popup_title" id="exampleModalLabel" >
                {props.isEdit ? "Duplicate Custom Order" : "Add Custom Order"}
              </h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={props.onClose}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>            
            <Form id="myform" noValidate validated={validated} onSubmit={handlesubmit}>
              <div class="modal-body form_fields">
                <Form.Row>
                  <div class="form-group col-md-6 select-user">
                    <Form.Group controlId="exampleForm.ControlSelect1">
                      <Form.Label>User<span class="required-red">*</span></Form.Label>
                      <AsyncSelect
                        styles={{ menuPortal: base => ({ ...base, zIndex: 9999}) }}
                        menuPortalTarget={document.querySelector('select-user')}
                        placeholder="Search User"
                        isClearable
                        onChange={handleOnSelect}
                        className="basic-single-select"
                        classNamePrefix={"select_dropdown"}
                        cacheOptions
                        loadOptions={loadUserOptions}
                        defaultOptions={userOptions}
                      />
                      <div className="invalid-user" >
                        {validated && user == "" && "Please select user."}
                      </div>
                    </Form.Group>
                  </div>

                  <div class="form-group col-md-6">
                    <Form.Label>Price <span class="required-red">*</span> </Form.Label>
                    <InputGroup hasValidation>
                      <Form.Control
                        type="number"
                        min="1"
                        step=".01"
                        value={price}
                        placeholder="Price *"
                        onChange={(e) => setPrice(e.target.value)}
                        required
                      />
                      <InputGroup.Prepend>
                        <InputGroup.Text>$</InputGroup.Text>
                      </InputGroup.Prepend>
                      <Form.Control.Feedback type="invalid">
                        {price == "" && "Please enter price." } 
                        {price != "" && !priceRegex.test(price) && "Please enter valid price."}
                      </Form.Control.Feedback>
                    </InputGroup>
                  </div>

                  <div class="form-group col-md-6">
                    <Form.Group controlId="exampleForm.ControlSelect1">
                      <Form.Label>Estimate Delivery <span class="required-red">*</span></Form.Label>
                      <Select
                        className="basic-single-select"
                        classNamePrefix={"select_dropdown"}
                        placeholder="Select Estimate Delivery"
                        isClearable
                        isSearchable={false}
                        onChange={(selected) => {
                          setEstimatedDelivery(selected);
                        }}
                        value={estimatedDelivery}
                        options={props.serviceEstimatedDelivery}
                      />
                      <div className="invalid-user">
                        {validated && estimatedDelivery == null && "Please select estimate delivery."}
                      </div>
                    </Form.Group>
                  </div>                  
                  <div class="form-group col-md-6">
                    <Form.Group controlId="exampleForm.ControlTextarea1">
                      <Form.Label>Description <span class="required-red">*</span></Form.Label>
                      <Form.Control
                        as="textarea"
                        value={description}
                        placeholder="Description"
                        rows={3}
                        required
                        maxlength="500"
                        onChange={(e) => {
                          setDescription(e.target.value);
                          setDescriptionCount(e.target.value.length);
                        }}
                      />
                      <Form.Control.Feedback type="invalid">
                        Please provide a description.
                      </Form.Control.Feedback>
                      <p className="text-muted mb-0">{descriptionCount}/500</p>
                    </Form.Group>
                    
                  </div>
                  <div className="col-md-12 d-flex justify-content-center">
                    <div className="fileupload_bg">
                      <span className="file-upload-section">
                        <img src={upload_icon_image} alt="" width="50px" />
                        <span className="upload_text">
                          Upload Service Image <span class="required-red">*</span>
                        </span>
                      </span>
                      <ImageCropperComponent 
                        isOpen={openImageCrop} 
                        onClose={() => {setOpenImageCrop(false)}} 
                        aspectRatio={600 / 500} 
                        width={400} 
                        height={400} 
                        src={imgUrlToCrop} 
                        setCroppedImage={(image) => {
                          setImgUrl(image.blobFile);
                          setDisplayImgUrl(image.croppedImageUrl);
                        }}
                      />
                      {imgUrlToDisplay == "" ? (
                      <span>                          
                        <label for="file-upload-custom" className="btn_light_green ml-3">
                          <span className="fa fa-plus"></span>&nbsp;Add Image
                        </label>
                          <input
                            id="file-upload-custom"
                            type="file"
                            onChange={onFileChange}
                            accept="image/*"
                            hidden="true"
                          />
                          <br/>
                      </span>
                      ) : (        
                      <div className="preview_uploaded">            
                        <label for="file-upload-custom" className="file-upload-section"><img width="100px" src={imgUrlToDisplay} />
                        </label>                        
                        <span className="close_icon_btn">
                          <Tooltip title="Cancel">
                            <IconButton
                              aria-label="cancel"
                              onClick={() => {
                                setDisplayImgUrl("");
                                setImgUrl("");
                                setImgUrlFile("")
                              }}
                            >
                              <Close fontSize="small" />
                            </IconButton>
                          </Tooltip>
                        </span>
                      </div>
                      )}
                      {validated && imgUrl === "" && imgUrlFile === "" && (
                        <span id="email-error" className="text-danger">Please upload custom order image.</span>
                      )}
                    </div>
                  </div>
                </Form.Row>
              </div>
              <div class="modal-footer border-0 text-center justify-content-center mb-3">  
                <Button
                  className="btn_light_green custom-width effect effect-2"
                  disabled={disable}
                  onClick={handlesubmit}
                >
                  {buttonLabel}
                </Button>
              </div>
            </Form>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default AddNewCustom;
