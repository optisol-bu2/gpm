import React, { useState, useEffect } from "react";
import Modal from "@material-ui/core/Modal";
import Loading from "../../components/core/Loading";
import { Link } from "react-router-dom";

const ContentWriterCongrats = (props) => {
  const [isLoading, setLoading] = useState(null);

  useEffect(() => {
    if (props.isVisible) {
    }
  }, [props.isVisible]);

  useEffect(() => {
    console.log("contentwritertest");
  }, [props.isVisible]);

  return (
    <div>
      <Loading isVisible={isLoading} />
      {
        <Modal
          open={props.isVisible}
          disableBackdropClick
          onClose={props.onClose}
          aria-labelledby="simple-modal-title"
          aria-describedby="simple-modal-description">
          <div className="modal-dialog modal_popup" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="popup_title" id="exampleModalLabel"></h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                  onClick={props.onClose}
                >
                  <span aria-hidden="true">X</span>
                </button>
              </div>
              <br />
              <div className="modal-body justify-content-center pt-3 pb-3 d-flex flex-column align-items-center text-center">
                {/* <i className="fa fa-times-circle-o fa-2x" aria-hidden="true"></i> */}
                <h4 className="sub_title_dash mt-3 mb-3 col-lg-10 mx-auto">Congratulations! You have been approved as a content writer on our platform.</h4>
              </div>
              <br />
              <div className="modal-footer justify-content-center">
                <Link
                  // to={"/profile/billing"}
                  onClick={() => { props.onSuccess(); }}
                  className="btn_light_green"
                >
                  Continue
                </Link>
              </div>
            </div>
          </div>
        </Modal>

      }
    </div>
  );
};
export default ContentWriterCongrats;
