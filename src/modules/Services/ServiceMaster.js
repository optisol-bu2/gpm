import React, { useState, useEffect } from "react";
import ServiceList from "./ServiceList";
import AddNewService from "./AddNewService";
import AddNewCustom from "./AddNewCustom";
import Loading from "../../components/core/Loading";
import Modal from "@material-ui/core/Modal";
import { Link } from 'react-router-dom';
import service_icon from "../../assets/images/menu-icons/service_menu_icon.svg";

const ServiceMaster = (props) => {
  const [isAddService, setAddService] = useState(false);
  const [isAddCustom, setAddCustom] = useState(false);
  const [isLoading, setLoading] = useState(null);
  const [serviceList, setServiceList] = useState([]);
  const [isDeleteEvent, setDeleteEvent] = useState(false);
  const [isModalOpen, setModalOpen] = useState(false);

  const getServiceList = (load) => {
    setLoading(true);
    props.getServiceList(
      (response) => {
        setLoading(false);
        if (response.data != null) {
          const responseData = response.data.map((record, index) => {
            return {
              ...record,
              key: index + 1,
              isViewMore: false,
            };
          });
          setServiceList(responseData);
        } else {
          setLoading(false);
        }
        if (load == undefined) {
          window.scrollTo({ top: 0, behavior: "smooth" });
        }
      },
      (error) => {
        setServiceList([])
        setLoading(false);
      }
    );
  };

  useEffect(() => {
    getServiceList();
  }, [isDeleteEvent]);

  /**
   * opens order create modal
   * @param {*} type 
   */
  const onCreate = (type) => {
    setLoading(true);
    props.checkBillingInfo(
      (response) => {
        setLoading(false);
        if (type == "service") {
          setAddCustom(false);
          setAddService(true);
        } else if (type == "custom") {
          setAddService(false);
          setAddCustom(true);
        }
      },
      (error) => {
        setLoading(false);
        setModalOpen(true);
      }
    );
  };

  return (
    <div class="page-wrapper" data-tour="service">
      <Loading isVisible={isLoading} />
      <div className="page_title row m-0 mb-4 pb-2">
          <div class="col-lg-1 col-md-2 col-3 p-0">
              <span className="left_icon_title"><img src={service_icon} alt="" /></span>
          </div>
          <div class="col-lg-11 col-md-10 col-9 p-0 d-flex justify-content-between servicedrop">
              <h2 className="right_title row m-0 d-flex flex-column justify-content-center">Service <span className="title_tagline m-0 row">Create and edit your services and custom orders in one place.</span></h2>
               
              <div class="about_pg_menu float-right">
                <div class="dropdown m-0 d-flex align-items-center d-flex">
                    <button
                      className="btn_menu_item dropdown-toggle active pl-3 pr-3"
                      type="button"
                      id="servicemenu"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                      onClick={(e) => {
                        e.currentTarget.blur();
                      }}
                    >Create&nbsp;&nbsp;<span className="fa fa-chevron-down"></span>
                    </button>
                    <div class="dropdown-menu" aria-labelledby="servicemenu">
                      <a className="dropdown-item" 
                          onClick={() => {
                            onCreate("service");
                          }}
                        >
                          Service
                        </a>
                        <a className="dropdown-item" 
                          onClick={() => {
                            onCreate("custom");
                          }}
                        >
                          Custom Order
                        </a>
                    </div>
                </div>
              </div> 
          </div>          
      </div>      
      <div class="servicelist_items">      
        <ServiceList
          data={serviceList}
          isUpdateEvent={(load) => getServiceList(load)}
          isDeleteEvent={() => setDeleteEvent(true)}
          {...props}
        />
      </div> 
      <AddNewService
        isVisible={isAddService}
        onClose={() => {
          setAddService(false);
        }}
        onSuccess={() => {
          setAddService(false);
          getServiceList();
        }}
        onFailure={() => {
          setAddService(false);
        }}
        {...props}
      />
      <AddNewCustom
        isVisible={isAddCustom}
        onClose={() => {
          setAddCustom(false);
        }}
        onSuccess={() => {
          setAddCustom(false);
          props.history.push("/inbox")
        }}
        onFailure={() => {
          setAddCustom(false);
        }}
        {...props}
      />
      <Modal
        open={isModalOpen}
        disableBackdropClick
        onClose={() => { setModalOpen(false); }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description">
        <div className="modal-dialog modal_popup" role="document">
            <div className="modal-content">
                <div className="modal-header">
                    <h5 className="popup_title" id="exampleModalLabel">Action not allowed</h5>
                    <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                        onClick={() => { setModalOpen(false); }}
                    >
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <br/>
                <div className="modal-body justify-content-center pt-3 pb-3 d-flex flex-column align-items-center text-center">
                    <i className="fa fa-times-circle-o fa-2x" aria-hidden="true"></i>                    
                    <h4 className="sub_title_dash mt-3 mb-3">Please fill out billing information first</h4>
                </div>
                <br/>
                <div className="modal-footer justify-content-center">
                    <Link 
                      to={"/profile/billing"}
                      className="btn_light_green"
                      onClick={() => { setModalOpen(false); }}>Go to Billing Information</Link>
                </div>
            </div>
        </div>
      </Modal>
    </div>
  );
};

export default ServiceMaster;
