import React, { useState, useEffect } from "react";
import { Button, Form, InputGroup } from "react-bootstrap";
import { ToastAlert, PlaySound } from "../../utils/sweetalert2";
import Modal from "@material-ui/core/Modal";
import Loading from "../../components/core/Loading";

const ContentWriterTest = (props) => {
  const [validated, setValidated] = useState(false);
  const [description, setDescription] = useState("");
  const [descriptionCount, setDescriptionCount] = useState(0);
  const [isLoading, setLoading] = useState(null);
  const [buttonLabel, setButtonLabel] = useState("Submit");
  const [attachment, setUrlFile] = useState("");
  const [viewurl, setViewUrl] = useState([]);
  const [minLength, setMinLength] = useState(0);
  const [disable, setDisabled ] = useState(false);

  useEffect(() => {
    if (props.isVisible) {
    }
    setValidated(false);
  }, [props.isVisible]);

  useEffect(() => {
    console.log("contentwritertest");
  }, [props.isVisible]);

  useEffect(() => {
    getQustion();
  }, [props.getViewQuestions]);

  const getQustion = () => {
    props.getViewQuestions(
      (question) => {
        if (question.data) {
          setMinLength(question.data.minlength);
          setQuestion(question.data.question);
          setQuestionId(question.data.id);
        }
      },
      (error) => {
        setQuestion("");
      }
    );
  }

  /**
   * handles file change
   * @param {*} e 
   */
  const onFileChange = (e) => {
    try {
      // if (e.target.files[0].size <= 1048576) {
        setUrlFile(URL.createObjectURL(e.target.files[0]));
        setViewUrl([...e.target.files]);
      // } else {
      //   ToastAlert('info', "Max size of 1MB allowed")
      //   PlaySound()
      // }
    } catch (error) { }
  };

  /**
   * renders file preview
   * @returns
   */
  const renderFilePreview = () => {
    return (
      <div className="ml-3">
        {viewurl.length > 0 &&
          <div className="list-group1 ml-3">
            {viewurl.map((item, index) => {
              return (
                <div className="list-group-item " key={item}>
                  <span>{item.name}</span> &nbsp;
                  <button type="button" className="close_icon" onClick={() => { deleteFile(index) }}>x</button>
                </div>
              );
            }
            )}
          </div>
        }
      </div>
    )
  }

  /**
   * removes file from the postFile state
   * @param {*} e
   */
  const deleteFile = (e) => {
    const files = viewurl.filter((item, index) => index !== e);
    setViewUrl(files);
  };

  /**
   * handles submit form
   * @param {*} e 
   */
  const handlesubmit = (e) => {
    const form = e.currentTarget;
    if (description == "" && viewurl == "") {
      setValidated(true);
      e.preventDefault();
      e.stopPropagation();
    } else {
      let formData = new FormData();
      formData.append("question", questionid);
      if (description == "") {
        formData.append("comment", "");
      } else {
        formData.append("comment", description);
      }
      if(description != "" && descriptionCount <= minLength && viewurl == "") {
        return ToastAlert("error", "Minimum of"+' '+minLength+' '+"words to submit.");
      }
      viewurl.forEach(file => {
        formData.append("attachment", file);
      })
      setLoading(true);
      setDisabled(true);
      props.addQuestions(
        formData,
        () => {
          setLoading(false);
          setDisabled(false);
          props.onSuccess();
          ToastAlert("success", "Your answer has been submitted.");
          PlaySound()
        },
        (error) => {
          setLoading(false);
          setDisabled(false);
          ToastAlert('error', error.message);
          PlaySound()
        }
      );
    }
  };

  const [question, setQuestion] = useState([]);
  const [questionid, setQuestionId] = useState([]);
  // const { getViewQuesitons } = props;

  return (
    <div>
      <Loading isVisible={isLoading} />
      {<Modal
        open={props.isVisible}
        disableBackdropClick
        style={{ overflow: "scroll" }}
        className="modal_popup"
        onClose={props.onClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div class="modal-dialog modal-lg" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="popup_title" id="exampleModalLabel" >Content Writer Exam
              </h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={props.onClose}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <Form id="myform" noValidate validated={validated} onSubmit={handlesubmit}>
              <div class="modal-body form_fields">
                <Form.Row>
                  <div class="form-group col-md-12 select-user">
                    <Form.Group controlId="exampleForm.ControlSelect1">
                      <Form.Label className="form-label">Question<span class="required-red">*</span></Form.Label>
                    </Form.Group>
                    {question}
                    {/* A requirement to sign up on EngineScale is to demonstrate your quality as a writer. Write a blog post of 2,500 words about education */}
                  </div>
                  <div class="form-group col-md-12">
                    <Form.Group controlId="exampleForm.ControlTextarea1">
                      <Form.Label className="form-label">Answer<span class="required-red">*</span></Form.Label>
                      <Form.Control
                        as="textarea"
                        value={description}
                        placeholder="Answer"
                        rows={3}
                        required
                        onChange={(e) => {
                          setDescription(e.target.value);
                          setDescriptionCount(e.target.value.length);
                        }}
                      />
                      <Form.Control.Feedback type="invalid">
                        Please provide your Answers.
                      </Form.Control.Feedback>
                      <p className="text-muted mb-0">{descriptionCount}/{minLength}</p>
                    </Form.Group>
                  </div>
                  <span>
                    <label for="file-upload-customs" className="btn_light_green ml-3">
                      <span className="fa fa-plus"></span>&nbsp;Add File
                    </label>
                    <input
                      id="file-upload-customs"
                      type="file"
                      onChange={onFileChange}
                      accept="application/pdf, application/vnd.ms-excel, application/msword, text/plain,
                      application/vnd.openxmlformats-officedocument.wordprocessingml.document"
                      hidden="true"
                    />
                    <br />
                  </span>
                  <div class="row file_updoaded_dets col-sm-12">
                    {renderFilePreview()}
                  </div>
                </Form.Row>
              </div>
              <div class="modal-footer border-0 text-center justify-content-center mb-3">
                <Button
                  className="btn_light_green effect effect-2"
                  disabled={disable}
                  onClick={handlesubmit}
                >
                  {buttonLabel}
                </Button>
              </div>
            </Form>
          </div>
        </div>
      </Modal>
      }
    </div>
  );
};

export default ContentWriterTest;
