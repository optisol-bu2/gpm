import React, { useState, useEffect } from "react";
import { Button, Form, InputGroup } from "react-bootstrap";
import Modal from "@material-ui/core/Modal";
import { ToastAlert,PlaySound } from "../../utils/sweetalert2";
import Loading from "../../components/core/Loading";
import { IconButton, Tooltip } from "@material-ui/core";
import { Close } from "@material-ui/icons";
import upload_icon_image from "../../assets/images/other-pages/upload_icon_image.svg";
import Select from 'react-select';
import ImageCropperComponent from "../../components/core/ImageCropperComponent";

const AddNewService = (props) => {
  const [validated, setValidated] = useState(false);
  const [imgUrl, setImgUrl] = useState("");
  const [serviceTitle, setServiceTitle] = useState("");
  const [category, setCategory] = useState(null);
  const [guestPostPrice, setGuestPostPrice] = useState("");
  const [backlinkPrice, setBackLinkPrice] = useState("");
  const [estimatedDelivery, setEstimatedDelivery] = useState(null);
  const [serviceWebLink, setServiceWebLink] = useState("");
  const [serviceDescription, setServiceDescription] = useState("");
  const [serviceGuideline, setServiceGuidelines] = useState("");
  const [alertt, setAlert] = useState("");
  const [descriptionCount, setDescriptionCount] = useState(0);
  const [guideCount, setGuideCount] = useState(0);
  const [alertCount, setAlertCount] = useState(0);
  const [isLoading, setLoading] = useState(null);
  const [imgUrlToDisplay, setDisplayImgUrl] = useState("");
  const [buttonLabel, setButtonLabel] = useState("Create Service");
  const [id, setId] = useState(0);
  const [openImageCrop, setOpenImageCrop] = useState(false);
  const [imgUrlToCrop, setCropImgUrl] = useState("");

  const priceRegex = RegExp(/^([1-9]\d{0,4}\.{0,1}\d{0,2})$/i);
  const urlRegex = RegExp(/^(http:\/\/|https:\/\/|www\.|ftp:\/\/|gopher:\/\/|mailto:|news:|telnet:\/\/|wais:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/gm);

  useEffect(() => {
      setValidated(false);
      cancel();
  }, [props.isVisible]);

  useEffect(() => {
    if (props.isEdit) {
      setButtonLabel("Update Service");
      setId(props.editData.id);
      setServiceTitle(props.editData.title);
      props.serviceCategories.forEach(categoryData => {
        if (categoryData.value == props.editData.category) {
          setCategory(categoryData);
        }
      })
      setGuestPostPrice(props.editData.price);
      setBackLinkPrice(props.editData.backlinkprice);
      props.serviceEstimatedDelivery.forEach(estimatedDelivery => {
        if (estimatedDelivery.value == props.editData.estimatedelivery) {
          setEstimatedDelivery(estimatedDelivery);
        }
      })
      setServiceWebLink(props.editData.websitelink);
      setServiceDescription(props.editData.description);
      setServiceGuidelines(props.editData.guideline);
      setDisplayImgUrl(props.editData.servicepic);
      setImgUrl(props.editData.servicepic);
      setAlert(props.editData.alert);
      setDescriptionCount(props.editData.description.length);
      setGuideCount(props.editData.guideline.length);
      setAlertCount(props.editData.alert.length);
    }
  }, [props.isEdit]);

  const cancel = () => {
    setServiceTitle("");
    setCategory(null);
    setGuestPostPrice("");
    setBackLinkPrice("");
    setEstimatedDelivery(null);
    setServiceWebLink("");
    setServiceDescription("");
    setServiceGuidelines("");
    setDisplayImgUrl("");
    setImgUrl("");
    setAlert("");
    setDescriptionCount(0);
    setGuideCount(0);
    setAlertCount(0);
  }

  const onFileChange = (e) => {
    try {
      if (e.target.files[0].size <= 1048576) {
        setImgUrl("");
        setDisplayImgUrl("");
        // setImgUrl(e.target.files[0]);
        // setDisplayImgUrl(URL.createObjectURL(e.target.files[0]));
        setCropImgUrl(URL.createObjectURL(e.target.files[0]));
        setOpenImageCrop(true);
      } else {
        ToastAlert('info', "Max size of 1MB allowed")
        PlaySound()
      }
    } catch (error) { }
  };

  const handlesubmit = (e) => {
    const form = e.currentTarget;
    //  setValidated(true);
    if (serviceTitle === "" || guestPostPrice === "" || !priceRegex.test(guestPostPrice) || !priceRegex.test(backlinkPrice) || estimatedDelivery === null || category === null || backlinkPrice === "" || serviceWebLink === "" || !urlRegex.test(serviceWebLink) ||
      serviceDescription === "" || serviceGuideline === "" || alertt === "" || imgUrl === "") {
      setValidated(true);
      e.preventDefault();
      e.stopPropagation();
    } else {
      let formData = new FormData(); //formdata object
      formData.set("id", id);
      formData.set("title", serviceTitle);
      formData.set("category", category.value);  //category
      formData.set("price", guestPostPrice);
      formData.set("backlinkprice", backlinkPrice);
      formData.set("estimatedelivery", estimatedDelivery.value); //estimatedDelivery
      formData.set("websitelink", serviceWebLink);
      formData.set("description", serviceDescription); //append the values with key, value pair
      formData.set("guideline", serviceGuideline);
      formData.set("alert", alertt);
      formData.append("image", imgUrl);
      setLoading(true);

      if (id > 0) {
        props.updateService(
          formData,
          () => {
            setLoading(false);
            props.onSuccess();
          },
          (error) => {
            setLoading(false);
            ToastAlert('error', error.message);
            PlaySound()
          }
        );
      }
      else {
        props.addService(
          formData,
          () => {
            setLoading(false);
            props.onSuccess();
          },
          (error) => {
            setLoading(false);
            ToastAlert('error', error.message);
            PlaySound()
          }
        );
      }
    }
  };

  return (
    <div>
      <Loading isVisible={isLoading} />
      <Modal
        open={props.isVisible}
        disableBackdropClick
        style={{ overflow: "scroll" }}
        className="modal_popup"
        onClose={props.onClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div class="modal-dialog modal-xl" role="document">
          <div class="modal-content add_new_service">
            <div class="modal-header">
              <h5 class="popup_title" id="exampleModalLabel" >
                {props.isEdit ? "Edit Service" : "Add New Service"}
              </h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={props.onClose}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <Form id="myform" noValidate validated={validated} onSubmit={handlesubmit}>              
              <div class="modal-body form_fields">
                <Form.Row>
                  <div class="form-group col-lg-4 col-md-6 col-sm-12">
                    <Form.Group controlId="validationCustom03">
                      <Form.Label>
                        Service Title <span class="required-red">*</span>
                      </Form.Label>
                      <Form.Control
                        type="text"
                        placeholder="Service Title *"
                        value={serviceTitle}
                        isValid={serviceTitle}
                        onChange={(e) => setServiceTitle(e.target.value)}
                        required
                      />
                      <Form.Control.Feedback type="invalid">
                        Please provide a service.
                      </Form.Control.Feedback>
                    </Form.Group>
                  </div>

                  <div class="form-group col-lg-4 col-md-6 col-sm-12">
                    <Form.Group controlId="exampleForm.ControlSelect1">
                      <Form.Label>Choose Category <span class="required-red">*</span></Form.Label>
                      <Select
                        className="basic-single-select"
                        classNamePrefix={"select_dropdown"}
                        placeholder="Choose Category"
                        isClearable
                        isSearchable={true}
                        onChange={(selected) => {
                          setCategory(selected);
                        }}
                        value={category}
                        options={props.serviceCategories}
                      />
                      <div className="invalid-user">
                        {validated && category == null && "Please choose category."}
                      </div>
                    </Form.Group>
                  </div>

                  <div class="form-group col-lg-4 col-md-6 col-sm-12">
                    <Form.Label>Per Guest Post Price <span class="required-red">*</span> </Form.Label>
                    <InputGroup hasValidation>
                      <InputGroup.Prepend>
                        <InputGroup.Text>$</InputGroup.Text>
                      </InputGroup.Prepend>
                      <Form.Control
                        type="number"
                        min="1"
                        step=".01"
                        value={guestPostPrice}
                        placeholder="Per Guest Post Price *"
                        onChange={(e) => setGuestPostPrice(e.target.value)}
                        required
                      />
                      <InputGroup.Prepend>
                        <InputGroup.Text>@</InputGroup.Text>
                      </InputGroup.Prepend>
                      <Form.Control.Feedback type="invalid">
                        {guestPostPrice == "" && "Please enter per guest post price."}
                        {guestPostPrice != "" && !priceRegex.test(guestPostPrice) && "Please enter valid per guest post price."}
                      </Form.Control.Feedback>
                    </InputGroup>
                  </div>
                  <div class="form-group col-lg-4 col-md-6 col-sm-12">
                    <Form.Label>Per Backlink Price <span class="required-red">*</span> </Form.Label>
                    <InputGroup hasValidation>
                      <InputGroup.Prepend>
                        <InputGroup.Text>$</InputGroup.Text>
                      </InputGroup.Prepend>
                      <Form.Control
                        type="number"
                        min="1"
                        step=".01"
                        value={backlinkPrice}
                        placeholder="Per Backlink Price *"
                        onChange={(e) => setBackLinkPrice(e.target.value)}
                        required
                      />
                      <InputGroup.Prepend>
                        <InputGroup.Text>@</InputGroup.Text>
                      </InputGroup.Prepend>
                      <Form.Control.Feedback type="invalid">
                        {backlinkPrice == "" && "Please enter per backlink price." } 
                        {backlinkPrice != "" && !priceRegex.test(backlinkPrice) && "Please enter valid per backlink price."}
                      </Form.Control.Feedback>
                    </InputGroup>
                  </div>
                  <div class="form-group col-lg-4 col-md-6 col-sm-12">
                    <Form.Group controlId="exampleForm.ControlSelect1">
                      <Form.Label>Estimate Delivery <span class="required-red">*</span></Form.Label>
                      <Select
                        className="basic-single-select"
                        classNamePrefix={"select_dropdown"}
                        placeholder="Select Estimate Delivery"
                        isClearable
                        isSearchable={false}
                        onChange={(selected) => {
                          setEstimatedDelivery(selected);
                        }}
                        value={estimatedDelivery}
                        options={props.serviceEstimatedDelivery}
                      />
                      <div className="invalid-user">
                        {validated && estimatedDelivery == null && "Please select estimate delivery."}
                      </div>
                    </Form.Group>
                  </div>
                  <div class="form-group col-lg-4 col-md-6 col-sm-12">
                    <Form.Group controlId="validationCustom03">
                      <Form.Label>Service Website Link <span class="required-red">*</span></Form.Label>
                      <Form.Control
                        type="text"
                        value={serviceWebLink}
                        isInvalid={validated && !urlRegex.test(serviceWebLink)}
                        placeholder="www.entrepreneur.com"
                        required
                        onChange={(e) => setServiceWebLink(e.target.value)}
                      />
                      <Form.Control.Feedback type="invalid">
                        {serviceWebLink == "" && "Please provide a website link." }
                        {serviceWebLink != "" && !urlRegex.test(serviceWebLink) && "Please enter valid website link."}
                      </Form.Control.Feedback>
                    </Form.Group>
                  </div>

                  <div class="form-group col-lg-12 col-md-12 col-sm-12">
                    <Form.Group controlId="exampleForm.ControlTextarea1">
                      <Form.Label>Service Description <span class="required-red">*</span></Form.Label>
                      <Form.Control
                        as="textarea"
                        value={serviceDescription}
                        placeholder="Service Description"
                        rows={3}
                        required
                        maxlength="500"
                        onChange={(e) => {
                          setServiceDescription(e.target.value);
                          setDescriptionCount(e.target.value.length);
                        }}
                      />
                      <Form.Control.Feedback type="invalid">
                        Please provide a service description.
                      </Form.Control.Feedback>
                    </Form.Group>
                    <p className="text-muted mb-0">{descriptionCount}/500</p>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12">
                    <Form.Group controlId="exampleForm.ControlTextarea1">
                      <Form.Label>
                        Service Guideline and Restrictions <span class="required-red">*</span>
                      </Form.Label>
                      <Form.Control
                        as="textarea"
                        value={serviceGuideline}
                        placeholder="Service Guideline and Restrictions"
                        rows={3}
                        required
                        maxlength="500"
                        onChange={(e) => {
                          setServiceGuidelines(e.target.value);
                          setGuideCount(e.target.value.length);
                        }}
                      />
                      <Form.Control.Feedback type="invalid">
                        Please provide a service guideline and restrictions.
                      </Form.Control.Feedback>
                    </Form.Group>
                    <p className="text-muted mb-0">{guideCount}/500</p>
                  </div>
                  <div class="form-group col-lg-12 col-md-12 col-sm-12">
                    <Form.Group controlId="alertId">
                      <Form.Label>Alert <span class="required-red">*</span></Form.Label>
                      <Form.Control
                        as="textarea"
                        value={alertt}
                        placeholder="Alert"
                        rows={3}
                        required
                        maxlength="500"
                        onChange={(e) => {
                          setAlert(e.target.value);
                          setAlertCount(e.target.value.length);
                        }}
                      />
                      <Form.Control.Feedback type="invalid">
                        Please provide alert.
                      </Form.Control.Feedback>
                    </Form.Group>
                    <p className="text-muted mb-0">{alertCount}/500</p>
                  </div>
                  <div className="col-md-12 d-flex justify-content-center">
                      <div className="fileupload_bg">
                        <span className="file-upload-section">
                          <img src={upload_icon_image} alt="" width="50px" />
                          <span className="upload_text">
                            Upload Service Image <span class="required-red">*</span>
                          </span>
                        </span>
                        <ImageCropperComponent 
                          isOpen={openImageCrop} 
                          onClose={() => {setOpenImageCrop(false)}} 
                          aspectRatio={600 / 500} 
                          width={400} 
                          height={400} 
                          src={imgUrlToCrop} 
                          setCroppedImage={(image) => {
                            setImgUrl(image.blobFile);
                            setDisplayImgUrl(image.croppedImageUrl);
                          }}
                        />
                        {imgUrlToDisplay == "" ? (
                        <span>                          
                          <label for="file-upload" className="btn_light_green ml-3">
                            <span className="fa fa-plus"></span>&nbsp;Add Image
                          </label>
                          <input
                            id="file-upload"
                            type="file"
                            onChange={onFileChange}
                            accept="image/*"
                            hidden="true"
                          />
                          <br/>
                        </span>
                        ) : (        
                        <div className="preview_uploaded">            
                          <label for="file-upload" className="file-upload-section"><img width="100px" src={imgUrlToDisplay} />
                          </label>                        
                          <span className="close_icon_btn">
                            <Tooltip title="Cancel">
                              <IconButton
                                aria-label="cancel"
                                onClick={() => {
                                  setDisplayImgUrl("");
                                  setImgUrl("");
                                }}
                              >
                                <Close fontSize="small" />
                              </IconButton>
                            </Tooltip>
                          </span>  
                        </div>                  
                      )}
                      {validated && imgUrl === "" && (
                        <span id="email-error" className="text-danger">Please upload service image.</span>
                      )}
                    </div>
                  </div>
                </Form.Row>
              </div>
              <div class="modal-footer border-0 text-center justify-content-center mb-3">                    
                <Button
                  className="btn_light_green effect effect-2"
                  onClick={handlesubmit}
                >
                  {buttonLabel}
                </Button>
              </div>              
            </Form>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default AddNewService;
