import { connect } from "react-redux";
import ServiceMaster from "./ServiceMaster";
import {
  userLogin,
  addService,
  addCustomOrder,
  getServiceList,
  checkBillingInfo,
  removeService,
  removeCustomService,
  updateService,
  searchServiceUsers,
  changeServiceStatus,
  changeCustomStatus,
  checkCongratulation
} from "../../Actions/actionContainer";

const mapStateToProps = (state) => {
  return {
    serviceCategories: state.mainReducer.serviceCategories,
    serviceEstimatedDelivery: state.mainReducer.serviceEstimatedDelivery,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userLogin: (params, onSuccess, onFailure) =>
      dispatch(userLogin(params, onSuccess, onFailure)),
    addService: (params, onSuccess, onFailure) =>
      dispatch(addService(params, onSuccess, onFailure)),
    addCustomOrder: (params, onSuccess, onFailure) =>
      dispatch(addCustomOrder(params, onSuccess, onFailure)),
    getServiceList: (onSuccess, onFailure) =>
      dispatch(getServiceList(onSuccess, onFailure)),
    checkBillingInfo: (onSuccess, onFailure) =>
      dispatch(checkBillingInfo(onSuccess, onFailure)),
    removeService: (id, onSuccess, onFailure) =>
      dispatch(removeService(id, onSuccess, onFailure)),
    removeCustomService: (id, onSuccess, onFailure) =>
      dispatch(removeCustomService(id, onSuccess, onFailure)),
    updateService: (params, onSuccess, onFailure) =>
      dispatch(updateService(params, onSuccess, onFailure)),
    changeServiceStatus: (params, onSuccess, onFailure) =>
      dispatch(changeServiceStatus(params, onSuccess, onFailure)),
    changeCustomStatus: (params, onSuccess, onFailure) =>
      dispatch(changeCustomStatus(params, onSuccess, onFailure)),
    searchServiceUsers: (searchText, onSuccess, onFailure) =>
      dispatch(searchServiceUsers(searchText, onSuccess, onFailure)),
    checkCongratulation: (params) =>
      dispatch(checkCongratulation(params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ServiceMaster);
