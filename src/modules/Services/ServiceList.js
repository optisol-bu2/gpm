import React, { useState } from "react";
import ServiceListItem from "./ServiceListItem";
import AddNewService from "./AddNewService";
import AddNewCustom from "./AddNewCustom";
import ConfirmationDialog from "../../components/core/ConfirmationDialog/ConfirmationDialog";
import Loading from "../../components/core/Loading";
import { ToastAlert,PlaySound } from "../../utils/sweetalert2";

const ServiceList = (props) => {
  const [isEditClicked, setEditClicked] = useState(false);
  const [confirmDialog, setConfirmDialog] = useState(false);
  const [removeServiceId, setRemoveServiceId] = useState();
  const [isCustomOrder, setIsCustomOrder] = useState(false);
  const [isLoading, setLoading] = useState(null);
  const [editData, setEditData] = useState([]);

  function setViewMoreFlag(data, flag) {
    data.isViewMore = flag;
  }

  /**
   * changes status
   * @param {*} id 
   * @param {*} status 
   * @param {*} customorder 
   */
  const changeStatus = (id, status, customorder) => {
    setLoading(true);
    let params = {
      id: id,
      status: status
    }
    if (!customorder) {
      props.changeServiceStatus(
        params,
        () => {
          setLoading(false);
          props.isUpdateEvent(true);
        },
        (error) => {
          setLoading(false);
          ToastAlert('error', error.message);
          PlaySound()
        }
      );
    } else {
      props.changeCustomStatus(
        params,
        () => {
          setLoading(false);
          props.isUpdateEvent(true);
        },
        (error) => {
          setLoading(false);
          ToastAlert('error', error.message);
          PlaySound()
        }
      );
    }
  }

  return (
    <div>
      <Loading isVisible={isLoading} />
      {confirmDialog && (
        <ConfirmationDialog
          open={confirmDialog}
          dialogTitle={"Remove"}
          dialogContentText={isCustomOrder ? `Are you sure you want to remove this custom order?` : `Are you sure you want to remove this service?`}
          cancelButtonText="Cancel"
          okButtonText={"Remove"}
          onCancel={() => {
            setConfirmDialog(false);
            setIsCustomOrder(false);
          }}
          onClose={() => {
            setConfirmDialog(false);
            setIsCustomOrder(false);
          }}
          onOk={(e) => {
            setConfirmDialog(false);
            setLoading(true);
            if (isCustomOrder) {
              props.removeCustomService(
                removeServiceId,
                () => {
                  setLoading(false);
                  setIsCustomOrder(false);
                  props.isDeleteEvent();
                },
                (error) => {
                  setLoading(false);
                  setIsCustomOrder(false);
                  ToastAlert('error', error.message);
                  PlaySound()
                }
              );
            } else {
              props.removeService(
                removeServiceId,
                () => {
                  setLoading(false);
                  props.isDeleteEvent();
                },
                (error) => {
                  setLoading(false);
                  ToastAlert('error', error.message);
                  PlaySound()
                }
              );
            }
          }}
        />
      )}

      <div>
        {props.data && props.data.length !== 0 && props.data.map((data) => (
          <ServiceListItem
            data={data}
            key={data.id}
            serviceEstimatedDelivery={props.serviceEstimatedDelivery}
            serviceCategories={props.serviceCategories}
            onViewClick={() => setViewMoreFlag(data, !data.isViewMore)}
            onEditClick={() => {
              setEditClicked(true);
              setEditData(data);
            }}
            onDeleteClick={(id) => {
              setConfirmDialog(true);
              setRemoveServiceId(id);
            }}
            onStatusClick={(id, status, customorder) => {
              changeStatus(id, status, customorder);
            }}
            onCustomEditClick={() => {
              setEditClicked(true);
              setEditData(data);
            }}
            onCustomDeleteClick={(id) => {
              setIsCustomOrder(true);
              setConfirmDialog(true);
              setRemoveServiceId(id);
            }}
          />
        ))}
        {props.data.length === 0 &&
          <h3 className="norec_found">No Record Found</h3>
        }

        {!editData.iscustomorder ? (
          <AddNewService
            isVisible={isEditClicked}
            isEdit={isEditClicked}
            editData={editData}
            onClose={() => {
              setEditClicked(false);
            }}
            onSuccess={() => {
              setEditClicked(false);
              props.isUpdateEvent();
            }}
            onFailure={() => {
              setEditClicked(false);
            }}
            {...props}
          />
        ) : (
          <AddNewCustom
            isVisible={isEditClicked}
            isEdit={isEditClicked}
            editData={editData}
            onClose={() => {
              setEditClicked(false);
            }}
            onSuccess={() => {
              setEditClicked(false);
              props.history.push("/inbox")
            }}
            onFailure={() => {
              setEditClicked(false);
            }}
            {...props}
          />
        )
      }
      </div>
    </div>
  );
};

export default ServiceList;
