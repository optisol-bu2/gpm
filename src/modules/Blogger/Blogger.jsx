import React from "react";
import { connect } from "react-redux";
import Loading from "../../components/core/Loading";
import TextLoading from "../../components/core/Loading";
import Modal from "@material-ui/core/Modal";
import ConfirmationDialog from "../../components/core/ConfirmationDialog/ConfirmationDialog";
import blogger_icon from "../../assets/images/menu-icons/blogger-white.svg";
import avatar from '../../assets/images/avatar.png';
import { Link } from "react-router-dom";
import Pagination from "@material-ui/lab/Pagination";
import { ToastAlert, PlaySound } from '../../utils/sweetalert2';
import upload_icon_image from "../../assets/images/other-pages/upload_icon_image.svg";
import icn_link from "../../assets/images/icn_link.svg";
import ImageCropperComponent from "../../components/core/ImageCropperComponent";
import {
    getBloggerList,
    searchBloggerList,
    filterBloggerList,
    getBloggerDetails,
    addBloggerDetails,
    addBloggerPortfolio,
    addBlogLink,
    removeBlogLink,
    addBloggerAboutMe,
    removeBloggerAttachment,
} from "../../Actions/bloggerAction";
import { updateProfilePic, myprofileAction, getProfilePic  } from '../../Actions/myprofileAction';
import { reLoginAction } from '../../Actions/actionContainer';
import {
    addFavoriteBlogger,
    removeFavoriteBlogger
} from "../../Actions/favoriteAction";
import { checkBillingInfo, addCustomOrder, addQuestions, getViewQuestions, searchServiceUsers, checkCongratulation } from "../../Actions/actionContainer";
import Select from 'react-select';
import { Close } from "@material-ui/icons";
import { IconButton, Tooltip } from "@material-ui/core";
import { ListGroup } from "react-bootstrap";
import AddNewCustom from "../Services/AddNewCustom";
import ContentWriterThankYou from "../Services/ContentWriterThankYou";
import { Prompt } from 'react-router';
import CreatableSelect from 'react-select/creatable';
import profileImage from '../../assets/images/avatar.png';
import _ from 'lodash';
import ContentWriterTest from "../Services/ContentWriterTest";
import ContentWriterWarning from "../Services/ContentWriterWarning";
import ContentWriterCongrats from "../Services/ContentWriterCongrats";
import ContentWriterReject from "../Services/ContentWriterReject";
import { getHeaderInfo } from "../../Actions/inboxAction";
import Pusher from 'pusher-js';
import { PUSHER_KEY, PUSHER_CLUSTER } from "../../common/constants";
import { Popover, OverlayTrigger } from 'react-bootstrap';
import moment from 'moment';

const priceRegex = RegExp(/^([1-9]\d{0,4}\.{0,1}\d{0,2})$/i);
const urlRegex = RegExp(/^(http:\/\/|https:\/\/|www\.|ftp:\/\/|gopher:\/\/|mailto:|news:|telnet:\/\/|wais:\/\/)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/m);

/**
 * Blogger Component
 */
class Blogger extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            expertise: [],
            otherExpertise: [],
            rate: '',
            wordsperhour: '',
            estimateddelivery: null,
            estimatedexpertise: null,
            lastlogin_at: '',
            aboutme: '',
            links: [''],
            link_data: [],
            modal_link_data: [],
            about_video: [],
            portfolio: [],
            about_video_data: '',
            portfolio_data: [],
            portfolio_delete_ids: [],
            activate_blogger: false,
            isLoading: false,
            confirmRemoveFile: false,
            removeFileId: '',
            isVideoRemove: false,
            bloggerListData: [],
            bloggerList: [],
            isModalOpen: false,
            totalRecord: 0,
            searchText: "",
            pageIndex: 1,
            isSearch: false,
            isTextLoading: null,
            loadingText: '',
            charCount: 0,
            favoriteArray: [],
            isFavoriteModalOpen: false,
            favoriteModalContent: '',
            isLinkModalOpen: false,
            isFileModalOpen: false,
            submitted: false,
            isLoading: false,
            isAddCustom: false,
            isContentWriterWarningModalOpen: false,
            isConterntWriterTestModelOpen: false,
            isThankyouModalOpen: false,
            isCongratulationModelOpen: false,
            isRejectedUserModelOpen: false,
            setModalOpen: false,
            file_size: 0,
            isVideoModalOpen: false,
            flag: 0,
            shouldBlockNavigation: false,
            openImageCrop: false,
            imgUrlToCrop: "",
            minPrice: "",
            maxPrice: "",
            minRate: "",
            maxRate: "",
            filterText: "",
            searchfield: "",
            showRating: false,
            showPrice: false,
        };
        this.wrapperRef = React.createRef();
    }

    /**
     * Handle if clicked on outside of element
     */
    handleClickOutside = (event) => {
        if (this.wrapperRef && !this.wrapperRef.current.contains(event.target)) {
            this.setState({showRating: false, showPrice: false})
        }
    }

    componentDidMount() {
        document.addEventListener('mousedown', this.handleClickOutside);
        this.props.getProfilePic();
        if (this.props.activeusertype === 1) {
            let userId = this.props.userData.id
            const pusher = new Pusher(PUSHER_KEY, {
                cluster: PUSHER_CLUSTER,
                encrypted: true
            });
            const userBecomeBlogerChannel = pusher.subscribe('GPM-SELLER-STATUS-' + userId);
            userBecomeBlogerChannel.bind('USER_SELLER_STATUS_NOTIFICATION', data => {
                this.props.getHeaderInfo(
                    (response) => {
                        this.setState({ isLoading: false });
                        if (this.props.headerInfo.iscongratulationmodel) {
                                this.props.checkCongratulation(true);  
                        }
                        if (this.props.userData.iscongratulationmodel) {
                            this.setState({ isCongratulationModelOpen: true });
                        }else{
                            this.setState({ isCongratulationModelOpen: false });
                        }
                        if (!this.props.userData.iscongratulationmodel && this.props.headerInfo.isapproveduser) {
                            this.checkBillingInfo();
                        }
                        this.checkTestInfo();

                    },
                    (error) => {
                        this.setState({ isLoading: false });
                    }
                );
            });
            this.props.getHeaderInfo(
                (response) => {
                    this.setState({ isLoading: false });
                    if (this.props.userData.iscongratulationmodel) {
                        this.setState({ isCongratulationModelOpen: true });
                    }else{
                        this.setState({ isCongratulationModelOpen: false });
                    }
                    if (!this.props.userData.iscongratulationmodel && this.props.headerInfo.isapproveduser) {
                        this.checkBillingInfo();
                    }
                    this.checkTestInfo();
                },
                (error) => {
                    this.setState({ isLoading: false });
                }
            );
        }
        if (this.props.activeusertype === 2) {
            this.getBloggerList();
        }
    }

    arrayOfArray = (array, size) => {
        const splitArray = [];
        for (let i = 0; i < array.length; i++) {
            const last = splitArray[splitArray.length - 1];
            if (!last || last.length === size) {
                splitArray.push([array[i]]);
            } else {
                last.push(array[i]);
            }
        };
        return splitArray;
    };

    /**
     * changes the given time to chat time format
     * @param {*} dateTime
     * @returns
     */
    chatTimeFormat = (dateTime) => {
        if(!dateTime) return "" ; 
        var utc = moment.utc(dateTime).toDate();
        var utcDate = moment.utc(dateTime).format('YYYY-MM-DD');
        var todayDate = moment.utc().format('YYYY-MM-DD');
        var utcTime = moment(utc).local().format("DD-MM-YYYY HH:mm:ss");
        var todayTime = moment().local().format("DD-MM-YYYY HH:mm:ss");
        var ms = moment(todayTime,"DD/MM/YYYY HH:mm:ss").diff(moment(utcTime,"DD/MM/YYYY HH:mm:ss"));
        var duration = moment.duration(ms);
        if (utcDate === todayDate) {
            var getMinutes = parseInt(duration.as('minutes'));
            var chatTime = getMinutes + " minutes ago"; 
            if(parseInt(getMinutes) == 1) {
                var chatTime = getMinutes + " minute ago";
            }
            if(parseInt(getMinutes) >= 60) {
                var getHours = parseInt(duration.as('hours'));
                var chatTime = getHours + " hours ago"; 
                if(parseInt(getHours) == 1) {
                    var chatTime = getHours + " hour ago";
                }
            }
        } else {
            var getDays = parseInt(duration.as('days'));
            var chatTime = getDays + " days ago";
            if(parseInt(getDays) == 1) {
                var chatTime = getDays + " day ago";
            }
        } 
        return chatTime
    }

    /**
     * checks whether the seller already added billing information or not
     */
    checkBillingInfo = () => {
        this.setState({ isLoading: true });
        this.props.checkBillingInfo(
            (response) => {
                this.setState({ isLoading: false });
                this.getBloggerDetails();
            },
            (error) => {
                this.setState({ isLoading: false, isModalOpen: true });
            }
        );
    }

    /**
    * checks whether the seller content writer exam info
    */
    checkTestInfo = () => {
        this.setState({ isLoading: true });
        if (!this.props.headerInfo.onboardingexam) {
            this.setState({isContentWriterWarningModalOpen:false, isThankyouModalOpen: false, isRejectedUserModelOpen:false, isConterntWriterTestModelOpen:false });
            this.checkBillingInfo();
        } else {
            this.setState({ isModalOpen: false, isContentWriterWarningModalOpen: !this.props.headerInfo.isalreadysubmitted, isRejectedUserModelOpen: this.props.headerInfo.isrejecteduser, isLoading: false, });
            if (!this.props.headerInfo.isalreadysubmitted || this.props.headerInfo.isapproveduser) {
                this.setState({ isThankyouModalOpen: false });
            } else {
                this.setState({ isThankyouModalOpen: true });
            }
            if (this.props.headerInfo.isrejecteduser) {
                this.setState({ isThankyouModalOpen: false });
            }
        }
    }

    /**
     * closes the modal
     */
    closeModal = () => {
        this.setState({
            isModalOpen: false,
        })
        this.props.history.push("/profile/billing")
    }

    /**
     * gets blogger details
     */
    getBloggerDetails = (load) => {
        if (load === undefined)
            this.setState({ isLoading: true });
        this.props.getBloggerDetails(
            (blogger) => {
                if (blogger.data) {
                    var data = blogger.data;
                    this.props.serviceEstimatedDelivery.forEach(estimatedDelivery => {
                        if (estimatedDelivery.value == data.estimateddelivery) {
                            this.setState({
                                estimateddelivery: estimatedDelivery,
                            });
                        }
                    })

                    var links = []
                    data.blogportfolio.forEach(link => {
                        links.push(link.document)
                    })
                    var selectedExpertise = [];
                    _.map(data.expertise, function (value) {
                        selectedExpertise.push({ label: value.name, value: value.id })
                        return (value);
                    })
                    this.setState({
                        expertise: selectedExpertise,
                        rate: data.rate,
                        wordsperhour: data.wordsperhour,
                        aboutme: data.aboutme,
                        activate_blogger: data.isactive === 1 ? true : false,
                        portfolio_data: data.portfolio,
                        links: links.length != 0 ? links : [''],
                        link_data: data.blogportfolio,
                        about_video_data: data.aboutme_document,
                        isLoading: false,
                        charCount: data.aboutme.length,
                        lastlogin_at: data.lastlogin_at
                    });
                }
            },
            (error) => {
                this.setState({ isLoading: false });
            }
        );
    }

    /**
     * gets blogger list data
     */
    getBloggerList = (load) => {
        if (load === undefined) {
            this.setState({ isLoading: true });
        }
        this.props.getBloggerList(this.state.pageIndex,
            (blogger) => {
                if (blogger.data) {
                    if (blogger.data.records && blogger.data.records.length) {
                        let favoriteArray = [];
                        blogger.data.records.forEach(bloggerData => {
                            if (bloggerData.isFavorite) {
                                if (!favoriteArray.includes(bloggerData.id))
                                    favoriteArray.push(bloggerData.id)
                            }
                        })
                        this.setState({ favoriteArray: favoriteArray });
                    }
                    this.setState({ totalRecord: Math.ceil(blogger.data.total / blogger.data.noofrecords) == 1 ? 0 : Math.ceil(blogger.data.total / blogger.data.noofrecords), bloggerListData: blogger.data.records, isLoading: false });
                } else {
                    this.setState({ totalRecord: 0, isLoading: false });
                }
            },
            (error) => {
                this.setState({ totalRecord: 0, isLoading: false });
            }
        );
    }

    /**
     * searches blogger in the list
     */
    searchBloggerList = (load) => {
        if (load === undefined) {
            this.setState({ isLoading: true });
        }
        var params = {
            pageIndex: this.state.pageIndex,
            searchText: this.state.searchText,
        }
        this.props.searchBloggerList(params, (blogger) => {
            if (blogger.data) {
                if (blogger.data.records && blogger.data.records.length) {
                    let favoriteArray = [];
                    blogger.data.records.forEach(bloggerData => {
                        if (bloggerData.isFavorite) {
                            if (!favoriteArray.includes(bloggerData.id))
                                favoriteArray.push(bloggerData.id)
                        }
                    })
                    this.setState({ favoriteArray: favoriteArray });
                }
                this.setState({ totalRecord: Math.ceil(blogger.data.total / blogger.data.noofrecords) == 1 ? 0 : Math.ceil(blogger.data.total / blogger.data.noofrecords), bloggerListData: blogger.data.records, isLoading: false });
            } else {
                this.setState({ totalRecord: 0, isLoading: false });
            }
        },
            () => {
                this.setState({ totalRecord: 0, isLoading: false });
            })
    }

     /**
     * filters blogger in the list
     */
    filterBloggerList = (load) => {
        if (load === undefined) {
            this.setState({ isLoading: true });
        }
        if(this.state.searchfield == "price" && (this.state.minPrice == "" && this.state.maxPrice == "")) {
            this.getBloggerList(true);
        } else if(this.state.searchfield == "price" && (this.state.minPrice == "" || this.state.maxPrice == "")) {
            ToastAlert('error', "Please fill both Minimum and Maximum Cost"); 
        } else if(this.state.searchfield == "rating" && (this.state.minRate == "" || this.state.maxRate == "")) {
            this.getBloggerList(true);
        } else if(this.state.searchfield == "rating" && (this.state.minRate == "" || this.state.maxRate == "")) {
            ToastAlert('error', "Please fill both Minimum and Maximum Rate"); 
        } else {
            var params = {
                pageIndex: this.state.pageIndex,
                searchText: this.state.filterText,
                searchfield: this.state.searchfield
            }
            this.props.filterBloggerList(params, (blogger) => {
                if (blogger.data) {
                    if (blogger.data.records && blogger.data.records.length) {
                        let favoriteArray = [];
                        blogger.data.records.forEach(bloggerData => {
                            if (bloggerData.isFavorite) {
                                if (!favoriteArray.includes(bloggerData.id))
                                    favoriteArray.push(bloggerData.id)
                            }
                        })
                        this.setState({ favoriteArray: favoriteArray });
                    }
                    this.setState({ showPrice: false, showRating: false, totalRecord: Math.ceil(blogger.data.total / blogger.data.noofrecords) == 1 ? 0 : Math.ceil(blogger.data.total / blogger.data.noofrecords), bloggerListData: blogger.data.records, isLoading: false });
                } else {
                    this.setState({ totalRecord: 0, isLoading: false });
                }
            },
                () => {
                    this.setState({ totalRecord: 0, isLoading: false });
            })
        }
    }


    /**
     * gets blogger list for given pageIndex
     * @param {*} pageIndex 
     */
    onPageChange = (pageIndex) => {
        this.setState({ pageIndex: pageIndex }, () => {
            if (this.state.isSearch) {
                this.searchBloggerList();
            } else {
                this.getBloggerList();
            }
        });
    }

    /**
     * handles changes in the event and updates searchText state
     * @param {*} e 
     */
    onChangeSearchText = (e) => {
        if (e.target.value.length >= 3) {
            this.setState({ searchText: e.target.value, pageIndex: 1, isSearch: true }, () => {
                this.searchBloggerList(true);
            });
        } else {
            this.setState({ searchText: e.target.value, pageIndex: 1, isSearch: false }, () => {
                if (!this.state.searchText)
                    this.getBloggerList(true);
            });
        }
    }

     /**
     * handles changes in the event and updates searchText state
     * @param {*} e 
     */
      onChangeFilterText = (e) => {
        if (e == "price" && (this.state.minPrice != "" || this.state.maxPrice != "")) {
            var mergePrice = this.state.minPrice + "-" + this.state.maxPrice;
            this.setState({ filterText: mergePrice, searchfield: e, pageIndex: 1}, () => {
                this.filterBloggerList(true);
            });
        } else if (e == "rating" && (this.state.minRate != "" || this.state.maxRate != "")) {
            var mergePrice = this.state.minRate + "-" + this.state.maxRate;
            this.setState({ filterText: mergePrice, searchfield: e, pageIndex: 1}, () => {
                this.filterBloggerList(true);
            });
        } 
    }

    /**
     * adds favorite blogger for the user
     * @param {*} id 
     */
    addFavoriteBlogger = (id) => {
        // let button = document.getElementById("favoriteblogger-"+id);
        // button.setAttribute("disabled", "disabled");
        var bloggerArray = this.state.bloggerListData;
        bloggerArray.forEach(blogger => {
            if (blogger.id === id) {
                blogger.isFavorite = true;
            }
        })
        this.setState({ bloggerListData: bloggerArray });
        var params = {
            "blogger": id.toString()
        }
        this.props.addFavoriteBlogger(params,
            (response) => {
                // let button = document.getElementById("favoriteblogger-"+id);
                // button.removeAttribute("disabled");
                // this.getBloggerList(true);
            },
            (error) => {
                // let button = document.getElementById("favoriteblogger-"+id);
                // button.removeAttribute("disabled");
                var bloggerArray = this.state.bloggerListData;
                bloggerArray.forEach(blogger => {
                    if (blogger.id === id) {
                        blogger.isFavorite = false;
                    }
                })
                ToastAlert("error", error.message);
                PlaySound()
            }
        )
    }

    /**
     * removes favorite blogger for the user
     * @param {*} id 
     */
    removeFavoriteBlogger = (id) => {
        // let button = document.getElementById("favoriteblogger-"+id);
        // button.setAttribute("disabled", "disabled");
        var bloggerArray = this.state.bloggerListData;
        bloggerArray.forEach(blogger => {
            if (blogger.id === id) {
                blogger.isFavorite = false;
            }
        })
        this.setState({ bloggerListData: bloggerArray });
        this.props.removeFavoriteBlogger(id,
            (response) => {
                // let button = document.getElementById("favoriteblogger-"+id);
                // button.removeAttribute("disabled");
                // this.getBloggerList(true);
            },
            (error) => {
                // let button = document.getElementById("favoriteblogger-"+id);
                // button.removeAttribute("disabled");
                var bloggerArray = this.state.bloggerListData;
                bloggerArray.forEach(blogger => {
                    if (blogger.id === id) {
                        blogger.isFavorite = true;
                    }
                })
                this.setState({ bloggerListData: bloggerArray });
                ToastAlert("error", error.message);
                PlaySound()
            }
        )
    }

    /**
     * handles all field changes and assigns it to the state
     * @param {*} e 
     */
    onBloggerChange = (e) => {
        if (!(e.target.name == "aboutme" && e.target.value.length > 500)) {
            console.log("inside")

            this.setState({ [e.target.name]: e.target.value, charCount: e.target.value.length })
        } 
        this.setState({ shouldBlockNavigation: true })
    }

    /**
     * changes activate_blogger state
     * @param {*} e 
     */
    checkBoxChange = (e) => {
        this.setState({ activate_blogger: !this.state.activate_blogger, shouldBlockNavigation: true })
    }

    /**
     * assigns files in the portfolio state
     * @param {*} e
     */
    onImageChange = (e) => {
        if (e.target.files && e.target.files.length != 0) {
            if (e.target.files.length <= 10) {
                if (this.state.portfolio_data && this.state.portfolio_data.length > 0 && this.state.portfolio_data.length + e.target.files.length > 10) {
                    this.setState({
                        portfolio: []
                    })
                    ToastAlert("error", "You can only upload a maximum of 10 files");
                    PlaySound()
                } else {
                    this.state.file_size = e.target.files[0].size + this.state.file_size;
                    if (this.state.file_size >= 8388608) {
                        this.setState({
                            portfolio: []
                        })
                        ToastAlert("error", "File size exceeds maximum limit 8 MB.");
                        PlaySound()
                        e.target.files = null
                    }
                    else {
                        this.setState({
                            portfolio: [...this.state.portfolio, ...e.target.files],
                            //flag:1
                        })
                        ToastAlert("success", "Files Added.");
                        PlaySound()
                    }
                    // this.setState({
                    //     portfolio: [ ...e.target.files]
                    // })
                }
            } else {
                this.setState({
                    portfolio: []
                })
                ToastAlert("error", "You can only upload a maximum of 10 files");
                PlaySound()
            }
        }
    };

    /**
     * assigns file in the about_video state
     * @param {*} e
     */
    onVideoChange = (e) => {
        if (e.target.files && e.target.files.length != 0) {
            if(!e.target.files[0].type.includes("video")) {
                ToastAlert("error", "Please Upload Video Files Only")
                PlaySound()
                e.target.files = null
            } else {
                if (e.target.files[0].size <= 20971520) {
                    this.setState({
                        about_video: [...this.state.about_video, ...e.target.files]
                    })
                    e.target.files = null
                } else {
                    ToastAlert("error", "File size exceeds maximum limit 20 MB.")
                    PlaySound()
                    e.target.files = null
                }
            }
        }
    };

    /**
     * saves the blogger profile
     * @param {*} e 
     * @returns 
     */
    saveBlogger = (e) => {
        const { expertise, otherExpertise, rate, wordsperhour, estimateddelivery, aboutme, links, link_data, about_video, portfolio, portfolio_delete_ids, about_video_data, portfolio_data, activate_blogger, imgUrlToCrop } = this.state;
        if (expertise.length == 0 && otherExpertise.length == 0) {
            return (ToastAlert('error', 'Expertise should not be blank.'), PlaySound())
        } else if (!rate) {
            return (ToastAlert('error', 'Rate/Hr should not be blank.'), PlaySound())
        } else if (rate && !priceRegex.test(rate)) {
            return (ToastAlert('error', 'Please enter valid Rate/Hr'), PlaySound())
        } else if (rate && rate < 15) {
            return (ToastAlert('error', 'The minimum rate is $15/hr'), PlaySound())
        } else if (!estimateddelivery || estimateddelivery == null) {
            return (ToastAlert('error', 'Please select delivery time'), PlaySound())
        } else if (!wordsperhour) {
            return (ToastAlert('error', 'Words/Hr should not be blank.'), PlaySound())
        } else if (!aboutme) {
            return (ToastAlert('error', 'About Me should not be blank.'), PlaySound())
        } else if (aboutme && aboutme.length > 500) {
            return (ToastAlert('error', 'About Me should not exceed 500 words'), PlaySound())
        } else if ((!portfolio || (portfolio && portfolio.length == 0)) && (!portfolio_data || (portfolio_data && portfolio_data.length == 0))) {
            return (ToastAlert('error', 'Please upload portfolio'), PlaySound())
        } else if (!imgUrlToCrop && !this.props.profilePic.profilepic) {
            return (ToastAlert('error', 'Please upload photo'), PlaySound())
        } else if (wordsperhour && wordsperhour < 100) {
            return (ToastAlert('error', 'Words/Hr should be minimum 100'), PlaySound())
        } else {
            this.setState({ isLoading: true });
            var selectedExpertise = [];
            var otherSelectedExpertise = [];
            var finalExpertise = [];
            _.filter(expertise, function (value) {
                selectedExpertise.push(value.value)
                return (value);
            })
            var newArra = _.filter(this.props.serviceEstimatedExpertise, function (items) {
                if (selectedExpertise.includes(items.value)) {
                    finalExpertise.push(items.value)
                    return items;
                }
            });
            _.filter(otherExpertise, function (value) {
                otherSelectedExpertise.push(value.value)
                return (value);
            })

            let data = {
                expertise: finalExpertise,
                otherexpertise: otherSelectedExpertise,
                rate: rate,
                wordsperhour: wordsperhour,
                estimateddelivery: estimateddelivery.value,
                aboutme: aboutme,
                isactive: activate_blogger ? "1" : "0"
            }
            this.props.addBloggerDetails(data,
                (blogger) => {
                    this.setState({ shouldBlockNavigation: false })
                    if (portfolio_delete_ids && portfolio_delete_ids.length !== 0) {
                        portfolio_delete_ids.forEach(id => {
                            this.props.removeBloggerAttachment(id);
                        });
                    }
                    var linkData = [];
                    link_data.forEach(link => {
                        linkData.push(link.document);
                    });
                    if (JSON.stringify(linkData) != JSON.stringify(links)) {
                        if (links && links.length !== 0) {
                            if (!(links.length == 1 && links[0] == '')) {
                                var count = 0
                                links.forEach(link => {
                                    count += 1;
                                    if (link != '' && link != null && link != undefined)
                                        this.props.addBlogLink({ link });
                                    if (count == links.length) this.getBloggerDetails(true);
                                });
                            }
                        }
                        if (link_data && link_data.length !== 0) {
                            var count = 0
                            link_data.forEach(link => {
                                count += 1;
                                this.props.removeBlogLink(link.id);
                                if (count == links.length) this.getBloggerDetails(true);
                            });
                        }
                    }
                    if (about_video && about_video.length !== 0 && about_video_data) {
                        this.props.removeBloggerAttachment(about_video_data.id);
                    }
                    if (about_video.length === 0 && portfolio.length === 0) {
                        this.setState({ isLoading: false, about_video: [], portfolio: [] });
                        ToastAlert("success", blogger.message)
                        PlaySound()
                        this.getBloggerDetails(true);
                    } else {
                        if (about_video.length !== 0 && portfolio.length !== 0) {
                            this.setState({ isLoading: false, isTextLoading: true, loadingText: "Uploding video..." });
                            let videoData = new FormData();
                            videoData.append("document", about_video[0]);
                            this.props.addBloggerAboutMe(videoData,
                                () => {
                                    this.setState({ isTextLoading: false, loadingText: "", about_video: [], about_video_data: videoData });
                                    this.uploadPortfolio(portfolio, blogger);
                                },
                                (error) => {
                                    this.setState({ isTextLoading: false, loadingText: "", about_video: [] });
                                    ToastAlert("error", error.message);
                                    PlaySound()
                                    this.uploadPortfolio(portfolio, blogger);
                                }
                            );
                        } else {
                            about_video.forEach(file => {
                                this.setState({ isLoading: false, isTextLoading: true, loadingText: "Uploding video..." });
                                let videoData = new FormData();
                                videoData.append("document", file);
                                this.props.addBloggerAboutMe(videoData,
                                    () => {
                                        this.setState({ isTextLoading: false, loadingText: "", about_video: [] });
                                        ToastAlert("success", blogger.message)
                                        PlaySound()
                                        this.getBloggerDetails(true);
                                    },
                                    (error) => {
                                        this.setState({ isTextLoading: false, loadingText: "", about_video: [] });
                                        ToastAlert("error", error.message)
                                        PlaySound()
                                        this.getBloggerDetails(true);
                                    }
                                );
                            });
                            this.uploadPortfolio(portfolio, blogger);
                        }
                    }

                },
                (error) => {
                    this.setState({ isLoading: false });
                    ToastAlert("error", error.message)
                    PlaySound()
                }
            );
        }
    }

    submitLink = () => {
        this.setState({ submitted: true });
        const { links } = this.state;
        var count = 0;
        links.forEach(link => {
            if (link == '' || (link != '' && !urlRegex.test(link))) {
                return;
            }
            count += 1;
            if (links.length == count) {
                this.setState({ isLinkModalOpen: false, submitted: false });
            }
        })
    }

    closeLinkModal = () => {
        const { modal_link_data } = this.state;
        this.setState({ isLinkModalOpen: false, links: modal_link_data, submitted: false });
    }

    closeFileModal = () => {
        const { modal_file_data } = this.state;
        this.setState({ isFileModalOpen: false, submitted: false });
    }

    closeVideoModal = () => {
        //  const { model_file_data } = this.state;
        this.setState({ isVideoModalOpen: false, submitted: false });
    }

    onChangeLink = (index, value) => {
        const { links } = this.state;
        const linkData = [...links];
        if (linkData.includes(value)) {
            ToastAlert('info', "Link already exists")
            PlaySound()
        } else {
            linkData[index] = value;
            this.setState({ links: linkData, shouldBlockNavigation: true })
        }

    }

    /**
     * Uploads portfolio files
     * @param {*} portfolio 
     * @param {*} blogger 
     */
    uploadPortfolio = (portfolio, blogger) => {
        portfolio.forEach((file, index) => {
            this.setState({ isLoading: false, isTextLoading: true, loadingText: "Uploding portfolio..." });
            let portfolioData = new FormData();
            portfolioData.append("document", file);
            this.props.addBloggerPortfolio(portfolioData,
                () => {
                    if (index + 1 == portfolio.length) {
                        this.setState({ isTextLoading: false, loadingText: "", portfolio: [] });
                        ToastAlert("success", blogger.message)
                        PlaySound()
                        this.getBloggerDetails(true);
                    }
                },
                (error) => {
                    if (index + 1 == portfolio.length) {
                        this.setState({ isTextLoading: false, loadingText: "", portfolio: [] });
                        ToastAlert("error", error.message)
                        PlaySound()
                        this.getBloggerDetails(true);
                    }
                }
            );
        });
    }

    /**
     * renders image file preview
     * @returns
     */
    renderImageFilePreview() {
        const { portfolio, portfolio_data } = this.state;

        if ((portfolio_data && portfolio_data.length > 0) || (portfolio && portfolio.length > 0)) {
            return (
                <div className="d-block mt-3 mb-3 box_light_bg">
                    <ListGroup>
                        {portfolio_data && portfolio_data.length > 0 &&
                            portfolio_data.map((item, index) => {
                                return (
                                    <ListGroup.Item>
                                        <div>
                                            <span>{item.name}</span>
                                            <Tooltip title="Cancel">
                                                <IconButton
                                                    aria-label="cancel"
                                                    onClick={() => {
                                                        this.setState({ confirmRemoveFile: true, removeFileId: item.id });
                                                    }}
                                                >
                                                    <Close fontSize="small" />
                                                </IconButton>
                                            </Tooltip>
                                        </div>
                                    </ListGroup.Item>
                                );
                            }
                            )}
                        {portfolio && portfolio.length > 0 &&
                            portfolio.map((item, index) => {
                                return (
                                    <ListGroup.Item>
                                        <div>
                                            <span>{item.name}</span>
                                            <Tooltip title="Cancel">
                                                <IconButton
                                                    aria-label="cancel"
                                                    onClick={() => {
                                                        this.deleteImageFile(index)
                                                    }}
                                                >
                                                    <Close fontSize="small" />
                                                </IconButton>
                                            </Tooltip>
                                        </div>
                                    </ListGroup.Item>
                                );
                            }
                            )}
                    </ListGroup>
                </div>
            )
        }
    }

    /**
     * renders link preview
     * @returns
     */
    renderLinkPreview() {
        const { links } = this.state;
        var linkData = []
        links.forEach((link) => {
            if (link != '' && link != null && link != undefined)
                linkData.push(link);
        })
        if (linkData && linkData.length > 0) {
            return (
                <div className="d-block mt-3 mb-3 box_light_bg">
                    <ListGroup>
                        {linkData && linkData.length > 0 &&
                            linkData.map((item, index) => {
                                return (
                                    <ListGroup.Item>
                                        <div>
                                            <span>{item}</span>
                                            <Tooltip title="Cancel">
                                                <IconButton
                                                    aria-label="cancel"
                                                    onClick={() => {
                                                        const linksData = [...linkData]
                                                        linksData.splice(index, 1);
                                                        this.setState({ links: linksData.length != 0 ? linksData : [''] })
                                                    }}
                                                >
                                                    <Close fontSize="small" />
                                                </IconButton>
                                            </Tooltip>
                                        </div>
                                    </ListGroup.Item>
                                );
                            }
                            )}
                    </ListGroup>
                </div>
            )
        }
    }

    /**
     * removes image file from the portfolio state
     * @param {*} e
     */
    deleteImageFile = (e) => {
        const files = this.state.portfolio.filter((item, index) => index !== e);
        this.setState({
            portfolio: files
        })
    };

    /**
     * renders video file preview
     * @returns
     */
    renderVideoFilePreview() {
        const { about_video, about_video_data } = this.state;

        if ((about_video && about_video.length > 0) || about_video_data) {
            return (
                <div className="d-block mt-3 mb-3 box_light_bg">
                    {about_video_data && about_video.length === 0 &&
                        <div className="preview_uploaded" key={about_video_data}>
                            <video src={about_video_data.document} alt="" width="80" />
                            <span className="close_icon_btn"><button type="button" className="MuiButtonBase-root MuiIconButton-root" onClick={() => { this.setState({ confirmRemoveFile: true, removeFileId: about_video_data.id, isVideoRemove: true }); }}>x</button></span>
                        </div>
                    }
                    {about_video && about_video.length > 0 &&
                        about_video.map((item, index) => {
                            return (
                                <div className="preview_uploaded" key={item}>
                                    <video src={URL.createObjectURL(item)} alt="" width="80" />
                                    <span className="close_icon_btn">
                                        <button type="button" className="MuiButtonBase-root MuiIconButton-root" onClick={() => { this.deleteVideoFile(index) }}>x</button>
                                    </span>
                                </div>
                            );
                        }
                        )}
                </div>
            )
        }
    }

    /**
     * removes video file from the portfolio state
     * @param {*} e
     */
    deleteVideoFile = (e) => {
        const files = this.state.about_video.filter((item, index) => index !== e);
        this.setState({
            about_video: files
        })
    };

    /**
     * remove attachment API
     * @param {*} id
     */
    deleteAttachment = (id) => {
        if (this.state.isVideoRemove) {
            this.setState({
                about_video_data: ''
            })
            this.props.removeBloggerAttachment(id);
        } else {
            var portfolioData = this.state.portfolio_data
            portfolioData = portfolioData.filter((item) => item.id !== id);
            this.setState({
                portfolio_data: portfolioData, portfolio_delete_ids: [...this.state.portfolio_delete_ids, id]
            })
        }
    };

    /**
  * opens order create modal
  * @param {*} type 
  */
    onCreate = () => {
        this.setState({ isLoading: true });
        this.props.checkBillingInfo(
            (response) => {
                this.setState({ isLoading: false, isAddCustom: true });
            },
            (error) => {
                this.setState({ isLoading: false, isModalOpen: true });
            }
        );
    };

    /**
* opens contert writer exam create modal
* @param {*} type 
*/
    onContentWriterCreate = () => {
        this.setState({ isLoading: true });
        this.setState({ isLoading: false, isConterntWriterTestModelOpen: true });
    };

    /**
* opens contert writer exam create modal
* @param {*} type 
*/
    onContentWriterCreateCheck = () => {
        this.checkBillingInfo();
        this.setState({ isLoading: false, isContentWriterWarningModalOpen: false, isConterntWriterTestModelOpen: false, isThankyouModalOpen: false, isCongratulationModelOpen: false });
    }

    /**
     * renders blogger list data
     * @returns 
     */
    renderBloggerList = () => {
        const { bloggerListData, pageIndex, favoriteArray } = this.state;
        if (bloggerListData && bloggerListData.length === 0) {
            return (
                <div class="row" data-tour="content-writer">
                    <h3 className="norec_found">No Record Found</h3>
                </div>
            );
        } else {
            return (
                <div class="row pt-3" data-tour="content-writer">
                    {bloggerListData && bloggerListData.length !== 0 && bloggerListData.map((blogger, index) => {
                        return (
                            <div class="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-4 blogger-items">
                                <div className={index % 4 == 0 ? "card h-100 blogger_green" : index % 4 == 1 ? "card h-100 blogger_brown" : index % 4 == 2 ? "card h-100 blogger_blue" : "card h-100 blogger_red"}>
                                    <div class="row m-0">
                                        <div class="col-sm-12 p-0 text-left"><span className="price-info mt-2 mb-2">${blogger.rate}/<span className="perhr_span">Hr</span></span></div>
                                        <div className="products_image">
                                            <Link to={`/contentWriter/${blogger.user}`}>
                                                <div class="blogger_img">
                                                    <img
                                                        src={blogger.profilepic ? blogger.profilepic : avatar}
                                                        onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                                                        class="rounded-circle img-fluid"

                                                        alt="" />
                                                    <span class="user-status on"></span>
                                                </div>
                                            </Link>
                                        </div>
                                        <h5 className="product_title" title={blogger.username}>{blogger.username ? blogger.username : "User"}</h5>
                                        <div className="new_seller">
                                            {blogger.review === 0 &&
                                                <span className="new_seller_btn">New Content Writer</span>
                                            }
                                        </div>
                                        <div className="rating_star d-flex mt-1">
                                            {blogger.review !== 0 && Array.from(Array(Math.round(blogger.review)), () => {
                                                return (
                                                    <span className="fa fa-star"></span>
                                                )
                                            })}
                                        </div>
                                        <div class="sellers_list">
                                            {blogger && blogger.lastlogin_at != "" && 
                                                <span class="on-date small timeFont-Right">Last Login: {this.chatTimeFormat(blogger.lastlogin_at)}</span>
                                            }   
                                        </div>
                                        <div class="sellers_list">
                                            {blogger && blogger.lastlogin_at == "" && 
                                                <span class="on-date small timeFont-Right">Last Login: Hasn’t logged in</span>
                                            }   
                                        </div>
                                        <div className="btn-groups d-flex justify-content-center mt-3 mb-3 w-100">
                                            {(blogger.isown) ? "" :
                                                <a
                                                    className="favorite_btn mr-2"
                                                    id={"favoriteblogger-" + blogger.id}
                                                    onClick={() => {
                                                        if (blogger.isFavorite) {
                                                            if (favoriteArray.includes(blogger.id)) {
                                                                var favoriteData = favoriteArray;
                                                                let unlike = favoriteData.filter((elem) => elem !== blogger.id);
                                                                this.setState({ favoriteArray: unlike });
                                                            }
                                                            this.removeFavoriteBlogger(blogger.id)
                                                        } else {
                                                            if (!favoriteArray.includes(blogger.id)) {
                                                                this.setState({ favoriteArray: [...favoriteArray, blogger.id] });
                                                            }
                                                            this.addFavoriteBlogger(blogger.id);
                                                            this.setState({ isFavoriteModalOpen: true, favoriteModalContent: "You have favorited " + blogger.username + " as your go-to content writer" });
                                                        }
                                                    }}
                                                >
                                                    {favoriteArray.includes(blogger.id) ? <span class="fa fa-heart"></span> :
                                                        <span class="fa fa-heart-o"></span>}{" "}Favorite
                                                </a>}
                                            {/* <a className="favorite_btn mr-2" onClick={() => { blogger.isFavorite ? this.removeFavoriteBlogger(blogger.id) : this.addFavoriteBlogger(blogger.id)}}>
                                                {blogger.isFavorite && <span class="fa fa-heart"></span>}
                                                {!blogger.isFavorite && <span class="fa fa-heart-o"></span>}{" "}Favorite
                                            </a> */}
                                            {(blogger.isown) ? <Link class="view_btn" to={`/contentWriter/${blogger.user}`}>  View
                                            </Link> :
                                                <Link class="view_btn" to={`/contentWriter/${blogger.user}`}>
                                                    <span class="fa fa-eye"></span> View
                                                </Link>}
                                        </div>
                                        <div class="col-sm-12 col-4 p-0 text-center">
                                            <p className="blogger_btn px-2">{blogger.expertise && blogger.expertise.length > 0 && blogger.expertise.map((item, index) => {
                                                return <span key={`demo_snap_${index}`}>{(index ? ', ' : '') + item.name}</span>;
                                            })}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        )
                    })}
                </div>
            );
        }
    }
    handleBlockedNavigation = (lastLocation) => {

        if (!this.state.isDirtyModalOpen) {
            this.setState({ isDirtyModalOpen: true, lastLocation: lastLocation })
            return false;
        }
        return true;
    };

    handleCreateOption = (value) => {
        var newOptionTag = { label: value, value: value };
        var newValue = this.state.expertise;
        var tempValue = this.state.otherExpertise;
        newValue.push(newOptionTag);
        tempValue.push(newOptionTag);
        this.setState({
            expertise: newValue,
            otherExpertise: tempValue
        });
    }

    onFileChange = (e) => {
        if (e.target.files[0].size <= 1048576) {
            this.setState({ imgUrlToCrop: URL.createObjectURL(e.target.files[0]), openImageCrop: true })
        } else {
            ToastAlert('info', "Max size of 1MB allowed")
            PlaySound()
        }
    };

    /**
    * saves profile picture
    */
    saveProfilePic = (profilePic) => {
        let formData = new FormData();
        formData.append("profilepic", profilePic);
        this.setState({ isLoading: true });
        this.props.updateProfilePic(formData,
            profilepic => {
                this.setState({ isLoading: false });
                this.props.getProfilePic();
                this.props.myprofileAction();
                this.props.reLoginAction();
                ToastAlert('success', profilepic.message)
                PlaySound()
            },
            error => {
                this.setState({ isLoading: false });
                ToastAlert('error', error.message)
                PlaySound()
            }
        );
    }

    handleOnPriceRangeChange = (value, type) => {
        if (type == "minPrice") {
          this.setState({minPrice: value})
        } else if (type == "maxPrice") {
          this.setState({maxPrice: value})
        } else if (type == "minRate") {
            this.setState({minRate: value})
        } else if (type == "maxRate") {
            this.setState({maxRate: value})
        }
    };

    clearFilter = () => {
        this.setState({minPrice: "", maxPrice: "", minRate: "", maxRate: ""})
        this.getBloggerList(true);
    }

    /**
     * main render
     * @returns 
     */
    render() {
        const { showRating, showPrice, minPrice, lastlogin_at, maxPrice, minRate, maxRate, expertise, rate, wordsperhour, estimateddelivery, aboutme, links, activate_blogger, isLoading, confirmRemoveFile, removeFileId, isVideoRemove, isModalOpen, totalRecord, pageIndex, searchText, charCount, isFavoriteModalOpen, favoriteModalContent, isLinkModalOpen, submitted, isAddCustom, isContentWriterWarningModalOpen, isConterntWriterTestModelOpen, isThankyouModalOpen, isCongratulationModelOpen, isRejectedUserModelOpen, isFileModalOpen, portfolio_data, portfolio, isVideoModalOpen, about_video, about_video_data } = this.state;
        return (
            <div>
                <Prompt
                    when={this.state.shouldBlockNavigation}
                    message={this.handleBlockedNavigation}
                />
                {this.props.activeusertype === 1 &&
                    <div class="page-wrapper">
                        <Loading isVisible={isLoading} />
                        <TextLoading isVisible={this.state.isTextLoading} loadingText={this.state.loadingText} />
                        <div className="page_title row m-0 mb-4 pb-2">
                            <div class="col-lg-1 col-md-2 col-3 p-0">
                                <span className="left_icon_title">
                                    {/* <img src={blogger_icon} alt="" /> */}
                                    <i class="fa fa-blog"></i>
                                </span>
                            </div>
                            <div class="col-lg-8 col-md-10 col-9 p-0">
                                <h2 className="right_title row m-0">Content Writer Profile</h2>
                                <span className="title_tagline m-0 row">Become a content writer in the marketplace by filling out your content writer profile.</span>
                            </div>
                            <div class="col-lg-3 col-md-5 col-12 p-2 text-right">
                                <button
                                    className="btn_light_green"
                                    type="button"
                                    onClick={() => {
                                        this.onCreate();
                                    }}
                                ><span className="fa fa-plus"> </span> Create Custom Order&nbsp;&nbsp;
                                </button>
                            </div>
                        </div>
                        
                        <div class="grey_box_bg p-3">
                            <div class="content-wrapper" data-tour="blogger">
                                <div class="card-body">
                                    <div class="mx-0 form_fields_grey_bg">
                                        <div class="row">
                                            <div class="form-group col-md-12">
                                                <div class="row m-0 mb-3 align-items-center">
                                                    <div class="col-lg-2 col-md-4 col-sm-6 p-0">
                                                        <input type="file" id="upload-button" style={{ display: "none" }} onChange={this.onFileChange} hidden accept="image/*" />
                                                        <label htmlFor="upload-button" class="row m-0 mb-3 ml-2 cursor-pointer">
                                                            <img
                                                                src={this.props.profilePic ? this.props.profilePic.profilepic ? this.props.profilePic.profilepic : profileImage : profileImage}
                                                                onError={(e) => { e.target.onerror = null; e.target.src = profileImage }}
                                                                className="rounded-circle"
                                                                alt=""
                                                                width="100"
                                                                height="100"
                                                            />
                                                        </label>
                                                        <div class="cw_last_login">
                                                            <span class="on-date small timeFont-Right">Last Login: {this.chatTimeFormat(lastlogin_at)}</span>
                                                        </div> 
                                                    </div>
                                                    <div class="col-lg-10 col-md-8 col-sm-6 p-0">
                                                        <ImageCropperComponent
                                                            isOpen={this.state.openImageCrop}
                                                            onClose={() => { this.setState({ openImageCrop: false }) }}
                                                            aspectRatio={400 / 400}
                                                            width={400}
                                                            height={400}
                                                            src={this.state.imgUrlToCrop}
                                                            setCroppedImage={(image) => {
                                                                if (image.blobFile != '')
                                                                    this.saveProfilePic(image.blobFile);
                                                            }}
                                                        />
                                                        <input type="file" id="actual-btn" onChange={this.onFileChange} hidden accept="image/*" />
                                                        <label for="actual-btn" className="btn_light_green mr-1">Upload Photo</label><span class="required-red">*</span>
                                                        <p className="text-muted mt-1 col-md-6 m-0 p-0"> Allowed JPG or PNG. Max size of 1MB</p>
                                                    </div>
                                                </div>                        
                                            </div>
                                             
                                            <div class="form-group col-md-6">
                                                <label class="form-label">Expertise<span class="required-red">*</span></label>
                                                <CreatableSelect
                                                    className="basic-single-select"
                                                    placeholder="Select Expertise"
                                                    isMulti
                                                    isSearchable={true}
                                                    classNamePrefix={"select_dropdown"}
                                                    isValidNewOption={(inputValue, selectValue) =>
                                                        inputValue.length > 0 && selectValue.length < 10}
                                                    noOptionsMessage={() => {
                                                        return expertise == null
                                                            ? false
                                                            : expertise.length === 10
                                                                ? "You've reached the max number of options."
                                                                : "No options available";
                                                    }}
                                                    onChange={(selected) => {
                                                        // this.setState({ expertise: selected });
                                                        var newOptions = this.state.expertise;
                                                        newOptions = selected;
                                                        this.setState({
                                                            shouldBlockNavigation: true,
                                                            expertise: newOptions,
                                                        });
                                                    }}
                                                    options={this.props.serviceEstimatedExpertise}
                                                    value={expertise.length > 0 ? expertise : ''}
                                                    onCreateOption={(value) => {
                                                        this.handleCreateOption(value)
                                                    }}
                                                />
                                                {/* <input type="text" class="form-control" name="expertise" value={expertise} placeholder="Food, Oil" onChange={this.onBloggerChange} /> */}
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label">Rate/Hr<span class="required-red">*</span></label>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text border-0">$</span>
                                                    </div>
                                                    <input type="number" min="1" class="form-control" name="rate" value={rate} placeholder="15" onChange={this.onBloggerChange} />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text border-0">Hr</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label">Delivery Time<span class="required-red">*</span></label>
                                                <Select
                                                    className="basic-single-select"
                                                    placeholder="Select Delivery Time"
                                                    isClearable
                                                    isSearchable={false}
                                                    classNamePrefix={"select_dropdown"}
                                                    onChange={(selected) => {
                                                        this.setState({
                                                            estimateddelivery: selected,
                                                            shouldBlockNavigation: true,
                                                        });
                                                    }}
                                                    value={estimateddelivery}
                                                    options={this.props.serviceEstimatedDelivery}
                                                />
                                            </div>
                                            <div class="form-group col-md-6">
                                                <label class="form-label">Words/Hr<span class="required-red">*</span></label>
                                                <div class="input-group">
                                                    <input type="number" min="100" class="form-control" name="wordsperhour" value={wordsperhour} placeholder="100" onChange={this.onBloggerChange} />
                                                    <div class="input-group-append">
                                                        <span class="input-group-text border-0">Hr</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-12">
                                                <label class="form-label">About Me<span class="required-red">*</span></label>
                                                <textarea
                                                    class="form-control"
                                                    rows="4"
                                                    name="aboutme"
                                                    value={aboutme}
                                                    placeholder="Recent graduate in communications with a passion for technology, design, and engineering. Enjoy writing about mobile and web applications, artificial intelligence and Internet-of-Things (IoT), and the latest innovations and trends."
                                                    onChange={this.onBloggerChange}
                                                />
                                                <p className="text-muted mb-0">{charCount}/500</p>
                                            </div>
                                            <div className="col-md-6 d-flex justify-content-center mb-3">
                                                <div className="fileupload_bg">
                                                    <span className="file-upload-section">
                                                        <img src={upload_icon_image} alt="" width="50px" />
                                                        <span className="upload_text">
                                                            Upload Video "About Yourself"
                                                        </span>
                                                    </span>
                                                    <span className="d-block mt-3">
                                                        <label for="video-upload" className="btn_light_green ml-3">
                                                            <span className="fa fa-plus"></span>&nbsp;Add Video
                                                        </label>
                                                        <input id="video-upload" type="file" accept="video/*" hidden onChange={e => { this.onVideoChange(e); this.setState({ shouldBlockNavigation: true }); }} />
                                                        <label className="btn_light_green ml-3" onClick={() => this.setState({ isVideoModalOpen: true, modal_link_data: about_video_data, model_file_data: about_video })}>
                                                            View <span class="badge badge-light">{about_video.length}</span>
                                                        </label>
                                                    </span>
                                                    {/* {this.renderVideoFilePreview()} */}
                                                </div>
                                            </div>
                                            <div className="col-md-6 d-flex justify-content-center mb-3">
                                                <div className="fileupload_bg">
                                                    <span className="file-upload-section">
                                                        <img src={upload_icon_image} alt="" width="50px" />
                                                        <span className="upload_text">
                                                            Upload Portfolio<span class="required-red">*</span>
                                                        </span>
                                                    </span>
                                                    <span className="d-block mt-3">
                                                        <label for="file-upload" className="btn_light_green ml-3">
                                                            <span className="fa fa-plus"></span>&nbsp;Add Files
                                                        </label>
                                                        <input id="file-upload" type="file" multiple hidden onChange={e => { this.onImageChange(e); this.setState({ shouldBlockNavigation: true }); }} />
                                                        <label className="btn_light_green ml-3" onClick={() => this.setState({ isFileModalOpen: true, modal_file_data: portfolio_data, modal_file_data1: portfolio })}>
                                                            View  <span class="badge badge-light">{portfolio_data.length}</span>
                                                        </label>
                                                    </span>
                                                    <span className="d-block">

                                                    </span>
                                                    {/* {this.renderImageFilePreview()} */}
                                                </div>
                                            </div>
                                            <div className="col-md-12 d-flex justify-content-center mb-3">
                                                <div className="fileupload_bg">
                                                    <span className="file-upload-section">
                                                        <img src={icn_link} alt="" width="50px" />
                                                        <span className="upload_text">
                                                            Add Link(s)
                                                        </span>
                                                    </span>
                                                    <span className="d-block">
                                                        <label className="btn_light_green ml-3" onClick={() => this.setState({ isLinkModalOpen: true, modal_link_data: links })}>
                                                            <span className="fa fa-plus"></span>&nbsp;Add Link
                                                        </label>
                                                    </span>
                                                    {this.renderLinkPreview()}
                                                </div>
                                            </div>
                                            {confirmRemoveFile && (
                                                <ConfirmationDialog
                                                    open={confirmRemoveFile}
                                                    dialogTitle={isVideoRemove ? "Remove Uploaded Video" : "Remove Uploaded File"}
                                                    dialogContentText={isVideoRemove ? `Are you sure you want to remove this uploaded video?` : `Are you sure you want to remove this uploaded file?`}
                                                    cancelButtonText="Cancel"
                                                    okButtonText={"Remove"}
                                                    onCancel={() => {
                                                        this.setState({ confirmRemoveFile: false, removeFileId: '', isVideoRemove: false });
                                                    }}
                                                    onClose={() => {
                                                        this.setState({ confirmRemoveFile: false, removeFileId: '', isVideoRemove: false });
                                                    }}
                                                    onOk={(e) => {
                                                        this.deleteAttachment(removeFileId)
                                                        this.setState({ confirmRemoveFile: false, removeFileId: '', isVideoRemove: false });
                                                    }}
                                                />
                                            )}
                                            <div class="col-12 mb-3">
                                                {/* <p class="mt-3 required-red">* required fields</p> */}
                                                <div class="custom-control custom-checkbox ">
                                                    <input type="checkbox" class="custom-control-input" id="customCheckBlogger" checked={activate_blogger} onChange={this.checkBoxChange} />
                                                    <label class="custom-control-label text-dark" for="customCheckBlogger"> Activate your blogging profile</label>
                                                </div>
                                                <p class="mt-3 text-info ">Checking this box and clicking save will place your profile into the blogger marketplace and go live.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="text-center justify-content-center mb-3">
                                        <button
                                            class="btn_light_green effect effect-2"
                                            onClick={(e) => {
                                                e.currentTarget.blur();
                                                this.saveBlogger();
                                            }}
                                        >
                                            Save
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                }
                {this.props.activeusertype === 2 &&
                    <div class="page-wrapper">
                        <Loading isVisible={isLoading} />
                        <div className="page_title row m-0 mb-3 pb-2">
                            <div class="col-lg-1 col-md-2 col-3 p-0">
                                <span className="left_icon_title">
                                    {/* <img src={blogger_icon} alt="" /> */}
                                    <i class="fa fa-store"></i>
                                </span>
                            </div>
                            <div class="col-lg-11 col-md-10 col-9 p-0">
                                <h2 className="right_title row m-0">Content Writers</h2>
                                <span className="title_tagline m-0 row">Our vetted talented content writers are ready to write amazing content to grow your organic traffic.</span>
                            </div>
                        </div>
                        <div className="row p-0 col-sm-12 justify-content-between pt-0"> 
                            <div class="col-lg-8 col-md-7 col-sm-6 col-12 pr-0 form-group search-box search_boxes mb-3">
                                <input type="text" class="form-control" placeholder="Search for Content Writer..." name="searchText" value={searchText} onChange={this.onChangeSearchText} />
                                <span class="fa fa-search bg_icon_green"></span>
                            </div>
                            <div className="col-lg-4 col-md-5 col-sm-6 col-12 pr-0 filter_box_align justify-content-end btn_alignset mb-3"> 
                                <div ref={this.wrapperRef}>
                                    <OverlayTrigger trigger="click" id="price" show={showPrice} placement="bottom" overlay={(
                                        <Popover id="popover-advance" onClick ={() => {this.setState({showPrice : true})}} >
                                            <div className="row align-items-center price_filter_tag p-3" >
                                                <div className="price-range-sec col-lg-12 row m-0 p-0 mb-3 mt-2">
                                                    <div class="col-sm-6 padleftzero">
                                                    <div class="form-group mb-0">
                                                        <label>Min</label>
                                                        <div class="input-group search-box">                  
                                                        <input
                                                            type="number"
                                                            className="form-control border-0 p-1"
                                                            placeholder="Min Cost"
                                                            name="min"
                                                            min="0"
                                                            value={minPrice}
                                                            onChange={e => this.handleOnPriceRangeChange(e.target.value, "minPrice")}
                                                        />
                                                        <div class="input-group-append cont_bg">
                                                            <span class="input-group-text border-0">$</span>
                                                        </div>
                                                        </div>
                                                    </div>
                                                    </div>
                                                    <div class="col-sm-6 padleftzero">
                                                        <div class="form-group mb-0">
                                                            <label>Max</label>  
                                                            <div class="input-group search-box">                 
                                                            <input
                                                                type="number"
                                                                className="form-control border-0 p-1"
                                                                placeholder="Max Cost"
                                                                name="max"
                                                                min="0"
                                                                value={maxPrice}
                                                                onChange={e => this.handleOnPriceRangeChange(e.target.value, "maxPrice")}
                                                            />           
                                                            <div class="input-group-append cont_bg">
                                                                <span class="input-group-text border-0">$</span>
                                                            </div>               
                                                            </div>
                                                        </div>
                                                    </div>                      
                                                </div>
                                                <div className="col-sm-12 row m-0 pt-3 bortopside"> 
                                                <div className="col-sm-6 p-0"> 
                                                    <button
                                                        type="button"
                                                        onClick={this.clearFilter}
                                                        className="btn_grey w-auto"
                                                        >
                                                        Reset
                                                        </button>  
                                                    </div>
                                                    <div className="col-sm-6 p-0 text-right"> 
                                                    <button
                                                        type="button"
                                                        onClick={(e) => { 
                                                        e.currentTarget.blur();
                                                        this.onChangeFilterText("price");
                                                        }}
                                                        className="btn_light_green"
                                                    >Apply
                                                    </button>                   
                                                </div>
                                                </div>
                                            </div>
                                            </Popover>)}
                                        >
                                        <button className="btn_light_green mr-3" onClick={() => this.setState({showRating: false, showPrice: !showPrice})} type="button" variant="success">Price Filter</button>
                                    </OverlayTrigger>
                                    <OverlayTrigger trigger="click" id="rating" show={showRating} placement="bottom" overlay={(  
                                    <Popover id="popover-advance" onClick ={() => {this.setState({showRating : true})}}>
                                        <div className="row align-items-center price_filter_tag p-3">
                                            <div className="price-range-sec col-lg-12 row m-0 p-0 mb-3 mt-2">
                                                <div class="col-sm-6 padleftzero">
                                                <div class="form-group mb-0">
                                                    <label>Min</label>
                                                    <div class="input-group search-box">                  
                                                    <input
                                                        type="number"
                                                        className="form-control border-0 p-1"
                                                        placeholder="Min Rate"
                                                        name="min"
                                                        min="0"
                                                        value={minRate}
                                                        onChange={e => this.handleOnPriceRangeChange(e.target.value, "minRate")}
                                                    />
                                                    <div class="input-group-append cont_bg">
                                                        <span class="input-group-text border-0">$</span>
                                                    </div>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col-sm-6 padleftzero">
                                                <div class="form-group mb-0">
                                                    <label>Max</label>  
                                                    <div class="input-group search-box">                 
                                                    <input
                                                        type="number"
                                                        className="form-control border-0 p-1"
                                                        placeholder="Max Rate"
                                                        name="max"
                                                        min="0"
                                                        max="5"
                                                        value={maxRate}
                                                        onChange={e => this.handleOnPriceRangeChange(e.target.value, "maxRate")}
                                                    />           
                                                    <div class="input-group-append cont_bg">
                                                        <span class="input-group-text border-0">$</span>
                                                    </div>               
                                                    </div>
                                                </div>
                                                </div>                      
                                            </div>
                                            <div className="col-sm-12 row m-0 pt-3 bortopside"> 
                                            <div className="col-sm-6 p-0"> 
                                                <button
                                                    type="button"
                                                    onClick={this.clearFilter}
                                                    className="btn_grey w-auto"
                                                    >
                                                    Reset
                                                    </button>  
                                                </div>
                                                <div className="col-sm-6 p-0 text-right"> 
                                                <button
                                                    type="button"
                                                    onClick={(e) => { 
                                                        e.currentTarget.blur();
                                                        this.onChangeFilterText("rating");
                                                    }}
                                                    className="btn_light_green"
                                                >Apply
                                                </button>                   
                                            </div>
                                            </div>
                                        </div>
                                        </Popover>)}
                                    >
                                    <button className="btn_light_green" onClick={() => this.setState({showPrice: false, showRating: !showRating})} type="button" variant="success">Rating Filter</button>
                                </OverlayTrigger>
                                </div>
                            </div>
                        </div>
                        <div class="grey_box_bg p-3">
                            <div class="col-12">
                                {this.renderBloggerList()}
                            </div>
                            {totalRecord !== 0 &&
                                <div className="favorite-pagination">
                                    <Pagination
                                        count={totalRecord}
                                        onChange={(event, val) => {
                                            if (pageIndex !== val) {
                                                this.onPageChange(val);
                                            }
                                        }}
                                        page={pageIndex}
                                        variant="outlined"
                                        color="primary"
                                    />
                                </div>
                            }
                        </div>
                    </div>
                }
                <AddNewCustom
                    isVisible={isAddCustom}
                    onClose={() => {
                        this.setState({ isAddCustom: false });
                    }}
                    onSuccess={() => {
                        this.setState({ isAddCustom: false });
                        this.props.history.push("/inbox")
                    }}
                    onFailure={() => {
                        this.setState({ isAddCustom: false });
                    }}
                    {...this.props}
                />
                <Modal
                    open={isModalOpen}
                    disableBackdropClick
                    onClose={() => { this.closeModal() }}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description">
                    <div className="modal-dialog modal_popup" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="popup_title" id="exampleModalLabel">Action not allowed</h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                    onClick={() => { this.closeModal() }}
                                >
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <br />
                            <div className="modal-body justify-content-center pt-3 pb-3 d-flex flex-column align-items-center text-center">
                                <i className="fa fa-times-circle-o fa-2x" aria-hidden="true"></i>
                                <h4 className="sub_title_dash mt-3 mb-3 col-lg-10 mx-auto">Please fill out billing information first to sign up as a content writer.</h4>
                            </div>
                            <br />
                            <div className="modal-footer justify-content-center">
                                <Link
                                    to={"/profile/billing"}
                                    onClick={() => { this.setState({ isModalOpen: false }); }}
                                    className="btn_light_green"
                                >
                                    Go to Billing Information
                                </Link>
                            </div>
                        </div>
                    </div>
                </Modal>

                {/* content writer test warning model */}
                <ContentWriterWarning
                    isVisible={isContentWriterWarningModalOpen}
                    onClose={() => {
                        this.setState({ isContentWriterWarningModalOpen: false });
                        this.props.history.push("/sellerDashboard")
                    }}
                    onSuccess={() => {
                        this.setState({ isLoading: false, isConterntWriterTestModelOpen: true });
                    }}
                    onFailure={() => {
                        this.setState({ isContentWriterWarningModalOpen: false });
                    }}
                    {...this.props}
                />
                {/* open content writer test model */}
                <ContentWriterTest
                    isVisible={isConterntWriterTestModelOpen}
                    onClose={() => {
                        this.setState({ isConterntWriterTestModelOpen: false });
                        this.props.history.push("/sellerDashboard")
                    }}
                    onSuccess={() => {
                        this.setState({ isConterntWriterTestModelOpen: false, isThankyouModalOpen: true, isContentWriterWarningModalOpen: false });
                        this.props.history.push("/contentWriter")
                    }}
                    onFailure={() => {
                        this.setState({ isConterntWriterTestModelOpen: false });
                    }}
                    {...this.props}
                />
                {/* Thank you contentWriter test model */}
                <ContentWriterThankYou
                    isVisible={isThankyouModalOpen}
                    onClose={() => {
                        this.setState({ isThankyouModalOpen: false });
                        this.props.history.push("/sellerDashboard")
                    }}
                    onSuccess={() => {
                        this.setState({ isThankyouModalOpen: false });
                        this.props.history.push("/contentWriter")
                    }}
                    onFailure={() => {
                        this.setState({ isThankyouModalOpen: false });
                    }}
                    {...this.props}
                />
                {/* congratulation contentWriter test model */}
                <ContentWriterCongrats
                    isVisible={isCongratulationModelOpen}
                    onClose={() => {
                        this.setState({
                            isCongratulationModelOpen: false,
                        })
                        this.props.history.push("/sellerDashboard")
                    }}
                    onSuccess={() => {
                        this.setState({ isLoading: false, isContentWriterWarningModalOpen: false, isConterntWriterTestModelOpen: false, isThankyouModalOpen: false, isCongratulationModelOpen: false });
                        this.props.checkCongratulation(false);
                        this.checkBillingInfo();
                        // this.props.history.push("/contentWriter")
                    }}
                    onFailure={() => {
                        this.setState({ isCongratulationModelOpen: false });
                    }}
                    {...this.props}
                />
                {/* reject contentWriter test model */}
                <ContentWriterReject
                    isVisible={isRejectedUserModelOpen}
                    onClose={() => {
                        this.setState({ isRejectedUserModelOpen: false });
                        this.props.history.push("/sellerDashboard")
                    }}
                    onSuccess={() => {
                        this.setState({ isRejectedUserModelOpen: false });
                        this.props.history.push("/contentWriter")
                    }}
                    onFailure={() => {
                        this.setState({ isRejectedUserModelOpen: false });
                    }}
                    {...this.props}
                />

                <Modal
                    open={isFavoriteModalOpen}
                    disableBackdropClick
                    onClose={() => {
                        this.setState({ isFavoriteModalOpen: false, favoriteModalContent: '' });
                    }}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description">
                    <div className="modal-dialog modal_popup" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="popup_title" id="exampleModalLabel">Favorited</h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                    onClick={() => {
                                        this.setState({ isFavoriteModalOpen: false, favoriteModalContent: '' });
                                    }}
                                >
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body justify-content-center">
                                {/* <i className="fa fa-heart heartred fa-5x" style={{"padding-left": "189px"}}aria-hidden="true"></i> */}
                                <h4 className="text-center">{favoriteModalContent}</h4>
                            </div>
                            <div className="modal-footer justify-content-center">
                                <button
                                    onClick={() => {
                                        this.setState({ isFavoriteModalOpen: false, favoriteModalContent: '' });
                                    }}
                                    className="btn_light_green"
                                >
                                    OK
                                </button>
                            </div>
                        </div>
                    </div>
                </Modal>
                <Modal
                    open={isLinkModalOpen}
                    disableBackdropClick
                    onClose={() => {
                        this.closeLinkModal();
                    }}
                    style={{ overflow: "scroll" }}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description">
                    <div className="modal-dialog modal_popup" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="popup_title" id="exampleModalLabel">Add Link(s)</h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                    onClick={() => {
                                        this.closeLinkModal();
                                    }}
                                >
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body justify-content-center">
                                {links && links.length > 0 && links.map((element, index) => (
                                    <div className="row" key={index}>
                                        <div className="form-group col-md-10 text-center">
                                            <input id={index} type="text" className={"form-control " + ((submitted && (element == '' || (element != '' && !urlRegex.test(element)))) ? "is-invalid" : "")} name="link" value={element} placeholder="https://example.com/" onChange={(e) => this.onChangeLink(index, e.target.value)} />
                                            {submitted && element == '' && (
                                                <span id="email-error" className="error invalid-feedback text-left">Link should not be blank.</span>
                                            )}
                                            {submitted && element != '' && !urlRegex.test(element) && (
                                                <span id="email-error" className="error invalid-feedback text-left">Enter a valid link.</span>
                                            )}
                                        </div>
                                        {index == 0 ?
                                            (
                                                links.length != 10 &&
                                                <div className="form-group col-md-2 text-center">
                                                    <button
                                                        className="btn_light_green"
                                                        onClick={() => {
                                                            if (links.length > 9) {
                                                                ToastAlert('info', "You can only add a maximum of 10 links")
                                                                PlaySound()
                                                            } else {
                                                                this.setState({ links: [...links, ''] })
                                                            }
                                                        }}
                                                        title="Add More"
                                                    >
                                                        <span className="fa fa-plus-square"></span>
                                                    </button>
                                                </div>
                                            ) : (
                                                <div className="form-group col-md-2 text-center">
                                                    <button
                                                        className="btn_remove_red"
                                                        onClick={() => {
                                                            const linkData = [...links];
                                                            linkData.splice(index, 1);
                                                            this.setState({ links: linkData })
                                                        }}
                                                        title="Remove"
                                                    >
                                                        <span className="fa fa-trash-o"></span>
                                                    </button>
                                                </div>
                                            )
                                        }
                                    </div>
                                ))}
                            </div>
                            <div className="modal-footer justify-content-center">
                                <button
                                    onClick={() => {
                                        this.submitLink();
                                    }}
                                    className="btn_light_green"
                                >
                                    Add Link
                                </button>
                            </div>
                        </div>
                    </div>
                </Modal>
                <Modal
                    open={isFileModalOpen}
                    disableBackdropClick
                    onClose={() => {
                        this.closeFileModal();
                    }}
                    style={{ overflow: "scroll" }}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description">
                    <div className="modal-dialog modal_popup" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="popup_title" id="exampleModalLabel">Uploaded Files</h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                    onClick={() => {
                                        this.closeFileModal();
                                    }}
                                >
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="d-block mt-3 mb-3 box_light_bg">
                                <ListGroup>
                                    {portfolio_data && portfolio_data.length > 0 &&
                                        portfolio_data.map((item, index) => {
                                            return (
                                                <ListGroup.Item>
                                                    <div>
                                                        <span>{item.name}</span>
                                                        <Tooltip title="Cancel">
                                                            <IconButton
                                                                aria-label="cancel"
                                                                onClick={() => {
                                                                    this.setState({ confirmRemoveFile: true, removeFileId: item.id });
                                                                }}
                                                            >
                                                                <Close fontSize="small" />
                                                            </IconButton>
                                                        </Tooltip>
                                                    </div>
                                                </ListGroup.Item>
                                            );
                                        }
                                        )}
                                    {portfolio && portfolio.length > 0 &&
                                        portfolio.map((item, index) => {
                                            return (
                                                <ListGroup.Item>
                                                    <div>
                                                        <span>{item.name}</span>
                                                        <Tooltip title="Cancel">
                                                            <IconButton
                                                                aria-label="cancel"
                                                                onClick={() => {
                                                                    this.deleteImageFile(index)
                                                                }}
                                                            >
                                                                <Close fontSize="small" />
                                                            </IconButton>
                                                        </Tooltip>
                                                    </div>
                                                </ListGroup.Item>
                                            );
                                        }
                                        )}
                                    {
                                        ((portfolio && portfolio.length == 0) && (portfolio_data && portfolio_data.length == 0)) &&
                                        <h1 className="small muted">No files are added. Please add files to view</h1>
                                    }
                                </ListGroup>
                            </div>
                        </div>
                    </div>
                </Modal>
                <Modal
                    open={isVideoModalOpen}
                    disableBackdropClick
                    onClose={() => {
                        this.closeVideoModal();
                    }}
                    style={{ overflow: "scroll" }}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description">
                    <div className="modal-dialog modal_popup" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="popup_title" id="exampleModalLabel">Uploaded Videos</h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                    onClick={() => {
                                        this.closeVideoModal();
                                    }}
                                >
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="d-block mt-3 mb-3 box_light_bg">
                                {about_video_data && about_video.length === 0 &&
                                    <div className="preview_uploaded" key={about_video_data}>
                                        <video src={about_video_data.document} alt="" width="80" />
                                        <span className="close_icon_btn"><button type="button" className="MuiButtonBase-root MuiIconButton-root" onClick={() => { this.setState({ confirmRemoveFile: true, removeFileId: about_video_data.id, isVideoRemove: true }); }}>x</button></span>
                                    </div>
                                }
                                {about_video && about_video.length > 0 &&
                                    about_video.map((item, index) => {
                                        return (
                                            <div className="preview_uploaded" key={item}>
                                                <video src={URL.createObjectURL(item)} alt="" width="80" />
                                                <span className="close_icon_btn">
                                                    <button type="button" className="MuiButtonBase-root MuiIconButton-root" onClick={() => { this.deleteVideoFile(index) }}>x</button>
                                                </span>
                                            </div>
                                        );
                                    }
                                    )}
                                {
                                    (about_video && about_video.length == 0 && (about_video_data == '' || about_video_data == undefined)) &&
                                    <h1 className="small muted">No files are added. Please add files to view</h1>
                                }

                            </div>
                        </div>
                    </div>
                </Modal>
                <Modal
                    open={this.state.isDirtyModalOpen}
                    disableBackdropClick
                    onClose={() => {
                        this.setState({ isDirtyModalOpen: false });
                    }}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description">
                    <div className="modal-dialog modal_popup" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="popup_title" id="exampleModalLabel">Close without saving?</h5>
                                <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                    onClick={() => {
                                        this.setState({ isDirtyModalOpen: false });
                                    }}
                                >
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body justify-content-center">
                                <h5 className="text-center">You have unsaved changes. Are you sure you want to leave this page without saving?</h5>
                            </div>
                            <div className="modal-footer justify-content-center">
                                <button
                                    onClick={() => {
                                        this.props.history.push(this.state.lastLocation);
                                        this.setState({ isDirtyModalOpen: false, shouldBlockNavigation: false });
                                    }}
                                    className="btn_light_green"
                                >
                                    Yes
                                </button>
                                <button
                                    onClick={() => {
                                        this.setState({ isDirtyModalOpen: false });
                                    }}
                                    className="btn_ebony_clay"
                                >
                                    No
                                </button>
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = ({ profileReducer, mainReducer, loginReducer, inboxReducer }) => {
    return {
        serviceEstimatedExpertise: mainReducer.serviceEstimatedExpertise,
        serviceEstimatedDelivery: mainReducer.serviceEstimatedDelivery,
        activeusertype: loginReducer.userData.activeusertype,
        profilePic: profileReducer.profilePic,
        userData: loginReducer.userData,
        headerInfo: inboxReducer.headerInfo,
    }
}

export default connect(mapStateToProps, {
    getProfilePic,
    myprofileAction,
    reLoginAction,
    updateProfilePic,
    getHeaderInfo,
    getViewQuestions,
    addQuestions,
    getBloggerList,
    getHeaderInfo,
    searchBloggerList,
    filterBloggerList,
    getBloggerDetails,
    addBloggerDetails,
    addBloggerPortfolio,
    addBlogLink,
    removeBlogLink,
    addBloggerAboutMe,
    removeBloggerAttachment,
    checkBillingInfo,
    checkCongratulation,
    addFavoriteBlogger,
    removeFavoriteBlogger,
    addCustomOrder,
    searchServiceUsers
})(Blogger);
