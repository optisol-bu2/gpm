import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Loading from "../../components/core/Loading";
import Modal from "@material-ui/core/Modal";
import ContactSeller from "../../components/ContactSeller/ContactSeller";
import avatar from '../../assets/images/avatar.png';
import blogger_icon from "../../assets/images/menu-icons/blogger-white.svg";
import icnDownload from "../../assets/images/icn_download.svg";
import { getBloggerProfile, addCartBlogger } from "../../Actions/bloggerAction";
import { NavigateBeforeSharp } from "@material-ui/icons";
import { PlaySound, ToastAlert } from "../../utils/sweetalert2";
import _ from 'lodash';

/**
 * BloggerDetail Component
 */
class BloggerDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            bloggerProfileData: '',
            about_video: '',
            expertise: [],
            orderOption: '',
            wordsCount: '',
            hrCount: '',
            orderPrice: 0,
            isLoading: null,
            isModalOpen: false,
            disableOrderSubmit : false
        };

    }

    componentDidMount() {
        if (this.props.match.params.id) {
            this.getBloggerProfile(this.props.match.params.id);
        }
    }

    /**
     * gets blogger profile data
     * @param {*} id 
     */
    getBloggerProfile = (id) => {
        this.setState({ isLoading: true });
        this.props.getBloggerProfile(id,
            (bloggerProfile) => {
                if (bloggerProfile.data) {
                    var finalExpertise = [];
                    _.filter(bloggerProfile.data.expertise, function (value) {
                        finalExpertise.push(value.name)
                        return (value);
                    })
                    this.setState({
                        bloggerProfileData: bloggerProfile.data,
                       // expertise: bloggerProfile.data.expertise ? bloggerProfile.data.expertise.split(',') : [],
                        expertise: finalExpertise,
                        about_video: bloggerProfile.data.aboutme_document ? bloggerProfile.data.aboutme_document.document : "",
                        isLoading: false
                    });
                }
            },
            (error) => {
                this.setState({ isLoading: false, bloggerProfileData: '', expertise: [], about_video: '' });
            }
        );
    }

    /**
     * adds blogger to cart
     * @param {*} id 
     */
    addCartBlogger = () => {
        const { bloggerProfileData, hrCount, wordsCount } = this.state;
        if(wordsCount < bloggerProfileData.wordsperhour) {
            ToastAlert('error', "Minimum " + bloggerProfileData.wordsperhour + " Words Required")
        } else {
            let params = {
                blogger: bloggerProfileData.id,
                noofhours: hrCount,
                noofwords: wordsCount
            }
            this.setState({ isLoading: true });
            this.setState({ disableOrderSubmit: true });
            this.props.addCartBlogger(params,
                (bloggerCart) => {
                    this.setState({ isLoading: false });
                    this.setState({ disableOrderSubmit: false });
                    PlaySound();
                    this.props.history.push("/cartdetails");
                },
                (error) => {
                    this.setState({ isLoading: false });
                    this.setState({ disableOrderSubmit: false });
                }
            );
        }
    }

    /**
     * closes the modal
     */
    closeModal = () => {
        this.setState({ isModalOpen: false, orderOption: '', hrCount: 0, orderPrice: 0, wordsCount: '' })
    }

    /**
     * sets order option 
     * @param {*} orderOption 
     */
    setOrderOptions = (orderOption) => {
        if (this.state.bloggerProfileData.wordsperhour !== 0) {
            if (orderOption !== "Other") {
                this.setState({ orderOption: orderOption, hrCount: orderOption, wordsCount: orderOption * this.state.bloggerProfileData.wordsperhour, orderPrice: this.floatFormat(parseFloat(orderOption) * parseFloat(this.state.bloggerProfileData.rate)) });
            } else {
                this.setState({ orderOption: orderOption, hrCount: 0, wordsCount: '', orderPrice: 0 });
            }
        } else {
            this.setState({ orderOption: orderOption, hrCount: 0, wordsCount: '', orderPrice: 0 });
        }
    }

    /**
     * handles field changes and assigns it to the state
     * @param {*} e 
     */
    onBloggerChange = (e) => {
        console.log(this.floatFormat(e.target.value / this.state.bloggerProfileData.wordsperhour),"this.floatFormat(e.target.value / this.state.bloggerProfileData.wordsperhour)")
        if (this.floatFormat(e.target.value / this.state.bloggerProfileData.wordsperhour) <= 12) {
            if (this.state.bloggerProfileData.wordsperhour !== 0) {
                this.setState({ [e.target.name]: e.target.value, hrCount: this.floatFormat(e.target.value / this.state.bloggerProfileData.wordsperhour) }, () => {
                    this.setState({ orderPrice: this.floatFormat(parseFloat(this.state.hrCount) * parseFloat(this.state.bloggerProfileData.rate)) });
                })
            } else {
                this.setState({ [e.target.name]: e.target.value, hrCount: 0, orderPrice: 0 });
            }
        }
    }

    /**
     * formats given string to float
     * @param {*} string 
     * @returns 
     */
    floatFormat = (string) => {
        return parseFloat(string).toFixed(2).replace(/[.,]00$/, "")
    }

    /**
     * main render
     * @returns 
     */
    render() {
        const { bloggerProfileData, disableOrderSubmit, about_video, expertise, isLoading, isModalOpen, orderOption, wordsCount, hrCount, orderPrice } = this.state;
        return (
            <div class="page-wrapper">
                <Loading isVisible={isLoading} />
                <div className="page_title row m-0 mb-4 pb-2">
                    <div class="col-lg-1 col-md-2 col-3 p-0">
                        <span className="left_icon_title"><img src={blogger_icon} alt="" /></span>
                    </div>
                    <div class="col-lg-11 col-md-10 col-9 p-0 page_title">
                        <h2 className="right_title row m-0">Content Writer Profile</h2>
                        <span className="title_tagline m-0 row">
                            <ol className="breadcrumb p-0 bg-none mb-0">
                                <li className="breadcrumb-item">
                                    <Link to={{ pathname: "/dashboard" }}>
                                        <i className="fa fa-home"></i>
                                    </Link>
                                </li>
                                <li className="breadcrumb-item">
                                    <Link to={{ pathname: "/contentWriter" }}>Content Writers</Link>
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">
                                    Profile
                                </li>
                            </ol>
                        </span>
                    </div>
                </div>
                <div class="grey_box_bg p-3 mb-4">
                    <div class="content-wrapper">
                        <div class="card-body blogger_profile">
                            <div class="d-flex w-100 justify-content-between align-items-center flex-wrap flex-column flex-md-row">
                                <div class="row w-100 d-flex justify-content-start align-items-center flex-wrap flex-column flex-md-row">
                                    <div class="blogger_img blogger_img_size col-lg-2">
                                        <img
                                            src={bloggerProfileData.profilepic ? bloggerProfileData.profilepic : avatar}
                                            onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                                            class="rounded-circle img-fluid shadow"
                                            width="120"
                                            alt=""
                                        />
                                        <span class="user-status on"></span>
                                    </div>
                                    <div class="d-flex bg_blogger_top col-lg-10 align-content-center justify-content-between">
                                        <div class="ml-0 ml-md-3 text-center text-md-left mt-3 mt-md-0">
                                            <h5 class="font-weight-bold">{bloggerProfileData.username ? bloggerProfileData.username : "User"}</h5>
                                            <div class="rating my-2 justify-content-center justify-content-md-start">
                                                {bloggerProfileData.review !== 0 && bloggerProfileData.review !== undefined && Array.from(Array(Math.round(bloggerProfileData.review)), () => {
                                                    return (
                                                        <div className="star"></div>
                                                    )
                                                })}
                                                {bloggerProfileData.review === 0 &&
                                                    <div className="text-muted">New Content Writer</div>
                                                }
                                            </div>
                                            <div className="d-flex">
                                                {about_video &&
                                                    <a
                                                        class="btn_light_green pl-2 pr-2 pt-1 pb-1 mr-3 about_me"
                                                        disabled={!about_video}
                                                        data-fancybox="video"
                                                        data-width="640"
                                                        data-height="360"
                                                        href={about_video}
                                                        target="_blank"
                                                    >
                                                        <i class="fa fa-play-circle"></i> About Me
                                                    </a>
                                                }
                                                <ContactSeller
                                                    receiverId={bloggerProfileData.user ? bloggerProfileData.user : ""}
                                                    receiverImage={bloggerProfileData.profilepic ? bloggerProfileData.profilepic : avatar}
                                                    receiverName={bloggerProfileData.username ? bloggerProfileData.username : "User"}
                                                    bloggerProfile={true}
                                                />
                                            </div>
                                        </div>
                                        <div class="text-center ml-md-auto mt-3 mt-md-0 ">
                                            <p class="font-weight-bold">${bloggerProfileData.rate}/Hr</p>
                                            {(this.props.userData ? this.props.userData.id : "") === (bloggerProfileData.user ? bloggerProfileData.user : "") ? "" :
                                                <button
                                                    class="btn_ebony_clay ordernow_btn effect effect-2 mt-4"
                                                    disabled={(this.props.userData ? this.props.userData.id : "") === (bloggerProfileData.user ? bloggerProfileData.user : "")}
                                                    onClick={(e) => {
                                                        e.currentTarget.blur();
                                                        this.setState({ isModalOpen: true })
                                                    }}
                                                >
                                                    Order Now
                                                </button>}
                                        </div>
                                    </div>
                                </div>

                                <Modal
                                    open={isModalOpen}
                                    className="modal_popup"
                                    disableBackdropClick
                                    onClose={() => { this.closeModal() }}
                                    aria-labelledby="simple-modal-title"
                                    aria-describedby="simple-modal-description">
                                    <div className="modal-dialog modal_popup" role="document">
                                        <div className="modal-content">
                                            <div className="modal-header">
                                                <h5 className="popup_title" id="exampleModalLabel">Order options</h5>
                                                <button
                                                    type="button"
                                                    className="close"
                                                    data-dismiss="modal"
                                                    aria-label="Close"
                                                    onClick={() => { this.closeModal() }}
                                                >
                                                    <span aria-hidden="true">×</span>
                                                </button>
                                            </div>
                                            <br />
                                            <div className="modal-body form_fields_white_bg">
                                                <div className="radio-float-left custom-control custom-radio mt-3">
                                                    <input
                                                        type="radio"
                                                        id="customRadio1hr"
                                                        name="customRadio"
                                                        className="custom-control-input"
                                                        checked={orderOption == '1'}
                                                        onChange={() => { this.setOrderOptions('1') }}
                                                    ></input>
                                                    <label
                                                        className="custom-control-label h6 text-dark font-weight-bold"
                                                        for="customRadio1hr"
                                                    >
                                                        {bloggerProfileData.wordsperhour} Words (1 Hr)
                                                    </label>
                                                </div>
                                                <div className="radio-float-left custom-control custom-radio mt-3">
                                                    <input
                                                        type="radio"
                                                        id="customRadio2hr"
                                                        name="customRadio"
                                                        className="custom-control-input"
                                                        checked={orderOption == '2'}
                                                        onChange={() => { this.setOrderOptions('2') }}
                                                    ></input>
                                                    <label
                                                        className="custom-control-label h6 text-dark font-weight-bold"
                                                        for="customRadio2hr"
                                                    >
                                                        {bloggerProfileData.wordsperhour * 2} Words (2 Hr)
                                                    </label>
                                                </div>
                                                <div className="radio-float-left custom-control custom-radio mt-3">
                                                    <input
                                                        type="radio"
                                                        id="customRadio3hr"
                                                        name="customRadio"
                                                        className="custom-control-input"
                                                        checked={orderOption == '3'}
                                                        onChange={() => { this.setOrderOptions('3') }}
                                                    ></input>
                                                    <label
                                                        className="custom-control-label h6 text-dark font-weight-bold"
                                                        for="customRadio3hr"
                                                    >
                                                        {bloggerProfileData.wordsperhour * 3} Words (3 Hr)
                                                    </label>
                                                </div>
                                                <div className="radio-float-left custom-control custom-radio mt-3">
                                                    <input
                                                        type="radio"
                                                        id="customRadio4hr"
                                                        name="customRadio"
                                                        className="custom-control-input"
                                                        checked={orderOption == '4'}
                                                        onChange={() => { this.setOrderOptions('4') }}
                                                    ></input>
                                                    <label
                                                        className="custom-control-label h6 text-dark font-weight-bold"
                                                        for="customRadio4hr"
                                                    >
                                                        {bloggerProfileData.wordsperhour * 4} Words (4 Hr)
                                                    </label>
                                                </div>
                                                <div className="radio-float-left custom-control custom-radio mt-3">
                                                    <input
                                                        type="radio"
                                                        id="customRadioOther"
                                                        name="customRadio"
                                                        className="custom-control-input"
                                                        checked={orderOption == 'Other'}
                                                        onChange={() => { this.setOrderOptions('Other') }}
                                                    ></input>
                                                    <label
                                                        className="custom-control-label h6 text-dark font-weight-bold"
                                                        for="customRadioOther"
                                                    >
                                                        Other
                                                    </label>
                                                    {orderOption == 'Other' &&
                                                        <div class="input-group input-group-blogger col-4 p-0 pl-4 mt-2">
                                                            <input type="number" min="4" class="form-control" name="wordsCount" value={wordsCount} placeholder={bloggerProfileData.wordsperhour} onChange={this.onBloggerChange} />
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">{hrCount}{hrCount > 1 ? "Hrs" : "Hr"}</span>
                                                            </div>
                                                        </div>
                                                    }
                                                </div>
                                            </div>
                                            <br />
                                            <div className="modal-footer justify-content-center">
                                                <button
                                                    class="btn_light_green"
                                                    disabled={disableOrderSubmit || !orderPrice || orderPrice === '0' || (this.props.userData ? this.props.userData.id : "") === (bloggerProfileData.user ? bloggerProfileData.user : "")}
                                                    onClick={() => { 
                                                        this.addCartBlogger() 
                                                    }}
                                                >
                                                    Add to Cart&nbsp;{(orderPrice && orderPrice !== '0') ? "( $" + orderPrice + " )" : ''}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </Modal>
                                <div>
                                </div>
                            </div>
                        </div>
                        <div class="card-body border-top blogger_profile">
                            <div class="d-flex flex-wrap align-items-center expertise_set">
                                <label class="font-weight-bold text-dark mb-0" for="">Expertise:</label>
                                {expertise && expertise.length > 0 && expertise.map((item, index) => {
                                    return (
                                        <span class="badge badge-pill badge-normal badge-success m-1">{item}</span>
                                    )
                                })}
                            </div>
                            <p class="mt-3">{bloggerProfileData.aboutme}</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-12 p-0 page_title mb-3">
                    <h2 className="right_title row m-0">Portfolio</h2>
                </div>
                <div class="grey_box_bg p-3 blogger_profile">
                    <div class="content-wrapper">
                        <div class="card-body">
                            <div class="d-flex flex-wrap justify-content-start">
                                <div class="file_name_attach">
                                    {bloggerProfileData.portfolio && bloggerProfileData.portfolio.length > 0 && bloggerProfileData.portfolio.map((item, index) => {
                                        return (
                                            item.document.match(/.(jpg|jpeg|png|gif|svg|pdf|mp4|webm|ogv)$/i) &&
                                            // <a key={item.id} data-fancybox class="portfolio-item  rounded mx-3 mb-3 bg-white lightbox" href={item.document}>
                                            //     <img src={item.document} class="shadow" alt="" />
                                            <a data-fancybox="files" href={item.document} >
                                                {item.document.match(/.(jpg|jpeg|png|gif|svg)$/i) &&
                                                    <img src={item.document} class="shadow" alt="" height="100" width="100" />
                                                }&nbsp;&nbsp;
                                                {item.document.match(/.(pdf)$/i) &&
                                                    <img src="https://play-lh.googleusercontent.com/nufRXPpDI9XP8mPdAvOoJULuBIH_OK4YbZZVu8i_-eDPulZpgb-Xp-EmI8Z53AlXHpqX" class="shadow" alt="" height="100" width="100" />
                                                }&nbsp;&nbsp;
                                                {item.document.match(/.(mp4|webm|ogv)$/i) &&
                                                    <img src=" https://png.pngtree.com/png-vector/20190223/ourmid/pngtree-vector-play-icon-png-image_695746.jpg" class="shadow" alt="" height="100" width="100" />
                                                }&nbsp;&nbsp;
                                            </a>
                                        )
                                    })}
                                    <br />
                                    <br />
                                    {bloggerProfileData.portfolio && bloggerProfileData.portfolio.length > 0 && bloggerProfileData.portfolio.map((item, index) => {
                                        return (
                                            // !item.document.match(/.(jpg|jpeg|png|gif|svg|pdf|mp4|webm|ogv)$/i) &&
                                            // <div key={item.id} class="uploaded_imgs">
                                            // <a
                                            //     href={item.document}
                                            //     target="_blank"
                                            //     download={item.document}
                                            //     class="upolade_filename"
                                            // >
                                            //   {item.name}{" "}  <span className="fa fa-download"></span>&nbsp;                                                                                          
                                            // </a>
                                            //      <a
                                            //         href={item.document}
                                            //         target="_blank"
                                            //         download={item.document}
                                            //         class="download_link_img"
                                            //     >
                                            //         <img src={icnDownload} class="ml-2" />
                                            //     </a>
                                            // </div>  
                                            <a href={item.document} download={item.name} class="uploaded_file" >
                                                {!item.document.match(/.(jpg|jpeg|png|gif|svg|pdf|mp4|webm|ogv)$/i) && 
                                                <>
                                                    <span className="fa fa-download mr-2 "></span>{item.name}
                                                </>
                                                }&nbsp;&nbsp;&nbsp;
                                            </a>
                                        )
                                    })}
                                    {!bloggerProfileData.portfolio &&
                                        <h4 className="norec_found" style={{height: "75px"}}>No Portfolio Found</h4>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
                <div class="col-lg-12 col-md-12 col-12 p-0 page_title mb-3">
                    <h2 className="right_title row m-0">Link(s)</h2>
                </div>
                <div class="grey_box_bg p-3 blogger_profile">
                    <div class="content-wrapper">
                        <div class="card-body">
                            <div class="d-flex flex-wrap justify-content-start">
                                <div class="file_name_attach">
                                    {bloggerProfileData.blogportfolio && bloggerProfileData.blogportfolio.length > 0 && bloggerProfileData.blogportfolio.map((item, index) => {
                                        return (
                                            <div key={item.id} class="uploaded_imgs">
                                                <a
                                                    href={item.document}
                                                    target="_blank"
                                                    download={item.document}
                                                    class="upolade_filename"
                                                >
                                                    {item.document}{" "}
                                                </a>
                                            </div>
                                        )
                                    })}
                                    {(!bloggerProfileData.blogportfolio || (bloggerProfileData.blogportfolio && bloggerProfileData.blogportfolio.length ==0 )) &&
                                    <div>
                                        <h4 className="norec_found" style={{height: "75px"}}>No Link Found</h4>
                                    </div>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({ loginReducer }) => {
    return {
        activeusertype: loginReducer.userData.activeusertype,
        userData: loginReducer.userData,
    }
}


export default connect(mapStateToProps, {
    getBloggerProfile,
    addCartBlogger,
})(BloggerDetail);
