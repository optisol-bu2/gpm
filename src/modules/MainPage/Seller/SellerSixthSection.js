import React from "react";
import IncredibleImg from "../../../assets/images/landing-page/incredible_img.jpg";

const SellerSixthSection = (props) => {
  return (
    <div>
      <section className="business-grow-sec py-5">
        <div className="container">
          <div className="col-md-10 mx-auto">
            <div className="banner-wrap">
              <img src={IncredibleImg} alt="" />
              <div className="main-title mb-5 mt-5">
                <h2 className="pg_mid_head">Write incredible content and help websites grow their <span className="text_orange">organic traffic and earn money</span></h2>            
              </div>
              <div className="text-center">
                <button type="button" className="btn_green effect effect-2" onClick={(e) => { e.currentTarget.blur(); props.history.push("/signup") }}>
                  Get started
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default SellerSixthSection;
