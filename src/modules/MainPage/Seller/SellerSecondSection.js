import React from "react";
import listyourblog from "../../../assets/images/landing-page/list-your-blog.svg";
import listyourbloghover from "../../../assets/images/landing-page/list-your-blog-hover.svg";
import matchbuyer from "../../../assets/images/landing-page/sponsor.svg";
import matchbuyerhover from "../../../assets/images/landing-page/sponsor-hover.svg";
import digitalicon from "../../../assets/images/landing-page/digital-marketing.svg";
import digitaliconhover from "../../../assets/images/landing-page/digital-marketing-hover.svg";
import greenarrow from "../../../assets/images/landing-page/green_arrow.svg";

const SellerSecondSection = () => {
  return (
    <div>
      <section className="bg-white">
        <div className="main_container pt-5">
          <div className="col-lg-12 p-0 mx-auto">
          <div className="main-title mb-5 mt-5">
            <h2 className="pg_mid_head">How a Content Writer makes money</h2>
          </div>

          <div className="row how_it_works pb-5">
            <div className="col-lg-4 col-md-4 col-sm-12 how_a_seller">
              <div className="pr_percent">
                <div className="d-block mb-3">
                  <img src={listyourblog} alt="" className="normal_img" />
                  <img src={listyourbloghover} alt="" className="hover_img" />
                </div>
                <h4 className="hiw_head mb-3">Match with a buyer<span className="greenborder_line"></span></h4>
                <p className="text-muted hiw_content">
                  List your services for buyers to discover. The higher your rating, the more likely you will be noticed.
                </p>
                <a className="my-2 green_arrow"><img src={greenarrow} alt="" className="" /></a>
              </div>
            </div>
            <div className="col-lg-4 col-md-4 col-sm-12 how_a_seller">
              <div className="pr_percent">
                  <div className="d-block mb-3">
                    <img src={matchbuyer} alt="" className="normal_img" />
                    <img src={matchbuyerhover} alt="" className="hover_img"/>
                  </div>
                  <h4 className="hiw_head mb-3">Write a blog<span className="greenborder_line"></span></h4>
                  <p className="text-muted hiw_content mb-5">
                    Write a blog post on a topic specified by the buyer.
                  </p>
                  <a className="my-2 green_arrow"><img src={greenarrow} alt="" className="" /></a>
                </div>
              </div>
            <div className="col-lg-4 col-md-4 col-sm-12 how_a_seller">
              <div className="pr_percent">
                <div className="d-block mb-3">
                    <img src={digitalicon} alt="" className="normal_img" />
                    <img src={digitaliconhover} alt="" className="hover_img"/>
                  </div>
                  <h4 className="hiw_head mb-3">Earn Money<span className="greenborder_line"></span></h4>
                  <p className="text-muted hiw_content mb-5">
                    Deliver your blog post, make necessary revisions, and earn money!
                  </p>
                  <a className="my-2 green_arrow"><img src={greenarrow} alt="" className="" /></a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default SellerSecondSection;
