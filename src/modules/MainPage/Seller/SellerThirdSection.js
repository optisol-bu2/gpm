import React from "react";
import mac2 from "../../../assets/images/landing-page/mac2.svg";
import BuyerImg from "../../../assets/images/landing-page/increase_left.svg";
import defaultImage from "../../../assets/images/placeholder.jpeg";
import Loading from "../../../components/core/ChatLoading";
import moment from 'moment';
import { Swiper, SwiperSlide } from "swiper/react";
import "../../../assets/css/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
import SwiperCore,{Pagination,Autoplay} from "swiper/core";
SwiperCore.use([Pagination,Autoplay]);

const SellerThirdSection = (props) => {

  /**
     * changes the given time to date format
     * @param {*} dateTime
     * @returns
     */
  const timeFormat = (dateTime) => {
    var utc = moment.utc(dateTime).toDate();
    var chatTime = moment(utc).local().format('MMM-DD-YYYY');
    return chatTime
  }

  return (
    <div>
      <section class="pt-5 bg_lightwhite">
        <div class="main_container pt-4 visual_dashboard">
        <div class="main-title mb-5 mt-0"><h2 class="pg_mid_head">Visual Dashboard</h2>
        <p class="subline_head">Write blog posts to help businesses grow and track your monthly earnings.</p></div>
          <div class="row">
            <div class="col-lg-12 justify-content-center text-center">
              <img
                src={mac2}
                class="img-fluid maxwid30"
                alt=""
              />
            </div>            
          </div>
        </div>
      </section>
      <section className="sellers-sec mt-0 animate__animated animate__slideInRight" data-wow-duration="0.75s" data-wow-delay="10s">
        <div className="container-fluid pt-5">
          <div className="main-title mb-5 mt-5">
            <h2 className="pg_mid_head">Popular Blogs</h2>
            {/* <p className="subline_head">People also search for this niche</p> */}
            {/* <p className="subline_head">Our popular content writers at EngineScale are ready to grow your organic search</p> */}
            <p className="subline_head">Read about our popular blogs at EngineScale</p>
          </div>
          <div className="position-relative landing_swiper swipernavDetail">
            <Loading isVisible={props.blogLoading} />
            <Swiper slidesPerView={3} spaceBetween={30} initialSlide={1} autoplay={{ delay: 3000, disableOnInteraction: false }} loop={true} pagination={{ clickable:true}} centeredSlides={true} breakpoints={{ 1: { slidesPerView: 1,spaceBetweenSlides: 30}, 1000: { slidesPerView: 3,spaceBetweenSlides: 30}}} navigation={{nextEl: '.swipernavDetail .swiper-button-next', prevEl: '.swipernavDetail .swiper-button-prev'}}  className="mySwiper">
            {props.blogData && props.blogData.length != 0 && props.blogData.map((blog) => {
                return (
                  <SwiperSlide>
                    <div className="swiper-slide">
                      <div className="card d-card h-100">
                        <div className="card-body">
                          <div className="row m-0">
                            <div className="col-lg-12 p-0">                      
                                <div className="media">
                                    <a 
                                      href={blog.post_url}
                                      target="_blank"
                                      className="pull-left mr-2"
                                    >
                                        <img 
                                          className="media-object dp img-circle" 
                                          src={blog.image_url} 
                                          onError={(e)=>{e.target.onerror = null; e.target.src=defaultImage}} 
                                        />
                                    </a>
                                    <div className="media-body">
                                        <h4 className="media-heading">{blog.post_title}</h4>
                                    </div>
                                </div>
                              </div>
                          </div>
                          <div className="row m-0 mid_cnt">
                            <p className="col-md-12 p-0">{blog.short_description}</p>
                            <p className="col-md-12 p-0 text-right"><span>Posted at: </span>{timeFormat(blog.post_date)}</p>
                          </div>
                          <div className="row m-0 btm_cnt d-flex align-items-center">
                            <div className="col-lg-7 p-0">                    
                              <div className="btn-groups text-right">
                                <a 
                                  href={blog.post_url}
                                  target="_blank"
                                  className="view_btn"
                                >
                                  <span className="fa fa-eye"></span> View
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                )
            })}
            </Swiper> 
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>            
          </div>
        </div>
      </section>
      <section className="pb-5 pt-5 bg_green">
        <div className="main_container pt-5">
          <div className="row">
            <div className="col-lg-7">
              <img src={BuyerImg} className="img-fluid" alt="" />
            </div>
            <div className="col-lg-5 align-self-center increase_org_topic">
              <h3 className="incres_top_head">
                Increase the organic traffic of your website
              </h3>
              <p className="incres_top_cnt">
                Organic traffic based upon two factors:  content and visibility. The more blog posts you have that target key words that users query for in search engines, the greater chance you can be found. The more pages that link to your content, the greater visibility and the higher your website ranks in search engines. At EngineScale, we provide a marketplace of incredible content writers and outreach specialists to increase visitors to your site.
              </p>
              <div className="text-center text-md-left">
                <button type="button" className="btn_white_border effect effect-2" onClick={(e) => { e.currentTarget.blur(); props.history.push("/signup") }}>
                  Get started
                </button>
                <button type="button" className="btn_ebony_clay effect effect-2 ml-3">
                  Watch Demo
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default SellerThirdSection;
