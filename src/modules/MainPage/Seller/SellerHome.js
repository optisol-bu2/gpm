import React from "react";
import { FRONT_END_URL } from "../../../common/constants";
import SellerSecondSection from "./SellerSecondSection";
import SellerThirdSection from "./SellerThirdSection";
import SellerFourthSection from "./SellerFourthSection";
import SellerFifthSection from "./SellerFifthSection";
import SellerSixthSection from "./SellerSixthSection";
import FeatureImg from "../../../assets/images/landing-page/banner-right-img_seller.png";
import logoImage from "../../../assets/images/landing-page/rocketreach-logo.png";

const SellerHome = (props) => {
  const { signInCall, signUpCall, switchUser, title } = props;
  return (
    <div>
      <section className="main_top_banner">
        <header className="header no-bg">
          <div className="main_container">
            <nav className="navbar navbar-expand-md p-0">
              <a className="navbar-brand" href="/">
                <img className="img-fluid" src={logoImage} alt="" />
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#mobile-nav"
                aria-controls="mobile-nav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <i className="fa fa-bars" aria-hidden="true"></i>
              </button>

              {props.isLogin &&
                <div className="collapse navbar-collapse" id="mobile-nav">
                  <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                      <button
                        className="btn_green"
                        onClick={(e) => {
                          e.currentTarget.blur();
                          if (props.activeusertype == 2) {
                            props.history.push("/dashboard");
                            window.location.reload();
                          } else {
                            props.history.push("/sellerDashboard");
                          }
                        }}
                      >
                        Go to Dashboard
                      </button>
                    </li>
                  </ul>
                </div>
              }


              {!props.isLogin && 
                <div className="collapse navbar-collapse" id="mobile-nav">
                  <ul className="navbar-nav ml-auto">
                    <li className="nav-item navItem">
                      <a
                        className="btn_link mr-3"
                        href={FRONT_END_URL+"/blog/"}
                      >
                        Our Blog
                      </a>
                    </li>
                    <li className="nav-item navItem">
                      <a
                        className="btn_link mr-3"
                        onClick={(e) => { 
                          e.currentTarget.blur();
                          switchUser();
                        }}
                      >
                        {title}
                      </a>
                    </li>
                    <li className="nav-item navItem">
                      <button
                        className="btn_white_border mr-3"
                        onClick={(e) => { 
                          e.currentTarget.blur();
                          signUpCall();
                        }}
                      >
                        Sign Up
                      </button>
                    </li>
                    <li className="nav-item navItem">
                      <button
                        className="btn_green"
                        data-toggle="modal"
                        data-target="#login-modal"
                        onClick={(e) => { 
                          e.currentTarget.blur();
                          signInCall();
                        }}
                      >
                        Sign In
                      </button>
                    </li>
                  </ul>
                </div>
              }
            </nav>
          </div>
        </header>
        <section className="about-sec">
          <div className="banner_rightside banner_rightside_seller">
            <div className="container">
              <div className="row m-0 justify-content-between">
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">    
                  <div className="about-content">
                    <h1 className="text-center text-md-left text-white">
                      Become a content writer.
                    </h1>
                    <p className="match_with_set">
                      Write blogs to help businesses grow their organic traffic
                    </p>

                    <div className=" text-lg-left text-md-left text-sm-center text-center">
                      <div
                        className="btn-group"
                        role="group"
                        aria-label="Basic example"
                      >
                        <div className="pricerange_border_bg">
                          <span>Price Range</span>
                          <span className="greenfont">$50-200</span>
                          <span className="lastline_blg">Per Blog</span>
                        </div>

                        <div className="pricerange_border_bg">
                          <span>Transactions</span>
                          <span className="greenfont">50M+</span>
                        </div>
                      </div>
                    </div>

                    <p className="blog_postcontent mt-3">
                    <span className="greenfont">7.5 million </span>blog posts are published every day
                    </p>
                  </div>
                </div>
                <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">
                  <div className="card bg-transparent shadow-0 border-0 h-100">
                      <div className="position-relative">
                          <img src={FeatureImg} alt="" class="img-fluid" />
                      </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      </section>
    
      <SellerSecondSection />
      <SellerThirdSection {...props}/>
      <SellerFourthSection />
      <SellerFifthSection />
      <SellerSixthSection {...props}/>
    </div>
  );
};

export default SellerHome;
