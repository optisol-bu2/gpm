import React, { useEffect, useState } from "react";
import {  Button, Form } from "react-bootstrap";
import Modal from "@material-ui/core/Modal";
import Loading from "../../components/core/Loading";

export default function ActivateAccount(props) {
  const [isLoading, setLoading] = useState(null);
  const [activateData, setActivateData] = useState({});
  const [isError, setError] = useState(false);
  const [errorMessage, setErrorMessage] = useState(false);

  /**
   * verifies activate account token
   */
  const verifyActivateToken = () => {
    setLoading(true);
    let params = {
      token : props.activateToken
    }
    props.verifyActivateToken(
      params,
      (response) => {
        setLoading(false);
        props.openActivate();
        setActivateData(response.data);
      },
      (error) => {
        setLoading(false);
        setError(true);
        setErrorMessage(error.message);
        props.openActivate();
      }
    );
  };

  /**
   * activates account
   * @param {*} e 
   * @returns 
   */
  const activateAccount = (e) => {
      props.onActivateAccount(activateData);
  };

  useEffect (() => {
    if (props.activateToken) {
      props.removeAuthorization();
      verifyActivateToken();
    }
  }, [props.activateToken])

  const body = (
    <div>
      <div
        className="modal-dialog modal_popup"
        role="document"
      >
        <button
          type="button"
          class="btn btn-ar btn-default"
          id="cancel-btn2"
          data-dismiss="modal"
        />
        <div className="modal-content">
          <div class="modal-header">
            <h5 class="popup_title" id="confirmModalLabel">{isError ? "Account action" : "Account activated"}</h5>
          </div>
          <div className="modal-body">
            <Form noValidate onSubmit={activateAccount}>
              <div className="form-group text-center">
                <h6 className="text-center my-3 p-3">{isError ? errorMessage : "Your account has been activated."}</h6>
              </div>
              <div className="form-group text-center">
                {isError ? 
                  <Button
                    className="btn_light_green"
                    onClick={props.onCloseReset}
                  >
                    OK
                  </Button>
                  :
                  <Button
                    type="submit"
                    onClick={activateAccount}
                    className="btn_light_green"
                  >
                    Login
                  </Button>
                }
              </div>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );

  return (
    <div>
      <Loading isVisible={isLoading} />
      <Modal
        disableBackdropClick
        open={props.isActivateVisible}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
