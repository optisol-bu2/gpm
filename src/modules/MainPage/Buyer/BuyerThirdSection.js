import React from "react";
import IcnMatch from "../../../assets/images/landing-page/works1.svg";
import IcnPurchase from "../../../assets/images/landing-page/works2.svg";
import IcnChart from "../../../assets/images/landing-page/works3.svg";

const BuyerThirdSection = () => {
  return (
    <div>
      <section className="bg_lightwhite">
        <div className="main_container pt-5">
          <div className="col-lg-12 p-0 mx-auto">
            <div className="main-title mb-5 mt-5">
              <h2 className="pg_mid_head">How It Works</h2>
              <p className="subline_head">How To Grow Your Organic Traffic</p>
            </div>
            <div className="row how_it_works pb-5">            
              <div className="max_wid_hiw1">
                <div className="pr_percent">
                  <div className="">
                    <img src={IcnMatch} alt="" />
                  </div>
                  <h4 className="hiw_head">Pick and choose <br/>your content writer<span className="border_bottom"></span></h4>
                  <p className="text-muted hiw_content">
                    Find a marketplace of highly vetted and talented content writers to produce amazing content and blog posts.
                  </p>
                </div>
              </div>  
              <div className="max_wid_hiw2">
                <div className="pr_percent">
                  <div className="">
                    <img src={IcnPurchase} alt="" />
                  </div>
                  <h4 className="hiw_head">Conduct<br/> Outreach<span className="border_bottom"></span></h4>
                  <p className="text-muted hiw_content">
                    Increase the visibility of those blog posts by having other pages link back to your content.
                  </p>
                </div>
              </div>
              <div className="max_wid_hiw3">
                <div className="">
                  <div className="">
                    <img src={IcnChart} alt="" />
                  </div>
                  <h4 className="hiw_head">Grow<br/> organic traffic<span className="border_bottom"></span></h4>
                  <p className="text-muted hiw_content">
                    Watch your organic traffic increase through content and outreach.
                  </p>                
                </div>
              </div>
            </div>
            <div className="row count_plus_names col-lg-11 mx-auto pb-5 pl-0 pr-0">
              <div className="col-sm-6 col-lg-3 mb-3">
                <div className="text-center bg_dec">
                  <div className="boxshade">
                    <p className="count_head">
                      200+
                    </p>
                    <p className="count_cnt">Enterprise Clients</p>
                  </div>
                </div>
              </div>

              <div className="col-sm-6 col-lg-3 mb-3">
                <div className="text-center bg_dec">
                  <div className="boxshade">
                    <p className="count_head">50%</p>
                    <p className="count_cnt">Countries</p>
                  </div>
                </div>
              </div>

              <div className="col-sm-6 col-lg-3 mb-3">
                <div className="text-center bg_dec">
                  <div className="boxshade">
                    <p className="count_head">
                      98<small>%</small>
                    </p>
                    <p className="count_cnt">Client Retention</p>
                  </div>
                </div>
              </div>

              <div className="col-sm-6 col-lg-3 mb-3">
                <div className="text-center bg_dec">
                  <div className="boxshade">
                    <p className="count_head">
                      120<small>%</small>
                    </p>
                    <p className="count_cnt">ROI</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default BuyerThirdSection;
