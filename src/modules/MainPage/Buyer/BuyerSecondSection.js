import React, { useState, useEffect } from "react";
import avatar from "../../../assets/images/avatar.png";
import bradford from "../../../assets/images/blogger-Img/bradford.jpeg";
import jessica from "../../../assets/images/blogger-Img/jessica.jpeg";
import kendric from "../../../assets/images/blogger-Img/kendric.jpeg";
import orson from "../../../assets/images/blogger-Img/orson.jpeg";
import suzanne from "../../../assets/images/blogger-Img/suzanne.jpeg";
import Loading from "../../../components/core/ChatLoading";
import { CONTENT_WRITER_DATA } from "../../../common/constants";
import { Link } from "react-router-dom";
import Modal from "@material-ui/core/Modal";
import { ToastAlert,PlaySound } from '../../../utils/sweetalert2';
import { Swiper, SwiperSlide } from "swiper/react";
import "../../../assets/css/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
import SwiperCore,{ Navigation
,  Pagination,Autoplay} from "swiper/core";
SwiperCore.use([Pagination,Autoplay,Navigation]);

const BuyerSecondSection = (props) => {
  const [contentData, setContentData] = useState([]);
  const [favoriteArray, setFavoriteArray] = useState([]);
  const [isFavoriteModalOpen, setFavoriteModalOpen] = useState(false);
  const [favoriteModalContent, setFavoriteModalContent] = useState('');

  useEffect(() => {
    setContentData(props.contentData);
    if (props.contentData && props.contentData.length) {
      let favoriteData = [];
      props.contentData.forEach(bloggerData => {
          if (bloggerData.isFavorite) {
              if (!favoriteData.includes(bloggerData.id))
              favoriteData.push(bloggerData.id)
          }
      })
      setFavoriteArray(favoriteData);
    }
  }, [props.contentData]);

  /**
     * adds favorite blogger for the user
     * @param {*} id 
     */
  const addFavorite = (id) => {
    const bloggerArray = [...contentData];
    bloggerArray.forEach(blogger => {
      if (blogger.id === id) {
        blogger.isFavorite = true;
      }
    })
    setContentData(bloggerArray);
    var params = {
        "blogger": id.toString()
    }
    props.addFavorite(params,
      (response) => {
      },
      (error) => {
        const bloggerArray = [...contentData];
        bloggerArray.forEach(blogger => {
          if (blogger.id === id) {
            blogger.isFavorite = false;
          }
        })
        setContentData(bloggerArray);
        ToastAlert("error", error.message);
        PlaySound();
      }
    )
  }

  /**
   * removes favorite blogger for the user
   * @param {*} id 
   */
  const removeFavorite = (id) => {
    const bloggerArray = [...contentData];
    bloggerArray.forEach(blogger => {
      if (blogger.id === id) {
        blogger.isFavorite = false;
      }
    })
    setContentData(bloggerArray);
    props.removeFavorite(id,
      (response) => {
      },
      (error) => {
        const bloggerArray = [...contentData];
        bloggerArray.forEach(blogger => {
          if (blogger.id === id) {
            blogger.isFavorite = true;
          }
        })
        setContentData(bloggerArray);
        ToastAlert("error", error.message);
        PlaySound();
      }
    )
  }

  return (
    <div>
      <section className="sellers-sec mt-0 animate__animated animate__slideInRight" data-wow-duration="0.75s" data-wow-delay="10s">
        <div className="container-fluid pt-5">
          <div className="main-title mb-5 mt-5">
            <h2 className="pg_mid_head">Popular Content Writers</h2>
            {/* <p className="subline_head">People also search for this niche</p> */}
            <p className="subline_head">Our popular content writers at EngineScale are ready to grow your organic search</p>
            {/* <p className="subline_head">Read about our popular blogs at EngineScale</p> */}
          </div>
          <div className="position-relative landing_swiper swipernavDetail">
            <Loading isVisible={props.contentLoading} />
            <Swiper slidesPerView={3} spaceBetween={30} initialSlide={1} autoplay={{ delay: 6000, disableOnInteraction: false }} loop={true} pagination={{ clickable:true}} centeredSlides={true} breakpoints={{ 1: { slidesPerView: 1,spaceBetweenSlides: 30}, 1000: { slidesPerView: 3,spaceBetweenSlides: 30}}} navigation={{nextEl: '.swipernavDetail .swiper-button-next', prevEl: '.swipernavDetail .swiper-button-prev'}}  className="mySwiper">
            {props.contentData && props.contentData.length >= 5 && props.contentData.map((content) => {
                return (
                  <SwiperSlide>
                    <div className="swiper-slide">
                      <div className="card d-card h-100">
                        <div className="card-body">
                          <div className="row m-0">
                            <div className="col-lg-8 p-0">                      
                                <div className="media">
                                    <a className="pull-left mr-2">
                                        <img 
                                          className="media-object dp img-circle" 
                                          src={content.profilepic} 
                                          onError={(e)=>{e.target.onerror = null; e.target.src=avatar}} 
                                        />
                                    </a>
                                    <div className="media-body">
                                        <h4 className="media-heading">{content.username}</h4>
                                        <div className="rating_star">
                                          {content.review !== 0 && Array.from(Array(Math.round(content.review)), () => {
                                            return (
                                              <span className="fa fa-star"></span>
                                            )
                                          })}
                                          {content.review === 0 && 
                                            <div className="text-muted">New Content Writer</div>
                                          }                               
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div className="col-lg-4 p-0 text-right">
                                <p className="btn_brown">{content.totalsold} Orders</p>
                              </div>
                          </div>
                          <div className="row m-0 mid_cnt">
                            {/* <p className="col-md-8 p-0"><span>Expertise: </span>{content.expertise}</p> */}
                            <span className="col-md-8 p-0">{content.expertise && content.expertise.length > 0 && content.expertise.map((item, index) => {
                                                return <span key={`demo_snap_${index}`}>{ (index ? ', ' : '') + item.name }</span>;
                                            })}</span>
                            <p className="col-md-4 p-0 text-right"><span>Words/Hr: </span>{content.wordsperhour}</p>
                            <p className="col-md-12 p-0 abt_cnt_text"><span>About: </span>{content.aboutme}</p>
                          </div>
                          <div className="row m-0 btm_cnt d-flex align-items-center">
                            <div className="col-lg-5 p-0">
                              <div className="price-info">                        
                                <span>${content.rate}/Hr</span>
                              </div> 
                            </div>
                            <div className="col-lg-7 p-0">                    
                              <div className="btn-groups text-right">
                                {props.isLogin ?
                                  <a 
                                    className="favorite_btn mr-2"
                                    onClick={() => {
                                        if (content.isFavorite) {
                                            if (favoriteArray.includes(content.id)) {
                                                const favoriteData = [...favoriteArray];
                                                let unlike = favoriteData.filter((elem) => elem !== content.id);
                                                setFavoriteArray(unlike);
                                            }
                                            removeFavorite(content.id)
                                        } else {
                                            if (!favoriteArray.includes(content.id)) {
                                              setFavoriteArray([...favoriteArray, content.id]);
                                            }
                                            addFavorite(content.id);
                                            setFavoriteModalOpen(true);
                                            setFavoriteModalContent("You have favorited "+ content.username + " as your go-to content writer");
                                        }
                                    }}
                                  >
                                    {favoriteArray.includes(content.id) ? <span class="fa fa-heart"></span> :
                                    <span class="fa fa-heart-o"></span>} Favorite
                                  </a>
                                   :
                                  <Link className="favorite_btn mr-2" to={`/signup`}>
                                    <span class="fa fa-heart-o"></span> Favorite
                                  </Link>
                                }
                                {props.isLogin ?
                                  <Link className="view_btn" to={`/contentWriter/${content.user}`}>
                                    <span class="fa fa-eye"></span> View
                                  </Link> :
                                  <Link className="view_btn" to={`/signup`}>
                                    <span class="fa fa-eye"></span> View
                                  </Link>
                                }
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                )
            })}
            {(!props.contentData || props.contentData.length < 5) && CONTENT_WRITER_DATA.map((element, index) => {
                return (
                  <SwiperSlide>
                    <div className="swiper-slide">
                      <div className="card d-card h-100">
                        <div className="card-body">
                          <div className="row m-0">
                            <div className="col-lg-8 p-0">                      
                                <div className="media">
                                    <a className="pull-left mr-2">
                                        <img 
                                          className="media-object dp img-circle" 
                                          src={index == 0 ? bradford : index == 1 ? kendric : index == 2 ? jessica : index == 3 ? orson : index == 4 ? suzanne : avatar}
                                        />
                                    </a>
                                    <div className="media-body">
                                        <h4 className="media-heading">{element.username}</h4>
                                        <div className="rating_star">
                                          {element.review !== 0 && Array.from(Array(Math.round(element.review)), () => {
                                            return (
                                              <span className="fa fa-star"></span>
                                            )
                                          })}
                                          {element.review === 0 && 
                                            <div className="text-muted">New Content Writer</div>
                                          }                               
                                        </div>
                                    </div>
                                </div>
                              </div>
                              <div className="col-lg-4 p-0 text-right">
                                <p className="btn_brown">{element.totalsold} Orders</p>
                              </div>
                          </div>
                          <div className="row m-0 mid_cnt">
                            <p className="col-md-8 p-0"><span>Expertise: </span>{element.expertise}</p>
                            <p className="col-md-4 p-0 text-right"><span>Words/Hr: </span>{element.wordsperhour}</p>
                            <p className="col-md-12 p-0 abt_cnt_text"><span>About: </span>{element.aboutme}</p>
                          </div>
                          <div className="row m-0 btm_cnt d-flex align-items-center">
                            <div className="col-lg-5 p-0">
                              <div className="price-info">                        
                                <span>${element.rate}/Hr</span>
                              </div> 
                            </div>
                            <div className="col-lg-7 p-0">                    
                              <div className="btn-groups text-right">
                                {props.isLogin ?
                                  <a className="favorite_btn mr-2">
                                    <span className="fa fa-heart-o"></span> Favorite
                                  </a> :
                                  <Link className="favorite_btn mr-2" to={`/signup`}>
                                    <span class="fa fa-heart-o"></span> Favorite
                                  </Link>
                                }
                                {props.isLogin ?
                                  <a className="view_btn">
                                    <span className="fa fa-eye"></span> View
                                  </a> :
                                  <Link className="view_btn" to={`/signup`}>
                                    <span class="fa fa-eye"></span> View
                                  </Link>
                                }
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                )
            })}
            
            </Swiper>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>  
                      
          </div>
        </div>
      </section>
      <Modal
        open={isFavoriteModalOpen}
        disableBackdropClick
        onClose={() => {
          setFavoriteModalOpen(false);
          setFavoriteModalContent('');
        }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description">
        <div className="modal-dialog modal_popup" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="popup_title" id="exampleModalLabel">Favorited</h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                  onClick={() => { 
                    setFavoriteModalOpen(false);
                    setFavoriteModalContent('');
                  }}
                >
                  <span aria-hidden="true">×</span>
                </button>
            </div>
            <div className="modal-body justify-content-center">
              <h4 className="text-center">{favoriteModalContent}</h4>
            </div>
            <div className="modal-footer justify-content-center">
              <button 
                onClick={() => { 
                  setFavoriteModalOpen(false);
                  setFavoriteModalContent('');
                }}
                className="btn_light_green"
              >
                 OK
              </button>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default BuyerSecondSection;
