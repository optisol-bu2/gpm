import React from "react";
import { FRONT_END_URL } from "../../../common/constants";
import BuyerFirstSection from "./BuyerFirstSection";
import BuyerSecondSection from "./BuyerSecondSection";
import BuyerThirdSection from "./BuyerThirdSection";
import BuyerFourthSection from "./BuyerFourthSection";
import BuyerFifthSection from "./BuyerFifthSection";
import BuyerSixthSection from "./BuyerSixthSection";
import BuyerFeature from "../../../assets/images/landing-page/banner-right-img.png";
import logoImage from "../../../assets/images/landing-page/rocketreach-logo.png";

const BuyerHome = (props) => {  
  const { signInCall, signUpCall, switchUser, title } = props;
  return (
    <div>
      {/* <section className="main_top_banner">
        <header className="header no-bg">
          <div className="main_container">
            <nav className="navbar navbar-expand-md p-0">
              <a className="navbar-brand" href="/">
                <img className="img-fluid" src={logoImage} alt="" />
              </a>
              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#mobile-nav"
                aria-controls="mobile-nav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <i className="fa fa-bars" aria-hidden="true"></i>
              </button>

              {props.isLogin &&
                <div className="collapse navbar-collapse" id="mobile-nav">
                  <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                      <button
                        className="btn_green"
                        onClick={(e) => {
                          e.currentTarget.blur();
                          if (props.activeusertype == 2) {
                            props.history.push("/dashboard");
                            window.location.reload();
                          } else {
                            props.history.push("/sellerDashboard");
                          }
                        }}
                      >
                        Go to Dashboard
                      </button>
                    </li>
                  </ul>
                </div>
              }


              {!props.isLogin && 
                <div className="collapse navbar-collapse" id="mobile-nav">
                  <ul className="navbar-nav ml-auto">
                    <li className="nav-item navItem">
                      <a
                        className="btn_link mr-3"
                        href={FRONT_END_URL+"/blog/"}
                      >
                        Blog
                      </a>
                    </li>
                    <li className="nav-item navItem">
                      <a
                        className="btn_link mr-3"
                        onClick={(e) => { 
                          e.currentTarget.blur();
                          switchUser();
                        }}
                      >
                        {title}
                      </a>
                    </li>
                    <li className="nav-item navItem">
                      <button
                        className="btn_white_border mr-3"
                        onClick={(e) => { 
                          e.currentTarget.blur();
                          signUpCall();
                        }}
                      >
                        Sign Up
                      </button>
                    </li>
                    <li className="nav-item navItem">
                      <button
                        className="btn_green"
                        data-toggle="modal"
                        data-target="#login-modal"
                        onClick={(e) => { 
                          e.currentTarget.blur();
                          signInCall();
                        }}
                      >
                        Sign In
                      </button>
                    </li>
                  </ul>
                </div>
              }
            </nav>
          </div>
        </header>
        <section className="about-sec">     
            <div className="banner_rightside">     
              <div className="container">              
                <div className="row m-0 justify-content-between">
                  <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12">                  
                    <div className="about-content">
                      <h1 className="text-center text-md-left text-white">
                      Engine Scale: The engine behind <span className="text-green">scaling your organic traffic.</span>
                      </h1>
                      <p className="text-center text-md-left text-white">
                        At EngineScale, we provide a marketplace of highly vetted, talented content writers ready to craft together incredible blog posts and articles to drive your organic search traffic.
                      </p>
                      <div className=" text-lg-left text-md-left text-sm-center text-center">
                        <button type="button" className="btn_white_border mr-lg-3 mr-md-3 mb-3 effect effect-2" onClick={(e) => { e.currentTarget.blur(); props.history.push("/signup") }}>
                          Get started
                        </button>
                        <button type="button" className="btn_green mb-3 effect effect-2">
                          Watch Demo
                        </button>
                      </div>
                    </div>
                  </div>  
                  <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12"> 
                    <div className="card bg-transparent shadow-0 border-0 h-100">
                      <div className="position-relative">
                        <img src={BuyerFeature} alt="" className="img-fluid" />
                      </div>
                    </div>
                  </div>              
                </div>   
              </div>                        
          </div>
        </section>
      </section>
       */}
       <BuyerFirstSection {...props}/>
      <BuyerSecondSection {...props}/>
      <BuyerThirdSection />
      <BuyerFourthSection />
      <BuyerFifthSection {...props}/>
      <BuyerSixthSection {...props}/>
    </div>
  );
};

export default BuyerHome;
