import React from "react";
import ImageDummy from "../../../assets/images/slider2/01.jpg";
import ImageDummy1 from "../../../assets/images/slider2/02.jpg";
import ImageDummy2 from "../../../assets/images/slider2/03.jpg";
import ImageDummy3 from "../../../assets/images/slider2/04.jpg";
import IncredibleImg from "../../../assets/images/landing-page/incredible_img.jpg";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.min.css";
import "swiper/components/pagination/pagination.min.css";
import SwiperCore,{Pagination,Autoplay} from "swiper/core";
SwiperCore.use([Pagination,Autoplay]);

const BuyerSixthSection = (props) => {
  return (
    <div>
      {/* <section className="industry-sec pb-5 pt-5">
        <div className="container mt-5">
          <div className="main-title mb-5 mt-5">
            <h2 className="pg_mid_head">Conduct outreach with guest posts</h2>
            <p className="subline_head">Guest posts that link to your blog content increases your visibility and search engine ranking</p>
          </div>
          <div className="position-relative swipernav6">
            <div className="conduct_content_bg">
              <div className="conduct_slider">
                <Swiper slidesPerView={3} spaceBetween={30} loop={true} pagination={{ clickable:true}} centeredSlides={true} breakpoints={{ 1: { slidesPerView: 1,spaceBetweenSlides: 30}, 1000: { slidesPerView: 3,spaceBetweenSlides: 30}}} autoplay={{ delay: 3000, disableOnInteraction: false }} className="mySwiper">
                  <SwiperSlide>
                    <div className="swiper-slide pb-2 pt-3">
                      <div className="card d-card h-100">
                        <div className="top-img">
                          <img src={ImageDummy} height="100%" width="100%" className="img-fluid" alt="..." />
                        </div>
                        <div className="card-body text-center">
                          <h5 className="card-title">www.ecommerce.com</h5>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div className="swiper-slide pb-2 pt-3">
                      <div className="card d-card h-100">
                        <div className="top-img">
                          <img src={ImageDummy1} height="100%" width="100%" className="img-fluid" alt="..." />
                        </div>
                        <div className="card-body text-center">
                          <h5 className="card-title">www.fashion.com</h5>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div className="swiper-slide pb-2 pt-3">
                      <div className="card d-card h-100">
                        <div className="top-img">
                          <img src={ImageDummy2} height="100%" width="100%" className="img-fluid" alt="..." />
                        </div>
                        <div className="card-body text-center">
                          <h5 className="card-title">www.medical.com</h5>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                  <SwiperSlide>
                    <div className="swiper-slide pb-2 pt-3">
                      <div className="card d-card h-100">
                        <div className="top-img">
                          <img src={ImageDummy3} height="100%" width="100%" className="img-fluid" alt="..." />
                        </div>
                        <div className="card-body text-center">
                          <h5 className="card-title">www.beauty.com</h5>
                        </div>
                      </div>
                    </div>
                  </SwiperSlide>
                </Swiper>
              </div>
            </div>
          </div>
        </div>
      </section> */}

      <section className="business-grow-sec py-5">
        <div className="container">
          <div className="col-md-10 mx-auto">
            <div className="banner-wrap">
              <img src={IncredibleImg} alt="" />
              <div className="main-title mb-5 mt-5">
                <h2 className="pg_mid_head">Find incredible content writers and outreach specialists to <span className="text_orange">grow your SEO organically</span></h2>            
              </div>
              <div className="text-center">
                <button type="button" className="btn_green effect effect-2"  onClick={(e) => { e.currentTarget.blur(); props.history.push("/signup") }}>
                  Get started
                </button>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default BuyerSixthSection;
