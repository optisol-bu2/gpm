import { connect } from "react-redux";
import MainPage from "./MainPage";
import {
  userLogin,
  userSocialLogin,
  checkEmail,
  registrationCall,
  forgotPassword,
  verifyToken,
  verifyActivateToken,
  resetPassword,
  callRefreshTokenSaga,
  switchUser,
  removeAuthorization,
  getTopServices,
  getTopContentWriters,
} from "../../Actions/actionContainer";
import { getBlogs } from "../../Actions/blogAction";
import {
  addFavoriteBlogger,
  removeFavoriteBlogger
} from "../../Actions/favoriteAction";
import { getAbout } from "../../Actions/aboutAction";

const mapStateToProps = (state) => {
  return {
    isLogin: state.loginReducer.isLogin,
    activeusertype: state.loginReducer.userData.activeusertype,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    userLogin: (params, onSuccess, onFailure) =>
      dispatch(userLogin(params, onSuccess, onFailure)),
    userSocialLogin: (params, onSuccess, onFailure) =>
      dispatch(userSocialLogin(params, onSuccess, onFailure)),
    checkEmail: (params, onSuccess, onFailure) =>
      dispatch(checkEmail(params, onSuccess, onFailure)),
    registrationCall: (params, onSuccess, onFailure) =>
      dispatch(registrationCall(params, onSuccess, onFailure)),
    forgotPassword: (params, onSuccess, onFailure) =>
      dispatch(forgotPassword(params, onSuccess, onFailure)),
    verifyToken: (params, onSuccess, onFailure) =>
      dispatch(verifyToken(params, onSuccess, onFailure)),
    verifyActivateToken: (params, onSuccess, onFailure) =>
      dispatch(verifyActivateToken(params, onSuccess, onFailure)),
    resetPassword: (params, onSuccess, onFailure) =>
      dispatch(resetPassword(params, onSuccess, onFailure)),
    switchUser: (onSuccess, onFailure) =>
      dispatch(switchUser(onSuccess, onFailure)),
    callRefreshTokenSaga: () => dispatch(callRefreshTokenSaga()),
    removeAuthorization: () => dispatch(removeAuthorization()),
    getTopServices: (onSuccess, onFailure) =>
      dispatch(getTopServices(onSuccess, onFailure)),
    getTopContentWriters: (onSuccess, onFailure) =>
      dispatch(getTopContentWriters(onSuccess, onFailure)),
    getBlogs: (params, onSuccess, onFailure) =>
      dispatch(getBlogs(params, onSuccess, onFailure)),
    addFavoriteBlogger: (params, onSuccess, onFailure) =>
      dispatch(addFavoriteBlogger(params, onSuccess, onFailure)),
    removeFavoriteBlogger: (id, onSuccess, onFailure) =>
      dispatch(removeFavoriteBlogger(id, onSuccess, onFailure)),
    getAbout: (params, onSuccess, onFailure) =>
      dispatch(getAbout(params, onSuccess, onFailure)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);
