import React from "react";

const SocialBar = () => {
  return (
    <div>
        <header className="header no-bg top_header_Bar">
            <div className="main_container">
                <nav className="navbar navbar-expand-md p-0">
                    {/* <a className="social_bar" href="/">
                        <i className="fa fa-facebook"></i>
                    </a>
                    <a className="social_bar" href="/">
                        <i className="fa fa-twitter"></i>
                    </a>
                    <a className="social_bar" href="/">
                        <i className="fa fa-linkedin"></i>
                    </a>
                    <a className="social_bar" href="/">
                        <i className="fa fa-instagram"></i>
                    </a> */}
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item mr-4 mail-info">
                            <span className="bgicon fa fa-envelope mr-2"></span><a href = "mailto: support@enginescale.com" target="_blank"><span>support@enginescale.com</span></a>
                        </li>
                        {/* <li className="nav-item mr-4 phone-info">
                            <span className="bgicon fa fa-phone-alt mr-2"></span><span>650-338-8226</span>
                        </li> */}
                    </ul>
                </nav>
            </div>
        </header>
    </div>
  );
};

export default SocialBar;
