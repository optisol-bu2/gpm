import React, { useEffect, useState } from "react";
import {  Button, Form } from "react-bootstrap";
import Modal from "@material-ui/core/Modal";
import LoginForm from "./LoginForm";
import { ToastAlert } from "../../utils/sweetalert2";
import { FRONT_END_URL } from "../../common/constants";

export default function ForgotPassword(props) {
  const [email, setEmail] = useState("");
  const [formValidated, setFormValidated] = useState(false);

  const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

  /**
   * sends forgot password email
   * @param {*} e 
   * @returns 
   */
  const forgotPassword = (e) => {
    e.preventDefault();
    const form = e.currentTarget;
    setFormValidated(true);
    if (!email || !validEmailRegex.test(email)) {
      return;
    } else {
      let params = {
        email: email,
        url: FRONT_END_URL + "/resetpassword"
      };
      props.onClickSubmit(params);
    }
  };

  /**
  * closes the modal
  */
  const closeModal = () => {
    setEmail("");
    setFormValidated(false);
    props.onCloseForgot();
  }

  useEffect (() => {
    if (!props.isForgotPasswordVisible) {
      closeModal();
    }
  }, [props.isForgotPasswordVisible])

  const body = (
    <div>
      <div
        className="modal-dialog modal_popup"
        role="document"
      >
        <button
          type="button"
          class="btn btn-ar btn-default"
          id="cancel-btn2"
          data-dismiss="modal"
        />
        <div className="modal-content">
          <div class="modal-header">
            <h5 class="popup_title" id="confirmModalLabel">Forgot Password</h5>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
              onClick={closeModal}
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body form_fields_white_bg">
            
            <Form noValidate validated={formValidated} onSubmit={forgotPassword}>
              <Form.Group controlId="formBasicEmail">
                <Form.Control
                  type="email"
                  placeholder="Enter Your Email"
                  name="email"
                  className={"form-control " + (formValidated && (!email || !validEmailRegex.test(email)) ? "is-invalid" : "")}
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                  value={email}
                  required
                />
                  <Form.Control.Feedback type="invalid">
                    {formValidated && !email && "Please enter your email"}
                    {formValidated && email && !validEmailRegex.test(email) && "Please enter valid email"}
                  </Form.Control.Feedback>
              </Form.Group>
              <div className="form-group text-center">
                <Button
                  type="submit"
                  className="btn_light_green"
                >
                  Submit
                </Button>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );

  return (
    <div>
      <Modal
        disableBackdropClick
        open={props.isForgotPasswordVisible}
        onClose={closeModal}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
