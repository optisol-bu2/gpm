import React, { useEffect, useState } from "react";
import LoginForm from "./LoginForm";
import Loading from "../../components/core/Loading";
import BuyerHome from "./Buyer/BuyerHome";
import Footer from "./MainPGFooter";
import SellerHome from "./Seller/SellerHome";
import JoinNow from "./JoinNow";
import ForgotPassword from "./ForgotPassword";
import ResetPassword from "./ResetPassword";
import ActivateAccount from "./ActivateAccount";
import SocialBar from "./SocialBar";
import { ToastAlert,PlaySound } from "../../utils/sweetalert2";
import ConfirmationDialog from "../../components/core/ConfirmationDialog/ConfirmationDialog";
import PrivacyPolicy from "./PrivacyPolicy";
import TermsCondition from "./TermsCondition";


const MainPage = (props) => {
  const [isLoginFormVisible, setLoginVisibility] = useState(false);
  const [isLoading, setLoading] = useState(null);
  const [userType, setUserType] = useState(2);
  const [switchText, setSwitchText] = useState("Become a Content Writer");
  const [isSignUpVisibility, setSignUpVisibility] = useState(false);
  const [isForgotPasswordVisibility, setForgotPasswordVisibility] = useState(false);
  const [isResetPasswordVisibility, setResetPasswordVisibility] = useState(false);
  const [isActivateVisibility, setActivateVisibility] = useState(false);
  const [popUpTitle, setPopUpTitle] = useState("Sign In");
  const [isJoinNowEvent, setJoinNow] = useState(false);
  const [confirmationModal, setConfirmationModal] = useState(false);
  const [loginType, setLoginType] = useState("");
  const [socialCallData, setSocialCallData] = useState({});
  const [contentLoading, setContentLoading] = useState(false);
  const [contentData, setContentData] = useState([]);
  const [blogLoading, setBlogLoading] = useState(false);
  const [blogData, setBlogData] = useState([]);

  const params = new URLSearchParams(props.location.search);
  const resetToken = params.get('k');
  const activateToken = props.match.params.token

  const isSignIn = props.location.pathname.includes('/signin')
  const isSignUp = props.location.pathname.includes('/signup')
  const isSeller = props.location.pathname.includes('/seller')
  const isPrivacyPolicy = props.location.pathname.includes('/privacypolicy')
  const isTermsCondition = props.location.pathname.includes('/termscondition')


  const signInClick = () => {
    setLoginVisibility(true);
    setSignUpVisibility(false);
    setPopUpTitle("Sign In");
  };

  const signUpCall = () => {
    setLoginVisibility(true);
    setSignUpVisibility(true);
    setPopUpTitle("Sign Up");
  };

  const switchUser = () => {
    if (userType == 2) {
      setSwitchText("Grow your Organic Traffic");
      setUserType(1);
    } else {
      setSwitchText("Become a Content Writer");
      setUserType(2);
    }
  };

  useEffect(() => {
    if (!(resetToken && resetToken !== "" && resetToken !== undefined && resetToken !== null) && !isTermsCondition && !isPrivacyPolicy) {
      props.history.push("/");
    }
  }, [resetToken]);

  useEffect(() => {
    if (!(activateToken && activateToken !== "" && activateToken !== undefined && activateToken !== null) && !isTermsCondition && !isPrivacyPolicy ) {
      props.history.push("/");
    }
  }, [activateToken]);

  useEffect(() => {
    if (isSignIn == true) {
      props.history.push("/");
      signInClick();
    }
  }, [isSignIn]);

  useEffect(() => {
    if (isSignUp == true) {
      props.history.push("/");
      signUpCall();
    }
  }, [isSignUp]);

  useEffect(() => {
    if (isSeller == true) {
      setSwitchText("Grow your Organic Traffic");
      setUserType(1);
    }
  }, [isSeller]);

  useEffect(() => {
    // setServiceLoading(true);
    // props.getTopServices(
    //   (response) => {
    //     setServiceLoading(false);
    //     setServiceData(response.data)
    //   },
    //   () => {
    //     setServiceLoading(false);
    //   }
    // );
    setContentLoading(true);
    props.getTopContentWriters(
      (response) => {
        setContentLoading(false);
        setContentData(response.data)
      },
      () => {
        setContentLoading(false);
      }
    );
  }, []);

  useEffect(() => {
    setBlogLoading(true);
    let params = {
      "numberposts": 5,
      "orderby":"id",
      "order":"DESC",
      "paged":1,
      "search_keyword": ""
    }
    props.getBlogs(params, 
      (blog) => {
        setBlogLoading(false);
        setBlogData(blog.data.posts)
      },
      () => {
        setBlogLoading(false);
      }
    );
  }, []);

  const onLoginSuccess = (response) => {
    if (response.data != null) {
      props.callRefreshTokenSaga();
      setLoading(false);
      if (userType === response.data.lastlogintype) {
        if (response.data.activeusertype == 2) {
          props.history.push("/dashboard");
          window.location.reload()
        } else {
          props.history.push("/sellerDashboard");
        }
      } else {
        setLoginVisibility(false);
        setSignUpVisibility(false);
        setLoginType(response.data.lastlogintype);
        setConfirmationModal(true);
      }
    } else {
      setLoading(false);
      ToastAlert('error', response.message);
      PlaySound();
    }
  };

  const onLoginFailure = (error) => {
    setLoading(false);
    ToastAlert('error', error.message);
    PlaySound();
  };

  const handleSubmit = (userName, password) => {
    const body = { username: userName, password: password, usertype: userType };
    setLoading(true);
    props.userLogin(body, onLoginSuccess, onLoginFailure);
  };

  const socialLoginCall = (data) => {
    setLoading(true);
    props.userSocialLogin(
      data,
      (response) => {
        setLoading(false);
        if (!response.data.isregistered) {
          openJoinModal(data);
        } else {
          if (userType === response.data.lastlogintype) {
            if (response.data.activeusertype == 2) {
              props.history.push("/dashboard");
              window.location.reload()
            } else {
              props.history.push("/sellerDashboard");
            }
          } else {
            setLoginVisibility(false);
            setSignUpVisibility(false);
            setLoginType(response.data.lastlogintype);
            setConfirmationModal(true);
          }
          
        }
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound();
      }
    );
  };

  const openJoinModal = (data) => {
    setSocialCallData(data);
    setSignUpVisibility(false);
    setLoginVisibility(false);
    setJoinNow(true);
  };

  const openForgotModal = (data) => {
    setSignUpVisibility(false);
    setLoginVisibility(false);
    setForgotPasswordVisibility(true);
  };

  return (
    <div>
      <Loading isVisible={isLoading} />

      <LoginForm
        {...props}
        isVisible={isLoginFormVisible}
        onClose={() => setLoginVisibility(false)}
        popUpTitle={popUpTitle}
        isSignUpVisible={isSignUpVisibility}
        userType={userType}
        singnInClicked={() => setSignUpVisibility(false)}
        setSignInForm={() => setSignUpVisibility(true)}
        onJoinClick={signUpCall}
        openJoinModal={(data) => openJoinModal(data)}
        openForgotModal={() => openForgotModal()}
        socialLoginSuccess={(data) => socialLoginCall(data)}
        onSubmit={(userName, password) => handleSubmit(userName, password)}
      />

      <JoinNow
        {...props}
        isVisible={isJoinNowEvent}
        data={socialCallData}
        isSocialLogin={true}
        userType={userType}
        onClose={() => setJoinNow(false)}
        singnInClicked={() => {
          setLoginVisibility(true);
          setJoinNow(false);
        }}
        onClickSubmit={(params) => {
          setLoading(true);
          props.registrationCall(
            params,
            ({ data, message }) => {
              setLoading(false);
              if (data == null) {
                ToastAlert('error', message);
                PlaySound()
              }
              else {
                if (socialCallData.isSocialLogin) {
                  props.callRefreshTokenSaga();
                  if (data.activeusertype == 2) {
                    props.history.push("/dashboard");
                    window.location.reload()
                  } else {
                    props.history.push("/sellerDashboard");
                  }
                } else {
                  setJoinNow(false)
                  ToastAlert('success', message);
                  PlaySound();
                }
              }
            },
            (error) => {
              setLoading(false);
              ToastAlert('error', error.message);
              PlaySound();
            }
          );
        }}
      ></JoinNow>
      <ForgotPassword
        {...props}
        isForgotPasswordVisible={isForgotPasswordVisibility}
        onCloseForgot={() => setForgotPasswordVisibility(false)}
        onClickSubmit={(params) => {
          setLoading(true);
          props.forgotPassword(
            params,
            ({ data, message }) => {
              setLoading(false);
              setForgotPasswordVisibility(false)
              ToastAlert('success', message);
              PlaySound()
            },
            (error) => {
              setLoading(false);
              ToastAlert('error', error.message);
              PlaySound()
            }
          );
        }}
      ></ForgotPassword>
      <ResetPassword
        {...props}
        isResetPasswordVisible={isResetPasswordVisibility}
        token={resetToken}
        openReset={() => setResetPasswordVisibility(true)}
        onCloseReset={() => setResetPasswordVisibility(false)}
        onResetPassword={(params) => {
          setLoading(true);
          props.resetPassword(
            params,
            ({ data, message }) => {
              setLoading(false);
              setResetPasswordVisibility(false)
              props.history.push("/");
              ToastAlert('success', message);
              PlaySound()
            },
            (error) => {
              setLoading(false);
              ToastAlert('error', error.message);
              PlaySound()
            }
          );
        }}
      ></ResetPassword>
      <ActivateAccount
        {...props}
        isActivateVisible={isActivateVisibility}
        activateToken={activateToken}
        openActivate={() => setActivateVisibility(true)}
        onCloseReset={() => setActivateVisibility(false)}
        onActivateAccount={(data) => {
          setActivateVisibility(false);
          setLoading(true);
          if (data != null) {
            props.callRefreshTokenSaga();
            setLoading(false);
            if (data.activeusertype == 2) {
              props.history.push("/dashboard");
              window.location.reload()
            } else {
              props.history.push("/sellerDashboard");
            }
          }
        }}
      ></ActivateAccount>
      {confirmationModal && (
        <ConfirmationDialog
            open={confirmationModal}
            dialogTitle={"Confirm Action"}
            dialogContentText={"You're previously logged in as "+ (loginType == 1 ? "Content Writer" : "Buyer")}
            cancelButtonText={"Switch to "+ (loginType == 1 ? "Buyer" : "Content Writer")}
            okButtonText={"Proceed as "+ (loginType == 1 ? "Content Writer" : "Buyer")}
            onCancel={() => {
              setUserType(loginType == 1 ? 2 : 1);
              setConfirmationModal(false);
              if (loginType == 1) {
                props.history.push("/dashboard");
                window.location.reload()
              } else {
                props.history.push("/sellerDashboard");
              }
            }}
            onClose={() => {
              setConfirmationModal(false);
            }}
            onOk={() => {
              setUserType(loginType);
              setConfirmationModal(false);
              setLoading(true);
              props.switchUser(
                () => {
                  setLoading(false);
                  if (loginType == 2) {
                    props.history.push("/dashboard");
                    window.location.reload()
                  } else {
                    props.history.push("/sellerDashboard");
                  }
                },
                () => setLoading(false)
              );
            }}
        />
      )}
      { isTermsCondition == false  && isPrivacyPolicy == false &&
        <SocialBar />
      }
      { isTermsCondition == false  && isPrivacyPolicy == false && userType == 1  ? 
        <SellerHome
          title={switchText}
          isLogin={props.isLogin}
          blogLoading={blogLoading}
          blogData={blogData}
          activeusertype={props.activeusertype}
          history={props.history}
          signInCall={signInClick}
          signUpCall={signUpCall}
          switchUser={switchUser}/>
      : isTermsCondition == false  && isPrivacyPolicy == false &&
        <BuyerHome
          title={switchText}
          isLogin={props.isLogin}
          contentLoading={contentLoading}
          contentData={contentData}
          blogLoading={blogLoading}
          blogData={blogData}
          activeusertype={props.activeusertype}
          history={props.history}
          addFavorite={props.addFavoriteBlogger}
          removeFavorite={props.removeFavoriteBlogger}
          signInCall={signInClick}
          signUpCall={signUpCall}
          switchUser={switchUser}/>
      } 
      { isPrivacyPolicy == true &&
      <PrivacyPolicy 
      title={switchText}
      isLogin={props.isLogin}
      contentLoading={contentLoading}
      contentData={contentData}
      blogLoading={blogLoading}
      blogData={blogData}
      activeusertype={props.activeusertype}
      history={props.history}
      addFavorite={props.addFavoriteBlogger}
      removeFavorite={props.removeFavoriteBlogger}
      signInCall={signInClick}
      signUpCall={signUpCall}
      switchUser={switchUser}
      getAbout ={props.getAbout}/>
       } 
      { isTermsCondition == true &&
      <TermsCondition
      title={switchText}
      isLogin={props.isLogin}
      contentLoading={contentLoading}
      contentData={contentData}
      blogLoading={blogLoading}
      blogData={blogData}
      activeusertype={props.activeusertype}
      history={props.history}
      addFavorite={props.addFavoriteBlogger}
      removeFavorite={props.removeFavoriteBlogger}
      signInCall={signInClick}
      signUpCall={signUpCall}
      switchUser={switchUser}
      getAbout ={props.getAbout} />
      }
      <Footer />
    </div>
  );
};

export default MainPage;
