import React, { useEffect, useState } from "react";
import {  Button, Form } from "react-bootstrap";
import Modal from "@material-ui/core/Modal";
import Loading from "../../components/core/Loading";
import { ToastAlert,PlaySound } from "../../utils/sweetalert2";

export default function ResetPassword(props) {
  const [formValidated, setFormValidated] = useState(false);
  const [isLoading, setLoading] = useState(null);
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");
  const [token, setToken] = useState("");

  const validPasswordRegex = RegExp(/^(?=.{0,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$/);

  /**
   * verifies forgot password token
   */
  const verifyToken = () => {
    setLoading(true);
    let params = {
      token : props.token
    }
    props.verifyToken(
      params,
      (data) => {
        setLoading(false);
        props.openReset();
      },
      (error) => {
        setLoading(false);
        props.history.push("/");
        ToastAlert('error', error.message);
        PlaySound();
      }
    );
  };

  /**
   * resets password
   * @param {*} e 
   * @returns 
   */
  const resetPassword = (e) => {
    e.preventDefault();
    const form = e.currentTarget;
    setFormValidated(true);
    if (!password || !validPasswordRegex.test(password) || !confirmPassword || !validPasswordRegex.test(confirmPassword)|| password !== confirmPassword) {
      return;
    } else {
      let params = {
        token: token,
        password: password
      };
      props.onResetPassword(params);
    }
  };

  /**
  * closes the modal
  */
  const closeModal = () => {
    setPassword("");
    setConfirmPassword("");
    setFormValidated(false);
    props.onCloseReset();
  }

  useEffect (() => {
    if (!props.isResetPasswordVisible) {
      closeModal();
    }
  }, [props.isResetPasswordVisible])

  useEffect (() => {
    if (props.token && props.token !== "" && props.token !== undefined && props.token !== null) {
      setToken(props.token);
      verifyToken();
    }
    
  }, [props.token])

  const body = (
    <div>
      <div
        className="modal-dialog modal_popup"
        role="document"
      >
        <button
          type="button"
          class="btn btn-ar btn-default"
          id="cancel-btn2"
          data-dismiss="modal"
        />
        <div className="modal-content">
          <div class="modal-header">
            <h5 class="popup_title" id="confirmModalLabel">Reset Password</h5>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
              onClick={closeModal}
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body form_fields_white_bg">
            <Form noValidate validated={formValidated} onSubmit={resetPassword}>
              <Form.Group controlId="formBasicPassword">
                <Form.Control
                  type="password"
                  name="newPassword"
                  placeholder="Enter New Password"
                  onChange={(e) => {
                    setPassword(e.target.value);
                  }}
                  value={password}
                  required
                />
                <Form.Control.Feedback type="invalid">
                  Please enter new password.
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="formBasicPassword">
                <Form.Control
                  type="password"
                  name="confirmPassword"
                  placeholder="Confirm New Password"
                  className={"form-control " + (formValidated && (!(confirmPassword === password) || (confirmPassword === password && !validPasswordRegex.test(confirmPassword))) ? "is-invalid" : "")}
                  onChange={(e) => {
                    setConfirmPassword(e.target.value);
                  }}
                  value={confirmPassword}
                  required
                />
                <Form.Control.Feedback type="invalid">
                  {formValidated && !confirmPassword && "Please enter confirm password." } 
                </Form.Control.Feedback>
                <Form.Control.Feedback type="invalid">
                  {formValidated && confirmPassword && !(confirmPassword === password) && "Password and confirm password should be same."}
                </Form.Control.Feedback>
                <Form.Control.Feedback type="invalid">
                  {formValidated && confirmPassword && confirmPassword === password && !validPasswordRegex.test(confirmPassword) && "Please enter valid password."}
                </Form.Control.Feedback>
                <Form.Control.Feedback type="invalid">
                  {formValidated && confirmPassword && confirmPassword === password && !validPasswordRegex.test(confirmPassword) && "Must include one uppercase letter and one special character(~!@#$%^&*()_+=)"}
                </Form.Control.Feedback>
              </Form.Group>
              <div className="form-group text-center">
                <Button
                  type="submit"
                  className="btn_light_green effect effect-2"
                >
                  Submit
                </Button>
              </div>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );

  return (
    <div>
      <Loading isVisible={isLoading} />
      <Modal
        disableBackdropClick
        open={props.isResetPasswordVisible}
        onClose={closeModal}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
