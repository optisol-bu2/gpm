import React, { useEffect, useState } from "react";
import { Form } from "react-bootstrap";
import Modal from "@material-ui/core/Modal";

export default function JoinNow(props) {
  const [email, setEmail] = useState("");
  const [username, setUsername] = useState(props.data.givenName);
  const [password, setPassword] = useState("");
  const [formValidated, setFormValidated] = useState(false);

  const validPasswordRegex = RegExp(/^(?=.{0,})(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=]).*$/);

  useEffect(() => {
    try {
      setEmail(props.data.email);
      setPassword("");
      if (!props.data.isSocialLogin) {
        let userName = props.data.email.split("@");
        setUsername(userName[0]+(Math.round(100+(Math.random()*(999-100)))).toString());
      } else {
        setUsername(props.data.userName+(Math.round(100+(Math.random()*(999-100)))).toString());
      }
    } catch (error) {}
  }, [props.data]);

  const register = (e) => {
    e.preventDefault();
    setFormValidated(true);

    if (
      !props.data.isSocialLogin
        ? email === "" || username === "" || password === "" || !validPasswordRegex.test(password)
        : email === "" || username === ""
    )
      e.stopPropagation();
    else {
      let params = {
        email: props.data.email,
        username: username,
        socialtype: props.data.socialtype,
        usertype: props.data.usertype,
        profilepic: props.data.profilepic ? props.data.profilepic : "",
        password: password,
        appid: props.data.appid,
      };
      props.onClickSubmit(params);
    }
  };

  /**
  * closes the modal
  */
  const closeModal = () => {
    setEmail("");
    setUsername("");
    setPassword("");
    setFormValidated(false);
    props.onClose();
  }

  const body = (
    <div>
      <div
        className="modal-dialog modal_popup"
        role="document"
      >
        <button
          type="button"
          class="btn btn-ar btn-default"
          id="cancel-btn2"
          data-dismiss="modal"
        />
        <div className="modal-content">
          <div class="modal-header">
            <h5 class="popup_title" id="confirmModalLabel">Join</h5>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
              onClick={closeModal}
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body form_fields_white_bg">
            <Form noValidate validated={formValidated}>
              <Form.Group controlId="formBasictext">
                <Form.Control
                  type="text"
                  placeholder="Choose Your Username"
                  name="username"
                  onChange={(e) => {
                    setUsername(e.target.value);
                  }}
                  value={username}
                  required
                />
                <Form.Control.Feedback type="invalid">
                  Please enter your username.
                </Form.Control.Feedback>
              </Form.Group>
              <Form.Group controlId="formBasicEmail">
                <Form.Control
                  type="email"
                  placeholder="Choose Your Email"
                  disabled
                  name="email"
                  onChange={(e) => {
                    setEmail(e.target.value);
                  }}
                  value={email}
                  required
                />
                <Form.Control.Feedback type="invalid">
                  Please enter your email.
                </Form.Control.Feedback>
              </Form.Group>
              {!props.data.isSocialLogin ? (
                <Form.Group controlId="formBasicPassword">
                  <Form.Control
                    type="password"
                    name="password"
                    placeholder="Enter Password"
                    className={"form-control " + (formValidated && (!password || (password && !validPasswordRegex.test(password))) ? "is-invalid" : "")}
                    onChange={(e) => {
                      setPassword(e.target.value);
                    }}
                    value={password}
                    required
                  />
                  <Form.Control.Feedback type="invalid">
                    {formValidated && !password && "Please enter your password." } 
                  </Form.Control.Feedback>
                  <Form.Control.Feedback type="invalid">
                    {formValidated && password && !validPasswordRegex.test(password) && "Please enter valid password."}
                  </Form.Control.Feedback>
                  <Form.Control.Feedback type="invalid">
                    {formValidated && password && !validPasswordRegex.test(password) && "Must include one uppercase letter and one special character(~!@#$%^&*()_+=)"}
                  </Form.Control.Feedback>
                </Form.Group>
              ) : (
                <div />
              )}
              <div className="form-group text-center">
                <button
                  className="btn_light_green effect effect-2"
                  onClick={(e) => {
                    register(e);
                  }}
                >
                  Continue
                </button>
              </div>
            </Form>
          </div>
          <div className="modal-footer justify-content-center">
            <div className="text-center join_now_text">
              <button
                type="button"
                class="join_now"
                data-dismiss="modal"
                data-toggle="modal"
                onClick={() => {
                  setEmail("");
                  setUsername("");
                  setPassword("");
                  setFormValidated(false);
                  props.singnInClicked();
                }}
                data-target="#login-modal"
              >
               Back
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );

  return (
    <div>
      <Modal
        open={props.isVisible}
        onClose={closeModal}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
}
