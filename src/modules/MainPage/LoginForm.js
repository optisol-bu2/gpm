import React, { useEffect, useState } from "react";
import Modal from "@material-ui/core/Modal";
import { Button, Form } from "react-bootstrap";
import GoogleLoginBtn from "../../components/core/GoogleLoginBtn";
import FacebookButton from "../../components/core/FacebookButton";
import { Link } from "react-router-dom";
import Loading from "../../components/core/Loading";
import { ToastAlert,PlaySound } from "../../utils/sweetalert2";

const LoginForm = (props) => {
  const [userName, setUserName] = useState();
  const [password, setPassword] = useState();
  const [formValidated, setFormValidated] = useState(false);
  const [isLoading, setLoading] = useState(null);
  const [email, setEmail] = useState("");

  const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);
  
  const handleSubmit = (e) => {
    e.preventDefault();
    const form = e.currentTarget;
    setFormValidated(true);
    if (form.checkValidity() === false) {
      e.stopPropagation();
    } else {
      props.onSubmit(userName, password);
    }
  };

  const checkEmail = (event) => {
    event.preventDefault();
    if (email !== "") {
      if (!validEmailRegex.test(email)) {
        ToastAlert('error', "Please enter a valid email");
        PlaySound()
      } else {
        setLoading(true);
        let params = {
          email: email,
        };
        props.checkEmail(
          params,
          ({ data, message }) => {
            setLoading(false);
            if (!data.isregisteruser) {
              let data = {
                isSocialLogin: false,
                email: email,
                userName: "",
                usertype: props.userType,
              };
              setEmail("");
              setUserName("");
              setPassword("");
              setFormValidated(false);
              props.openJoinModal(data);
            } else {
              ToastAlert('error', message);
              PlaySound()
            }
          },
          (error) => {
            setLoading(false);
            ToastAlert('error', error.message);
            PlaySound()
          }
        );
      }
      
    }
  };

  useEffect(() => {
    localStorage.clear()
  }, [])

  const onGoogleLoginSuccess = (response) => {
    let obj = {
      email: response.profileObj.email,
      appid: response.profileObj.googleId,
      profilepic: response.profileObj.imageUrl,
      socialtype: "google",
      usertype: props.userType,
      isSocialLogin: true,
      userName: response.profileObj.givenName,
    };
    props.socialLoginSuccess(obj);
  };

  const onGoogleLoginFailure = (error) => {
    setLoading(false);
    if (!error.message == undefined) {
      ToastAlert('error', error.message);
      PlaySound()
    }
  };

  const onFacebbokLoginSuccess = (response) => {
    let obj = {
      email: response.email,
      appid: response.userID,
      profilepic: "https://graph.facebook.com/"+response.userID+"/picture?width=500&height=500",
      socialtype: "facebook",
      usertype: props.userType,
      isSocialLogin: true,
      userName:
        response.name.replace(/\s/g, "").toLowerCase() +
        response.userID.substring(0, 3),
    };
    props.socialLoginSuccess(obj);
  };

  /**
  * closes the modal
  */
  const closeModal = () => {
    setEmail("");
    setUserName("");
    setPassword("");
    setFormValidated(false);
    props.onClose();
  }

  const body = (
      <div className="modal_popup login_popup">
        <div className="modal-content">
          <div className="modal-header">
            <h4 className="popup_title">{props.popUpTitle}</h4>
            <button
            type="button"
            class="close"
            data-dismiss="modal"
            aria-label="Close"
            onClick={closeModal}
            >
            &times;
            </button>
          </div>
          <div className="modal-body">
            <div className="row mb-2">
              <div className="col-lg-6 col-sm-12 mb-3">
                <FacebookButton responseFacebook={onFacebbokLoginSuccess} />
              </div>
              <div className="col-lg-6 col-sm-12 mb-3 google_btn">
                <GoogleLoginBtn
                  onSuccess={onGoogleLoginSuccess}
                  onFailure={onGoogleLoginFailure}
                />
              </div>
            </div>
            <div className="row mb-3">
              <div className="col-lg-12 text-center">
                <div className="or_span_login"><span>or</span></div>
              </div>
            </div>
            <div className="row mb-4">
              <div className="col-lg-6 mx-auto">
                {!props.isSignUpVisible && (
                  <Form noValidate validated={formValidated} onSubmit={handleSubmit}>
                    <Form.Group controlId="formBasicEmail">
                      <Form.Control
                        type="text"
                        placeholder="Email / Username"
                        name="username"
                        onChange={(e) => {
                          setUserName(e.target.value);
                        }}
                        value={userName}
                        required
                      />
                      <Form.Control.Feedback type="invalid">
                        Please enter your username.
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Group controlId="formBasicPassword">
                      <Form.Control
                        type="password"
                        name="password"
                        placeholder="Enter Password"
                        onChange={(e) => {
                          setPassword(e.target.value);
                        }}
                        value={password}
                        required
                      />
                      <div className="text-right">
                        <Link onClick={props.openForgotModal} className="link_forgot_pswd">
                          Forgot Password?
                        </Link>
                      </div>
                      <Form.Control.Feedback type="invalid">
                        Please enter your password.
                      </Form.Control.Feedback>
                    </Form.Group>
                    <Form.Text className="forgot-password text-right">
                      {/* <Link to="/forgetPassword">Forgot Password?</Link> */}
                    </Form.Text>
                    <div className="d-flex flex-wrap justify-content-between ">
                      <div className="custom-control custom-checkbox mb-4">
                        <input
                          type="checkbox"
                          className="custom-control-input"
                          id="customCheckLogin"
                        />
                        <label className="custom-control-label link_remenberme" for="customCheckLogin">
                          Remember me
                        </label>
                      </div>
                    </div>  
                    <div className="form-group text-center">
                      <Button
                        type="submit"
                        className="btn_light_green effect effect-2"
                      >
                        Continue
                      </Button>
                    </div>
                                      
                  </Form>
                )}
                {props.isSignUpVisible && (
                  <Form onSubmit={checkEmail}>
                    <Form.Group controlId="formBasicEmail">
                      <Form.Control
                        type="text"
                        placeholder="Email"
                        name="email"
                        onChange={(e) => {
                          setEmail(e.target.value);
                        }}
                        value={email}
                        required
                      />
                      <Form.Control.Feedback type="invalid">
                        Please enter your username.
                      </Form.Control.Feedback>
                    </Form.Group>
                    <div className="form-group text-center mt-5">
                      <Button
                        type="submit"
                        className="btn_light_green effect effect-2"
                      >
                        Join Now
                      </Button>
                    </div>
                    {/* <Button
                      type="submit"
                      className="btn btn-primary rounded btn-block"
                      onClick={checkEmail}
                    >
                      Join Now
                    </Button> */}
                  </Form>
                )}
              </div>
             </div>
              {!props.isSignUpVisible && (
                <div className="modal-footer justify-content-center">
                  <div className="text-center join_now_text">
                    Not a member yet?
                    <button
                      type="button"
                      className="join_now"
                      data-dismiss="modal"
                      data-toggle="modal"
                      data-target="#signup-modal"
                      onClick={props.onJoinClick}
                    >
                      Join Now
                    </button>
                  </div>
                </div>
              )}            
          </div>
        </div>
      </div>
  );

  return (
    <div>
      <Loading isVisible={isLoading} />
      <Modal
        open={props.isVisible}
        onClose={closeModal}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
        className="modalsetpopup"
      >
        {body}
      </Modal>
    </div>
  );
};

export default LoginForm;
