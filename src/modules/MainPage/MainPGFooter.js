import React from "react";
import FooterBg from "../../assets/images/landing-page/footer_img.svg";
import logoImage from "../../assets/images/landing-page/rocketreach-logo.png";
import GoToTop from "../../assets/images/landing-page/gototop_arrow.png";
import { FRONT_END_URL } from "../../common/constants";;

const MainPGFooter = () => {
  return (
    <div>
      <footer>
        <section className="footer_bg">
           <div className="main_container">
              <div className="row">
                <div className="col-lg-5 text-center">
                    <div className="footer_top_img">
                      <img src={FooterBg} alt="" className="img-fluid maxwid_img" />
                    </div>
                </div>
              </div>
              <div className="row mt_minus">
                <div className="col-lg-4 col-md-12 text-lg-right text-md-center text-sm-center mb-3">
                    <div className="footer_logo pt-5 pr-5">
                      <a href="/">
                        <img src={logoImage} alt="" />
                      </a>
                    </div>
                </div>
                <div className="col-lg-8 mb-0">
                  <div className="row footer_menu_list">
                    <div className="col-lg-3 col-md-3 col-sm-12 mb-2">
                      {/* <h4 className="footer_side_head">Explore</h4> */}
                      {/* <ul className="footer_list">
                        <li><a href="">Content Writers</a></li>
                        <li><a href="">Outreach Specialists</a></li>
                        <li><a href="">SEO Marketing</a></li>
                        <li><a href="">SEO Services</a></li>
                        <li><a href="">Social Media</a></li>
                      </ul> */}
                      
                    </div>
                    <div className="col-lg-3 col-md-3 col-sm-12 mb-2">
                      <h4 className="footer_side_head">Explore</h4>
                      <ul className="footer_list"> 
                         {/* <li><a href="">Our Product</a></li>
                        <li><a href="">Documentation</a></li>
                        <li><a href="">Our Services</a></li>
                        <li><a href="">Company</a></li>
                        <li><a href="">What We Do?</a></li>  */}
                        <li><a href="">Our Company</a></li>
                        <li><a href="">What is SEO</a></li>
                       </ul>
                    </div> 
                    <div className="col-lg-6 col-md-6 col-sm-12 mb-2">
                      <h4 className="footer_side_head">Contact us</h4>
                      <ul className="contact_list">
                        <li><span className="bgicon fa fa-phone-alt"></span><span>650-338-8226</span></li>
                        <li><span className="bgicon fa fa-envelope"></span><a href = "mailto: support@enginescale.com" target="_blank"><span>support@enginescale.com</span></a></li>
                        <li><span className="bgicon fa fa-map-marker-alt"></span><span>Cupertino, USA</span></li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div className="row m-0 gototop">
              <span onClick={()=>{ window.scrollTo({top:0, behavior:"smooth"}); }}> <img src={GoToTop} alt="" width="40px" /></span>
              </div>
              <div className="row m-0">
                <div className="col-lg-6 text-lg-left text-md-center text-sm-center p-0">
                  <p className="copy_rts_text">2021 &copy; Engine<span className="text_green">Scale</span>. All rights reserved.</p>
                </div>
                <div className="col-lg-6 text-lg-right text-md-center text-sm-center p-0">
                  <ul className="footer_cp_links">
                    <li><a href={FRONT_END_URL+"/privacypolicy"}>Privacy Policy</a></li>
                    <li><a href={FRONT_END_URL+"/termscondition"}>Terms & Condition</a></li>
                    <li><a href="">Credits</a></li>
                  </ul>
                </div>
              </div>
            </div>
        </section>        
      </footer>
    </div>
  );
};

export default MainPGFooter;
