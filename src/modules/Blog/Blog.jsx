import React from "react";
import { connect } from "react-redux";
import Loading from "../../components/core/Loading";
import Pagination from "@material-ui/lab/Pagination";
import { ToastAlert } from "../../utils/sweetalert2";
import {
	getBlogs,
} from "../../Actions/blogAction";
import defaultImage from "../../assets/images/placeholder.jpeg";
import blog from "../../assets/images/menu-icons/blog-white.svg";
import moment from 'moment';
import ReactReadMoreReadLess from "react-read-more-read-less";

/**
 * Blog Component
 */
class Blog extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			totalRecord: 0,
			noofrecords: 12,
			searchText: "",
			pageIndex: 1,
			isLoading: null,
            blogList: [],
		};

	}

	componentDidMount() {
		this.getBlogs();
	}

    /**
     * function to get blog list
     */
     getBlogs = (loading) => {
        if (loading === undefined) {
            this.setState({ isLoading: true });
        }
        let params = {
            "numberposts": this.state.noofrecords,
            "orderby":"id",
            "order":"DESC",
            "paged":this.state.pageIndex,
            "search_keyword":this.state.searchText
        }
		this.props.getBlogs(params, (blog) => {
            if (blog.data) {
                this.setState({ totalRecord: Math.ceil(blog.data.total_posts/this.state.noofrecords) == 1 ? 0 : Math.ceil(blog.data.total_posts/this.state.noofrecords), blogList: blog.data.posts, isLoading: false });
                if (loading === undefined) {
                    window.scrollTo({ top: 0, behavior: "smooth" });
                }
            } else {
                this.setState({ totalRecord: 0, blogList: [], isLoading: false});
            }
		}, 
        () => {
            this.setState({ totalRecord: 0, blogList: [], isLoading: false});
        })
	}

    /**
     * gets blog list for given pageIndex
     * @param {*} pageIndex 
     */
    onPageChange = (pageIndex) => {
        this.setState({ pageIndex: pageIndex }, () => {
            this.getBlogs();
        });
    }

    /**
     * handles changes in the event and updates searchText state
     * @param {*} e 
     */
	onChangeSearchText = (e) => {
        if (e.target.value.length >= 3) {
            this.setState({ searchText: e.target.value, pageIndex: 1 }, () => {
                this.getBlogs(true);
            });
        } else {
            this.setState({ searchText: e.target.value, pageIndex: 1 }, () => {
                if (!this.state.searchText)
                this.getBlogs(true);
            });
        }
	}

    /**
     * changes the given time to date format
     * @param {*} dateTime
     * @returns
     */
    timeFormat = (dateTime) => {
        var utc = moment.utc(dateTime).toDate();
        var chatTime = moment(utc).local().format('MMM-DD-YYYY');
        return chatTime
    }

    /**
     * renders blog list
     * @returns 
     */
    renderBlogList = () => {
        const { blogList } = this.state;
        return (            
            <div class="row productscontainer-box">
                {blogList && blogList.length === 0 && 
                    <h3 className="norec_found">No Record Found</h3>
                }
                {blogList && blogList.length !== 0 && blogList.map((blog) => {
                    return (
                        <div class="col-lg-3 col-md-6 mb-4 item grid-view-products">
                            <div className="card h-100 bg-white">
                                <div className="position-relative">
                                    <a
                                        href={blog.post_url}
                                        target="_blank"
                                        class="position-relative"
                                    >
                                        <img 
                                            src={blog.image_url}
                                            onError={(e)=>{e.target.onerror = null; e.target.src=defaultImage}} 
                                            class="img_crop img-fluid" 
                                            alt="..." 
                                        />
                                    </a>
                                </div>
                                <div class="card-body w-100 p-2">
                                    <h5 className="product_title" title={blog.post_title}>{blog.post_title}</h5>
                                    <div class="row m-0 mt-3 mb-3 align-items-center">
                                        <div class="col-sm-12 col-8 p-0 text-left">
                                            <span className="blog-info mt-2 mb-2">
                                                <ReactReadMoreReadLess
                                                    charLimit={60}
                                                    readMoreText={"▼"}
                                                    readLessText={"▲"}
                                                    readMoreClassName="read-more-less--more"
                                                    readLessClassName="read-more-less--less"
                                                >
                                                    {blog.short_description}
                                                </ReactReadMoreReadLess>
                                            </span>
                                        </div>                    
                                        <div class="col-sm-12 col-4 p-0 text-right"><p className="sold_count_text">Posted at: {this.timeFormat(blog.post_date)}</p></div>
                                    </div>
                                    <div className="btn-groups d-flex justify-content-center mb-3">
                                        <a
                                            href={blog.post_url}
                                            target="_blank"
                                            class="view_btn"
                                        >
                                            <span class="fa fa-eye"></span> View
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}
            </div>
        )
    }


    /**
     * main render
     * @returns 
     */
	render() {
		const { searchText, pageIndex, totalRecord, isLoading } = this.state;
		return (
            <div class="page-wrapper">
                <Loading isVisible={isLoading} />
                <div className="page_title row m-0 mb-4 pb-2">
                    <div class="col-lg-1 col-md-2 col-3 p-0">
                        <span className="left_icon_title">
                            <i class="fab fa-blogger-b"></i>                            
                        </span>
                    </div>
                    <div class="col-lg-11 col-md-10 col-9 p-0 d-flex align-items-center">
                        <h2 className="right_title row m-0">Blog</h2>
                    </div>
                </div>
                <div class="form-group search-box search_boxes mb-3">
                    <input type="text" class="form-control mb-3" placeholder="Search for Blog..." name="searchText" value={searchText} onChange={this.onChangeSearchText} />
                    <span class="fa fa-search bg_icon_green"></span>
                </div>
                <div class="grey_box_bg p-3">			
                    <div class="content-wrapper">
                        <div class="row m-0">
                            <div class="col-12 p-0">                                
                                <div class="products-sec product-grid mt-4">
                                    {this.renderBlogList()}
                                </div>
                            </div>
                        </div>
                    </div>
                    {totalRecord !== 0 && 
                    <div className="favorite-pagination">
                        <Pagination
                            count={totalRecord}
                            onChange={(event, val) => {
                                if (pageIndex !== val) {
                                    this.onPageChange(val);
                                }
                            }}
                            page={pageIndex}
                            variant="outlined"
                            color="primary"
                        />
                    </div>
                    }
                </div>
            </div>
		);
	}
}

const mapStateToProps = ({}) => {
	return {}
}


export default connect(mapStateToProps, {
    getBlogs,
})(Blog);