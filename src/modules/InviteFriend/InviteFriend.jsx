import React from "react";
import { connect } from "react-redux";
import Loading from "../../components/core/Loading";
import { ToastAlert,PlaySound } from "../../utils/sweetalert2";
import { getAbout } from "../../Actions/aboutAction";
import { inviteFriend } from "../../Actions/inviteAction";
import inviteafriend from "../../assets/images/menu-icons/invite-a-friend-white.svg";
import invite_pg from "../../assets/images/other-pages/front-view-couple-planning-together-redecorate-home.png";

const validEmailRegex = RegExp(/^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i);

/**
 * InviteFriend Component
 */
class InviteFriend extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: null,
            inviteData: '',
            name: '',
            email: '',
            disable: false,
            validated : false,
            nameError: '',
            emailError: ''
		};

	}

	componentDidMount() {
        this.getAbout('invite_a_friend');
	}

    /**
     * gets about data
     */
    getAbout = (type) => {
        this.setState({ isLoading: true, inviteData: '' });
        this.props.getAbout(type,
            (about) => {
                if (about.data) {
                    this.setState({ inviteData: about.data, isLoading: false });
                }
            },
            (error) => {
                this.setState({ isLoading: false, inviteData: '' });
            }
        ); 
    }

    onFieldChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }


    /**
     * invites a friend
     */
    inviteFriend = () => {
        const {name, email} = this.state;
        if (!name || name == '') {
            this.setState({nameError: "Name is required!"})
            PlaySound()
        } else if (!email || email == '') {
            this.setState({emailError: "Email is required!"})
            PlaySound()
        } else if (email && !validEmailRegex.test(email)) {
            this.setState({emailError: "Please enter a valid email"})
            PlaySound()
        } else {
            var params = {
                name: name,
                email: email
            }
            this.setState({ isLoading: true, disable: true, nameError:'', emailError:''});
            this.props.inviteFriend(params,
                (invite) => {
                    this.setState({ name: '', email: '', isLoading: false, disable: false });
                    ToastAlert("success", invite.message)
                    PlaySound()
                },
                (error) => {
                    this.setState({ isLoading: false, disable: false });
                    ToastAlert("error", error.message)
                    PlaySound()
                }
            ); 
        }
    }

    /**
     * main render
     * @returns 
     */
	render() {
		const { isLoading, disable, validated, isModalOpen, inviteLink, inviteData } = this.state;
		return (
            <div class="page-wrapper">
                <Loading isVisible={isLoading} />
                <div className="page_title row m-0 mb-4 pb-2">
                    <div class="col-lg-1 col-md-2 col-3 p-0">
                        <span className="left_icon_title">
                            {/* <img src={inviteafriend} alt="" /> */}
                            <i class="fa fa-user-plus"></i>
                        </span>
                    </div>
                    <div class="col-lg-11 col-md-10 col-9 p-0 d-flex justify-content-between align-items-center">
                        <h2 className="right_title row m-0">Invite a Friend</h2>
                    </div>
                </div>
                <div class="grey_box_bg">
                    <div class="row m-0">
                        <div class="col-md-12 col-lg-6 col-xl-6 pl-5 pr-5 pt-4 pb-0 pad_sets_zero order-2 order-sm-1 order-md-1 order-lg-1">                            
                            <h4 class="sub_sub_title mb-3 mt-3">Invite a Friend</h4>
                            <p class="text-justify" dangerouslySetInnerHTML={{ __html: inviteData}}></p>
                            <div className="form_fields">
                                <div className="form-group">
                                    <input type="text" name="name" value={this.state.name} onChange={this.onFieldChange} className="input_field" placeHolder="Enter Name" />
                                    <div class="error-text">{this.state.nameError}</div> 
                                </div>
                                <div className="form-group">
                                    <input type="text" name="email" value={this.state.email} onChange={this.onFieldChange} className="input_field" placeHolder="Enter Email" />
                                    <div class="error-text">{this.state.emailError}</div> 
                                </div>                               
                                <div className="form-group text-right">
                                    <button type="submit" className="btn_light_green" disabled={disable} onClick={() => this.inviteFriend()}>Invite <span className="fa fa-arrow-right"></span></button>
                                </div>
                            </div> 
                        </div>
                        <div class="col-md-12 col-lg-6 col-xl-6 p-0 order-1 order-sm-2 order-md-2 order-lg-2">
                            <img src={invite_pg} alt="" className="img-fluid bor_rad_set" />
                        </div>
                    </div>
                </div>                    
            </div>
		);
	}
}

const mapStateToProps = ({ state }) => {
	return {};
}


export default connect(mapStateToProps, {
    getAbout,
    inviteFriend,
})(InviteFriend);