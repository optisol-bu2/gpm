import React from "react";
import { connect } from 'react-redux';
import { Elements } from "@stripe/react-stripe-js";
import CardInfoForm from "../../modules/MarketPlace/CardInfoForm";
import { ToastAlert, PlaySound, PlayAudio } from '../../utils/sweetalert2';
import Loading from "../../components/core/Loading";
import TextLoading from "../../components/core/TextLoading";
import ConfirmationDialog from "../../components/core/ConfirmationDialog/ConfirmationDialog";
import { STRIPE_KEY } from "../../common/constants";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from 'moment';
import PhoneInput from "react-phone-input-2";
import 'react-phone-input-2/lib/style.css'
import Pusher from 'pusher-js';
import { PUSHER_KEY, PUSHER_CLUSTER } from "../../common/constants";
import {
    myprofileAction,
    deactivateAccountAction,
    myprofileUpdateAction,
    passwordChangeAction,
    countryAction,
    stateAction,
    userInfoUpdateAction,
    userInfoAction,
    notificationAction,
    notificationPutAction,
    getBillingAction,
    billingAction,
    getProfilePic,
    updateProfilePic,
    removeProfilePic,
    getReasonAction,
    billingInfoAccountLink,
    getBillingInformationStatus,
    addBillingToken,
    addPayPalCode,
    getPayPalLink,
    changeDefaultMethod,
    KycInformationAction,
} from '../../Actions/myprofileAction';
import { reLoginAction, getSavedCards, addCard, removeCard, forceLogOut } from '../../Actions/actionContainer';
import profileImage from '../../assets/images/avatar.png';
import Select from 'react-select';
import ImageCropperComponent from "../../components/core/ImageCropperComponent";
import { FRONT_END_URL } from "../../common/constants";
import { ThumbUpAltOutlined } from "@material-ui/icons";

import ContentWriterWarning from "../Services/ContentWriterWarning";
import ContentWriterCongrats from "../Services/ContentWriterCongrats";
import ContentWriterReject from "../Services/ContentWriterReject";
import ContentWriterThankYou from "../Services/ContentWriterThankYou";
import ContentWriterTest from "../Services/ContentWriterTest";
import { getHeaderInfo } from "../../Actions/inboxAction";
import { checkBillingInfo, addCustomOrder, addQuestions, getViewQuestions, searchServiceUsers } from "../../Actions/actionContainer";

const stripe = window.Stripe(STRIPE_KEY);
const ssnRegex = /^(\d{3}-?\d{2}-?\d{4})$/;

class Profile extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            username: '',
            onlinestatus: { label: "Online", value: "1" },
            email: '',
            notification_sound: '',
            togglePassword: false,
            // old_password: '',
            // new_password: '',
            // repeat_new_password: '',
            country: null,
            state: null,
            phone: '',
            phoneValid: true,
            bio: '',
            // notification: true,
            billing_firstname: '',
            billing_lastname: '',
            billing_country: null,
            billing_state: null,
            billing_address: '',
            billing_city: '',
            billing_postalcode: '',
            billing_gender: null,
            billing_dob: null,
            billing_phone: '',
            billingPhoneValid: true,
            billing_ssn: '',
            billing_ssn_value: '',
            billing_isemailbillinginfo: false,
            billing_stripe_account_id: false,
            reason: null,
            isLoading: null,
            isTextLoading: null,
            loadingText: '',
            isModalOpen: false,
            account_type: null,
            card_details: null,
            bank_country: null,
            bank_currency: null,
            bank_routing_number: '',
            bank_account_number: '',
            bank_account_holder_name: '',
            bank_account_holder_type: null,
            isaccountadded: null,
            isbankdetailsadded: null,
            isbillinginformationadded: null,
            banking_country: null,
            banking_currency: null,
            banking_routing_number: '',
            banking_account_number: '',
            banking_account_holder_name: '',
            banking_account_holder_type: null,
            billingMethod: 'card',
            billingType: 'paypal',
            activeBilling: null,
            isEditBank: false,
            isEditCard: false,
            isEditPaypal: false,
            paypalInfo: null,
            saved_cards: [],
            confirmRemoveCard: false,
            removeCardId: '',
            countryArray: [],
            stateArray: [],
            kycinformation: '',
            isContentWriterWarningModalOpen: false,
            isConterntWriterTestModelOpen: false,
            isThankyouModalOpen: false,
            isCongratulationModelOpen: false,
            isRejectedUserModelOpen: false,
            statusOptions: [
                { label: "Online", value: "1" }, { label: "Offline", value: "0" }
            ],
            genderOptions: [
                { label: "Male", value: "male" }, { label: "Female", value: "female" }
            ],
            countryOptions: [
                { label: "USA", value: "US" }
            ],
            stateOptions: [
                { label: "Alabama", value: "AL" },
                { label: "Alaska", value: "AK" },
                { label: "Arizona", value: "AZ" },
                { label: "Arkansas", value: "AR" },
                { label: "California", value: "CA" },
                { label: "Colorado", value: "CO" },
                { label: "Connecticut", value: "CT" },
                { label: "Delaware", value: "DE" },
                { label: "District of Columbia", value: "DC" },
                { label: "Florida", value: "FL" },
                { label: "Georgia", value: "GA" },
                { label: "Hawaii", value: "HI" },
                { label: "Idaho", value: "ID" },
                { label: "Illinois", value: "IL" },
                { label: "Indiana", value: "IN" },
                { label: "Iowa", value: "IA" },
                { label: "Kansas", value: "KS" },
                { label: "Kentucky", value: "KY" },
                { label: "Louisiana", value: "LA" },
                { label: "Maine", value: "ME" },
                { label: "Maryland", value: "MD" },
                { label: "Massachusetts", value: "MA" },
                { label: "Michigan", value: "MI" },
                { label: "Minnesota", value: "MN" },
                { label: "Mississippi", value: "MS" },
                { label: "Missouri", value: "MO" },
                { label: "Montana", value: "MT" },
                { label: "Nebraska", value: "NE" },
                { label: "Nevada", value: "NV" },
                { label: "New Hampshire", value: "NH" },
                { label: "New Jersey", value: "NJ" },
                { label: "New Mexico", value: "NM" },
                { label: "New York", value: "NY" },
                { label: "North Carolina", value: "NC" },
                { label: "North Dakota", value: "ND" },
                { label: "Ohio", value: "OH" },
                { label: "Oklahoma", value: "OK" },
                { label: "Oregon", value: "OR" },
                { label: "Pennsylvania", value: "PA" },
                { label: "Puerto Rico", value: "PR" },
                { label: "Rhode Island", value: "RI" },
                { label: "South Carolina", value: "SC" },
                { label: "South Dakota", value: "SD" },
                { label: "Tennessee", value: "TN" },
                { label: "Texas", value: "TX" },
                { label: "Utah", value: "UT" },
                { label: "Vermont", value: "VT" },
                { label: "Virginia", value: "VA" },
                { label: "Washington", value: "WA" },
                { label: "West Virginia", value: "WV" },
                { label: "Wisconsin", value: "WI" },
                { label: "Wyoming", value: "WY" },
            ],
            currencyOptions: [
                { label: "US Dollar", value: "usd" },
            ],
            accountOptions: [
                { label: "Individual", value: "individual" },
                { label: "Company", value: "company" },
            ],
            openImageCrop: false,
            imgUrlToCrop: "",
        }
        this.billing_firstname = React.createRef();
        this.billing_lastname = React.createRef();
        this.billing_address = React.createRef();
        this.billing_city = React.createRef();
        this.billing_postalcode = React.createRef();
        this.billing_dob = React.createRef();
        this.billing_phone = React.createRef();
        this.billing_ssn = React.createRef();
        this.bank_routing_number = React.createRef();
        this.bank_account_number = React.createRef();
        this.bank_account_holder_name = React.createRef();
        const params = new URLSearchParams(this.props.location.search);
        this.code = params.get('code');
    }


    componentDidMount() {
        //this. soundNotification();
        // this.onToggle();
        this.changeProfileTab();
        this.changeInfoTab();
        this.props.getProfilePic();
        this.props.myprofileAction();
        this.getCountry();
        this.getKycInformation();
        this.props.userInfoAction();
        // this.props.notificationAction();
        this.props.getReasonAction();
        if (this.props.activeusertype === 1) {
            if (this.code && this.code !== "" && this.code !== undefined && this.code !== null) {
                this.addPayPalCode(this.code);
            }
            this.setState({ isLoading: true });
            this.props.getBillingInformationStatus();
            this.props.getBillingAction(
                billing => {
                    this.setState({ isLoading: false });
                },
                error => {
                    this.setState({ isLoading: false });
                }
            );
            let userId = this.props.userData.id
            const pusher = new Pusher(PUSHER_KEY, {
                cluster: PUSHER_CLUSTER,
                encrypted: true
            });
            const userBecomeBlogerChannel = pusher.subscribe('GPM-SELLER-STATUS-' + userId);
            userBecomeBlogerChannel.bind('USER_SELLER_STATUS_NOTIFICATION', data => {
                this.props.getHeaderInfo(
                    (response) => {
                        this.setState({ isLoading: false });
                        if (this.props.headerInfo.iscongratulationmodel) {
                            this.props.checkCongratulation(true);  
                            }
                        if (this.props.userData.iscongratulationmodel) {
                            this.setState({ isCongratulationModelOpen: true });
                        }else{
                            this.setState({ isCongratulationModelOpen: false });
                        }
                        this.checkTestInfo();
                    },
                    (error) => {
                        this.setState({ isLoading: false });
                    }
                );

            });
            this.props.getHeaderInfo(
                (response) => {
                    this.setState({ isLoading: false });
                    if (this.props.userData.iscongratulationmodel) {
                        this.setState({ isCongratulationModelOpen: true });
                    }else{
                        this.setState({ isCongratulationModelOpen: false });
                    }
                    this.checkTestInfo();
                },
                (error) => {
                    this.setState({ isLoading: false });
                }
            );
            this.changeBillingTab();
        } else if (this.props.activeusertype === 2) {
            this.getSavedCards();
            this.changeCardTab();
        }
    }

    componentDidUpdate() {
        let interval = setInterval(() => {
            if (this.billing_firstname.current.value && this.billing_firstname.current.value !== this.state.billing_firstname) {
                this.setState({ billing_firstname: this.billing_firstname.current.value }, () => {
                    clearInterval(interval)
                });
            }
            if (this.billing_lastname.current.value && this.billing_lastname.current.value !== this.state.billing_lastname) {
                this.setState({ billing_lastname: this.billing_lastname.current.value }, () => {
                    clearInterval(interval)
                });
            }
            if (this.billing_address.current.value && this.billing_address.current.value !== this.state.billing_address) {
                this.setState({ billing_address: this.billing_address.current.value }, () => {
                    clearInterval(interval)
                });
            }
            if (this.billing_city.current.value && this.billing_city.current.value !== this.state.billing_city) {
                this.setState({ billing_city: this.billing_city.current.value }, () => {
                    clearInterval(interval)
                });
            }
            if (this.billing_postalcode.current.value && this.billing_postalcode.current.value !== this.state.billing_postalcode) {
                this.setState({ billing_postalcode: this.billing_postalcode.current.value }, () => {
                    clearInterval(interval)
                });
            }
            if (this.billing_dob.current.value && this.billing_dob.current.value !== this.state.billing_dob) {
                this.setState({ billing_dob: this.billing_dob.current.value }, () => {
                    clearInterval(interval)
                });
            }
            if (this.billing_phone.current.value && this.billing_phone.current.value !== this.state.billing_phone) {
                this.setState({ billing_phone: this.billing_phone.current.value }, () => {
                    clearInterval(interval)
                });
            }
            if (this.billing_ssn.current.value && (this.billing_ssn.current.value !== this.state.billing_ssn || this.billing_ssn.current.value !== this.state.billing_ssn_value)) {
                this.setState({ billing_ssn: this.billing_ssn.current.value, billing_ssn_value: this.billing_ssn.current.value ? this.ssnFormat(this.billing_ssn.current.value) : '', }, () => {
                    clearInterval(interval)
                });
            }
            if (this.bank_routing_number.current.value && this.bank_routing_number.current.value !== this.state.bank_routing_number) {
                this.setState({ bank_routing_number: this.bank_routing_number.current.value }, () => {
                    clearInterval(interval)
                });
            }
            if (this.bank_account_number.current.value && this.bank_account_number.current.value !== this.state.bank_account_number) {
                this.setState({ bank_account_number: this.bank_account_number.current.value }, () => {
                    clearInterval(interval)
                });
            }
            if (this.bank_account_holder_name.current.value && this.bank_account_holder_name.current.value !== this.state.bank_account_holder_name) {
                this.setState({ bank_account_holder_name: this.bank_account_holder_name.current.value }, () => {
                    clearInterval(interval)
                });
            }
        }, 100)
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.profile) {
            this.state.statusOptions.forEach(status => {
                if (status.value == nextProps.profile.onlinestatus) {
                    this.setState({
                        onlinestatus: status,
                    });
                }
            })
            this.setState({ username: nextProps.profile.username, email: nextProps.profile.email, notification_sound: nextProps.profile.notification_sound })
            PlayAudio(nextProps.profile.notification_sound)
        }
        if (nextProps.userInfo.bio) {
            if (nextProps.userInfo.country && nextProps.userInfo.country.id) {
                this.getState(nextProps.userInfo.country.id, nextProps.userInfo.state.id);
            }
            if (nextProps.userInfo.country) {
                this.state.countryArray.forEach(country => {
                    if (country.value == nextProps.userInfo.country.id) {
                        this.setState({
                            country: country,
                        });
                    }
                })
            }
            this.setState({ bio: nextProps.userInfo.bio, phone: nextProps.userInfo.phone });
        }

        // if (nextProps.notification) {
        //     this.setState({ notification: nextProps.notification.notification })
        // }
        if (nextProps.billingInfo) {
            if (this.props.activeusertype === 1) {
                if (nextProps.billingInfo.paypal && nextProps.billingInfo.paypal.length != 0 && Object.keys(nextProps.billingInfo.paypal).length != 0) {
                    this.setState({
                        paypalInfo: nextProps.billingInfo.paypal,
                    }, () => {
                        if (this.state.paypalInfo.isactive != undefined && this.state.paypalInfo.isactive == true) {
                            this.setState({
                                activeBilling: "paypal",
                                billingType: "paypal",
                            });
                        }
                    })
                }
                if (nextProps.billingInfo.stripe && nextProps.billingInfo.stripe.length != 0 && Object.keys(nextProps.billingInfo.stripe).length != 0) {
                    if (nextProps.billingInfo.stripe.isactive != undefined && nextProps.billingInfo.stripe.isactive == true) {
                        this.setState({
                            activeBilling: "stripe",
                            billingType: "stripe",
                        });
                    }
                    // this.state.genderOptions.forEach(gender => {
                    //     if (gender.value == nextProps.billingInfo.stripe.billing_gender) {
                    //         this.setState({
                    //             billing_gender: gender,
                    //         });
                    //     }
                    // })
                    // this.state.countryOptions.forEach(country => {
                    //     if (country.value == nextProps.billingInfo.stripe.billing_country) {
                    //         this.setState({
                    //             billing_country: country,
                    //         });
                    //     }
                    // })
                    // this.state.stateOptions.forEach(state => {
                    //     if (state.value == nextProps.billingInfo.stripe.billing_state) {
                    //         this.setState({
                    //             billing_state: state,
                    //         });
                    //     }
                    // })
                    this.setState({
                        billing_firstname: nextProps.billingInfo.stripe.billing_firstname,
                        billing_lastname: nextProps.billingInfo.stripe.billing_lastname,
                        billing_address: nextProps.billingInfo.stripe.billing_address,
                        billing_city: nextProps.billingInfo.stripe.billing_city,
                        billing_postalcode: nextProps.billingInfo.stripe.billing_postal_code,
                        billing_dob: nextProps.billingInfo.stripe.billing_dob ? nextProps.billingInfo.stripe.billing_dob : null,
                        billing_phone: nextProps.billingInfo.stripe.billing_phone,
                        billing_ssn: nextProps.billingInfo.stripe.billing_ssn != '' ? new Array(3).join('*') + "-" + new Array(2).join('*') + "-" + new Array(4).join('*') : '',
                        billing_ssn_value: nextProps.billingInfo.stripe.billing_ssn ? this.ssnFormat("*********") : '',
                        billing_isemailbillinginfo: nextProps.billingInfo.stripe.billing_isemailbillinginfo ? nextProps.billingInfo.stripe.billing_isemailbillinginfo : false,
                        billing_stripe_account_id: nextProps.billingInfo.stripe.billing_stripe_account_id ? nextProps.billingInfo.stripe.billing_stripe_account_id : false,
                        billing_gender: nextProps.billingInfo.stripe.billing_gender,
                        billing_state: nextProps.billingInfo.stripe.billing_state,
                        billing_country: nextProps.billingInfo.stripe.billing_country


                    })
                    if (nextProps.billingInfo.stripe.billing_stripe_default_payment_method && nextProps.billingInfo.stripe.billing_stripe_default_payment_method.length != 0) {
                        this.setState({
                            account_type: nextProps.billingInfo.stripe.billing_stripe_default_payment_method.type,
                        });
                        if (nextProps.billingInfo.stripe.billing_stripe_default_payment_method.type == "bank_account") {
                            this.state.currencyOptions.forEach(currency => {
                                if (currency.value == nextProps.billingInfo.stripe.billing_stripe_default_payment_method.currency) {
                                    this.setState({
                                        bank_currency: currency,
                                        banking_currency: currency,
                                    });
                                }
                            })
                            this.state.accountOptions.forEach(account => {
                                if (account.value == nextProps.billingInfo.stripe.billing_stripe_default_payment_method.account_holder_type) {
                                    this.setState({
                                        bank_account_holder_type: account,
                                        banking_account_holder_type: account,
                                    });
                                }
                            })
                            this.state.countryOptions.forEach(country => {
                                if (country.value == nextProps.billingInfo.stripe.billing_stripe_default_payment_method.country) {
                                    this.setState({
                                        bank_country: country,
                                        banking_country: country,
                                    });
                                }
                            })
                            this.setState({
                                billingMethod: "bank",
                                bank_routing_number: new Array(nextProps.billingInfo.stripe.billing_stripe_default_payment_method.routing_number.length - 3).join('X') + nextProps.billingInfo.stripe.billing_stripe_default_payment_method.routing_number.slice(-4),
                                bank_account_number: nextProps.billingInfo.stripe.billing_stripe_default_payment_method.last4 ? new Array(9).join('X') + nextProps.billingInfo.stripe.billing_stripe_default_payment_method.last4 : '',
                                bank_account_holder_name: nextProps.billingInfo.stripe.billing_stripe_default_payment_method.account_holder_name,
                                banking_routing_number: new Array(nextProps.billingInfo.stripe.billing_stripe_default_payment_method.routing_number.length - 3).join('X') + nextProps.billingInfo.stripe.billing_stripe_default_payment_method.routing_number.slice(-4),
                                banking_account_number: nextProps.billingInfo.stripe.billing_stripe_default_payment_method.last4 ? new Array(9).join('X') + nextProps.billingInfo.stripe.billing_stripe_default_payment_method.last4 : '',
                                banking_account_holder_name: nextProps.billingInfo.stripe.billing_stripe_default_payment_method.account_holder_name,
                            });
                        } else if (nextProps.billingInfo.stripe.billing_stripe_default_payment_method.type == "card") {
                            this.setState({
                                billingMethod: "card",
                                bank_country: null,
                                bank_currency: null,
                                bank_routing_number: '',
                                bank_account_number: '',
                                bank_account_holder_name: '',
                                bank_account_holder_type: null,
                                banking_country: null,
                                banking_currency: null,
                                banking_routing_number: '',
                                banking_account_number: '',
                                banking_account_holder_name: '',
                                banking_account_holder_type: null,
                                card_details: nextProps.billingInfo.stripe.billing_stripe_default_payment_method,
                            });
                        }
                    }
                }
            }
        }
        if (nextProps.bankInfo) {
            this.setState({
                isaccountadded: nextProps.bankInfo.isaccountadded,
                isbankdetailsadded: nextProps.bankInfo.isbankdetailsadded,
                isbillinginformationadded: nextProps.bankInfo.isbillinginformationadded,
            })
        }
    }

    onToggle = () => {
        this.setState({ notification_sound: !this.state.notification_sound });
        PlayAudio(!this.state.notification_sound);
    }


    /**
     * redirects to Billing Information Tab
     */
    changeBillingTab = () => {
        const isBilling = this.props.location.pathname.includes('/profile/billing')
        if (isBilling == true) {
            let button = document.getElementById("v-billing-tab");
            button.click();
        }
    }

    /**
     * checks whether the seller content writer exam info
     */
    checkTestInfo = () => {
        this.setState({ isLoading: true });
        if (this.props.headerInfo.onboardingexam) {
            this.setState({ isContentWriterWarningModalOpen: !this.props.headerInfo.isalreadysubmitted, isRejectedUserModelOpen: this.props.headerInfo.isrejecteduser, isLoading: false, });
            if (!this.props.headerInfo.isalreadysubmitted || this.props.headerInfo.isapproveduser) {
                this.setState({ isThankyouModalOpen: false });
            } else {
                this.setState({ isThankyouModalOpen: true });
            }
            if (this.props.headerInfo.isrejecteduser) {
                this.setState({ isThankyouModalOpen: false });
            }
        }else{
            this.setState({isLoading:false, isContentWriterWarningModalOpen:false, isThankyouModalOpen: false, isRejectedUserModelOpen:false, isConterntWriterTestModelOpen:false });
        }
    }


    /**
     * redirects to Profile Tab
     */
    changeProfileTab = () => {
        const isProfile = this.props.location.pathname.includes('/profile/details')
        if (isProfile == true) {
            let button = document.getElementById("v-profile-tab");
            button.click();
        }
    }

    /**
     * redirects to Information Tab
     */
    changeInfoTab = () => {
        const isInfo = this.props.location.pathname.includes('/profile/info')
        if (isInfo == true) {
            let button = document.getElementById("v-info-tab");
            button.click();
        }
    }

    /**
     * redirects to Card Information Tab
     */
    changeCardTab = () => {
        const isCard = this.props.location.pathname.includes('/profile/card')
        if (isCard == true) {
            let button = document.getElementById("v-card-tab");
            button.click();
        }
    }

    onProfileChange = (e) => {
        if (e.target.name !== "billing_ssn") this.setState({ [e.target.name]: e.target.value })
        if (e.target.name === "bank_account_number") {
            if (e.target.value.includes("X")) {
                this.setState({ [e.target.name]: '' })
            }
        }
        if (e.target.name === "billing_ssn") {
            if (e.target.value.includes("*")) {
                this.setState({ [e.target.name]: '', billing_ssn_value: '' })
            } else if (e.target.value.split('-').join('').length <= 9) {
                this.setState({ [e.target.name]: e.target.value, billing_ssn_value: this.ssnFormat(e.target.value) })
            }
        }
    }

    onProfileSubmit = (e) => {
        if (!this.state.username) {
            return (ToastAlert('error', 'Username should not be blank.'), PlaySound())
        }
        else if (!this.state.email) {
            return (ToastAlert('error', 'Email should not be blank.'), PlaySound())
        }
        else {
            this.setState({ isLoading: true });
            let data = {
                username: this.state.username,
                email: this.state.email,
                onlinestatus: this.state.onlinestatus.value,
                notification_sound: this.state.notification_sound
            }
            this.props.myprofileUpdateAction(data,
                profile => {
                    this.setState({ isLoading: false });
                    this.props.myprofileAction();
                    this.props.reLoginAction();
                    ToastAlert('success', profile.message)
                    PlaySound()
                },
                error => {
                    this.setState({ isLoading: false });
                    ToastAlert('error', error.message)
                    PlaySound()
                }
            );
        }

    }

    // changePassword = (e) => {
    //     if (!this.state.old_password) {
    //         return ToastAlert('error', 'Please enter old password.')
    //     } else if (!this.state.new_password) {
    //         return ToastAlert('error', 'Please enter new password.')
    //     } else if (!this.state.repeat_new_password) {
    //         return ToastAlert('error', 'Please retype new password.')
    //     } else if (this.state.new_password !== this.state.repeat_new_password) {
    //         return ToastAlert('error', 'Please retype the same new password.')
    //     } else {
    //         this.setState({ isLoading : true });
    //         let body = {
    //             oldpassword: this.state.old_password,
    //             newpassword: this.state.new_password
    //         }
    //         this.props.passwordChangeAction(body,
    //             password => {
    //                 this.setState({ 
    //                     old_password: '',
    //                     new_password: '',
    //                     repeat_new_password: '',
    //                     isLoading : false
    //                 });
    //                 ToastAlert('success', password.message)
    //             },
    //             error => {
    //                 this.setState({ isLoading : false });
    //                 ToastAlert('error', error.message)
    //             }
    //         );
    //     }
    // }

    /**
     * gets country values for dropdown
     */
    getCountry = () => {
        this.props.countryAction(
            country => {
                if (country.data) {
                    var countryArray = [];
                    country.data.forEach(countryData => {
                        let countryObj = {
                            label: countryData.name,
                            value: countryData.id.toString()
                        }
                        countryArray.push(countryObj);
                    })
                    this.setState({ countryArray: countryArray });
                } else {
                    this.setState({ countryArray: [] });
                }
            },
            error => {
                this.setState({ countryArray: [] });
            }
        );
    }

    /**
    * kyc information
    * 
    */
    getKycInformation = () => {
        this.setState({ isLoading: true, kycinformation: '' });
        this.props.KycInformationAction(
            (kycinformation) => {
                if (kycinformation.data) {
                    this.setState({ kycinformation: kycinformation.data, isLoading: false });
                }
            },
            (error) => {
                this.setState({ isLoading: false, kycinformation: '' });
            }
        );
    }



    /**
     * gets states for the country
     */
    getState = (country, stateId) => {
        this.props.stateAction(country,
            state => {
                if (state.data) {
                    var stateArray = [];
                    state.data.forEach(stateData => {
                        let stateObj = {
                            label: stateData.name,
                            value: stateData.id.toString()
                        }
                        stateArray.push(stateObj);
                    })
                    if (stateId != undefined) {
                        stateArray.forEach(state => {
                            if (state.value == stateId) {
                                this.setState({
                                    state: state,
                                });
                            }
                        })
                    }
                    this.setState({ stateArray: stateArray });
                } else {
                    this.setState({ stateArray: [] });
                }
            },
            error => {
                this.setState({ stateArray: [] });
            }
        );
    }

    handleInfo = () => {
        // if (!this.state.bio) {
        //     return ToastAlert('error', 'Bio should not be blank.')
        // } else if (!this.state.country || this.state.country === null) {
        //     return ToastAlert('error', 'Please select country.')
        // } else if (!this.state.phone) {
        //     return ToastAlert('error', 'Please enter your phone number')
        // } else if (this.state.phone && !this.state.phoneValid) {
        //     ToastAlert("error", "Please enter valid phone number")
        // } else if (!this.state.state || this.state.state === null) {
        //     return ToastAlert('error', 'Please select state.')
        // } 
        if (this.state.phone && !this.state.phoneValid) {
            ToastAlert("error", "Please enter valid phone number")
            PlaySound()
        } else {
            this.setState({ isLoading: true });
            let data = {
                "bio": this.state.bio,
                "country": this.state.country ? this.state.country.value : '',
                "state": this.state.state ? this.state.state.value : '',
                "phoneno": this.state.phone
            }
            this.props.userInfoUpdateAction(data,
                userInfo => {
                    this.setState({ isLoading: false });
                    ToastAlert('success', userInfo.message)
                    PlaySound()
                },
                error => {
                    this.setState({ isLoading: false });
                    ToastAlert('error', error.message)
                    PlaySound()
                }
            );
        }
    }

    // checkBoxChange = (e) => {
    //     this.setState({ notification: !this.state.notification, isLoading : true }, () => {
    //         let data = {
    //             notification: this.state.notification
    //         }
    //         this.props.notificationPutAction(data,
    //             notification => {
    //                 this.setState({ isLoading : false });
    //                 ToastAlert('success', notification.message)
    //             },
    //             error => {
    //                 this.setState({ isLoading : false });
    //                 ToastAlert('error', error.message)
    //             }
    //         );
    //     })
    // }

    /**
     * changes given string to SSN format
     * @param {*} ssnString 
     * @returns 
     */
    ssnFormat = (ssnString) => {
        var string = ssnString.split('-').join('');
        var len = string.length;
        var hypenFormat = len > 0 ? len > 1 ? len > 2 ? len > 3 ? len > 4 ? len > 5 ? string[0] + string[1] + string[2] + '-' + string[3] + string[4] + '-' : string[0] + string[1] + string[2] + '-' + string[3] + string[4] : string[0] + string[1] + string[2] + '-' + string[3] : string[0] + string[1] + string[2] : string[0] + string[1] : string[0] : '';
        var formattedString = hypenFormat + string.substring(5);
        return formattedString.substring(0, 11);
    }

    /**
     * updates email checkbox value to the state
     * @param {*} e 
     */
    emailCheckBoxChange = (e) => {
        if (e.target.id === "customCheckBuyerEmail") {
            if (this.state.billing_buyer_isemailbillinginfo == "0") {
                this.setState({ billing_buyer_isemailbillinginfo: "1" });
            } else {
                this.setState({ billing_buyer_isemailbillinginfo: "0" });
            }
        } else if (e.target.id === "customCheckEmail") {
            // if (this.state.billing_isemailbillinginfo == "0") {
            //     this.setState({ billing_isemailbillinginfo: "1" });
            // } else {
            //     this.setState({ billing_isemailbillinginfo: "0" });
            // }
            let data = {};
            if (this.state.billing_isemailbillinginfo == false) {
                this.setState({ billing_isemailbillinginfo: true });
                data = {
                    billing_isemailbillinginfo: true
                }
            } else {
                this.setState({ billing_isemailbillinginfo: false });
                data = {
                    billing_isemailbillinginfo: false,
                }
            }
            this.setState({ isLoading: true });
            this.props.billingAction(data,
                billing => {
                    if (billing.data) {
                        this.props.getBillingAction();
                        this.setState({ isLoading: false });
                        ToastAlert('success', billing.message)
                        PlaySound()
                    } else {
                        this.setState({ isLoading: false });
                        ToastAlert('error', billing.message)
                        PlaySound()
                    }
                },
                error => {
                    this.setState({ isLoading: false });
                    ToastAlert('error', error.message)
                    PlaySound()
                }
            );

        }
    }

    /**
     * updates billing_dob state with formatted date
     * @param {*} e 
     */
    onDobChange = (e) => {
        if (e != null) {
            this.setState({ billing_dob: moment(e).format('DD/MM/YYYY') });
        } else {
            this.setState({ billing_dob: null });
        }
    }

    onBillingSubmit = (e) => {
        if (!this.state.billing_firstname) {
            return (ToastAlert('error', 'Please enter first name.'), PlaySound())
        } else if (!this.state.billing_lastname) {
            return (ToastAlert('error', 'Please enter last name.'), PlaySound())
        } else if (!this.state.billing_dob) {
            return (ToastAlert('error', 'Please enter DOB.'), PlaySound())
        } else if (!this.state.billing_gender || this.state.billing_gender == null) {
            return (ToastAlert('error', 'Please select gender.'), PlaySound())
        } else if (!this.state.billing_phone) {
            return (ToastAlert('error', 'Please enter phone number.'), PlaySound())
        } else if (this.state.billing_phone && !this.state.billingPhoneValid) {
            ToastAlert("error", "Please enter valid phone number")
            PlaySound()
        } else if (!this.state.billing_address) {
            return (ToastAlert('error', 'Please enter billing address.'), PlaySound())
        } else if (!this.state.billing_city) {
            return (ToastAlert('error', 'Please enter city.'), PlaySound())
        } else if (!this.state.billing_country || this.state.billing_country == null) {
            return (ToastAlert('error', 'Please select country.'), PlaySound())
        } else if (!this.state.billing_state || this.state.billing_state == null) {
            return (ToastAlert('error', 'Please select state.'), PlaySound())
        } else if (!this.state.billing_postalcode) {
            return (ToastAlert('error', 'Please enter postal code.'), PlaySound())
        } else if (!this.state.billing_ssn) {
            return (ToastAlert('error', 'Please enter ssn.'), PlaySound())
        } else if (this.state.billing_ssn && this.state.billing_ssn.includes("*")) {
            this.setState({ billing_ssn: '', billing_ssn_value: '' });
            let ssn_button = document.getElementById("billing_ssn");
            ssn_button.focus();
            return (ToastAlert('error', 'Please enter ssn'), PlaySound())
        } else if (!ssnRegex.test(this.state.billing_ssn) || this.state.billing_ssn.split('-').join('').length !== 9) {
            return (ToastAlert('error', 'Please enter valid ssn.'), PlaySound())
        } else if (!this.state.bank_currency || this.state.bank_currency == null) {
            return (ToastAlert('error', 'Please select currency.'), PlaySound())
        } else if (!this.state.bank_routing_number) {
            return (ToastAlert('error', 'Please enter routing number.'), PlaySound())
        } else if (!this.state.bank_account_number) {
            return (ToastAlert('error', 'Please enter account number.'), PlaySound())
        } else if (this.state.bank_account_number && this.state.bank_account_number.includes("X")) {
            this.setState({ bank_account_number: '' });
            let button = document.getElementById("bank_account_number");
            button.focus();
            return (ToastAlert('error', 'Please enter full account number.'), PlaySound())
        } else if (!this.state.bank_account_holder_name) {
            return (ToastAlert('error', 'Please enter account holder name.'), PlaySound())
        } else if (!this.state.bank_account_holder_type || this.state.bank_account_holder_type == null) {
            return (ToastAlert('error', 'Please select account holder type.'), PlaySound())
        } else {
            this.setState({ isTextLoading: true, loadingText: "Updating Billing Info..." });
            let bankData = {
                country: this.state.billing_country.value,
                currency: this.state.bank_currency.value,
                routing_number: this.state.bank_routing_number,
                account_number: this.state.bank_account_number,
                account_holder_name: this.state.bank_account_holder_name,
                account_holder_type: this.state.bank_account_holder_type.value,
            }
            stripe.createToken('bank_account', bankData).then(
                result => {
                    if (result.token) {
                        let data = {
                            billing_firstname: this.state.billing_firstname,
                            billing_lastname: this.state.billing_lastname,
                            billing_country: this.state.billing_country.value,
                            billing_state: this.state.billing_state.value,
                            billing_address: this.state.billing_address,
                            billing_city: this.state.billing_city,
                            billing_postal_code: this.state.billing_postalcode,
                            billing_gender: this.state.billing_gender.value,
                            billing_dob: this.state.billing_dob,
                            billing_phone: this.state.billing_phone,
                            billing_ssn: this.state.billing_ssn.split('-').join(''),
                            billing_isemailbillinginfo: this.state.billing_isemailbillinginfo,
                            token: result.token.id
                        }
                        this.setState({ loadingText: "Stripe Verification in progress..." });
                        this.props.billingAction(data,
                            billing => {
                                if (billing.data) {
                                    this.setState({ isTextLoading: false, loadingText: "", billing_ssn: "***-**_****", bank_account_number: new Array(this.state.bank_account_number.length - 3).join('X') + this.state.bank_account_number.slice(-4) });
                                    ToastAlert('success', billing.message)
                                    PlaySound()
                                    this.props.history.push("/sellerDashboard");
                                } else {
                                    this.setState({ isTextLoading: false, loadingText: "" });
                                    ToastAlert('error', billing.message)
                                    PlaySound()
                                }
                            },
                            error => {
                                this.setState({ isTextLoading: false, loadingText: "" });
                                ToastAlert('error', error.message)
                                PlaySound()
                            }
                        );
                    } else {
                        this.setState({ isTextLoading: false, loadingText: "" });
                        ToastAlert('error', result.error.message);
                        PlaySound()
                    }
                }
            )
        }
    }

    /**
     * redirects to stripe boarding page
     */
    billingInfoAccountLink = () => {
        this.setState({ isLoading: true });
        this.props.billingInfoAccountLink(
            {
                "returnurl": FRONT_END_URL + "/profile/billing",
                "refreshurl": FRONT_END_URL + "/profile/billing"
            },
            response => {
                this.setState({ isLoading: false });
                if (response.data && response.data.accountlink) {
                    window.location.href = response.data.accountlink;
                }
            },
            error => {
                this.setState({ isLoading: false });
                ToastAlert('error', error.message);
                PlaySound()
            }
        );
    }

    /**
     * redirects to paypal login page
     */
    getPayPalLink = () => {
        this.setState({ isLoading: true });
        this.props.getPayPalLink(
            response => {
                this.setState({ isLoading: false });
                if (response.data && response.data.url) {
                    window.location.href = response.data.url + FRONT_END_URL + "/profile/billing";
                }
            },
            error => {
                this.setState({ isLoading: false });
                ToastAlert('error', error.message);
                PlaySound()
            }
        );
    }

    /**
     * changes default payment method
     * @param {*} methodType
     */
    changeDefaultMethod = (methodType) => {
        this.setState({ isLoading: true });
        this.props.changeDefaultMethod({ method: methodType },
            response => {
                this.props.getBillingInformationStatus();
                this.props.getBillingAction();
                this.setState({ isLoading: false });
                ToastAlert('success', response.message);
                PlaySound()
            },
            error => {
                this.setState({ isLoading: false });
                ToastAlert('error', error.message);
                PlaySound()
            }
        );
    }

    /**
     * Adds card for seller
     * @param {*} response 
     */
    onAddBillingCard = (response) => {
        let data = {
            token: response.id
        }
        this.setState({ isTextLoading: true, loadingText: "Adding Card Details..." });
        this.props.addBillingToken(data,
            billing => {
                this.setState({ isTextLoading: false, loadingText: "", isEditCard: false });
                ToastAlert('success', billing.message);
                PlaySound()
                // this.props.history.push("/sellerDashboard");
                this.props.getBillingInformationStatus();
                this.props.getBillingAction();
            },
            error => {
                this.setState({ isTextLoading: false, loadingText: "" });
                ToastAlert('error', error.message)
                PlaySound()
            }
        );
    }

    /**
     * Adds bank info for seller
     * @param {*} response 
     */
    saveBillingBank = (response) => {
        if (!this.state.banking_account_number) {
            return (ToastAlert('error', 'Please enter account number.'), PlaySound())
        } else if (this.state.banking_account_number && this.state.banking_account_number.includes("X")) {
            this.setState({ banking_account_number: '' });
            let button = document.getElementById("banking_account_number");
            button.focus();
            return (ToastAlert('error', 'Please enter full account number.'), PlaySound())
        } else if (!this.state.banking_account_holder_name) {
            return (ToastAlert('error', 'Please enter account holder name.'), PlaySound())
        } else if (!this.state.banking_account_holder_type || this.state.banking_account_holder_type == null) {
            return (ToastAlert('error', 'Please select account holder type.'), PlaySound())
        } else if (!this.state.banking_routing_number) {
            return (ToastAlert('error', 'Please enter routing number.'), PlaySound())
        } else if (this.state.banking_routing_number && this.state.banking_routing_number.includes("X")) {
            this.setState({ banking_routing_number: '' });
            let routingButton = document.getElementById("banking_routing_number");
            routingButton.focus();
            return (ToastAlert('error', 'Please enter full routing number.'), PlaySound())
        } else if (!this.state.banking_country || this.state.banking_country == null) {
            return (ToastAlert('error', 'Please select country.'), PlaySound())
        } else if (!this.state.banking_currency || this.state.banking_currency == null) {
            return (ToastAlert('error', 'Please select currency.'), PlaySound())
        } else {
            this.setState({ isTextLoading: true, loadingText: "Updating Billing Info..." });
            let bankData = {
                country: this.state.banking_country.value,
                currency: this.state.banking_currency.value,
                routing_number: this.state.banking_routing_number,
                account_number: this.state.banking_account_number,
                account_holder_name: this.state.banking_account_holder_name,
                account_holder_type: this.state.banking_account_holder_type.value,
            }
            stripe.createToken('bank_account', bankData).then(
                result => {
                    if (result.token) {
                        let data = {
                            token: result.token.id
                        }
                        this.setState({ loadingText: "Adding Bank Details..." });
                        this.props.addBillingToken(data,
                            billing => {
                                this.setState({ isTextLoading: false, loadingText: "", isEditBank: false });
                                ToastAlert('success', billing.message);
                                PlaySound()
                                // this.props.history.push("/sellerDashboard");
                                this.props.getBillingInformationStatus();
                                this.props.getBillingAction();
                            },
                            error => {
                                this.setState({ isTextLoading: false, loadingText: "" });
                                ToastAlert('error', error.message)
                                PlaySound()
                            }
                        );
                    } else {
                        this.setState({ isTextLoading: false, loadingText: "" });
                        ToastAlert('error', result.error.message);
                        PlaySound()
                    }
                }
            )
        }
    }

    /**
     * Adds PayPal code
     */
    addPayPalCode = (code) => {
        let data = {
            code: code
        }
        this.setState({ isLoading: true });
        this.props.addPayPalCode(data,
            paypal => {
                this.code = null
                this.setState({ isLoading: false });
                ToastAlert('success', paypal.message);
                PlaySound()
                this.props.getBillingInformationStatus();
                this.props.getBillingAction();
            },
            error => {
                this.setState({ isLoading: false });
                ToastAlert('error', error.message)
                PlaySound()
            }
        );
    }

    /**
     * Opens intimation modal if billing information is not filled
     */
    handleTabChange = () => {
        if (!this.state.billing_stripe_account_id) {
            this.setState({ isModalOpen: true });
        }
    }

    /**
     * closes the modal
     */
    closeModal = () => {
        this.setState({
            isModalOpen: false,
        })
        let button = document.getElementById("v-billing-tab");
        button.click();
    }

    onFileChange = (e) => {
        if (e.target.files[0].size <= 1048576) {
            this.setState({ imgUrlToCrop: URL.createObjectURL(e.target.files[0]), openImageCrop: true })
        } else {
            ToastAlert('info', "Max size of 1MB allowed")
            PlaySound()
        }
    };

    /**
     * saves profile picture
     */
    saveProfilePic = (profilePic) => {
        let formData = new FormData();
        formData.append("profilepic", profilePic);
        this.setState({ isLoading: true });
        this.props.updateProfilePic(formData,
            profilepic => {
                this.props.getProfilePic();
                this.props.reLoginAction();
                this.setState({ isLoading: false });
                ToastAlert('success', profilepic.message)
                PlaySound()
            },
            error => {
                this.setState({ isLoading: false });
                ToastAlert('error', error.message)
                PlaySound()
            }
        );
    }

    /**
     * remove profile picture
     */
    removeProfilePic = () => {
        this.setState({ isLoading: true });
        this.props.removeProfilePic(
            profilepic => {
                this.props.getProfilePic();
                this.props.reLoginAction();
                this.setState({ isLoading: false });
                ToastAlert('success', profilepic.message)
                PlaySound()
            },
            error => {
                this.setState({ isLoading: false });
                ToastAlert('error', error.message)
                PlaySound()
            }
        );
    };

    onReset = (e) => {
        // this.setState({ 
        //     old_password: '',
        //     new_password: '',
        //     repeat_new_password: '',
        // })
        this.props.getProfilePic();
        this.props.myprofileAction();
        this.getCountry();
        this.props.userInfoAction();
        this.props.notificationAction();
        if (this.props.activeusertype === 1) {
            this.props.getBillingAction();
        } else if (this.props.activeusertype === 2) {
            this.getSavedCards();
        }
    }

    onDeactivate = (e) => {
        if (!this.state.reason || this.state.reason === null) {
            return (ToastAlert('error', 'Please select reason for deactivating account.'), PlaySound())
        }
        else {
            let body = {
                reason: this.state.reason.value
            }
            this.props.deactivateAccountAction(body);
            this.props.forceLogOut();
        }
    }

    /**
     * Gets saved cards of the user
     */
    getSavedCards = () => {
        this.setState({ isLoading: true }, () => {
            this.props.getSavedCards(
                (response) => {
                    this.setState({ saved_cards: response.data }, () => {
                        this.setState({ isLoading: false });
                    });
                },
                (error) => {
                    this.setState({ isLoading: false });
                }
            );
        });
    }

    /**
     * Adds saved cards of the user
     * @param {*} response 
     */
    onAddCard = (response) => {
        let data = {
            paymentmethodid: response.paymentMethod.id
        }
        this.setState({ isLoading: true });
        this.props.addCard(data,
            card => {
                this.getSavedCards();
                this.setState({ isLoading: false });
                ToastAlert('success', card.message)
                PlaySound()
            },
            error => {
                this.getSavedCards();
                this.setState({ isLoading: false });
                ToastAlert('error', error.message)
                PlaySound()
            }
        );
    }

    /**
     * Removes saved cards of the user
     * @param {*} cardId 
     */
    removeCard = (cardId) => {
        this.setState({ isLoading: true });
        this.props.removeCard(cardId,
            card => {
                ToastAlert('success', card.message)
                PlaySound()
                this.getSavedCards();
                this.setState({ isLoading: false });
            },
            error => {
                this.setState({ isLoading: false });
            }
        );
    }


    render() {
        const { reasonData } = this.props;
        const { countryArray, stateArray, genderOptions, countryOptions, stateOptions, currencyOptions, accountOptions, kycinformation, isContentWriterWarningModalOpen, isConterntWriterTestModelOpen, isThankyouModalOpen, isCongratulationModelOpen, isRejectedUserModelOpen } = this.state;
        return (
            <div className="page-wrapper">
                <Loading isVisible={this.state.isLoading} />
                <TextLoading isVisible={this.state.isTextLoading} loadingText={this.state.loadingText} />
                <div className="row">
                    <div className="col-lg-12">
                        <div className="profile_pg_menu nav" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a className="nav-link active" id="v-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false" onClick={() => this.props.history.push("/profile/details")}>
                                <span className="fa fa-user"></span>&nbsp;Profile
                            </a>
                            <a className="nav-link" id="v-info-tab" data-toggle="pill" href="#v-pills-info" role="tab" aria-controls="v-pills-info" aria-selected="false" onClick={() => this.props.history.push("/profile/info")}>
                                <span className="fa fa-info-circle"></span>&nbsp;Info
                            </a>
                            {this.props.activeusertype === 1 &&
                                <a className="nav-link" id="v-billing-tab" data-toggle="pill" href="#v-pills-billing" role="tab" aria-controls="v-pills-billing" aria-selected="false" onClick={() => this.props.history.push("/profile/billing")}>
                                    <span className="fa fa-wallet"></span>&nbsp;Billing Information
                                </a>
                            }
                            {this.props.activeusertype === 2 &&
                                <a className="nav-link" id="v-card-tab" data-toggle="pill" href="#v-pills-card" role="tab" aria-controls="v-pills-card" aria-selected="false" onClick={() => this.props.history.push("/profile/card")}>
                                    <span className="fa fa-credit-card"></span>&nbsp;Card Info
                                </a>
                            }
                        </div>
                    </div>
                    {this.props.location.pathname.includes('/billing') && this.props.activeusertype === 1 &&
                        <div>
                            {/* content writer test warning model */}
                            <ContentWriterWarning
                                isVisible={isContentWriterWarningModalOpen}
                                onClose={() => {
                                    this.setState({ isContentWriterWarningModalOpen: false });
                                    this.props.history.push("/sellerDashboard")
                                }}
                                onSuccess={() => {
                                    this.setState({ isLoading: false, isConterntWriterTestModelOpen: true });
                                }}
                                onFailure={() => {
                                    this.setState({ isContentWriterWarningModalOpen: false });
                                }}
                                {...this.props}
                            />
                            {/* open content writer test model */}
                            <ContentWriterTest
                                isVisible={isConterntWriterTestModelOpen}
                                onClose={() => {
                                    this.setState({ isConterntWriterTestModelOpen: false });
                                    this.props.history.push("/sellerDashboard")
                                }}
                                onSuccess={() => {
                                    this.setState({ isConterntWriterTestModelOpen: false, isThankyouModalOpen: true, isContentWriterWarningModalOpen: false });
                                    this.props.history.push("/profile/billing")
                                }}
                                onFailure={() => {
                                    this.setState({ isConterntWriterTestModelOpen: false });
                                }}
                                {...this.props}
                            />
                            {/* Thank you contentWriter test model */}
                            <ContentWriterThankYou
                                isVisible={isThankyouModalOpen}
                                onClose={() => {
                                    this.setState({ isThankyouModalOpen: false });
                                    this.props.history.push("/sellerDashboard")
                                }}
                                onSuccess={() => {
                                    this.setState({ isThankyouModalOpen: false });
                                    this.props.history.push("/profile/billing")
                                }}
                                onFailure={() => {
                                    this.setState({ isThankyouModalOpen: false });
                                }}
                                {...this.props}
                            />
                            {/* congratulation contentWriter test model */}
                            <ContentWriterCongrats
                                isVisible={isCongratulationModelOpen}
                                onClose={() => {
                                    this.setState({
                                        isCongratulationModelOpen: false,
                                    })
                                    this.props.history.push("/sellerDashboard")
                                }}
                                onSuccess={() => {
                                    this.setState({ isLoading: false, isContentWriterWarningModalOpen: false, isConterntWriterTestModelOpen: false, isThankyouModalOpen: false, isCongratulationModelOpen: false });
                                    this.checkBillingInfo();
                                    this.props.history.push("/profile/billing")
                                }}
                                onFailure={() => {
                                    this.setState({ isCongratulationModelOpen: false });
                                }}
                                {...this.props}
                            />
                            {/* reject contentWriter test model */}
                            <ContentWriterReject
                                isVisible={isRejectedUserModelOpen}
                                onClose={() => {
                                    this.setState({ isRejectedUserModelOpen: false });
                                    this.props.history.push("/sellerDashboard")
                                }}
                                onSuccess={() => {
                                    this.setState({ isRejectedUserModelOpen: false });
                                    this.props.history.push("/profile/billing")
                                }}
                                onFailure={() => {
                                    this.setState({ isRejectedUserModelOpen: false });
                                }}
                                {...this.props}
                            />
                        </div>
                    }
                    <div className="col-lg-12 tab_cnt_profile">
                        <div className="tab-content form_fields_white_bg grey_box_bg" id="v-pills-tabContent">
                            <div className="tab-pane p-2 fade show active" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                <div className="m-3 bg-white bor-rad p-3">
                                    <h6 className="profile_heading">Profile Details</h6>
                                    <div class="row bor_btm_span">
                                        <div className="col-md-3 text-center text-md-left">
                                        <input type="file" id="upload-button" style={{ display: "none" }} onChange={this.onFileChange} hidden accept="image/*" />
                                        <label htmlFor="upload-button " className="cursor-pointer">
                                            <img
                                                src={this.props.profilePic ? this.props.profilePic.profilepic ? this.props.profilePic.profilepic : profileImage : profileImage}
                                                onError={(e) => { e.target.onerror = null; e.target.src = profileImage }}
                                                className="rounded-circle"
                                                alt=""
                                                width="100"
                                                height="100"
                                            />
                                            </label>

                                        </div>
                                        <div className="col-md-6 mt-4 text-center text-md-left">
                                            <ImageCropperComponent
                                                isOpen={this.state.openImageCrop}
                                                onClose={() => { this.setState({ openImageCrop: false }) }}
                                                aspectRatio={400 / 400}
                                                width={400}
                                                height={400}
                                                src={this.state.imgUrlToCrop}
                                                setCroppedImage={(image) => {
                                                    if (image.blobFile != '')
                                                        this.saveProfilePic(image.blobFile);
                                                }}
                                            />
                                            <input type="file" id="actual-btn" onChange={this.onFileChange} hidden accept="image/*" />
                                            <label for="actual-btn" className="btn_light_green mr-3">Upload Photo</label>
                                            <button className="btn_red" disabled={this.props.profilePic ? this.props.profilePic.profilepic ? false : true : true} onClick={this.removeProfilePic}>Remove</button>
                                            <p className="text-muted mt-2">Allowed JPG or PNG. Max size of 1MB</p>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="form-group col-md-5 mt-0 mt-md-3">
                                            <label className="small text-muted" for="">Username (You can change one time) </label>
                                            <input type="text" className="input_field_w" name="username" value={this.state.username} onChange={this.onProfileChange} />
                                        </div>
                                        <div className="form-group col-md-5 mt-0 mt-md-3">
                                            <label className="small text-muted" for="">Email</label>
                                            <input type="email" className="input_field_w" name="email" value={this.state.email} disabled />
                                        </div>
                                        <div className="form-group col-md-2 mt-0 mt-md-3">
                                            <label className="small text-muted" for="">Online Status</label>
                                            <Select
                                                className="basic-single-select"
                                                isSearchable={false}
                                                onChange={(selected) => {
                                                    this.setState({ onlinestatus: selected });
                                                }}
                                                value={this.state.onlinestatus}
                                                classNamePrefix={"select_dropdown"}
                                                options={this.state.statusOptions}
                                            />
                                        </div>
                                        <div className="form-group col-md-5 mt-0 mt-md-3">
                                            <div className="d-flex w-100">
                                                <label className="small text-muted" for="switch11">Disable/Enable Sound</label>
                                                <div className="custom-control custom-switch ml-5">
                                                    <input type="checkbox" className="custom-control-input" id="switch11" value={this.state.notification_sound} checked={this.state.notification_sound} onChange={this.onToggle} />
                                                    <label className="custom-control-label" for="switch11"></label>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="col-12 text-center text-md-right mt-3 mb-3">
                                            <a onClick={this.onReset} className="btn_grey w-auto mr-3">Reset</a>
                                            <button
                                                className="btn_light_green"
                                                onClick={(e) => {
                                                    e.currentTarget.blur();
                                                    this.onProfileSubmit();
                                                }}
                                            >Save Changes
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div className="row m-3 bg-white bor-rad p-3">
                                    <h6 className="profile_heading">Account Deactivation</h6>
                                    <div className="row w-100">
                                        <div className="form-group col-md-6">
                                            <label className="small text-muted w-100" for="">I'm leaving because...</label>
                                            <Select
                                                className="basic-single-select"
                                                placeholder="Choose a reason"
                                                isClearable
                                                isSearchable={true}
                                                onChange={(selected) => {
                                                    this.setState({ reason: selected });
                                                }}
                                                value={this.state.reason}
                                                classNamePrefix={"select_dropdown"}
                                                options={reasonData}
                                            />
                                        </div>
                                        <div className="col-md-2 mt-3 pt-3">
                                            <button
                                                onClick={(e) => {
                                                    e.currentTarget.blur();
                                                    this.onDeactivate();
                                                }}
                                                className="btn_light_green"
                                            >Deactivate</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="tab-pane p-2 fade" id="v-pills-info" role="tabpanel" aria-labelledby="v-pills-info-tab">
                                <div className="m-3 bg-white bor-rad p-3">
                                    <h6 className="profile_heading">Information</h6>
                                    {/* <label className="small text-muted" for="switch11">Notification</label>
                                    <div className="d-flex w-100">
                                        <label className="text-muted" for="switch11">Enable/Disable Notification</label>
                                        <div className="custom-control custom-switch ml-5">
                                            <input type="checkbox" className="custom-control-input" id="switch11" checked={this.state.notification} onChange={this.checkBoxChange} />
                                            <label className="custom-control-label" for="switch11"></label>
                                        </div>
                                    </div> */}
                                    <div className="row">
                                        <div className="form-group col-12">
                                            <label className="small text-muted" for="">Bio</label>
                                            <textarea className="input_field_w" name="bio" value={this.state.bio} onChange={this.onProfileChange} rows="8"></textarea>
                                        </div>
                                        <div className="form-group col-md-6">
                                            <label className="small text-muted" for="">Country</label>
                                            <Select
                                                className="basic-single-select"
                                                classNamePrefix={"select_dropdown"}
                                                placeholder="Choose Country"
                                                isClearable
                                                isSearchable={true}
                                                onChange={(selected) => {
                                                    this.setState({ country: selected }, () => {
                                                        if (selected != null) {
                                                            this.getState(selected.value);
                                                        } else {
                                                            this.setState({ state: null, stateArray: [] });
                                                        }
                                                    });


                                                }}
                                                value={this.state.country}
                                                options={countryArray}
                                            />
                                        </div>
                                        <div className="form-group col-md-6">
                                            <label className="small text-muted" for="">State</label>
                                            <Select
                                                className="basic-single-select"
                                                classNamePrefix={"select_dropdown"}
                                                placeholder="Choose State"
                                                isClearable
                                                isSearchable={true}
                                                onChange={(selected) => {
                                                    this.setState({ state: selected });
                                                }}
                                                value={this.state.state}
                                                options={stateArray}
                                            />
                                        </div>
                                        <div className="form-group col-md-6 phone_field">
                                            <label className="small text-muted" for="">Phone</label>
                                            <PhoneInput
                                                country={'us'}
                                                countryCodeEditable={false}
                                                value={this.state.phone}
                                                onChange={(value, country) => {
                                                    if (value.length == (country.format.match(/\./g) || []).length) {
                                                        this.setState({ phone: value, phoneValid: true })
                                                    } else {
                                                        this.setState({ phone: value, phoneValid: false })
                                                    }
                                                }}
                                            />
                                        </div>
                                        <div className="col-12 text-center text-md-right">
                                            <a onClick={this.onReset} className="btn_grey w-auto mr-3">Reset</a>
                                            <button
                                                onClick={(e) => {
                                                    e.currentTarget.blur();
                                                    this.handleInfo();
                                                }}
                                                className="btn_light_green"
                                            >
                                                Save Changes
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade p-2" id="v-pills-billing" role="tabpanel" aria-labelledby="v-pills-billing-tab">
                                <div className="m-3 bg-white bor-rad p-3">
                                    <h6 className="profile_heading">Billing Information</h6>
                                    <div className="col-lg-12">
                                        <div class="border-billing">
                                            {/* <div class="col-lg-12 mb-2">
                                                <div className="custom-control custom-radio mt-2">
                                                <input
                                                    type="radio"
                                                    id="paypalRadio1"
                                                    name="paypalRadio"
                                                    className="custom-control-input"
                                                    checked={this.state.billingType == "paypal"}
                                                    onChange={() => {
                                                        this.setState({ 
                                                            billingType : 'paypal',
                                                            isEditBank: false,
                                                            isEditCard: false,
                                                            banking_country: this.state.bank_country,
                                                            banking_currency:this.state.bank_currency,
                                                            banking_routing_number: this.state.bank_routing_number,
                                                            banking_account_number: this.state.bank_account_number,
                                                            banking_account_holder_name: this.state.bank_account_holder_name,
                                                            banking_account_holder_type: this.state.bank_account_holder_type,
                                                        });
                                                    }}
                                                ></input>
                                                <label
                                                    className="custom-control-label h6 text-dark font-weight-bold"
                                                    for="paypalRadio1"
                                                >
                                                    PayPal
                                                </label>
                                                {this.state.activeBilling == "paypal" &&
                                                    <span className="green-font">Default</span>
                                                }
                                                {this.state.paypalInfo && this.state.paypalInfo.length != 0 && this.state.paypalInfo != null && this.state.activeBilling == "stripe" &&
                                                    <a className="green-font" onClick={() => this.changeDefaultMethod(1)}>Make this Default</a>
                                                }
                                                <p className="text-muted ml-2">PayPal is an online financial service that allows you to connect using a secure internet account.</p>
                                                </div>
                                            </div> */}
                                            {/* {this.state.billingType == "paypal" && 
                                                <div className="m-3 bg-white bor-rad p-3">
                                                    {this.state.isEditPaypal == true && 
                                                        <div className="col-12 text-right">
                                                            <button 
                                                                className="btn_ebony_clay" 
                                                                onClick={(e) => { 
                                                                    e.currentTarget.blur();
                                                                    this.setState({ 
                                                                        isEditPaypal : false,
                                                                    })
                                                                }}
                                                            >
                                                                Close
                                                            </button>
                                                        </div>
                                                    }
                                                    {(this.state.paypalInfo == null || this.state.isEditPaypal == true) &&
                                                        <div className="text-center">
                                                            <img 
                                                                className="link-cursor" 
                                                                src="https://www.paypalobjects.com/digitalassets/c/website/marketing/apac/C2/logos-buttons/optimize/44_Yellow_PayPal_Pill_Button.png" 
                                                                alt="PayPal" 
                                                                onClick={() => this.getPayPalLink()}
                                                            />
                                                        </div>
                                                    }
                                                    {this.state.paypalInfo && this.state.paypalInfo.length != 0 && this.state.paypalInfo != null && this.state.isEditPaypal != true &&
                                                        <div className="row">
                                                            <div className="col-12 text-right">
                                                                <button 
                                                                    className="btn_light_green" 
                                                                    onClick={(e) => { 
                                                                        e.currentTarget.blur();
                                                                        this.setState({ isEditPaypal : true })
                                                                    }}
                                                                >
                                                                    Edit PayPal Info
                                                                </button>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <label className="small text-muted" for="bank_account_number">PayPal Email</label>
                                                                <p>{this.state.paypalInfo.billing_paypal_email}</p>
                                                            </div>
                                                        </div>
                                                    }
                                                </div>
                                            } */}
                                        </div>
                                        <div class="border-billing">
                                            <div class="col-lg-12 mb-2">
                                                <div className="custom-control custom-radio mt-2">
                                                    {/* <input
                                                        type="radio"
                                                        id="stripeRadio1"
                                                        name="stripeRadio"
                                                        className="custom-control-input"
                                                        checked={this.state.billingType == 'stripe'}
                                                        onChange={() => {
                                                            this.setState({
                                                                billingType: 'stripe',
                                                                isEditPaypal: false,
                                                            });
                                                        }}
                                                    ></input> */}
                                                    {/* <label
                                                        className="custom-control-label h6 text-dark font-weight-bold"
                                                        for="stripeRadio1"
                                                    > */}
                                                    <span className="profile_heading">Stripe</span>
                                                    {/* </label> */}
                                                    {/* {this.state.activeBilling == "stripe" &&
                                                        <span className="green-font">Default</span>
                                                    } */}
                                                    {/* {this.state.isbankdetailsadded == true && this.state.activeBilling == "paypal" &&
                                                        <a className="green-font" onClick={() => this.changeDefaultMethod(2)}>Make this Default</a>
                                                    } */}
                                                    <p className="ml-2">Stripe is a quick, easy onboarding process. You simply add your bank account, credit card or debit card details securely. All of your account information is stored safely and securely in Stripe. EngineScale does not store your billing data, but pulls your data from Stripe. </p>
                                                </div>
                                            </div>
                                            {
                                                <div className="m-3 bg-white bor-rad p-3">
                                                    {this.state.isbillinginformationadded != null && this.state.isbillinginformationadded == false &&
                                                        <div className="connect_stripe" onClick={() => this.billingInfoAccountLink()} />
                                                    }

                                                    <div className="row mt-4">
                                                        {this.state.isbillinginformationadded != null && this.state.isbillinginformationadded == false &&
                                                            <p class="text-muted" dangerouslySetInnerHTML={{ __html: kycinformation }}>
                                                            </p>
                                                        }
                                                    </div>

                                                    {(this.state.isbillinginformationadded == true && this.state.isaccountadded == true) &&
                                                        <div className="card-body">
                                                            <div class="row mb-4">
                                                                <div class="col-lg-4">
                                                                    <div className="custom-control custom-radio">
                                                                        <input
                                                                            type="radio"
                                                                            id="creditRadio1"
                                                                            name="creditRadio"
                                                                            className="custom-control-input"
                                                                            checked={this.state.billingMethod === 'card'}
                                                                            onChange={() => {
                                                                                this.setState({
                                                                                    billingMethod: 'card',
                                                                                    isEditBank: false,
                                                                                    banking_country: this.state.bank_country,
                                                                                    banking_currency: this.state.bank_currency,
                                                                                    banking_routing_number: this.state.bank_routing_number,
                                                                                    banking_account_number: this.state.bank_account_number,
                                                                                    banking_account_holder_name: this.state.bank_account_holder_name,
                                                                                    banking_account_holder_type: this.state.bank_account_holder_type,
                                                                                });
                                                                            }}
                                                                        ></input>
                                                                        <label
                                                                            className="custom-control-label h6 text-dark font-weight-bold"
                                                                            for="creditRadio1"
                                                                        >
                                                                            Use Debit Card Details
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                                <div class="col-lg-4">
                                                                    <div className="custom-control custom-radio">
                                                                        <input
                                                                            type="radio"
                                                                            id="bankRadio1"
                                                                            name="bankRadio"
                                                                            className="custom-control-input"
                                                                            checked={this.state.billingMethod === 'bank'}
                                                                            onChange={() => {
                                                                                this.setState({ billingMethod: 'bank', isEditCard: false });
                                                                            }}
                                                                        ></input>
                                                                        <label
                                                                            className="custom-control-label h6 text-dark font-weight-bold"
                                                                            for="bankRadio1"
                                                                        >
                                                                            Use Bank Details
                                                                        </label>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            {this.state.billingMethod === 'bank' && this.props.activeusertype === 1 && (this.state.isbankdetailsadded == false || this.state.account_type == "card" || this.state.isEditBank == true) &&
                                                                <div className="options_bg_Settings">
                                                                    {this.state.isEditBank == true &&
                                                                        <div className="col-12 text-right">
                                                                            <button
                                                                                className="btn_ebony_clay"
                                                                                onClick={(e) => {
                                                                                    e.currentTarget.blur();
                                                                                    if (this.state.account_type == "bank_account") {
                                                                                        this.setState({
                                                                                            banking_country: this.state.bank_country,
                                                                                            banking_currency: this.state.bank_currency,
                                                                                            banking_routing_number: this.state.bank_routing_number,
                                                                                            banking_account_number: this.state.bank_account_number,
                                                                                            banking_account_holder_name: this.state.bank_account_holder_name,
                                                                                            banking_account_holder_type: this.state.bank_account_holder_type,
                                                                                        })
                                                                                    }
                                                                                    this.setState({
                                                                                        isEditBank: false,
                                                                                    })
                                                                                }}
                                                                            >
                                                                                Close
                                                                            </button>
                                                                        </div>
                                                                    }
                                                                    <div className="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="banking_account_number">Account Number</label><span class="required-red">*</span>
                                                                            <input className="input_field_w" type="text" maxLength="12" id="banking_account_number" placeholder="Account Number" name="banking_account_number" value={this.state.banking_account_number} onChange={this.onProfileChange} />
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="banking_account_holder_name">Account Holder Name</label><span class="required-red">*</span>
                                                                            <input className="input_field_w" type="text" name="banking_account_holder_name" placeholder="Account Holder Name" value={this.state.banking_account_holder_name} onChange={this.onProfileChange} />
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="banking_account_holder_type">Account Holder Type</label><span class="required-red">*</span>
                                                                            <Select
                                                                                className="basic-single-select"
                                                                                classNamePrefix={"select_dropdown"}
                                                                                placeholder="Select Account Holder Type"
                                                                                isClearable
                                                                                isSearchable={false}
                                                                                onChange={(selected) => {
                                                                                    this.setState({ banking_account_holder_type: selected });
                                                                                }}
                                                                                value={this.state.banking_account_holder_type}
                                                                                options={accountOptions}
                                                                            />
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="banking_routing_number">Routing Number</label><span class="required-red">*</span>
                                                                            <input className="input_field_w" type="text" maxLength="9" placeholder="Routing Number" id="banking_routing_number" name="banking_routing_number" value={this.state.banking_routing_number} onChange={this.onProfileChange} />
                                                                        </div>
                                                                        <div className="form-group col-md-6">
                                                                            <label className="small text-muted" for="">Country</label><span class="required-red">*</span>
                                                                            <Select
                                                                                className="basic-single-select"
                                                                                classNamePrefix={"select_dropdown"}
                                                                                placeholder="Select Country"
                                                                                isClearable
                                                                                isSearchable={true}
                                                                                onChange={(selected) => {
                                                                                    this.setState({ banking_country: selected });
                                                                                }}
                                                                                value={this.state.banking_country}
                                                                                options={countryOptions}
                                                                            />
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="banking_currency">Currency</label><span class="required-red">*</span>
                                                                            <Select
                                                                                className="basic-single-select"
                                                                                classNamePrefix={"select_dropdown"}
                                                                                placeholder="Select Currency"
                                                                                isClearable
                                                                                isSearchable={true}
                                                                                onChange={(selected) => {
                                                                                    this.setState({ banking_currency: selected });
                                                                                }}
                                                                                value={this.state.banking_currency}
                                                                                options={currencyOptions}
                                                                            />
                                                                        </div>
                                                                        <div className="col-12">
                                                                            <p class="mt-3 required-red">* required fields</p>
                                                                        </div>

                                                                        <div className="col-12">
                                                                            <h5 className="card_notes text-left mt-3 mb-3">* By law, EngineScale does not store any of your account information.
                                                                                All of your account information is stored securely in Stripe. </h5>
                                                                        </div>

                                                                        <div className="col-12 text-center mt-5">
                                                                            <button
                                                                                className="btn_light_green"
                                                                                onClick={(e) => {
                                                                                    e.currentTarget.blur();
                                                                                    this.saveBillingBank();
                                                                                }}
                                                                            >
                                                                                Save Bank Info
                                                                            </button>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            }
                                                            <Elements stripe={stripe}>
                                                                {this.state.billingMethod === 'card' && this.props.activeusertype === 1 && (this.state.isbankdetailsadded == false || this.state.account_type == "bank_account" || this.state.isEditCard == true) ?
                                                                    (
                                                                        <div className="options_bg_Settings">
                                                                            {this.state.isEditCard == true &&
                                                                                <div className="col-12 text-right">
                                                                                    <button
                                                                                        className="btn_ebony_clay"
                                                                                        onClick={(e) => {
                                                                                            e.currentTarget.blur();
                                                                                            if (this.state.account_type == "bank_account") {
                                                                                                this.setState({
                                                                                                    banking_country: this.state.bank_country,
                                                                                                    banking_currency: this.state.bank_currency,
                                                                                                    banking_routing_number: this.state.bank_routing_number,
                                                                                                    banking_account_number: this.state.bank_account_number,
                                                                                                    banking_account_holder_name: this.state.bank_account_holder_name,
                                                                                                    banking_account_holder_type: this.state.bank_account_holder_type,
                                                                                                })
                                                                                            }
                                                                                            this.setState({
                                                                                                isEditCard: false,
                                                                                            })
                                                                                        }}
                                                                                    >
                                                                                        Close
                                                                                    </button>
                                                                                </div>
                                                                            }
                                                                            <CardInfoForm
                                                                                tabData="billing"
                                                                                {...this.props}
                                                                                onAddBillingCard={(response) => {
                                                                                    this.onAddBillingCard(response);
                                                                                }}
                                                                            />
                                                                        </div>
                                                                    ) : (
                                                                        <div />
                                                                    )
                                                                }
                                                            </Elements>
                                                            {this.state.billingMethod === 'bank' && this.props.activeusertype === 1 && this.state.isbankdetailsadded == true && this.state.account_type == "bank_account" && this.state.isEditBank == false &&
                                                                <div className="options_bg_Settings">
                                                                    <span className="active-billing">Active</span>
                                                                    <div className="col-12 text-right">
                                                                        <button
                                                                            className="btn_light_green"
                                                                            onClick={(e) => {
                                                                                e.currentTarget.blur();
                                                                                this.setState({ isEditBank: true })
                                                                            }}
                                                                        >
                                                                            Edit Bank Info
                                                                        </button>
                                                                    </div>
                                                                    <div className="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="bank_account_number">Account Number</label>
                                                                            <p>{this.state.bank_account_number}</p>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="bank_account_holder_name">Account Holder Name</label>
                                                                            <p>{this.state.bank_account_holder_name}</p>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="bank_account_holder_type">Account Holder Type</label>
                                                                            <p>{this.state.bank_account_holder_type.label}</p>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="bank_routing_number">Routing Number</label>
                                                                            <p>{this.state.bank_routing_number}</p>
                                                                        </div>
                                                                        <div className="form-group col-md-6">
                                                                            <label className="small text-muted" for="">Country</label>
                                                                            <p>{this.state.bank_country.label}</p>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="bank_currency">Currency</label>
                                                                            <p>{this.state.bank_currency.label}</p>
                                                                        </div>
                                                                        <div className="col-lg-12 p-0">
                                                                            <h5 className="card_notes mt-3 mb-3"> * By law, EngineScale does not store any of your account information. All of your account information is stored securely in Stripe. </h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            }
                                                            {this.state.billingMethod === 'card' && this.props.activeusertype === 1 && this.state.isbankdetailsadded == true && this.state.account_type == "card" && this.state.isEditCard == false &&
                                                                <div className="options_bg_Settings">
                                                                    <span className="active-billing">Active</span>
                                                                    <div className="col-12 text-right">
                                                                        <button
                                                                            className="btn_light_green"
                                                                            onClick={(e) => {
                                                                                e.currentTarget.blur();
                                                                                this.setState({ isEditCard: true })
                                                                            }}
                                                                        >
                                                                            Edit Card Info
                                                                        </button>
                                                                    </div>
                                                                    <div className="row">
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="bank_account_number">Card Type</label>
                                                                            <p>{(this.state.card_details.brand).toUpperCase()}</p>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="bank_account_holder_name">Card Number</label>
                                                                            <p>************{this.state.card_details.last4}</p>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="bank_account_holder_type">Expiry</label>
                                                                            <p>{this.state.card_details.exp_month}/{this.state.card_details.exp_year}</p>
                                                                        </div>
                                                                        <div class="form-group col-md-6">
                                                                            <label className="small text-muted" for="bank_currency">Currency</label>
                                                                            <p>{this.state.card_details.currency.toUpperCase()}</p>
                                                                        </div>
                                                                        <div className="col-lg-12 p-0">
                                                                            <h5 className="card_notes text-left mt-3 mb-3">* By law, EngineScale does not store any of your account information. All of your account information is stored securely in Stripe. </h5>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            }
                                                        </div>
                                                    }
                                                    {this.props.activeusertype === 1 && this.state.isbillinginformationadded == true &&
                                                        <div className="card-body">
                                                            <div className="options_bg_Settings">
                                                                <div className="row">
                                                                    <div className="form-group col-md-6">
                                                                        <label className="small text-muted" for="">First Name</label>
                                                                        <p>{this.state.billing_firstname}</p>
                                                                        {/* <label  className="input_field_w" disabled ref={this.billing_firstname} name="billing_firstname" placeholder="First Name" value={this.state.billing_firstname} onChange={this.onProfileChange} /> */}
                                                                    </div>
                                                                    <div className="form-group col-md-6">
                                                                        <label className="small text-muted" for="">Last Name</label>
                                                                        <p>{this.state.billing_lastname}</p>
                                                                        {/* <input type="text" className="input_field_w" disabled ref={this.billing_lastname} name="billing_lastname" placeholder="Last Name" value={this.state.billing_lastname} onChange={this.onProfileChange} /> */}
                                                                    </div>
                                                                    <div className="form-group col-md-6">
                                                                        <label className="small text-muted" for="">DOB</label>
                                                                        <br />
                                                                        {/* <DatePicker
                                                                        selected={this.state.billing_dob ? moment(this.state.billing_dob, "DD/MM/YYYY").toDate() : null}
                                                                        maxDate={new Date()}
                                                                        // isClearable
                                                                        showMonthDropdown
                                                                        showYearDropdown
                                                                        disabled
                                                                        className="input_field_date"
                                                                        dateFormat="MM/dd/yyyy"
                                                                        ref={this.billing_dob}
                                                                        onChange={this.onDobChange}
                                                                        placeholderText="Select DOB"
                                                                    /> */}
                                                                        <p>{this.state.billing_dob}</p>
                                                                    </div>
                                                                    <div className="form-group col-md-6">
                                                                        <label className="small text-muted" for="">Gender</label>
                                                                        {/* <Select
                                                                        className="basic-single-select"
                                                                        classNamePrefix={"select_dropdown"}
                                                                        placeholder="Select Gender"
                                                                        isClearable
                                                                        isDisabled
                                                                        isSearchable={false}
                                                                        onChange={(selected) => {
                                                                            this.setState({ billing_gender: selected });
                                                                        }}
                                                                        value={this.state.billing_gender}
                                                                        options={genderOptions}
                                                                    /> */}
                                                                        <p>{this.state.billing_gender ? this.state.billing_gender : ""}</p>
                                                                    </div>
                                                                    <div className="form-group col-md-6 phone_field">
                                                                        <label className="small text-muted" for="">Phone</label>
                                                                        {/* <PhoneInput
                                                                        country={'us'}
                                                                        countryCodeEditable={false}
                                                                        value={this.state.billing_phone}
                                                                        ref={this.billing_phone}
                                                                        disabled
                                                                        onChange={(value, country) => {
                                                                            if (value.length == (country.format.match(/\./g) || []).length) {
                                                                                this.setState({ billing_phone: value, billingPhoneValid: true })
                                                                            } else {
                                                                                this.setState({ billing_phone: value, billingPhoneValid: false })
                                                                            }
                                                                        }}
                                                                    /> */}
                                                                        <p>{this.state.billing_phone}</p>
                                                                    </div>
                                                                    <div className="form-group col-md-6">
                                                                        <label className="small text-muted" for="">Address</label>
                                                                        <p>{this.state.billing_address}</p>
                                                                        {/* <input type="text" className="input_field_w" disabled name="billing_address" placeholder="Address" value={this.state.billing_address} ref={this.billing_address} onChange={this.onProfileChange} /> */}
                                                                    </div>
                                                                    <div className="form-group col-md-6">
                                                                        <label className="small text-muted" for="">City</label>
                                                                        {/* <input type="text" className="input_field_w" disabled name="billing_city" placeholder="City" value={this.state.billing_city} ref={this.billing_city} onChange={this.onProfileChange} /> */}
                                                                        <p>{this.state.billing_city}</p>
                                                                    </div>
                                                                    <div className="form-group col-md-6">
                                                                        <label className="small text-muted" for="">Country</label>
                                                                        {/* <Select
                                                                        className="basic-single-select"
                                                                        classNamePrefix={"select_dropdown"}
                                                                        placeholder="Select Country"
                                                                        isClearable
                                                                        isDisabled
                                                                        isSearchable={true}
                                                                        onChange={(selected) => {
                                                                            this.setState({ billing_country: selected });
                                                                        }}
                                                                        value={this.state.billing_country}
                                                                        options={countryOptions}
                                                                    /> */}
                                                                        <p>{this.state.billing_country ? this.state.billing_country : ''}</p>
                                                                    </div>
                                                                    <div className="form-group col-md-6">
                                                                        <label className="small text-muted" for="">State</label>
                                                                        {/* <Select
                                                                        className="basic-single-select"
                                                                        classNamePrefix={"select_dropdown"}
                                                                        placeholder="Select State"
                                                                        isClearable
                                                                        isDisabled
                                                                        isSearchable={true}
                                                                        onChange={(selected) => {
                                                                            this.setState({ billing_state: selected });
                                                                        }}
                                                                        value={this.state.billing_state}
                                                                        options={stateOptions}
                                                                    /> */}
                                                                        <p>{this.state.billing_state ? this.state.billing_state : ''}</p>
                                                                    </div>
                                                                    <div className="form-group col-md-6">
                                                                        <label className="small text-muted" for="">Postal Code</label>
                                                                        {/* <input type="text" className="input_field_w" disabled name="billing_postalcode" placeholder="Postal Code" value={this.state.billing_postalcode} ref={this.billing_postalcode} onChange={this.onProfileChange} /> */}
                                                                        <p>{this.state.billing_postalcode}</p>
                                                                    </div>
                                                                    {/* <div className="form-group col-md-6">
                                                                <label className="small text-muted" for="billing_ssn">SSN</label><span class="required-red">*</span>
                                                                <input type="text" className="input_field_w" disabled id="billing_ssn" name="billing_ssn" value={this.state.billing_ssn_value} placeholder="XXX-XX-XXXX" ref={this.billing_ssn} onChange={this.onProfileChange} />
                                                            </div> */}
                                                                    <div className="col-12">
                                                                        {/* <p class="mt-3 required-red">* required fields</p> */}
                                                                        {/* <div className="custom-control custom-checkbox ">
                                                                        <input type="checkbox" className="custom-control-input" id="customCheckEmail" checked={this.state.billing_isemailbillinginfo} onChange={this.emailCheckBoxChange} />
                                                                        <label className="custom-control-label" for="customCheckEmail">Yes, email my billing info and original invoices.</label>
                                                                    </div> */}
                                                                    </div>
                                                                    {/* <div className="col-12 text-center text-md-right mt-5">
                                                                <a onClick={this.onReset}  className="btn_grey w-auto mr-2">Reset</a>
                                                                <button 
                                                                    className="btn_light_green" 
                                                                    onClick={(e) => { 
                                                                        e.currentTarget.blur();
                                                                        this.onBillingSubmit();
                                                                    }}
                                                                >
                                                                    Save Changes
                                                                </button>
                                                            </div> */}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    }
                                                </div>
                                            }
                                        </div>
                                        {/* </div> */}
                                        {/* <div className="profile_pg_menu nav" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                            <a className="nav-link active" id="v-pills-stripe-tab" data-toggle="pill" href="#v-pills-stripe" role="tab" aria-controls="v-pills-stripe" aria-selected="false">
                                                Stripe
                                            </a>
                                            <a className="nav-link" id="v-paypal-tab" data-toggle="pill" href="#v-pills-paypal" role="tab" aria-controls="v-pills-paypal" aria-selected="false">
                                                PayPal
                                            </a>
                                        </div> */}
                                    </div>
                                </div>
                            </div>
                            <div className="tab-pane fade p-2" id="v-pills-card" role="tabpanel" aria-labelledby="v-pills-card-tab">
                                {this.state.saved_cards && this.state.saved_cards.length != 0 &&
                                    <div className="m-3 bg-white bor-rad p-3  mb-3">
                                        <div className="row">
                                            <div class="form-group col-md-12 mb-0">
                                                <h6 className="profile_heading">Saved Cards</h6>
                                                <div class="form-group col-md-12 p-0 mb-0">
                                                    {this.state.saved_cards.map(card => {
                                                        return (
                                                            <div class="saved-group">
                                                                <label class="saved-label-profile">
                                                                    <div class="fontBlack">{(card.brand).toUpperCase()} Card Ending **** {card.last4} | Expires {card.exp_month}/{card.exp_year}</div>
                                                                </label>
                                                                <label class="saved-label-profile ml-2 trash_clr floatRight">
                                                                    <i class="fa fa-trash-o link-cursor" aria-hidden="true" id={card.id} onClick={() => { this.setState({ confirmRemoveCard: true, removeCardId: card.id }); }}></i>
                                                                </label>
                                                            </div>
                                                        )
                                                    })}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                }
                                <div className="m-3 bg-white bor-rad p-3">
                                    <div className="row">
                                        <div class="form-group col-md-12 addnew_Card">
                                            <h6 className="profile_heading">Add New Card</h6>
                                            <Elements stripe={stripe}>
                                                <CardInfoForm
                                                    tabData="profile"
                                                    {...this.props}
                                                    onAddCard={(response) => {
                                                        this.onAddCard(response);
                                                    }}
                                                />
                                            </Elements>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {this.state.confirmRemoveCard && (
                                <ConfirmationDialog
                                    open={this.state.confirmRemoveCard}
                                    dialogTitle={"Remove Card"}
                                    dialogContentText={`Are you sure you want to remove this card?`}
                                    cancelButtonText="Cancel"
                                    okButtonText={"Remove"}
                                    onCancel={() => {
                                        this.setState({ confirmRemoveCard: false, removeCardId: '' });
                                    }}
                                    onClose={() => {
                                        this.setState({ confirmRemoveCard: false, removeCardId: '' });
                                    }}
                                    onOk={(e) => {
                                        this.removeCard(this.state.removeCardId)
                                        this.setState({ confirmRemoveCard: false, removeCardId: '' });
                                    }}
                                />
                            )}
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}


const mapStateToProps = ({ profileReducer, loginReducer, inboxReducer }) => {
    return {
        profile: profileReducer.profile,
        userInfo: profileReducer.userInfo,
        // notification: profileReducer.notification,
        billingInfo: profileReducer.billingInfo,
        bankInfo: profileReducer.bankInfo,
        profilePic: profileReducer.profilePic,
        reasonData: profileReducer.reasonsData,
        activeusertype: loginReducer.userData.activeusertype,
        headerInfo: inboxReducer.headerInfo,
    }
}

export default connect(mapStateToProps, {
    getViewQuestions,
    addQuestions,
    getHeaderInfo,
    myprofileAction,
    myprofileUpdateAction,
    deactivateAccountAction,
    passwordChangeAction,
    countryAction,
    stateAction,
    userInfoUpdateAction,
    userInfoAction,
    notificationAction,
    notificationPutAction,
    getBillingAction,
    billingAction,
    getProfilePic,
    updateProfilePic,
    removeProfilePic,
    getReasonAction,
    reLoginAction,
    getSavedCards,
    addCard,
    removeCard,
    forceLogOut,
    billingInfoAccountLink,
    getBillingInformationStatus,
    addBillingToken,
    addPayPalCode,
    getPayPalLink,
    changeDefaultMethod,
    KycInformationAction,
})(Profile);
