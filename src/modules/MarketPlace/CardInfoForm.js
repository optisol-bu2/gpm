import React, { useState, useMemo } from "react";
import useResponsiveFontSize from "../../utils/useResponsiveFontSize";
import { ToastAlert,PlaySound } from '../../utils/sweetalert2';
import {
  useStripe,
  useElements,
  CardNumberElement,
  CardCvcElement,
  CardExpiryElement,
} from "@stripe/react-stripe-js";
import ChatLoading from "../../components/core/ChatLoading";
import TextLoading from "../../components/core/TextLoading";
import Select from 'react-select';

const useOptions = () => {
  const fontSize = useResponsiveFontSize();
  const options = useMemo(
    () => ({
      style: {
        base: {
          fontSize,
          color: "#424770",
          letterSpacing: "0.025em",
          fontFamily: "Source Code Pro, monospace",
          "::placeholder": {
            color: "#aab7c4",
          },
        },
        invalid: {
          color: "#9e2146",
        },
      },
      showIcon: true,
    }),
    [fontSize]
  );

  return options;
};
function CardInfoForm(props) {
  const [isCardNumberValidated, setCardNumberValidity] = useState(false);
  const [isCVCValidated, setCVCValidity] = useState(false);
  const [isCardExpiryValidated, setCardExpiryValidity] = useState(false);
  const [checked, setChecked] = useState(false);
  const [loading, setLoading] = useState(null);
  const [isTextLoading, setIsTextLoading] = useState(null);
  const [loadingText, setLoadingText] = useState('');
  const [currency, setCurrency] = useState(null);
  const [currencyOptions, setCurrencyOptions] = useState([
    { label: "US Dollar" , value: "usd"},
  ]);

  const [cardError, setcarderror] = useState("");
  const [cvcerror, setCVCError] = useState("");
  const [cardEpiryerror, setCardExiryerror] = useState("");

  const stripe = useStripe();
  const elements = useElements();
  const options = useOptions();

  const validateFields = () => {
    if (
      isCardNumberValidated &&
      isCVCValidated &&
      isCardExpiryValidated 
    ) {
      return true;
    } else {
      return false;
    }
  };

  const handleSubmit = async (event) => {
    if (!stripe || !elements) {
      return;
    }

    if (validateFields()) {
      const cardElement = elements.getElement(CardNumberElement);

      setLoading(true);
      stripe.confirmCardPayment(props.paymentData.client_secret, {
        payment_method: {
        card: cardElement
        },
      })
      .then(
        result => {
          setLoading(false);
          if (result.error) {
            ToastAlert('error', result.error.message)
            PlaySound()
          } else if (result.paymentIntent.status == "succeeded") {
            ToastAlert('success', "Payment Processed.")
            PlaySound()
            props.onPaymentsuccess(result, stripe, checked, cardElement);
          }
        }
      );
    }
  };

  /**
   * saves card for seller billing
   * @param {*} event 
   * @returns 
   */
   const saveBillingCard = async (event) => {
    if (!stripe || !elements) {
      return;
    }

    if (validateFields()) {
      if (!currency || currency == null) {
        return (ToastAlert('error', 'Please select currency.'),PlaySound())
      } else {
        const cardElement = elements.getElement(CardNumberElement);

        setIsTextLoading(true);
        setLoadingText("Updating Billing Info...")
        stripe.createToken(cardElement, {
          type: "card",
          currency: currency.value,
        })
        .then(
          result => {
            setIsTextLoading(false);
            if (result.error) {
              ToastAlert('error', result.error.message)
              PlaySound()
            } else {
              elements.getElement(CardNumberElement).unmount();
              elements.getElement(CardExpiryElement).unmount();
              elements.getElement(CardCvcElement).unmount();
              props.onAddBillingCard(result.token);
              elements.getElement(CardNumberElement).mount('#card-number');;
              elements.getElement(CardExpiryElement).mount('#card-expiry');
              elements.getElement(CardCvcElement).mount('#card-cvc');
              setCardNumberValidity(false);
              setCardExpiryValidity(false);
              setCVCValidity(false);
              setcarderror("");
              setCardExiryerror("");
              setCVCError("");
              setCurrency(null)
            }
          }
        );
      }
    }
  };

  /**
   * Adds saved cards of the user
   * @param {*} event 
   * @returns 
   */
  const addCard = async (event) => {
    if (!stripe || !elements) {
      return;
    }

    if (validateFields()) {
      const cardElement = elements.getElement(CardNumberElement);

      setLoading(true);
      stripe.createPaymentMethod({
        type: "card",
        card: cardElement,
      })
      .then(
        result => {
          setLoading(false);
          if (result.error) {
            ToastAlert('error', result.error.message);
            PlaySound()
          } else {
            elements.getElement(CardNumberElement).unmount();
            elements.getElement(CardExpiryElement).unmount();
            elements.getElement(CardCvcElement).unmount();
            props.onAddCard(result);
            elements.getElement(CardNumberElement).mount('#card-number');;
            elements.getElement(CardExpiryElement).mount('#card-expiry');
            elements.getElement(CardCvcElement).mount('#card-cvc');
            setCardNumberValidity(false);
            setCardExpiryValidity(false);
            setCVCValidity(false);
            setcarderror("");
            setCardExiryerror("");
            setCVCError("");
          }
        }
      );
    }
  };

  return (
    <div className="row w-100 card_info_form addnew_Card">
      <ChatLoading isVisible={loading} />
      <TextLoading isVisible={isTextLoading} loadingText={loadingText}/>
      <div className={props.tabData==="billing" ? "col-6" : "col-12" +" form-group chkout_card mb-0"}>
        <label className="left_label_prc" for="card-number">Card Number</label>
        <div id="card-number" className="form-control input_field_set">
          <CardNumberElement
            options={options}
            showIcon={true}
            onReady={() => {}}
            onChange={(event) => {
              if (event.complete) {
                setCardNumberValidity(true);
              } else if (event.empty){
                setcarderror("Card Number is required");
                setCardNumberValidity(false);
              } else {
                setcarderror("Invalid card number");
                setCardNumberValidity(false);
              }
            }}
            onBlur={() => {}}
            onFocus={() => {}}
          />
        </div>
        <p style={{ marginTop: 5, color: "red", fontSize: 11 }}>
          {isCardNumberValidated ? "" : cardError}
        </p>
      </div>
      <div className="col-md-6 form-group">
        <label className="left_label_prc" for="card-expiry">Expiry</label>
        <div id="card-expiry"className="form-control input_field_set">
          <CardExpiryElement
            options={options}
            onReady={() => {}}
            onChange={(event) => {
              if (event.complete) {
                setCardExpiryValidity(true);
              } else if (event.empty){
                setCardExiryerror("Expiry is required");
                setCardExpiryValidity(false);
              } else {
                setCardExiryerror("Invalid card expiry");
                setCardExpiryValidity(false);
              }
            }}
            onBlur={() => {}}
            onFocus={() => {}}
          />
        </div>
        <p style={{ marginTop: 5, color: "red", fontSize: 11 }}>
          {isCardExpiryValidated ? "" : cardEpiryerror}
        </p>
      </div>
      <div className="col-md-6 form-group">
        <label className="left_label_prc" for="card-cvc">CVC</label>
        <div id="card-cvc" className="form-control input_field_set">
          <CardCvcElement
            options={options}
            onReady={() => {}}
            onChange={(event) => {
              if (event.complete) {
                setCVCValidity(true);
              } else if (event.empty){
                setCVCError("CVC is required");
                setCVCValidity(false);
              } else {
                setCVCError("Invalid CVC number");
                setCVCValidity(false);
              }
            }}
            onBlur={() => {}}
            onFocus={(event) => {}}
          />
        </div>
        <p style={{ marginTop: 5, color: "red", fontSize: 11 }}>
          {isCVCValidated ? "" : cvcerror}
        </p>
      </div>
      {props.tabData==="cart" &&
        <div className="col-md-12 row m-0">
          <div class="form-group custom-control custom-checkbox">
            <input
              type="checkbox"
              className="custom-control-input"
              id="customCheckSave"
              checked={checked}
              onChange={() => setChecked(!checked)}
            />
            <label
              className="custom-control-label text-dark margin-left-fifty"
              for="customCheckSave"
            >
              Save this card for future payments
            </label>  
          </div>
        </div>
      }
      {props.tabData==="billing" && 
        <div className="col-md-6 form-group">
          <label className="left_label_prc" for="card-currency">Currency</label>
          <Select
            id="card-currency" 
            className="basic-single-select"
            classNamePrefix={"select_dropdown"}
            placeholder="Select Currency"
            isClearable
            isSearchable={true}
            onChange={(selected) => {
                setCurrency(selected);
            }}
            value={currency}
            options={currencyOptions}
          />
        </div>
      }
      <div className={(props.tabData==="profile") ? "text-center text-md-right col-md-12 form-group " : "col-md-12 form-group text-center padding-pay "}>
        <button
          type="button"
          
          disabled={!isCardNumberValidated || !isCardExpiryValidated || !isCVCValidated}
          onClick={() => {
            if (props.tabData === "profile") {
              addCard();
            } else if (props.tabData === "cart") {
              handleSubmit();
            } else if (props.tabData === "billing") {
              saveBillingCard();
            }
          }}
          className={(props.tabData==="profile" ? "" : "btn_light_green ") +" btn_light_green"}
        >
          {props.tabData==="profile" ? "Add Card" : props.tabData==="billing" ? "Save Card Info" : "Pay $" + props.cartData.totalprice}
        </button>
        <div className="col-lg-12 p-0">
          <h5 className="card_notes text-left mt-3 mb-3">* By law, EngineScale does not store any of your account information. All of your account information is stored securely in Stripe. </h5>
        </div>
      </div>
    </div>
  );
}

export default CardInfoForm;