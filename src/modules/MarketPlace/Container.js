import { connect } from "react-redux";
import CartCheckOut from "./CartCheckOut";
import {
  getCartData,
  paymentSession,
  placeOrder,
  createOrderForPaypal,
  getSavedCards,
  removeCartItem,
  createOrder,
  uploadAttachment,
  removeAttachment,
  startOrder,
  getOrderDetail,
  getCartCustom,
  paymentSessionCustom,
} from "../../Actions/actionContainer";
import {
  getCartBlogger,
  paymentSessionBlogger,
} from "../../Actions/bloggerAction";

const mapStateToProps = (state) => {
  return {
    serviceCategories: state.mainReducer.serviceCategories,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCartData: (params, onSuccess, onFailure) =>
      dispatch(getCartData(params, onSuccess, onFailure)),
    paymentSession: (params, onSuccess, onFailure) =>
      dispatch(paymentSession(params, onSuccess, onFailure)),
    getCartBlogger: (onSuccess, onFailure) =>
      dispatch(getCartBlogger(onSuccess, onFailure)),
    paymentSessionBlogger: (params, onSuccess, onFailure) =>
      dispatch(paymentSessionBlogger(params, onSuccess, onFailure)),
    getCartCustom: (onSuccess, onFailure) =>
      dispatch(getCartCustom(onSuccess, onFailure)),
    paymentSessionCustom: (params, onSuccess, onFailure) =>
      dispatch(paymentSessionCustom(params, onSuccess, onFailure)),
    placeOrder: (params, onSuccess, onFailure) =>
      dispatch(placeOrder(params, onSuccess, onFailure)),
    createOrderForPaypal: (params, onSuccess, onFailure) =>
      dispatch(createOrderForPaypal(params, onSuccess, onFailure)),
    getSavedCards: (onSuccess, onFailure) =>
      dispatch(getSavedCards(onSuccess, onFailure)),
    removeCartItem: (id, onSuccess, onFailure) =>
      dispatch(removeCartItem(id, onSuccess, onFailure)),
    createOrder: (params, onSuccess, onFailure) =>
      dispatch(createOrder(params, onSuccess, onFailure)),
    uploadAttachment: (params, onSuccess, onFailure) =>
      dispatch(uploadAttachment(params, onSuccess, onFailure)),
    removeAttachment: (id, onSuccess, onFailure) =>
      dispatch(removeAttachment(id, onSuccess, onFailure)),
    startOrder: (params, onSuccess, onFailure) =>
      dispatch(startOrder(params, onSuccess, onFailure)),
    getOrderDetail: (id, onSuccess, onFailure) =>
      dispatch(getOrderDetail(id, onSuccess, onFailure)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartCheckOut);
