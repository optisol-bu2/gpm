import React, { useState, useEffect } from "react";
import Button from "../../components/core/Button";
import TextField from "../../components/core/TextField";
import icn_done from "../../assets/images/dashboard-icons/thanks-hand-icon.svg";
import Loading from "../../components/core/Loading";
import FileUpload from "../../components/core/FileUpload";
import { Close } from "@material-ui/icons";
import { IconButton, Tooltip } from "@material-ui/core";
import { ListGroup } from "react-bootstrap";
import { ToastAlert,PlaySound } from '../../utils/sweetalert2';
import moment from "moment";

export default function SubmitRequest(props) {
  const [error, setError] = useState(false);
  const [disable, setDisable] = useState(true);
  const [loading, setLoading] = useState(null);
  const [base64, setBase64] = useState(null);
  const [file, setFile] = useState(null);
  const [isImageDeleted, setIsImageDeleted] = useState(false);
  const [isImageCanceled, setIsImageCanceled] = useState(false);
  const [isFileError, setIsFileError] = useState(false);
  const [fileList, setFileList] = useState([]);
  const [fileName, setFileName] = useState("");
  const [fileListHtml, setFileListHtml] = useState();
  const [orderDetail, setOrderDetail] = useState();
  const [descriptionText, setDescriptionText] = useState("");
  const [charCount, setCharCount] = useState(0);
  const [confirmCheckbox, setConfirmCheckbox] = useState(false);
  
  useEffect(() => {
    if (file) {
      uploadAttachment();
    }
  }, [file]);

  useEffect(() => {
    if (
      descriptionText === "" ||
      descriptionText === null ||
      descriptionText === undefined
    ) { 
      setDisable(true);
    } else {
      setDisable(!confirmCheckbox);
    }
  }, [descriptionText, confirmCheckbox]);

  useEffect(() => {
    getOrderDetail();
  }, []);

  useEffect(() => {
    if (props.orderRedirect) {
      getOrderDetail();
      props.orderList(false);
    }
  }, [props.orderRedirect]);

  /**
   * changes the given date to local date format
   * @param {*} dateTime
   * @returns
   */
  const orderTimeFormat = (dateTime) => {
    var utc = moment.utc(dateTime).toDate();
    var orderTime = moment(utc).local().format('MMM-DD-YYYY hh:mmA');
    return orderTime;
  }

  const getOrderDetail = () => {
    try {
      setLoading(true);
      props.getOrderDetail(
        props.orderId,
        (response) => {
          setLoading(false);
          if (response.data !== null) {
            setOrderDetail(response.data);
            if (response.data.comments[0].buyer.attachments !== null) {
              var attachments = response.data.comments[0].buyer.attachments;
              if (attachments.length > 0) {
                var list = fileList;
                for (let i = 0; i < attachments.length; i++) {
                  var fileName = attachments[i].name.split("/")[
                    attachments[i].name.split("/").length - 1
                  ];
                  var id = attachments[i].id;
                  list.push({ name: fileName, id: id });
                }
                setFileList(list);
                setFileListHtml(loadFileList());
              }
            }
          }
        },
        (error) => {
          setLoading(false);
          ToastAlert('error', error.message)
          PlaySound()
        }
      );
    } catch (error) {}
  };

  const startOrder = () => {
    let formData = new FormData();
    formData.set("id", orderDetail.id);
    formData.append("requirement", descriptionText);

    if (disable) {
      if (!confirmCheckbox) {
        ToastAlert('error', "Please accept condition")
        PlaySound()
      }
    } else {
      setLoading(true);
      setDisable(true);
      props.startOrder(
        formData,
        (response) => {
          setLoading(false);
          setDisable(false);
          ToastAlert('success', "Requirements successfully submitted.")
          PlaySound()
          if (props.orderRedirect === undefined) {
            props.history.push("/myOrder/"+orderDetail.id)
          } else {
            props.setModalContent("Requirements successfully submitted.");
            props.reloadMainScreen();
          }
        },
        (error) => {
          setLoading(false);
          ToastAlert('error', "Start order failed.")
          PlaySound()
        }
      );
    }
  };

  const uploadAttachment = () => {
    let formData = new FormData();
    formData.set("orderid", orderDetail.id);
    formData.append("document", file);
    setLoading(true);

    props.uploadAttachment(
      formData,
      (response) => {
        setLoading(false);

        if (file.name !== "") {
          var list = fileList;
          list.push({ name: file.name, id: response.data.id });
          setFileList(list);
          setFileListHtml(loadFileList());
        }
      },
      (error) => {
        setLoading(false);
      }
    );
  };

  const removeAttachment = (attachmentId, index) => {
    setLoading(true);
    props.removeAttachment(
      attachmentId,
      (response) => {
        setLoading(false);
        fileList.splice(index, 1);
        setFileList(fileList);
        setFileListHtml(loadFileList());
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message)
        PlaySound()
      }
    );
  };

  const loadFileList = () => {
    let items = [];
    try {
      for (let i = 0; i <= fileList.length; i++) {
        items.push(
          <ListGroup.Item>
            <div>
              <span>{fileList[i].name}</span>
              <Tooltip title="Cancel">
                <IconButton
                  aria-label="cancel"
                  onClick={() => {
                    removeAttachment(fileList[i].id, i);
                  }}
                >
                  <Close fontSize="small" />
                </IconButton>
              </Tooltip>
            </div>
          </ListGroup.Item>
        );
      }
    } catch (error) {}
    return items;
  };

  return (
    <div className="tab-pane" role="tabpanel" id="step3">
      <div className="col-lg-12">
        <div className="thankyou_Bg_des fade show mt-3 mb-1 alert col-lg-12" role="alert" >
          <button
            type="button"
            className="close mt-3"
            data-dismiss="alert"
            aria-label="Close"
          >
            <span aria-hidden="true">×</span>
          </button>
          <div className="d-flex align-items-center">
            <img src={icn_done} className="mr-3" alt="" />
            <div className="thankyou_text">
              <h3 className="mb-0">Thank for your order</h3>
              <p className="small mb-0">
                A receipt was sent to your email address
              </p>
            </div>
          </div>
        </div>
      </div>
      
      <div className="row mar_zero">
        <div className="col-lg-8 col-xl-8 mt-3 order-2 order-lg-1 chkout_section pad_zero">
          <div className="card">
            <div className="card-body">
            <h5 className="submit_title mb-2">Submit requirements to start your order</h5>

              <div className="form-group input_field_set">
                <TextField
                  required={true}
                  value={descriptionText}
                  label=" Please provide Image, Title, tags, keywords, description"
                  multiline={true}
                  rows={8}
                  onChange={e => {
                    if (e.target.value.length <= 2000) {
                      setCharCount(e.target.value.length);
                      setDescriptionText(e.target.value);
                    }
                    setError(false);
                  }}
                  error={error}
                  errorMessage={
                    "Image, Title, tags, keywords, description is Required"
                  }
                  maxLength={2000}
                />

                <div className="d-flex p-2 form-control input_field_set newinput_filed justify-content-between">
                  <p className="text-muted mb-0">{charCount}/2000</p>
                  <label for="upload_doc" className="mb-0">
                    <i className="fa fa-paperclip fa-lg text-muted link-cursor" title="Attach files"></i>
                    <FileUpload
                      required={true}
                      id="upload_doc"
                      multiple={true}
                      setBase64={setBase64}
                      setFileName={setFileName}
                      file={file}
                      accept={"*/*"}
                      isFileCanceled={isImageCanceled}
                      isFileDeleted={isImageDeleted}
                      setIsImageCanceled={setIsImageCanceled}
                      setFile={setFile}
                      setIsFileError={setIsFileError}
                      size={20000}
                      error={isFileError}
                      errorMessage={"Please select file."}
                    />
                  </label>
                </div>

                <div className="file_updoaded_dets">
                  {fileList.length > 0 && <ListGroup>{fileListHtml}</ListGroup>}
                </div>
              </div>

              <div className="custom-control custom-checkbox mb-3">
                <input
                  type="checkbox"
                  className="custom-control-input"
                  id="customCheckSubmit"
                  onChange={(e) => {
                    setConfirmCheckbox(e.target.checked);
                  }}
                />
                <label
                  className="custom-control-label text-dark"
                  for="customCheckSubmit"
                >
                  The information I provided is
                  <strong> accurate and complete.</strong> Any
                  <strong> changes</strong> will require the content writer's approval
                  and may be subject to additional costs.
                </label>
              </div>
              <button
                type="button"
                class="btn_light_green effect effect-2"
                disabled={disable}
                onClick={(e) => {
                  if (
                    descriptionText === "" ||
                    descriptionText === null ||
                    descriptionText === undefined
                  ) {
                    setError(true);
                    if (!confirmCheckbox) {
                      ToastAlert('error', "Please accept condition")
                      PlaySound()
                    } 
                  } else {
                    startOrder();
                    setError(false);
                  }
                }}
              >
                Start Order
              </button>
            </div>
          </div>
        </div>
        <div className="col-lg-4 col-xl-4 mt-3 order-1 order-lg-2 chkout_section chkout_card">
          <div className="card">
            <div className="card-body">
              <h5 className="chk_out_title">Price Details</h5>
              <div className="d-flex justify-content-between">
                <p className="left_label_prc">Price:</p>
                <p className="right_price_set">
                  ${orderDetail ? orderDetail.actualprice : ""}
                </p>
              </div>
              <div className="d-flex justify-content-between">
                <p className="left_label_prc">Service Fee:</p>
                <p className="right_price_set">
                  ${orderDetail ? orderDetail.servicefees : ""}
                </p>
              </div>
              <div className="d-flex justify-content-between">
                <p className="left_label_prc">Delivery time:</p>
                <p className="right_price_set">
                  {orderDetail ? orderDetail.deliverytime + " " +orderDetail.deliverytimein : ""}
                </p>
              </div>
              <div className="d-flex justify-content-between">
                <p className="left_label_prc">Status:</p>
                <p className="right_price_set">
                  {orderDetail ? orderDetail.statusname : ""}
                </p>
              </div>
              <div className="d-flex justify-content-between">
                <p className="left_label_prc">Order Id:</p>
                <p className="right_price_set">
                  {orderDetail ? orderDetail.invoiceid : ""}
                </p>
              </div>
              <div className="d-flex justify-content-between">
                <p className="left_label_prc">Order date:</p>
                <p className="right_price_set">
                  {orderDetail ? orderTimeFormat(orderDetail.created_at) : ""}
                </p>
              </div>
            </div>
            <hr className="my-0" />
            <div className="card-body">
              <div className="d-flex justify-content-between">
                <p className="total_f_price">Total:</p>
                <p className="total_f_price">
                  ${orderDetail ? orderDetail.total : ""}
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <Loading isVisible={loading}></Loading>
    </div>
  );
}
