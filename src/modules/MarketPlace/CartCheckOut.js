import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import CartDetails from "./CartDetails";
import ConfirmPay from "./ConfirmPay";
import Loading from "../../components/core/Loading";
import { ToastAlert,PlaySound } from '../../utils/sweetalert2';
import marketplace from "../../assets/images/menu-icons/marketplace_white.svg";
import Pusher from 'pusher-js';
import { PUSHER_KEY, PUSHER_CLUSTER } from "../../common/constants";

function CartCheckOut(props) {
  const [isOrderDetailEnabled, setOrderDetail] = useState(true);
  const [isOrderConfirmationEnabled, setOrderConfirmation] = useState(false);
  const [isOrderSubmitEnabled, setOrderSubmit] = useState(false);
  const [isOrderPlaced, setIsOrderPlaced] = useState(false);
  const [isOrderConfirmed, setIsOrderConfirmed] = useState(false);
  const [paymentData, setPaymentData] = useState({});
  const [loading, setLoading] = useState(null);
  const [cartData, setCartData] = useState({});

  /**
   * gets service cart data
   */
  const getCartData = () => {
    setLoading(true);
    props.getCartData(
      (response) => {
        setLoading(false);
        setCartData(response.data);
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
        setCartData([]);
      }
    );
  };

  useEffect(() => {
    let userId = props.userData.id
		const pusher = new Pusher(PUSHER_KEY, {
			cluster: PUSHER_CLUSTER,
			encrypted: true
		});
    const cartChannel = pusher.subscribe('GPM-CART-NOTIFICATION-' + userId);
		cartChannel.bind('CART_NOTIFICATION', data => {
      getCartData();
		});
    getCartData();
  }, []);

  useEffect(() => {
    if (isOrderPlaced) {
      setOrderConfirmation(true);
      setOrderSubmit(false);
      setOrderDetail(false);
    }
  }, [isOrderPlaced]);

  useEffect(() => {
    if (isOrderConfirmed) {
      setOrderConfirmation(false);
      setOrderSubmit(true);
      setOrderDetail(false);
    }
  }, [isOrderConfirmed]);

  const renderChildComponent = () => {
    if (isOrderConfirmationEnabled) {
      return (
        <ConfirmPay
          isOCSelected={setIsOrderConfirmed}
          isOPSelected={setIsOrderPlaced}
          cartData={cartData}
          paymentData={paymentData}
          transactionInfo={(data) => {
            setLoading(true);
            cartData.services.forEach(service => {
              props.removeCartItem(service.id);
            })
            if (cartData.services && cartData.services.length > 1) {
              setLoading(false);
              props.history.push("/myOrder");
            } else {
              setLoading(false);
              props.history.push("/myOrder/"+data[0].id);
            }
          }}
          {...props}
        />
      );
    } else if (isOrderDetailEnabled) {
      return (
        <CartDetails
          cartData={cartData}
          isOCSelected={setIsOrderConfirmed}
          isOPSelected={setIsOrderPlaced}
          paymentData={(data) => setPaymentData(data)}
          {...props}
        />
      );
    }
  };
  return (
    <div class="page-wrapper">
      <div className="page_title row m-0">
        <div class="col-lg-1 col-md-2 col-3 p-0">
          <span className="left_icon_title"><img src={marketplace} alt="" /></span>
        </div>
        <div class="col-lg-11 col-md-10 col-9 p-0">
            <h2 className="right_title row m-0">Checkout</h2>
            <span className="title_tagline m-0 row">
              <ol className="breadcrumb p-0 bg-none mb-0">
                <li className="breadcrumb-item">
                  <Link to={{pathname:"/dashboard"}}>
                    <i className="fa fa-home"></i>
                  </Link>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  Checkout
                </li>
              </ol>
            </span>
        </div>
      </div>
      <div class="grey_box_bg content_grey_bg">
        <div className="content-wrapper wizard_tab">
          <div className="wizard">
            <div className="wizard-inner">
              <ul className="nav nav-tabs" role="tablist">
                <li
                  role="presentation"
                  className={isOrderDetailEnabled ? "active" : "disabled"}
                >
                  <a
                    className="text-nowrap link-cursor"
                    data-toggle="tab"
                    aria-controls="step1"
                    role="tab"
                    aria-expanded="true"
                  >
                    <i class="fas fa-shopping-basket"></i> Cart/Order details
                  </a>
                </li>

                <li
                  role="presentation"
                  className={isOrderConfirmationEnabled ? "active" : "disabled"}
                >
                  <a
                    className="text-nowrap link-cursor"
                    data-toggle="tab"
                    aria-controls="step2"
                    role="tab"
                    aria-expanded="false"
                  >
                    <i class="far fa-credit-card"></i> Confirm & Pay
                  </a>
                </li>

                <li
                  role="presentation"
                  dis
                  className={isOrderSubmitEnabled ? "active" : "disabled"}
                >
                  <a
                    className="text-nowrap link-cursor"
                    data-toggle="tab"
                    aria-controls="step3"
                    role="tab"
                  >
                    <i className="fa fa-info-circle"></i> Submit Requirements
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
        <form role="form" className="login-box">
          {renderChildComponent()}
        </form>
      </div>      
      <Loading isVisible={loading}></Loading>
    </div>
  );
}

export default CartCheckOut;
