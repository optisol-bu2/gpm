import React, { useState, useEffect } from "react";
import { ToastAlert,PlaySound } from '../../utils/sweetalert2';
import Button from "../../components/core/Button";
import ConfirmationDialog from "../../components/core/ConfirmationDialog/ConfirmationDialog";
import Loading from "../../components/core/Loading";

function CartDetails(props) {
  const [loading, setLoading] = useState(null);
  const [confirmDialog, setConfirmDialog] = useState(false);
  const [serviceData, setServiceData] = useState([]);
  const [removeServiceId, setRemoveServiceId] = useState();
  const [disable, setDisabled ] = useState(false);

  const placeOrderSubmit = () => {
    setLoading(true);
    setDisabled(true);
    props.paymentSession(
      null,
      (response) => {
        setLoading(false);
        setDisabled(false);
        props.isOPSelected(true);
        props.isOCSelected(false);
        props.paymentData(response.data);
      },
      (error) => {
        setLoading(false);
        setDisabled(false);
        ToastAlert('error', error.message);
        PlaySound();
      }
    );
  };


  useEffect(() => {
    if (props.cartData.services !== undefined) {
      setServiceData(props.cartData.services);
    } else {
      setServiceData([]);
    }
  }, [props.cartData]);

  return (
    <div className="tab-content" id="main_form">
      <div className="tab-pane active" role="tabpanel" id="step1">
        <div className="row m-0">
          {serviceData && serviceData.length != 0  && serviceData.map(serviceData => {
            if (serviceData.servicetype == 1) {
              return (                
                <div className="col-lg-8 col-xl-8 mt-4 chkout_section p_setz">              
                  <div className="chkout_card h-100">                 
                    <h6 className="chk_out_title">
                      Order for Service: {" "}
                      {serviceData.title}
                    </h6>
                    <div class="row m-0 chkout_card_inner d-flex">
                      <div className="col-lg-8 pl-0 p_setz">                     
                        <div className="d-flex row m-0 w-100 justify-content-between flex-wrap flex-md-nowrap">
                          <p className="col-8 bg_grey_chkout">No. of guest post</p>
                          <p className="col-4 bg_grey_chkout countclr">
                            {serviceData.noofguestpost}
                          </p>
                        </div>
                        <div className="d-flex row m-0 w-100 justify-content-between flex-wrap flex-md-nowrap">
                          <p className="col-8 bg_grey_chkout">No. of Backlink</p>
                          <p className="col-4 bg_grey_chkout countclr">
                            {serviceData.noofbacklink}
                          </p>
                        </div>
                        <div className="d-flex row m-0 w-100 justify-content-between flex-wrap flex-md-nowrap">
                          <p className="col-8 bg_grey_chkout">Standard Turn Around Time</p>
                          <p className="col-4 bg_grey_chkout countclr">
                            {serviceData.estimatedelivery.name}
                          </p>
                        </div>
                        <div className="d-flex row m-0 w-100 justify-content-between flex-wrap flex-md-nowrap">
                          <p className="col-8 bg_grey_chkout">Service Fee</p>
                          <p className="col-4 bg_grey_chkout countclr">
                            ${serviceData.servicefeevalue} ({serviceData.servicefee})
                          </p>
                        </div>
                      </div>
                      <div className="col-lg-4 pl-0 pr-0 mb-3">
                        <div className="bg_grey_chkout price_greychokout text-center">
                          <h4 className="price_size_set mb-4">
                            ${serviceData.price}
                          </h4>

                          <div className="mt-2 btn_button_clr">
                            <Button
                              label={"Remove"}
                              onClick={(e) => {
                                setConfirmDialog(true);
                                setRemoveServiceId(serviceData);
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )
            } else if (serviceData.servicetype == 2) {
              return (
                <div className="col-lg-8 col-xl-8 mt-4 chkout_section p_setz">
                  <div className="chkout_card h-100">
                    <h6 className="chk_out_title">
                      Order for Content Writer: {" "}
                      {serviceData.user} 
                      {/* ({serviceData.expertise}) */}
                    </h6>
                    <div className="row m-0 chkout_card_inner d-flex">
                      <div className="col-lg-8 pl-0 p_setz">
                        <div className="d-flex row m-0 w-100 justify-content-between flex-wrap flex-md-nowrap">
                          <p className="col-8 bg_grey_chkout">No. of Hours</p>
                          <p className="col-4 bg_grey_chkout countclr">
                            {serviceData.noofhours}{serviceData.noofhours > 1 ? " Hrs": " Hr"}
                          </p>
                        </div>
                        <div className="d-flex row m-0 w-100 justify-content-between flex-wrap flex-md-nowrap">
                          <p className="col-8 bg_grey_chkout">No. of Words</p>
                          <p className="col-4 bg_grey_chkout countclr">
                            {serviceData.noofwords}{" "}Words
                          </p>
                        </div>
                        <div className="d-flex row m-0 w-100 justify-content-between flex-wrap flex-md-nowrap">
                          <p className="col-8 bg_grey_chkout">Standard Turn Around Time</p>
                          <p className="col-4 bg_grey_chkout countclr">
                            {serviceData.estimateddelivery.name}
                          </p>
                        </div>
                        <div className="d-flex row m-0 w-100 justify-content-between flex-wrap flex-md-nowrap">
                          <p className="col-8 bg_grey_chkout">Service Fee</p>
                          <p className="col-4 bg_grey_chkout countclr">
                            ${serviceData.servicefeevalue} ({serviceData.servicefee})
                          </p>
                        </div>
                      </div>
                      <div className="col-lg-4 pl-0 pr-0 mb-3">
                        <div className="bg_grey_chkout price_greychokout text-center">
                          <h4 className="price_size_set mb-4">
                            ${serviceData.price}
                          </h4>

                          <div className="mt-2 btn_button_clr">
                            <Button
                              label={"Remove"}
                              onClick={(e) => {
                                setConfirmDialog(true);
                                setRemoveServiceId(serviceData);
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )
            } else if (serviceData.servicetype == 3) {
              return (
                <div className="col-lg-8 col-xl-8 mt-4 chkout_section p_setz">
                  <div className="chkout_card h-100">
                    <h6 className="chk_out_title">
                      Custom Order
                    </h6>
                    <div className="row m-0 chkout_card_inner d-flex">
                      <div className="col-lg-8 pl-0 p_setz">
                        <div className="d-flex row m-0 w-100 justify-content-between flex-wrap flex-md-nowrap">
                          <p className="col-8 bg_grey_chkout">Standard Turn Around Time</p>
                          <p className="col-4 bg_grey_chkout countclr">
                            {serviceData.estimatedelivery.name}
                          </p>
                        </div>
                        <div className="d-flex row m-0 w-100 justify-content-between flex-wrap flex-md-nowrap">
                          <p className="col-8 bg_grey_chkout">Service Fee</p>
                          <p className="col-4 bg_grey_chkout countclr">
                            ${serviceData.servicefeevalue} ({serviceData.servicefee})
                          </p>
                        </div>
                      </div>
                      <div className="col-lg-4 pl-0 pr-0 mb-3">
                        <div className="bg_grey_chkout price_greychokout text-center">
                          <h4 className="price_size_set mb-4">
                            ${serviceData.price}
                          </h4>

                          <div className="mt-2 btn_button_clr">
                            <Button
                              label={"Remove"}
                              onClick={(e) => {
                                setConfirmDialog(true);
                                setRemoveServiceId(serviceData);
                              }}
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )
            }
          })}
          {serviceData.length != 0 && (
            <div className="col-lg-4 col-xl-4 mt-4 p_setz">
              <div className="chkout_section chkout_card">
                <div className="card">
                  <div className="card-body">
                    <h5 className="chk_out_title">Price Details</h5>
                    <div className="d-flex justify-content-between">
                      <p className="left_label_prc">Price:</p>
                      <p className="right_price_set">
                        ${props.cartData.serviceprice}
                      </p>
                    </div>
                    <div className="d-flex justify-content-between">
                      <p className="left_label_prc">Service Fee:</p>
                      <p className="right_price_set_green">
                        ${props.cartData.servicefeevalue}
                      </p>
                    </div>
                  </div>
                  <hr className="my-0" />
                  <div className="card-body">
                    <div className="d-flex justify-content-between">
                      <p className="total_f_price">Total:</p>
                      <p className="total_f_price">
                        ${props.cartData.totalprice}
                      </p>
                    </div>

                    <div className="mt-3 btn_button_clr_green">
                      <Button
                        label={"Place Order"}
                        disabled={disable}
                        onClick={(e) => {
                          placeOrderSubmit();
                        }}
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
                
      {serviceData.length === 0 && (
        <div className="tab-pane active" role="tabpanel" id="step1">
          <div className="row">
            <div className="col-lg-12 col-xl-12 mt-4 text-center cart_empty_div">
              <div className="card d-flex flex-md-row">
                <div className="card-body border-r-1">
                  <h3>
                    Your cart is empty
                  </h3>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}

      {confirmDialog && (
        <ConfirmationDialog
          open={confirmDialog}
          dialogTitle={"Remove"}
          dialogContentText={`Are you sure you want to remove the data?`}
          cancelButtonText="Cancel"
          okButtonText={"Remove"}
          onCancel={() => {
            setConfirmDialog(false);
          }}
          onClose={() => {
            setConfirmDialog(false);
          }}
          onOk={(e) => {
            setConfirmDialog(false);
            setLoading(true);
            props.removeCartItem(
              removeServiceId.id,
              (response) => {
                setLoading(false);
                // props.getCartInfo();
              },
              (error) => {
                setLoading(false);
                ToastAlert('error', error.message)
                PlaySound()
              }
            );
          }}
        />
      )}
      <Loading isVisible={loading}></Loading>
    </div>
  );
}

export default CartDetails;
