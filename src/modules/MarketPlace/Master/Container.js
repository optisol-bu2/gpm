import { connect } from "react-redux";
import MarketPlaceParent from "./MarketPlaceParent";
import {
  filterMarketPlace,
  getMarketPlaceList,
} from "../../../Actions/actionContainer";
import {
  addFavorite, removeFavoriteService
} from "../../../Actions/favoriteAction";

const mapStateToProps = (state) => {
  return {
    serviceCategories: state.mainReducer.serviceCategories,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    filterMarketPlace: (params, onSuccess, onFailure) =>
      dispatch(filterMarketPlace(params, onSuccess, onFailure)),
    getMarketPlaceList: (id, onSuccess, onFailure) =>
      dispatch(getMarketPlaceList(id, onSuccess, onFailure)),
    addFavorite: (params, onSuccess, onFailure) =>
      dispatch(addFavorite(params, onSuccess, onFailure)),
    removeFavoriteService: (id, onSuccess, onFailure) =>
      dispatch(removeFavoriteService(id, onSuccess, onFailure)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MarketPlaceParent);
