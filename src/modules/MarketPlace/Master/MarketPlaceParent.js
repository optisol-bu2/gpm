import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import "rc-slider/assets/index.css";
import defaultImage from "../../../assets/images/placeholder.jpeg";
import avatar from "../../../assets/images/avatar.png";
import Loading from "../../../components/core/Loading";
import marketplace from "../../../assets/images/menu-icons/marketplace_white.svg";
import { ToastAlert,PlaySound } from "../../../utils/sweetalert2";
import Select from 'react-select';
import { Image, Popup } from 'semantic-ui-react'
import Modal from "@material-ui/core/Modal";

const MarketPlaceParent = (props) => {
  const [pageIndex, setPageIndex] = useState(1);
  const [viewOption, setViewOption] = useState("grid");
  const [searchText, setSearchText] = useState("");
  const [industry, setIndustries] = useState(null);
  const [topType, setTopType] = useState([{ label: "Top Seller" , value: "1"},{ label: "Top Buyer" , value: "2"}]);
  const [isLoading, setLoading] = useState(null);
  const [isFilter, setFilter] = useState(false);
  const [isSearchFilter, setSearchFilter] = useState(false);
  const [isOwn, setOwn] = useState(false);
  const [bestReview, setBestReview] = useState(false);
  const [minRange, setMinRange] = useState("");
  const [maxRange, setMaxRange] = useState("");
  const [domainrange, setDomainRange] = useState([0, 100]);
  const [serviceData, setServiceData] = useState([]);
  const [totalPageNumber, setTotalPageNumber] = useState(1);
  const [totalRecord, setTotalRecord] = useState(0);
  const [favoriteArray, setFavoriteArray] = useState([]);
  const [isFavoriteModalOpen, setFavoriteModalOpen] = useState(false);
  const [favoriteModalContent, setFavoriteModalContent] = useState("");

  useEffect(() => {
    callMarketList();
  }, []);

  const callMarketList = (pageId, scroll, loading) => {
    pageId = pageId === undefined ? 1 : pageId;
    if (loading === undefined) {
      setLoading(true);
    }
    props.getMarketPlaceList(
      pageId,
      (response) => {
        setLoading(false);
        setTotalRecord(response.data.total);
        setTotalPageNumber(Math.ceil(response.data.total/response.data.noofrecords));
        if (response.data.records.length != 0) {
          if (pageId == 1) {
            if (response.data.records && response.data.records.length) {
              response.data.records.forEach(service => {
                if (service.isfavorite) {
                  if (!favoriteArray.includes(service.id))
                  favoriteArray.push(service.id)
                }
              })
            }
            setServiceData(response.data.records);
          } else {
            var serviceArray = [];
            serviceArray = [...serviceData, ...response.data.records]
            if (serviceArray && serviceArray.length) {
              serviceArray.forEach(service => {
                if (service.isfavorite) {
                  if (!favoriteArray.includes(service.id))
                  favoriteArray.push(service.id)
                }
              })
            }
            setServiceData(serviceArray);
          }
        } else {
          if (pageId == 1) {
            setServiceData([]);
          } else {
            setPageIndex(pageIndex-1);
          }
        }
        if (scroll === undefined && pageId == 1) {
          window.scrollTo({ top: 0, behavior: "smooth" });
        }
      },
      (error) => {
        if (pageId == 1) {
          setServiceData([]);
        } else {
          setPageIndex(pageIndex-1);
        }
        setLoading(false);
      }
    );
  };

  const callFilter = (pageId, searchText, scroll, loading) => {
    if (loading === undefined) {
      setLoading(true);
    }
    props.filterMarketPlace(
      getParams(pageId, searchText),
      (response) => {
        setLoading(false);
        setTotalRecord(response.data.total);
        setTotalPageNumber(Math.ceil(response.data.total/response.data.noofrecords));
        if (response.data.records.length != 0) {
          if (pageId == 1) {
            if (response.data.records && response.data.records.length) {
              response.data.records.forEach(service => {
                if (service.isfavorite) {
                  if (!favoriteArray.includes(service.id))
                  favoriteArray.push(service.id)
                }
              })
            }
            setServiceData(response.data.records);
          } else {
            var serviceArray = [];
            serviceArray = [...serviceData, ...response.data.records]
            if (serviceArray && serviceArray.length) {
              serviceArray.forEach(service => {
                if (service.isfavorite) {
                  if (!favoriteArray.includes(service.id))
                  favoriteArray.push(service.id)
                }
              })
            }
            setServiceData(serviceArray);
          }
        } else {
          if (pageId == 1) {
            setServiceData([]);
          } else {
            setPageIndex(pageIndex-1);
          }
        }
        if (scroll === undefined && pageId == 1) {
          window.scrollTo({ top: 0, behavior: "smooth" });
        }
      },
      (error) => {
        if (pageId == 1) {
          setServiceData([]);
        } else {
          setPageIndex(pageIndex-1);
        }
        setLoading(false);
      }
    );
  };

  const onTextChange = (e) => {
    if (e.target.value.length >= 3) {
      setSearchFilter(true);
      callFilter(1, e.target.value, true, false);
    } else if (!e.target.value) {
      setSearchFilter(false);
      if (isFilter) {
        callFilter(1, e.target.value, true, false);
      } else {
        callMarketList(pageIndex, true, false);
      }
    }
  };

  const handleOnPriceRangeChange = (value, type) => {
    if (type=="min") {
      setMinRange(value);
    } else if (type=="max") {
      setMaxRange(value);
    }
  };


  /**
   * changes own state for filter
   */
  const changeOwn = () => {
    setOwn(!isOwn);
  };

  /**
   * changes best review state for filter
   */
  const changeBestReview = () => {
    setBestReview(!bestReview);
  };

  /**
   * adds favorite website for the user
   * @param {*} id 
   */
  const addFavorite = (id) => {
    // let button = document.getElementById("favorite-"+id);
    // button.setAttribute("disabled", "disabled");
    var params = {
      "service": id.toString()
    }
    var serviceArray = serviceData;
    serviceArray.forEach(service => {
      if (service.id === id) {
        service.isfavorite = true;
      }
    })
    setServiceData(serviceArray);
    props.addFavorite(params, (favorite) => {
      // let button = document.getElementById("favorite-"+id);
      // button.removeAttribute("disabled");
      // if (isFilter || isSearchFilter) {
      //   callFilter(pageIndex, searchText, true, false);
      // } else if (searchText.length >= 3) {
      //   callFilter(pageIndex, searchText, true, false);
      // } else {
      //   callMarketList(pageIndex, true, false);
      // }
    }, 
    (error) => {
      // let button = document.getElementById("favorite-"+id);
      // button.removeAttribute("disabled");
      var serviceArray = serviceData;
      serviceArray.forEach(service => {
        if (service.id === id) {
          service.isfavorite = false;
        }
      })
      setServiceData(serviceArray);
      ToastAlert("error", error.message);
      PlaySound()
    })
  };

  /**
   * removes favorite website for the user
   * @param {*} id 
   */
  const removeFavoriteService = (id) => {
    // let button = document.getElementById("favorite-"+id);
    // button.setAttribute("disabled", "disabled");
    var serviceArray = serviceData;
      serviceArray.forEach(service => {
        if (service.id === id) {
          service.isfavorite = false;
        }
      })
      setServiceData(serviceArray);
    props.removeFavoriteService(id, (favorite) => {
      // let button = document.getElementById("favorite-"+id);
      // button.removeAttribute("disabled");
      // if (isFilter || isSearchFilter) {
      //   callFilter(pageIndex, searchText, true, false);
      // } else if (searchText.length >= 3) {
      //   callFilter(pageIndex, searchText, true, false);
      // } else {
      //   callMarketList(pageIndex, true, false);
      // }
    },
    (error) => {
      // let button = document.getElementById("favorite-"+id);
      // button.removeAttribute("disabled");
      var serviceArray = serviceData;
      serviceArray.forEach(service => {
        if (service.id === id) {
          service.isfavorite = true;
        }
      })
      setServiceData(serviceArray);
      ToastAlert("error", error.message);
      PlaySound()
    })
  };

  const getParams = (pageId, searchText) => {
    return {
      pageindex: pageId,
      search: searchText,
      pricerange: {
        min: minRange,
        max: maxRange,
      },
      industry: industry != null ? industry.value : "all",
      domainauthority: {
        min: domainrange[0],
        max: domainrange[1],
      },
      own: isOwn,
      best_overall_reviews: bestReview
    };
  };

  /**
   * clears filter values
   * @param {*} e 
   */
  const clearFilter = (e) => {
    setPageIndex(1);
    setSearchText("");
    setMinRange("");
    setMaxRange("");
    setIndustries(null);
    setDomainRange([0, 100]);
    setOwn(false);
    setBestReview(false);
    setFilter(false);
    setSearchFilter(false);
    callMarketList();
  };

  return (
    <div class="page-wrapper">
      <Loading isVisible={isLoading} />
      <div className="page_title row m-0">
        <div class="col-lg-1 col-md-2 col-3 p-0">
          <span className="left_icon_title"><img src={marketplace} alt="" /></span>
        </div>
        <div class="col-lg-11 col-md-10 col-9 p-0">
            <h2 className="right_title row m-0">Marketplace </h2>
            <span className="title_tagline m-0 row">Publish guest posts on other reputable sites to grow your outreach and visibility.</span>
        </div>
      </div>
      <div class="grey_box_bg" data-tour="marketplace">
        <div className="row mt-4 p-3 mb-4">          
          <div className="col-lg-12">
            <div class="bg-white w-100 bor-rad">
              <div class="row m-0 p-2 pl-3 pr-3">
                <div class="col-sm-12 col-lg-4 col-md-4 col-12 p-0">
                  <div className="search-box mb-3">
                    <input
                      type="text"
                      className="form-control border-0"
                      placeholder="Search here..."
                      name="search"
                      value={searchText}
                      onChange={(e) => {
                        setSearchText(e.target.value);
                        setPageIndex(1);
                        onTextChange(e);
                      }}
                    />
                    <span class="fa fa-search bg_icon_green"></span>
                  </div>
                </div>   
                <div class="col-sm-12 col-lg-8 col-md-8 col-12 p-0">
                  <div className="show-result d-flex align-items-center justify-content-end">                    
                    <div className="row m-0 w-100 justify-content-between">
                      <div className="mr-3 mb-3 col-xs-12 col-sm-4 col-lg-4 col-md-4 p-0 pt-2 text-right"><span class="count_results">{totalRecord} result found</span></div>
                      <div class="selectpicker_form mr-3 mb-3 col-xs-12  col-sm-4 col-lg-4 col-md-4 p-0">
                        <Select
                          className="basic-single-select"
                          isSearchable={false}
                          defaultValue={topType[0]}
                          options={topType}
                          //defaultMenuIsOpen={true}
                          classNamePrefix={"select_dropdown"}
                        />
                      </div>
                      <div className="btn-group btn-group-toggle mb-3 d_sm_none col-sm-3 col-lg-3 col-md-3 p-0 col-12" data-toggle="buttons">
                        <label
                          className={viewOption == "list" ? "btn btn-outline-secondary rounded-left active" : "btn btn-outline-secondary rounded-left"}
                          onClick={() => { setViewOption("list")}}
                        >
                          <input
                            type="radio"
                            name="list"
                            id="list"
                            autocomplete="off"
                            checked={viewOption == "list" ? true : false}
                          />{" "}
                          <i className="fa fa-list"></i>
                        </label>
                        <label
                          className={viewOption == "grid" ? "btn btn-outline-secondary rounded-right active" : "btn btn-outline-secondary rounded-right"}
                          onClick={() => { setViewOption("grid")}}
                        >
                          <input
                            type="radio"
                            name="grid"
                            id="grid"
                            autocomplete="off"
                            checked={viewOption == "grid" ? true : false}
                          />{" "}
                          <i className="fa fa-th-large"></i>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>               
              </div>      
              <div className="col-lg-12 row filter_box_align">           
                <h4 className="sidesub_green_head pb-0 pt-0 col-lg-1">Filters</h4>
                <div className="filter-box bor-rad_left_btm col-lg-11 pt-0 pb-0">
                  <div className="row align-items-center">
                    <div className="price-range-sec col-lg-3 col-md-6 col-sm-12">
                      <h4 class="sub_sub_title">Price Range</h4>                  
                      <div class="row incre_input_fl">
                        <div class="col-sm-6 col-md-6 col-sm-12 padleftzero">
                          <div class="form-group">
                            <div class="input-group search-box">
                              <div class="input-group-append cont_bg">
                                <span class="input-group-text border-0">$</span>
                              </div>
                              <input
                                type="number"
                                className="form-control border-0 p-1"
                                placeholder="Min"
                                name="min"
                                min="0"
                                value={minRange}
                                onChange={e => handleOnPriceRangeChange(e.target.value, "min")}
                              />
                              
                            </div>
                          </div>
                        </div>
                        <div class="col-sm-6 col-md-6 col-sm-12 padleftzero">
                          <div class="form-group">
                            <div class="input-group search-box">
                              <div class="input-group-append cont_bg">
                                <span class="input-group-text border-0">$</span>
                              </div>
                              <input
                                type="number"
                                className="form-control border-0 p-1"
                                placeholder="Max"
                                name="max"
                                min="0"
                                value={maxRange}
                                onChange={e => handleOnPriceRangeChange(e.target.value, "max")}
                              />                          
                            </div>
                          </div>
                        </div>                      
                      </div>
                    </div>
                    <div className="industry-dropdown col-lg-3 col-md-4 col-sm-12">
                      <h4 class="sub_sub_title">Industry</h4>
                      <div className="form-group selectpicker_form">
                        <Select
                          className="basic-single-select"
                          placeholder="Choose Industry"
                          isClearable
                          isSearchable={true}
                          onChange={(selected) => {
                            setIndustries(selected);
                          }}
                          value={industry}
                          options={props.serviceCategories}
                          classNamePrefix={"select_dropdown"}
                        />
                      </div>
                    </div>
                    <div className="domain-range-sec col-lg-3 col-md-4 col-sm-12">
                      <div class="row m-0">
                        <div className="custom-control custom-checkbox col-lg-12 col-md-12 col-sm-12">
                          <input type="checkbox" className="custom-control-input" id="customCheckOwn" checked={isOwn} onChange={changeOwn} />
                          <label className="custom-control-label filter_values" for="customCheckOwn">Show own services</label>
                        </div>
                        <div className="custom-control custom-checkbox col-lg-12 col-md-12 col-sm-12">
                          <input type="checkbox" className="custom-control-input" id="customCheckReview" checked={bestReview} onChange={changeBestReview}/>
                          <label className="custom-control-label filter_values" for="customCheckReview">Best Overall Reviews</label>
                        </div>
                      </div>
                    </div>
                    <div className="col-lg-3"> 
                      <div className="row">  
                        <div class="col-sm-6">                                                       
                          {(isFilter || isSearchFilter) &&                                            
                            <button
                              type="button"
                              onClick={clearFilter}
                              className="btn_grey"
                            >
                              Reset
                            </button>                                                 
                          }  
                        </div>      
                        <div class="col-sm-6">
                          <button
                            type="button"
                            onClick={(e) => { 
                              e.currentTarget.blur();
                              setPageIndex(1);
                              setFilter(true);
                              callFilter(1, searchText);
                            }}
                            className="btn_light_green effect effect-2"
                          >Apply
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="productscontainer-box bor-rad_left_btm p-0 pt-3 pb-3 mt-3">
              <div id="products" className="row">
                {serviceData.length ? (
                  serviceData.map((value) => (
                    <div className={viewOption == "list" ? "col-lg-12 col-md-12 mb-4 item list-view-products" : "col-xl-3 col-lg-4 col-md-6 mb-4 item grid-view-products"}>
                      <div className="card h-100 bg-white">
                        
                          {/* <div className={viewOption == "list" ? "products_image col-sm-3 p-0" : "products_image"}> */}
                          <div className={viewOption == "list" ? "col-sm-3 p-0" : "position-relative"}>
                            <Link to={`/marketplace/${value.id}`} className="position-relative">
                              {viewOption == "list" ? (
                                <img
                                  src={value.servicepic}
                                  onError={(e)=>{e.target.onerror = null; e.target.src=defaultImage}}
                                  className="img_crop img-fluid"
                                  alt="..."
                                  
                                />
                              ) : (
                                <img
                                  src={value.servicepic}
                                  onError={(e)=>{e.target.onerror = null; e.target.src=defaultImage}}
                                  className="img_crop img-fluid"
                                  alt="..."
                                  
                                />
                              )}
                            </Link>
                            <div className="new_seller">
                              {value.review === 0 && 
                                <span className="new_seller_btn">New Content Writer</span>
                              }
                            </div> 
                          </div>
                          <div className={viewOption == "list" ? "card-body w-100 p-2 pl-3 pr-3 col-sm-9" : "card-body w-100 p-2"}>
                            <h5 className="product_title"  title={value.title}>
                              {value.title}
                            </h5>                            
                            <div className="rating_star d-flex mt-1">
                              {value.review !== 0 && Array.from(Array(Math.round(value.review)), () => {
                                return (
                                  <span className="fa fa-star star"></span>
                                )
                              })}                              
                            </div> 
                            <div class="row m-0 mt-3 mb-3 align-items-center">
                              <div class="col-sm-12 col-4 p-0 mb-2 text-left">
                                <Popup
                                  key={value.id}
                                  trigger={<span className="sold_count_text link-cursor">By: {value.seller.name}</span>}
                                >
                                  <Popup.Header>
                                    <Image 
                                      src={value.seller.image}
                                      onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                                      className="rounded-circle mr-2"
                                      width="50"
                                      height="50"
                                      alt="..."
                                    />
                                    {value.seller.name}
                                  </Popup.Header>
                                  <Popup.Content>
                                    {value.seller.bio}
                                  </Popup.Content>
                                </Popup>
                              </div>
                              <div class="col-sm-6 col-8 p-0 text-left"><span className="price-info mt-2 mb-2">${value.price}</span></div>                    
                              <div class="col-sm-6 col-4 p-0 text-right"><p className="sold_count_text">{value.totalsold} Sold</p></div>
                            </div>
                            <div className="btn-groups d-flex justify-content-center mb-3">
                              <a 
                                className="favorite_btn mr-2"
                                disabled={value.isownservice}
                                id={"favorite-"+value.id}
                                onClick={() => {
                                  if (value.isfavorite) {
                                    if (favoriteArray.includes(value.id)) {
                                      let unlike = favoriteArray.filter((elem) => elem !== value.id);
                                      setFavoriteArray(unlike);
                                    }
                                    removeFavoriteService(value.id)
                                  } else {
                                    if (!favoriteArray.includes(value.id)) {
                                      setFavoriteArray([...favoriteArray, value.id]);
                                    }
                                    addFavorite(value.id);
                                    setFavoriteModalOpen(true);
                                    setFavoriteModalContent("You have favorited " + value.seller.name + "'s " + value.title+ " service.");
                                  }
                                }}
                              >
                                {favoriteArray.includes(value.id) ? <span class="fa fa-heart"></span> :
                                <span class="fa fa-heart-o"></span>}{" "}Favorite
                              </a>
                              <Link className="view_btn" to={`/marketplace/${value.id}`}>
                                  <span class="fa fa-eye"></span> View
                              </Link>
                            </div>
                          </div>
                        
                      </div>
                    </div>
                  ))
                ) : (
                  <div className="col-lg-12">
                    <h3 className="norec_found">No Record Found</h3>
                  </div>
                )}
              </div>
              {totalPageNumber > pageIndex &&
                <div class="col-sm-12 text-center mt-5 mb-5">
                  <button
                    type="button"
                    class="btn_light_green effect effect-2"
                    onClick={() => {
                      setPageIndex(pageIndex+1);
                      if (isFilter || isSearchFilter) {
                        callFilter(pageIndex+1, searchText, true, true);
                      } else {
                        callMarketList(pageIndex+1, true, true);
                      }
                    }}
                  >Load More</button>
                </div>
              }
            </div>
          </div>
        </div>
      </div>
      <Modal
        open={isFavoriteModalOpen}
        disableBackdropClick
        onClose={() => { 
          setFavoriteModalOpen(false);
          setFavoriteModalContent("");
        }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description">
        <div className="modal-dialog modal_popup" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="popup_title" id="exampleModalLabel">Favorited</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => { 
                  setFavoriteModalOpen(false);
                  setFavoriteModalContent("");
                }}
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body justify-content-center">
              {/* <i className="fa fa-heart heartred fa-5x" style={{"padding-left": "189px"}}aria-hidden="true"></i> */}
              <h4 className="text-center">{favoriteModalContent}</h4>
            </div>
            <div className="modal-footer justify-content-center">
              <button 
                onClick={() => { 
                  setFavoriteModalOpen(false);
                  setFavoriteModalContent("");
                }}
                className="btn_light_green"
              >
                OK
              </button>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default MarketPlaceParent;
