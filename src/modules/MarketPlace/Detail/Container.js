import { connect } from "react-redux";
import MarketDetail from "./MarketDetail";
import {
  userLogin,
  getRecommendations,
  getServiceDetail,
  verifyCartRedirect,
  getServiceBackLinkData,
  getServiceGuestPostData
} from "../../../Actions/actionContainer";
import {
  addFavorite, removeFavoriteService
} from "../../../Actions/favoriteAction";

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    userLogin: (params, onSuccess, onFailure) =>
      dispatch(userLogin(params, onSuccess, onFailure)),
    getRecommendations: (id, onSuccess, onFailure) =>
      dispatch(getRecommendations(id, onSuccess, onFailure)),
    getServiceDetail: (id, onSuccess, onFailure) =>
      dispatch(getServiceDetail(id, onSuccess, onFailure)),
    verifyCartRedirect: (params, onSuccess, onFailure) =>
      dispatch(verifyCartRedirect(params, onSuccess, onFailure)),
    getServiceBackLinkData: (onSuccess, onFailure) =>
      dispatch(getServiceBackLinkData(onSuccess, onFailure)),    
    getServiceGuestPostData: (onSuccess, onFailure) =>
      dispatch(getServiceGuestPostData(onSuccess, onFailure)),
    addFavorite: (params, onSuccess, onFailure) =>
      dispatch(addFavorite(params, onSuccess, onFailure)),
    removeFavoriteService: (id, onSuccess, onFailure) =>
      dispatch(removeFavoriteService(id, onSuccess, onFailure)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MarketDetail);
