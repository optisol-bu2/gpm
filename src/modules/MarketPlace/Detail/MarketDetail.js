import React, { useState, useEffect } from "react";
import Loading from "../../../components/core/Loading";
import ContactSeller from "../../../components/ContactSeller/ContactSeller";
import marketplace from "../../../assets/images/menu-icons/marketplace_white.svg";
import { Link } from "react-router-dom";
import { Swiper, SwiperSlide } from "swiper/react";
import "../../../assets/css/swiper.min.css";
import "swiper/components/pagination/pagination.min.css"
import "swiper/components/navigation/navigation.min.css"
import SwiperCore, { Navigation, Pagination, Autoplay } from 'swiper/core';
import { ToastAlert,PlaySound } from "../../../utils/sweetalert2";
import defaultImage from "../../../assets/images/placeholder.jpeg";
import profilelogo from "../../../assets/images/avatar.png";
import icnMdl from "../../../assets/images/icn_medal.svg";
import icnLink from "../../../assets/images/icn_link.svg";
import icnCar from "../../../assets/images/icn_car.svg";
import Select from 'react-select';
import Modal from "@material-ui/core/Modal";
import { Image, Popup } from 'semantic-ui-react'

SwiperCore.use([Navigation, Pagination, Autoplay]);

const MarketDetail = (props) => {
  const [isLoading, setLoading] = useState(null);
  const [serviceData, setServiceData] = useState([]);
  const [serviceDetail, setServiceDetail] = useState([]);
  const [serviceBackLinkData, setServiceBackLinkData] = useState([]);
  const [serviceGuestPostData, setServiceGuestPostData] = useState([]);
  const [guestPostValue, setGuestPostValue] = useState({});
  const [backLinkValue, setBackLinkValue] = useState({});
  const [favoriteArray, setFavoriteArray] = useState([]);
  const [isFavoriteModalOpen, setFavoriteModalOpen] = useState(false);
  const [favoriteModalContent, setFavoriteModalContent] = useState("");

  const getServiceBackLinkData = () => {
    setLoading(true);
    props.getServiceBackLinkData(
      (response) => {
        setLoading(false);
        var backLinkArray = [];
        response.data.forEach(data => {
          let backLinkObj = {
            label: data.name,
            value: data.id.toString()
          }
          backLinkArray.push(backLinkObj);
        })
        setBackLinkValue(backLinkArray[0]);
        setServiceBackLinkData(backLinkArray);
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound();
      }
    );
  };

  const getServiceGuestPostData = () => {
    setLoading(true);
    props.getServiceGuestPostData(
      (response) => {
        setLoading(false);
        var guestPostArray = [];
        response.data.forEach(data => {
          let guestPostObj = {
            label: data.name,
            value: data.id.toString()
          }
          guestPostArray.push(guestPostObj);
        })
        setGuestPostValue(guestPostArray[0]);
        setServiceGuestPostData(guestPostArray);
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  /**
   * gets service recommendations
   * @param {*} loading 
   */
  const getRecommendations = (id, loading) => {
    if (loading === undefined) {
      setLoading(true);
    }
    props.getRecommendations(id,
      (response) => {
        setLoading(false);
        if (response.data && response.data.length) {
          response.data.forEach(service => {
            if (service.isfavorite) {
              if (!favoriteArray.includes(service.id))
              favoriteArray.push(service.id)
            }
          })
        }
        setServiceData(response.data);
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  const getServiceDetail = () => {
    setLoading(true);
    props.getServiceDetail(
      props.match.params.id,
      (response) => {
        setLoading(false);
        setServiceDetail(response.data);
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  const verifyCartRedirect = () => {
    const params = {
      service: serviceDetail.id,
      noofguestpost: guestPostValue.value,
      noofbacklink: backLinkValue.value,
    };
    setLoading(true);
    props.verifyCartRedirect(
      params,
      (response) => {
        setLoading(false);
        PlaySound();
        props.history.push("/cartdetails");
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  useEffect(() => {
    getServiceDetail();
    getServiceBackLinkData();
    getServiceGuestPostData();
  }, []);

  useEffect(() => {
    if (serviceDetail && serviceDetail.id !== undefined) {
      getRecommendations(serviceDetail.id);
    }
  }, [serviceDetail]);

  useEffect(() => {
    getServiceDetail();
    getServiceBackLinkData();
    getServiceGuestPostData();
  }, [props.match.params.id]);

  /**
   * adds favorite website for the user
   * @param {*} id 
   */
  const addFavorite = (id) => {
    // let button = document.getElementById("favoritedetail-"+id);
    // button.setAttribute("disabled", "disabled");
    var serviceArray = serviceData;
    serviceArray.forEach(service => {
      if (service.id === id) {
        service.isfavorite = true;
      }
    })
    setServiceData(serviceArray);
    var params = {
      "service": id.toString()
    }
    props.addFavorite(params,
      (favorite) => {
        // let button = document.getElementById("favoritedetail-"+id);
        // button.removeAttribute("disabled");
        // getRecommendations(serviceDetail.id, true);
      }, 
      (error) => {
        // let button = document.getElementById("favoritedetail-"+id);
        // button.removeAttribute("disabled");
        var serviceArray = serviceData;
        serviceArray.forEach(service => {
          if (service.id === id) {
            service.isfavorite = false;
          }
        })
        setServiceData(serviceArray);
        ToastAlert("error", error.message);
        PlaySound()
      }
    )
  };

  /**
   * removes favorite website for the user
   * @param {*} id 
   */
  const removeFavoriteService = (id) => {
    // let button = document.getElementById("favoritedetail-"+id);
    // button.setAttribute("disabled", "disabled");
    var serviceArray = serviceData;
    serviceArray.forEach(service => {
      if (service.id === id) {
        service.isfavorite = false;
      }
    })
    setServiceData(serviceArray);
    props.removeFavoriteService(id,
      (favorite) => {
        // let button = document.getElementById("favoritedetail-"+id);
        // button.removeAttribute("disabled");
        // getRecommendations(serviceDetail.id, true);
      },
      (error) => {
        // let button = document.getElementById("favoritedetail-"+id);
        // button.removeAttribute("disabled");
        var serviceArray = serviceData;
        serviceArray.forEach(service => {
          if (service.id === id) {
            service.isfavorite = true;
          }
        })
        setServiceData(serviceArray);
        ToastAlert("error", error.message);
        PlaySound()
      }
    )
  };

  return (
    <div class="page-wrapper">
      <Loading isVisible={isLoading} />
      <div className="page_title row m-0">
        <div class="col-lg-1 col-md-2 col-3 p-0">
          <span className="left_icon_title"><img src={marketplace} alt="" /></span>
        </div>
        <div class="col-lg-11 col-md-10 col-9 p-0">
            <h2 className="right_title row m-0">Marketplace </h2>
            <span className="title_tagline m-0 row">
              <ol className="breadcrumb p-0 bg-none mb-0">
                <li className="breadcrumb-item">
                  <Link to={{pathname:"/dashboard"}}>
                    <i className="fa fa-home"></i>
                  </Link>
                </li>
                <li className="breadcrumb-item">
                  <Link to={{pathname:"/marketplace"}}>Marketplace</Link>
                </li>
                <li className="breadcrumb-item active" aria-current="page">
                  {serviceDetail.title ? serviceDetail.title : ""} Details
                </li>
              </ol>
            </span>
        </div>
      </div>
      <div class="grey_box_bg">
        <div className="row m-0 mt-4 p-0 mb-4 content_details">
          <div className="card card_inline">                       
            <div className="col-md-12 col-lg-6 col-xl-4 col-sm-12 p-4">
              <div className="position-relative">
                <img
                  src={serviceDetail.servicepic}
                  onError={(e)=>{e.target.onerror = null; e.target.src=defaultImage}}
                  className="img_crop"                  
                  alt=""
                />
              </div>
            </div>
            <div className="card-body pb-0 col-md-12 col-lg-6 col-xl-8 col-sm-12">
              <div class="row m-0">
                <h1 className="product_d_title col-lg-8 p-0">
                  {serviceDetail.title ? serviceDetail.title : ""}
                </h1>  
                <p className="tagline_text">{serviceDetail.category ? serviceDetail.category.name : ""}</p>                    
              </div>                    
              <div className="row d-flex align-items-center mb-2">
                <div class="col-lg-8">
                  <span className="price-info">
                    $
                    {serviceDetail.price && serviceDetail.backlinkprice
                      ? serviceDetail.price + serviceDetail.backlinkprice
                      : ""
                    }
                  </span>
                  <span class="mx-3 text-muted">|</span>
                  <span className="rating_star">
                    {serviceDetail.review !== 0 && Array.from(Array(Math.round(serviceDetail.review ? serviceDetail.review : 0)), () => {
                    return (
                        <span className="fa fa-star star"></span>
                    )
                    })}
                    {serviceDetail.review === 0 && 
                        <span className="favorite_btn">New Content Writer</span>
                    }
                  </span>
                </div>
                <div class="col-lg-4">
                  <div className="text-right justify-content-right float-right">
                    <ContactSeller 
                      receiverId={serviceDetail.createdby ? serviceDetail.createdby.id : ""}
                      receiverImage={serviceDetail.createdby ? serviceDetail.createdby.profilepic ? serviceDetail.createdby.profilepic : profilelogo: profilelogo}
                      receiverName={serviceDetail.createdby ? serviceDetail.createdby.username: ""}
                    />
                  </div>
                </div>
              </div>
              <p className="text-muted text-justify m-0">{serviceDetail.description}</p>                   
            </div>
          </div>
          <div className="card w-100"> 
            <div className="card-body pb-0"> 
              <div class="bg-white w-100 p-3 mb-3 bor-rad">
                <div className="row">                  
                  <div className="col-12">
                    <h4 class="mainline_head">Guideline and restrictions</h4>
                    <p className="text-muted">{serviceDetail.guideline} </p>
                  </div>          
                  
                </div>
              </div>
              <div className="col-12 p-0">
                <div className="alert alert-success">{serviceDetail.alert}</div>
              </div> 
            </div>         
            <div className="card-body row m-0 p-0">
              <div className="text-center col-md-6 col-lg-3">
                <div class="bg-white w-100 p-3 mb-3 bor-rad">
                  <img src={icnMdl} alt="" />
                  <h4 className="font-weight-bold mt-3">100% Organic</h4>
                  <p className="mb-4 pb-4">Your guest post or backlink will be 100% organic.</p>
                </div>
              </div>
              <div className="text-center  col-md-6 col-lg-3">
                <div class="bg-white w-100 p-3 mb-3 bor-rad">
                  <img src={icnLink} alt="" />
                  <h4 className="font-weight-bold mt-3">Guest Post</h4>
                  <p>
                    Per Guest Post ($
                    {serviceDetail.price ? serviceDetail.price : ""})
                  </p>
                  <div className="form-group selectpicker_form mx-2 mb-3">
                    <Select
                      className="basic-single-select"
                      isSearchable={false}
                      onChange={(selected) => {
                        setGuestPostValue(selected);
                      }}
                      classNamePrefix={"select_dropdown"}
                      value={guestPostValue}
                      options={serviceGuestPostData}
                    />
                  </div>
                </div>
              </div>
              <div className="text-center  col-md-6 col-lg-3">
                <div class="bg-white w-100 p-3 mb-3 bor-rad">
                  <img src={icnLink} alt="" />
                  <h4 className="font-weight-bold mt-3">Backlink</h4>
                  <p>
                    Per Backlink ($
                    {serviceDetail.backlinkprice ? serviceDetail.backlinkprice : ""}
                    )
                  </p>
                  <div className="form-group selectpicker_form mx-2 mb-3">
                    <Select
                      className="basic-single-select"
                      isSearchable={false}
                      onChange={(selected) => {
                        setBackLinkValue(selected);
                      }}
                      classNamePrefix={"select_dropdown"}
                      value={backLinkValue}
                      options={serviceBackLinkData}
                    />
                  </div>
                </div>
              </div>
              <div className="text-center  col-md-6 col-lg-3">
                <div class="bg-white w-100 p-3 pb-4 mb-4 bor-rad">
                  <img src={icnCar} alt="" />
                  <h4 className="font-weight-bold mt-3 pb-3">Estimate Delivery</h4>
                  <p>
                    {serviceDetail.estimatedelivery
                      ? serviceDetail.estimatedelivery.name + (serviceDetail.estimatedelivery.name > 1 ? " days" : " day")
                      : ""}
                  </p>
                </div>
              </div>
            </div>
            <div className="card-body p-0">
              <div className="action-btn text-center w-100 mb-3">
                <a
                  onClick={verifyCartRedirect}
                  href="#"
                  disabled={serviceDetail.isownservice}
                  className="btn_light_green effect effect-2 width-195"
                >Add to Cart($
                  {serviceDetail.price && serviceDetail.backlinkprice
                    ? serviceDetail.price * guestPostValue.value +
                      serviceDetail.backlinkprice * backLinkValue.value
                    : ""
                  })
                </a>
              </div>
              {serviceDetail.createdby &&
                <div className="p-3 bg-white bord_rad mx-4">
                  <h4 class="mainline_head">About the Seller</h4>
                  <div className="row">                    
                    <div className="col-md-12 col-lg-4 d-flex">                      
                      <div className="media mt-3 row m-0 d-flex align-items-center">
                        <div class="col-4 col-sm-4 col-md-4 col-lg-3 pl-0">
                          <img
                            className="d-flex mr-3 rounded-circle"
                            height="60"
                            width="60"
                            src={ serviceDetail.createdby.profilepic ? serviceDetail.createdby.profilepic : profilelogo }
                            onError={(e)=>{e.target.onerror = null; e.target.src=profilelogo}}
                            alt=""
                          />
                        </div>                        
                        <div className="media-body col-8 col-sm-8 col-md-8 col-lg-9 pl-0">
                          <h3 className="mb-1 mt-2">
                            {serviceDetail.createdby.username}
                          </h3>
                          <p className="small mb-2">
                            From: <span className="text-muted">{serviceDetail.createdby.country ? serviceDetail.createdby.country : "United States"}</span>
                          </p>
                          <div className="rating_star">
                            {serviceDetail.createdby.review !== 0 && Array.from(Array(Math.round(serviceDetail.createdby.review)), () => {
                              return (
                                <span className="fa fa-star star"></span>
                              )
                            })}
                            {serviceDetail.createdby.review === 0 && 
                              <div className="text-muted">New Content Writer</div>
                            }
                          </div>
                        </div>
                        <div className="col-lg-12 col-md-12 p-0 d-flex justify-content-center mt-3">
                          <ContactSeller 
                              receiverId={serviceDetail.createdby ? serviceDetail.createdby.id : ""}
                              receiverImage={serviceDetail.createdby ? serviceDetail.createdby.profilepic ? serviceDetail.createdby.profilepic : profilelogo : profilelogo}
                              receiverName={serviceDetail.createdby ? serviceDetail.createdby.username: ""}
                            />
                        </div>
                      </div>
                    </div>
                    <div className="recent-review col-md-12 col-lg-8">
                      <p className="text-muted font-weight-bold mt-3">
                        Recent Reviews
                      </p>
                      {serviceDetail.createdby.recentreview && serviceDetail.createdby.recentreview.length !== 0 && 
                        serviceDetail.createdby.recentreview.map((value) => (
                          <div className="w-100 d-flex justify-content-between align-items-center flex-wrap bor_btm mb-3">
                             <div className="row m-0 w-100 mb-3 d-flex align-items-center">
                              <div className="d-flex align-items-center col-lg-8 p-0">
                                <img
                                  src={value.profilepic ? value.profilepic : profilelogo}
                                  onError={(e)=>{e.target.onerror = null; e.target.src=profilelogo}}
                                  width="30"
                                  className="mr-2 rounded-circle"
                                  alt=""
                                />
                                <p className="mb-0 mr-2 small font-weight-bold text-truncate">
                                  {value.username}
                                </p>
                              </div>
                              <div className="rating_star col-lg-4 p-0 text-right">
                                {value.review !== 0 && Array.from(Array(Math.round(value.review)), () => {
                                  return (
                                    <span className="fa fa-star star"></span>
                                  )
                                })}
                                {value.review === 0 && 
                                  <div className="text-muted">New Content Writer</div>
                                }
                              </div>
                            </div>
                            <p className="w-100 text-muted text-justify">
                              {value.comment}
                            </p>
                          </div>
                        )
                      )}
                      {serviceDetail.createdby.recentreview.length === 0 &&
                        <div className="w-100 d-flex justify-content-between align-items-center flex-wrap text-muted">
                          No recent reviews
                        </div>
                      }
                    </div>
                  </div>
                </div>
              }
              {serviceData &&
                <div className="w-100 text-center py-5 mt-3">
                  <h3 className="sidesub_green_head">Suggestion and Recommendations</h3>
                  <h4 className="text-muted">People also search for this niche</h4>
                </div>
              }
              <section className="productscontainer-box py-0">
                <div className="container">
                  {serviceData.length === 0 &&
                    <h3 className="norec_found">No Record Found</h3>
                  }
                  {serviceData && serviceData.length != 0 &&
                    <div className="position-relative swipernavDetail swiper_pg_detail">
                      <Swiper slidesPerView={4} spaceBetween={30} loop={true} pagination={{ clickable:true}} navigation={{nextEl: '.swipernavDetail .swiper-button-next', prevEl: '.swipernavDetail .swiper-button-prev'}} breakpoints={{ 1: { slidesPerView: 1,spaceBetweenSlides: 30}, 1000: { slidesPerView: 4,spaceBetweenSlides: 30}}} autoplay={{ delay: 3000, disableOnInteraction: false, pauseOnMouseEnter: true }} className="mySwiper">
                        {serviceData && serviceData.length != 0 &&
                          serviceData.map((value) => (
                            <SwiperSlide>
                              <div className="swiper-slide grid-view-products">
                                <div className="card h-100 bg-white">                                  
                                    <div className="position-relative">
                                      <Link to={`/marketplace/${value.id}`} className="position-relative">
                                        <img
                                          src={value.servicepic}
                                          onError={(e)=>{e.target.onerror = null; e.target.src=defaultImage}}
                                          height="186"
                                          className="img_crop"
                                          alt="..."
                                        />
                                      </Link>
                                      <div className="new_seller">
                                        {value.review === 0 && 
                                          <div className="new_seller_btn">New Content Writer</div>
                                        }
                                      </div>
                                    </div>
                                    <div className="card-body p-2">
                                      <h5 className="product_title" title={value.title}>{value.title}</h5>                                      
                                      <div className="rating_star d-flex mt-1">
                                        {value.review !== 0 && Array.from(Array(Math.round(value.review)), () => {
                                          return (
                                            <span className="fa fa-star star"></span>
                                          )
                                        })}
                                      </div>
                                      <div class="row m-0 mt-3 mb-3 align-items-center">
                                        <div class="col-sm-12 col-4 p-0 mb-2 text-left">
                                          <Popup
                                            key={value.id}
                                            trigger={<span className="sold_count_text link-cursor">By: {value.seller.name}</span>}
                                          >
                                            <Popup.Header>
                                              <Image 
                                                src={value.seller.image}
                                                onError={(e)=>{e.target.onerror = null; e.target.src=profilelogo}}
                                                className="rounded-circle mr-2"
                                                width="50"
                                                height="50"
                                                alt="..."
                                              />
                                              {value.seller.name}
                                            </Popup.Header>
                                            <Popup.Content>
                                              {value.seller.bio}
                                            </Popup.Content>
                                          </Popup>
                                        </div>
                                        <div class="col-sm-6 p-0 text-left"><span className="price-info mt-2 mb-2">${value.price}</span></div>                    
                                        <div class="col-sm-6 p-0 text-right"><p className="sold_count_text">{value.totalsold} Sold</p></div>
                                      </div>
                                      <div className="btn-groups d-flex justify-content-center">
                                        <a 
                                          className="favorite_btn mr-2"
                                          disabled={value.isownservice}
                                          id={"favoritedetail-"+value.id}
                                          onClick={() => {
                                            if (value.isfavorite) {
                                              if (favoriteArray.includes(value.id)) {
                                                let unlike = favoriteArray.filter((elem) => elem !== value.id);
                                                setFavoriteArray(unlike);
                                              }
                                              removeFavoriteService(value.id)
                                            } else {
                                              if (!favoriteArray.includes(value.id)) {
                                                setFavoriteArray([...favoriteArray, value.id]);
                                              }
                                              addFavorite(value.id);
                                              setFavoriteModalOpen(true);
                                              setFavoriteModalContent("You have favorited " + value.seller.name + "'s " + value.title+ " service.");
                                            }
                                          }}
                                        >
                                          {favoriteArray.includes(value.id) ? <span class="fa fa-heart"></span> :
                                          <span class="fa fa-heart-o"></span>}{" "}Favorite
                                        </a>
                                        <Link className="view_btn" to={`/marketplace/${value.id}`}>
                                            <span class="fa fa-eye"></span> View
                                        </Link>
                                      </div>
                                    </div>
                                </div>
                              </div>
                            </SwiperSlide>
                          ))
                        }                        
                      </Swiper>
                      <div class="swiper-button-prev"></div>
                      <div class="swiper-button-next"></div>
                    </div>
                  }
                  </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <Modal
        open={isFavoriteModalOpen}
        disableBackdropClick
        onClose={() => { 
          setFavoriteModalOpen(false);
          setFavoriteModalContent("");
        }}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description">
        <div className="modal-dialog modal_popup" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="popup_title" id="exampleModalLabel">Favorited</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={() => { 
                  setFavoriteModalOpen(false);
                  setFavoriteModalContent("");
                }}
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body justify-content-center">
              {/* <i className="fa fa-heart heartred fa-5x" style={{"padding-left": "189px"}}aria-hidden="true"></i> */}
              <h4 className="text-center">{favoriteModalContent}</h4>
            </div>
            <div className="modal-footer justify-content-center">
              <button 
                onClick={() => { 
                  setFavoriteModalOpen(false);
                  setFavoriteModalContent("");
                }}
                className="btn_light_green"
              >
                OK
              </button>
            </div>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default MarketDetail;
