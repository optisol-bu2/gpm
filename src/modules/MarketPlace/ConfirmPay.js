import React, { useState, useEffect } from "react";
import ReactDOM from "react-dom";
import { loadStripe } from "@stripe/stripe-js";
import { Elements } from "@stripe/react-stripe-js";
import Loading from "../../components/core/Loading";
import ChatLoading from "../../components/core/ChatLoading";
import { STRIPE_KEY } from "../../common/constants";
import CardInfoForm from "./CardInfoForm";
import { ToastAlert,PlaySound } from '../../utils/sweetalert2';

const stripePromise = loadStripe(
  STRIPE_KEY
);

const PayPalButton = window.paypal.Buttons.driver("react", { React, ReactDOM });

const ConfirmPay = (props) => {
  const [loading, setLoading] = useState(null);
  const [savedCardLoading, setSavedCardLoading] = useState(false);
  const [paymentMethod, setPaymentMethod] = useState('credit');
  const [savedCard, setSavedCard] = useState("");
  const [clientSecret, setClientSecret] = useState("");
  const [transferGroup, setTransferGroup] = useState("");
  const [savedCardData, setSavedCardData] = useState([]);
  const [description, setDescription] = useState("");
  const [cartIds, setCartIds] = useState([]);

  /**
   * Updates when cartData value updated
   */
  useEffect(() => {
    getSavedCardsData();
    if (props.cartData.services && props.cartData.services.length != 0) {
      var description = "Payment for service - "
      var cartArray = [];
      props.cartData.services.forEach((service, index) => {
        cartArray.push(service.id)
        if (index == 0) {
          description = description.concat(service.id)
        } else {
          description = description.concat(", "+service.id)
        }
        if (index == props.cartData.services.length-1) {
          setDescription(description);
          setCartIds(cartArray);
        }
      });
    }
  }, [props.cartData]);

  /**
   * Gets saved cards of the user
   */
  const getSavedCardsData = () => {
    setLoading(true);
    props.getSavedCards(
      (response) => {
        setSavedCardData(response.data);
        setLoading(false);
      },
      (error) => {
        setLoading(false);
      }
    );
  }

  const createOrder = (data, actions) => {
    const response = actions.order.create({
      intent: "CAPTURE",
      purchase_units: [
        {
          description: description,
          amount: {
            currency_code: "USD",
            value: props.cartData.totalprice
          },
        },
      ]
    });
    return response;
  };

  const onApprove = async (data, actions) => {
    const order = await actions.order.capture();
    if (order.status !== undefined && order.status == "COMPLETED") {
      let params = {
        cartids: cartIds,
        orderid: order.id,
      }
      props.createOrderForPaypal(params,
        (response) => {
          setLoading(false);
          setDescription("");
          setCartIds([]);
          let params = {
            paymenttype: 1,
            orderid: response.data.payment_intent_id,
          };
          placeOrderSubmit(params);
        },
        (error) => {
          setLoading(false);
          ToastAlert('error', error.message)
          PlaySound()
        }
      );
    }
    // setPaid(true);
  };


  /**
   * On successful payment it places the order
   * @param {*} paymentResponse 
   * @param {*} stripe 
   * @param {*} storecard 
   * @param {*} cardElement 
   * @returns 
   */
  const onSuccessPayment = async (paymentResponse, stripe, storecard, cardElement) => {
    if (!stripe) {
      return;
    }

    if (storecard) {
      setLoading(true);
      setSavedCardLoading(true);
      stripe.createPaymentMethod({
        type: "card",
        card: cardElement,
      }).then(
        result => {
          if (result.error) {
            setLoading(false);
            setSavedCardLoading(false);
            ToastAlert('error', result.error.message)
            PlaySound()
          } else {
            setLoading(false);
            setSavedCardLoading(false);
            let params = {
              paymenttype: 2,
              paymentintentid: paymentResponse.paymentIntent.id,
              storecard: storecard,
              paymentmethodid: result.paymentMethod.id,
              transfergroup: props.paymentData.transfergroup
            };
            placeOrderSubmit(params);
          }
        }
      );
    } else {
      let params = {
        paymenttype: 2,
        paymentintentid: paymentResponse.paymentIntent.id,
        storecard: storecard,
        transfergroup: props.paymentData.transfergroup
      };
      placeOrderSubmit(params);
    }
  };

  /**
   * Handles when place order clicked
   * @param {*} params 
   */
  const placeOrderSubmit = (params) => {
    setLoading(true);
    setSavedCardLoading(true);
    props.placeOrder(
      params,
      (response) => {
        // setLoading(false);
        setSavedCardLoading(false);
        ToastAlert('success', response.message)
        PlaySound()
        props.isOPSelected(false);
        props.isOCSelected(false);
        props.transactionInfo(response.data);
      },
      (error) => {
        setLoading(false);
        setSavedCardLoading(false);
        ToastAlert('error', error.message)
        PlaySound()
      }
    );
  }

  /**
   * Gets payment intent for the order
   * @param {*} id 
   */
  const getCardPayment = (id) => {
    let data = {
      "paymentmethodid": id
    }
    setSavedCardLoading(true);
    props.paymentSession(
      data,
      (response) => {
        setSavedCard(id);
        setSavedCardLoading(false)
        setClientSecret(response.data.client_secret)
        setTransferGroup(response.data.transfergroup)
      },
      (error) => {
        setSavedCard("");
        setSavedCardLoading(false)
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  }

  /**
   * Confirms payment for the saved card
   * @returns 
   */
  const submitSavedCard = async () => {
    setLoading(true);
    setSavedCardLoading(true);
    var stripe = window.Stripe(STRIPE_KEY);

    if (!stripe) {
      return;
    }

    stripe.confirmCardPayment(clientSecret, {
        payment_method: savedCard,
    }).then(
      result => {
        if (result.error) {
          setLoading(false);
          setSavedCardLoading(false);
          ToastAlert('error', result.error.message)
          PlaySound()
        } else if (result.paymentIntent.status == "succeeded") {
          setLoading(false);
          setSavedCardLoading(false);
          let params = {
            paymenttype: 2,
            paymentintentid: result.paymentIntent.id,
            storecard: false,
            transfergroup: transferGroup
          };
          placeOrderSubmit(params);
        }
      }
    );
  };


  return (
    <div className="tab-content">
      <div className="tab-pane active" role="tabpanel">
        <div className="row m-0">
          <div className="col-lg-8 col-xl-8 mt-4 chkout_section pad_zero">
            <div className="card d-flex row flex_chk_md chkout_card h-100">
              <div className="card-body">
                <h5 className="chk_out_title mb-4">Payment Options</h5>
                <div class="row mb-4">
                  
                    {savedCardData && savedCardData.length != 0 &&
                      <div class="col-lg-4">
                        <div className="custom-control custom-radio">
                          <input
                            type="radio"
                            id="customRadioSaved"
                            name="customRadio"
                            className="custom-control-input"
                            checked={paymentMethod === 'saved'}
                            onChange={() => setPaymentMethod('saved')}
                          ></input>
                          <label
                            className="custom-control-label h6 text-dark font-weight-bold"
                            for="customRadioSaved"
                          >
                            Saved Cards
                          </label>
                        </div>
                      </div>
                    }
                  
                  <div class="col-lg-4">
                    <div className="custom-control custom-radio">
                      <input
                        type="radio"
                        id="customRadio1"
                        name="customRadio"
                        className="custom-control-input"
                        checked={paymentMethod === 'credit'}
                        onChange={() => {
                          setPaymentMethod('credit')
                          setSavedCard("")}
                        }
                      ></input>
                      <label
                        className="custom-control-label h6 text-dark font-weight-bold"
                        for="customRadio1"
                      >
                        Debit/Credit Card
                      </label>
                    </div>
                  </div>
                  <div class="col-lg-4">
                    {/* <div className="custom-control custom-radio">
                      <input
                        type="radio"
                        id="customRadio2"
                        name="customRadio"
                        className="custom-control-input"
                        checked={paymentMethod === 'paypal'}
                        onChange={() => {
                          setPaymentMethod('paypal')
                          setSavedCard("")}
                        }
                      ></input>
                      <label
                        className="custom-control-label h6 text-dark font-weight-bold"
                        for="customRadio2"
                      >
                        PayPal
                      </label>
                    </div> */}
                  </div>
                </div>
                
                {paymentMethod === 'saved' &&
                  <div className="options_bg_Settings">
                    <ChatLoading isVisible={savedCardLoading} />
                    {savedCardData.map((data) => (
                    <div class="saved-group">
                      <div className="custom-control custom-radio">                        
                        <input
                          type="radio"
                          id={"payment-source"+data.id}
                          name="payment-source"
                          className="custom-control-input"
                          onChange={() => {
                            getCardPayment(data.id);
                          }}
                        />
                        <label id={data.id} className="custom-control-label h6 text-dark font-weight-bold" for={"payment-source"+data.id}>{data.brand} Card Ending **** {data.last4} | Expires {data.exp_month}/{data.exp_year}</label>
                      </div>
                    </div>
                    ))}                    
                    <div className="col-md-12 text-center form-group mt-3 mb-0">
                      <button
                        type="button"
                        disabled={!savedCard}
                        onClick={() => {
                          submitSavedCard();
                        }}
                        className="btn_light_green saved"
                      >
                        {"Pay $" + props.cartData.totalprice}
                      </button>
                    </div>
                  </div>
                }               
                
                  <Elements stripe={stripePromise}>
                    {paymentMethod === 'credit' ? (
                      <div className="options_bg_Settings">
                        <ChatLoading isVisible={savedCardLoading} />
                        <CardInfoForm
                          tabData="cart"
                          cartData={props.cartData}
                          paymentData={props.paymentData}
                          {...props}
                          onPaymentsuccess={(paymentResponse, stripe, storecard, cardElement) => {
                            onSuccessPayment(paymentResponse, stripe, storecard, cardElement)
                          }}
                        />
                      </div>
                      ) : (   
                        <div />                   
                      )}
                  </Elements>              
                            
                  {paymentMethod === 'paypal' ? (
                    <div className="options_bg_Settings">
                      <div style={{ marginTop: 20, marginRight: "auto", marginLeft: "auto", width: "98%" }}>
                        <PayPalButton 
                          style= {{
                            shape: 'pill',
                            color: 'gold',
                            layout: 'vertical',
                            label: 'paypal'
                          }} 
                          createOrder={(data, actions) => createOrder(data, actions)}
                          onApprove={(data, actions) => onApprove(data, actions)}
                        />
                      </div>
                    </div>
                    ) : (
                    <div />
                   )}
              </div>
              {/* <div class="row m-0 card-body">
                <div className="bg_buyer_proct mb-3 col-lg-12 p-3">
                  <h5 className="chk_out_title mb-3">Buyer Protection</h5>              
                  <div className="custom-control custom-checkbox pl-0">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheckPay1"
                    ></input>
                    <label
                      className="custom-control-label text-dark"
                      for="customCheckPay1"
                    >
                      Full refund if you don't receive your order
                    </label>
                  </div>

                  <div className="custom-control custom-checkbox pl-0 mt-2">
                    <input
                      type="checkbox"
                      className="custom-control-input"
                      id="customCheckPay2"
                    ></input>
                    <label
                      className="custom-control-label text-dark"
                      for="customCheckPay2"
                    >
                      Full or Partial Refund.if the product is not as described in
                      details
                    </label>
                  </div>
                </div>
              </div> */}
              <div className="row m-0 card-body text-center p-0 mb-4 justify-content-center">
                <h5 className="alltotal_price_style">
                  <strong>All Total:</strong> ${props.cartData.totalprice}
                </h5>
              </div>
            </div>
          </div>
          <div className="col-lg-4 col-xl-4 mt-4 chkout_section pad_zero chkout_card">
            <div className="card">
              <div className="card-body">
                <h5 className="chk_out_title">Price Details</h5>
                <div className="d-flex justify-content-between">
                  <p className="left_label_prc">Price:</p>
                  <p className="right_price_set">
                    ${props.cartData.serviceprice}
                    </p>
                </div>
                <div className="d-flex justify-content-between">
                  <p className="left_label_prc">Service Fee:</p>
                  <p className="right_price_set_green">
                    ${props.cartData.servicefeevalue}
                    </p>
                </div>
              </div>
              <hr className="my-0" />
              <div className="card-body">
                <div className="d-flex justify-content-between">
                  <p className="total_f_price">Total:</p>
                  <p className="total_f_price">
                    ${props.cartData.totalprice}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <Loading isVisible={loading}></Loading>
      </div>
    </div>
  );
};

export default ConfirmPay;
