import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Loading from "../../../components/core/Loading";
import moment from "moment";
import TimerControl from "../../../components/core/Timer";
import iconhours from "../../../assets/images/other-pages/esti-deli-icon.svg";
import iconnoofhours from "../../../assets/images/other-pages/no_of_hours.png";
import iconnoofwords from "../../../assets/images/other-pages/no_of_words.png";
import iconbacklink from "../../../assets/images/other-pages/backlink-icon.svg";
import icongplink from "../../../assets/images/other-pages/guest-post-icon.svg";
import icnDownload from "../../../assets/images/icn_download.svg";
import avatar from '../../../assets/images/avatar.png';
import FeedbackView from "../FeedbackView";
import SubmitFeedback from "../SubmitFeedback";
import { ToastAlert, PlaySound } from "../../../utils/sweetalert2";
import my_order_icon from "../../../assets/images/menu-icons/my_orders_white.svg";
import ContactSeller from "../../../components/ContactSeller/ContactSeller";
import ReactReadMoreReadLess from "react-read-more-read-less";
import { getBloggerProfile } from "../../../Actions/bloggerAction";
import { connect } from "react-redux";
import ConfirmationDialog from "../../../components/core/ConfirmationDialog/ConfirmationDialog";

const UpdateDelivery = (props) => {
  const [isLoading, setLoading] = useState(null);
  const [orderData, setOrderData] = useState({});
  const [guestPostArray, setGuestPostArray] = useState([]);
  const [backLinkArray, setBackLinkArray] = useState([]);
  const [addElementToGuestPostArray, setAddElementToGuestPostArray] = useState(false);
  const [addElementToBackLinkArray, setAddElementToBackLinkArray] = useState(false);
  const [updateElementToGuestPostArray, setUpdateElementToGuestPostArray] = useState(false);
  const [updateElementToBackLinkArray, setUpdateElementToBackLinkArray] = useState(false);
  const [recordValue, setRecordValue] = useState();
  const [recordId, setRecordId] = useState();
  const [isLinkUpdate, setIsLinkUpdate] = useState(false);
  const [sellerName, setSellerName] = useState("");
  const [buyerName, setBuyerName] = useState("");
  const [buyerImage, setBuyerImage] = useState("");
  const [sellerImage, setSellerImage] = useState("");
  const [sellerId, setSellerId] = useState("");
  const [revisionReqAttachArray, setRevisionReqAttachArray] = useState([]);
  const [sellerFeedback, setSellerFeedback] = useState(null);
  const [sellerComment, setSellerComment] = useState(false);
  const [isExtenstion, setExtension] = useState(false);
  const [bloggerProfileData, setbloggerProfileData] = useState("");
  const [confirmDialog, setConfirmDialog] = useState(false);
  const [approveDialog, setApproveDialog] = useState(false);
  const [extendDialog, setExtendDialog] = useState(false);

  useEffect(() => {
    props.setEndTimer(false)
    props.last24Hours(false)
  },[]);

  useEffect(() => {
    if (updateElementToGuestPostArray)
      updateElementToArray(recordValue, recordId, isLinkUpdate, 0);
  }, [updateElementToGuestPostArray]);

  useEffect(() => {
    if (updateElementToBackLinkArray)
      updateElementToArray(recordValue, recordId, isLinkUpdate, 1);
  }, [updateElementToBackLinkArray]);

  useEffect(() => {
    if (isExtenstion) {
      getOrderDetail();
      getFeedback();
    }
  }, [isExtenstion]);

  useEffect(() => {
    if (props.orderId && props.userData.activeusertype === 2) {
      getOrderDetail();
      getFeedback();
    }
  }, [props.orderId]);

  useEffect(() => {
    if (props.orderRedirect && props.userData.activeusertype === 2) {
      getOrderDetail();
      getFeedback();
      props.orderList(false);
    }
  }, [props.orderRedirect]);

  useEffect(() => {
    if (sellerId) {
      getBloggerProfile(sellerId);
    }
  }, [sellerId]);

  useEffect(() => {
    if (addElementToGuestPostArray) addElementToArray(0);
  }, [addElementToGuestPostArray]);

  useEffect(() => {
    if (addElementToBackLinkArray) addElementToArray(1);
  }, [addElementToBackLinkArray]);

  const getBloggerProfile = (id) => {
    props.getBloggerProfile(id,
      (bloggerProfile) => {
        if (bloggerProfile.data) {
          setbloggerProfileData(bloggerProfile.data);
        }
      },
      (error) => {
      }
    );
  }

  const orderExtension = () => {
    setLoading(true);
    let params = {
      orderid: props.orderId,
    };
    props.orderExtention(
      params,
      (response) => {
        setExtension(true);
        setLoading(false);
        ToastAlert('success', response.message);
        PlaySound()
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  /**
   * adds blogger to cart
   * @param {*} id 
   */
  const cancelOrder = () => {
    setLoading(true);
    let params = {
      id: props.orderId,
    };
    props.orderCancel(
      params,
      (response) => {
        setLoading(false);
        ToastAlert('success', response.message);
        PlaySound()
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  const getFeedback = () => {
    try {
      props.getFeedback(
        props.orderId,
        (response) => {
          if (response.data !== null) {
            setSellerFeedback(response.data.seller ? response.data.seller : null);
          }
        },
        () => { }
      );
    } catch (error) { }
  };

  const addElementToArray = (flag) => {
    if (guestPostArray.length > 0 && flag === 0) {
      var list = guestPostArray;
      list.push({ id: guestPostArray.length, key: "", value: "" });
      setGuestPostArray(list);
      setAddElementToGuestPostArray(false);
    }
    if (backLinkArray.length > 0 && flag === 1) {
      var list = backLinkArray;
      list.push({ id: backLinkArray.length, key: "", value: "" });
      setBackLinkArray(list);
      setAddElementToBackLinkArray(false);
    }
  };

  const updateElementToArray = (value, id, isLinkUpdate, flag) => {
    if (guestPostArray.length > 0 && flag === 0) {
      var list = guestPostArray;
      for (let i = 0; i < list.length; i++) {
        if (list[i].id === id) {
          isLinkUpdate === true
            ? (list[i].value = value)
            : (list[i].key = value);
        }
      }
      setGuestPostArray(list);
      setUpdateElementToGuestPostArray(false);
    }
    if (backLinkArray.length > 0 && flag === 1) {
      var list = backLinkArray;
      for (let i = 0; i < list.length; i++) {
        if (list[i].id === id) {
          isLinkUpdate === true
            ? (list[i].value = value)
            : (list[i].key = value);
        }
      }
      setBackLinkArray(list);
      setUpdateElementToBackLinkArray(false);
    }
  };

  /**
   * changes the given date to local date format
   * @param {*} date
   * @returns
   */
  const orderDateFormat = (date) => {
    var utc = moment.utc(date).toDate();
    var orderDate = moment(utc).local().format('MMM-DD-YYYY');
    return orderDate;
  }

  /**
   * changes the given date to local date format
   * @param {*} dateTime
   * @returns
   */
  const orderTimeFormat = (dateTime) => {
    var utc = moment.utc(dateTime).toDate();
    var orderTime = moment(utc).local().format('MMM-DD-YYYY hh:mm A');
    return orderTime;
  }

  const getOrderDetail = () => {
    setLoading(true);
    props.getOrderDetail(
      props.orderId,
      (response) => {
        setLoading(false);
        if (response.data !== null) {
          setSellerId(response.data.seller.id);
          setSellerImage(response.data.seller.image);
          setSellerName(response.data.seller.name);
          setBuyerName(response.data.buyer.name);
          setBuyerImage(response.data.buyer.image);
          setSellerComment(response.data.issellercommented);
          response.data.timerInitialState =
            parseInt(response.data.deliverytime) * 3600000;
          if (response.data.comments.length > 0) {
            var list = [];
            for (let i = 0; i < response.data.comments.length; i++) {
              var buyerObj =
                response.data.comments[i].buyer !== null &&
                  response.data.comments[i].buyer !== undefined
                  ? createReqAttachObj(response.data.comments[i].buyer)
                  : { comment: "", date: "", attachments: [] };
              var sellerObj =
                response.data.comments[i].seller !== null &&
                  response.data.comments[i].seller !== undefined
                  ? createReqAttachObj(response.data.comments[i].seller)
                  : { comment: "", date: "", attachments: [] };
              var revision = response.data.comments[i].revision;
              list.push({
                buyerObj: buyerObj,
                sellerObj: sellerObj,
                revisionNumber: revision,
              });
            }
            setRevisionReqAttachArray(list);
          } else {
            setRevisionReqAttachArray({
              buyerObj: { comment: "", date: "", attachments: [] },
              sellerObj: { comment: "", date: "", attachments: [] },
            });
          }
          setOrderData(response.data);
        }
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  const createReqAttachObj = (commentReqObj) => {
    var obj = { comment: "", date: "", attachments: [] };
    obj.comment =
      commentReqObj.comment !== undefined && commentReqObj.comment !== null
        ? commentReqObj.comment
        : "";
    obj.date =
      commentReqObj.date !== undefined && commentReqObj.date !== null
        ? orderTimeFormat(commentReqObj.date)
        : "";
    if (
      commentReqObj.attachments !== undefined &&
      commentReqObj.attachments !== null
    ) {
      var attachments = commentReqObj.attachments;
      if (attachments.length > 0) {
        var list = [];
        for (let i = 0; i < attachments.length; i++) {
          var fileName = attachments[i].name.split("/")[
            attachments[i].name.split("/").length - 1
          ];
          var link = attachments[i].document;
          list.push({ name: fileName, link: link, attachmentName: attachments[i].name });
        }
        obj.attachments = list;
      }
    }

    return obj;
  };

  const approveOrder = () => {
    let params = {
      id: props.orderId,
    };
    setLoading(true);
    props.orderApproved(
      params,
      () => {
        setLoading(false);
        props.reloadScreen();
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  const askForRevision = () => {
    setLoading(false);
    props.startRevision();
  };

  const progress_complete = {
    width: "25%",
  };

  return (
    <div>
      <Loading isVisible={isLoading} />
      <div className="page_title row m-0 mb-4 pb-2">
        <div class="col-lg-1 col-md-2 col-3 p-0">
          <span className="left_icon_title"><img src={my_order_icon} alt="" /></span>
        </div>
        <div class="col-lg-11 col-md-10 col-9 p-0 ">
          <h2 className="right_title row m-0">Order Detail</h2>
          <span className="title_tagline m-0 row">
            <ol className="breadcrumb p-0 bg-none mb-0">
              <li className="breadcrumb-item">
                <Link to={{ pathname: "/dashboard" }}>
                  <i className="fa fa-home"></i>
                </Link>
              </li>
              <li className="breadcrumb-item">
                <Link to={{ pathname: "/myOrder" }}>My Orders</Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                {orderData.invoiceid ? orderData.invoiceid : ""} Details
              </li>
            </ol>
          </span>
        </div>
      </div>
      {orderData !== null && isLoading === false && (
        <div class="grey_box_bg orderpage_details">
          <div class="card mt-3 pb-0">
            <div class="card-body">
              <div class="d-flex flex-column flex-sm-row text-center text-sm-left justify-content-between flex-wrap align-items-center">
                <div className="col-lg-3 text-center">
                  <h5 class="order_id">
                    <span class="order_span">Order</span>{" "}
                    {orderData.invoiceid}
                  </h5>
                  <p class="orderp font-weight-bold">
                    {orderData.startdate == null ? "" : orderDateFormat(orderData.startdate)}
                  </p>
                </div>
                <div className="col-lg-4 progress_div">
                  <h6>{orderData.statusname}</h6>
                  <div class="progress">
                    <div
                      class={orderData.status == 9 ? "progress-bar bg-danger" : "progress-bar bg-success"}
                      role="progressbar"
                      style={{ width: (orderData.status == 8 || orderData.status == 9) ? "100%" : orderData.revision == 0 ? ((orderData.revision + (0.4 + (sellerComment ? 0.4 : 0))) / orderData.max_revision) * 100 + "%" : orderData.revision == orderData.max_revision ? ((orderData.revision - (0.4 + (sellerComment ? 0 : 0.4))) / orderData.max_revision) * 100 + "%" : ((orderData.revision + (sellerComment ? 0.5 : 0)) / orderData.max_revision) * 100 + "%" }}
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </div>
                <div className="col-lg-3">
                  <h5 class="order_id text-center">
                    <span class="order_span">${orderData.total}</span>{" "}
                  </h5>
                </div>
              </div>
            </div>

            <div class="row">
              <div class={orderData.servicetype == 3 ? "text-left  col-sm-6 col-lg-6" : "text-left  col-sm-6 col-lg-3"}>
                <h5 class="mid_head_side">Seller</h5>
                <div class="bg_settings_parts user-profile d-flex">
                  <img
                    src={sellerImage ? sellerImage : avatar}
                    onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                    alt="..."
                    width="65"
                    height="65"
                    class="rounded-circle"
                    data-pagespeed-url-hash="3993073969"
                    onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                  />
                  {orderData && orderData.seller &&
                    <Link
                      style={{ marginLeft: 10 }}
                      class="user-name text-dark mr-2 small"
                    >
                      <ContactSeller
                        orderId={orderData.id} 
                        sellerName={sellerName}
                        sellerBio={orderData.seller.bio}
                        sellerReview={orderData.seller.review}
                        sellerLastLoggingTime={orderData.seller.lastlogin_at}
                        receiverId={bloggerProfileData.user ? bloggerProfileData.user : ""}
                        receiverImage={bloggerProfileData.profilepic ? bloggerProfileData.profilepic : avatar}
                        receiverName={bloggerProfileData.username ? bloggerProfileData.username : "User"}
                        bloggerProfile={true}
                      />
                    </Link>
                  }
                </div>
              </div>
              {orderData.servicetype == 1 &&
                <div class="text-center col-sm-6 col-lg-3">
                  <h5 class="mid_head_side">Guest Post</h5>
                  <div class="bg_settings_parts d-flex">
                    <span className="leftimg_gp">
                      <img
                        src={icongplink}
                        alt=""
                        data-pagespeed-url-hash="1714040194"
                        onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                      />
                    </span>
                    <h4 class="big_price_fnt  w-auto">{orderData.noofguestpost}</h4>
                  </div>
                </div>
              }
              {orderData.servicetype == 1 &&
                <div class="text-center col-sm-6 col-lg-3">
                  <h5 class="mid_head_side">Backlink</h5>
                  <div class="bg_settings_parts d-flex">
                    <span className="leftimg_gp">
                      <img
                        src={iconbacklink}
                        alt=""
                        data-pagespeed-url-hash="1714040194"
                        onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                      />
                    </span>
                    <h4 class="big_price_fnt  w-auto">{orderData.noofbacklink}</h4>
                  </div>
                </div>
              }
              {orderData.servicetype == 2 &&
                <div class="text-center col-sm-6 col-lg-3">
                  <h5 class="mid_head_side">No. of Hours</h5>
                  <div class="bg_settings_parts d-flex">
                    <span className="leftimg_gp">
                      <img
                        src={iconnoofhours}
                        alt=""
                        data-pagespeed-url-hash="1714040194"
                        onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                      />
                    </span>
                    <h4 class="big_price_fnt w-auto">{orderData.noofhours}</h4>
                  </div>
                </div>
              }
              {orderData.servicetype == 2 &&
                <div class="text-center col-sm-6 col-lg-3">
                  <h5 class="mid_head_side">No. of Words</h5>
                  <div class="bg_settings_parts d-flex">
                    <span className="leftimg_gp">
                      <img
                        src={iconnoofwords}
                        alt=""
                        data-pagespeed-url-hash="1714040194"
                        onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                      />
                    </span>
                    <h4 class="big_price_fnt w-auto">{orderData.noofwords}</h4>
                  </div>
                </div>
              }
              <div class={orderData.servicetype == 3 ? "text-center  col-sm-6 col-lg-6" : "text-center  col-sm-6 col-lg-3"}>
                <h5 class="mid_head_side">Estimate Delivery</h5>
                <div class="bg_settings_parts d-flex">
                  <span className="leftimg_gp">
                    <img
                      src={iconhours}
                      alt=""
                      data-pagespeed-url-hash="3978192268"
                      onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                    />
                  </span>
                  <h4 class="biga_price_fnt">{orderData.deliverytime}  {orderData.deliverytimein}</h4>
                </div>
              </div>
              {orderData.servicetype == 3 && orderData.description != "" &&
                <div class="text-center col-sm-6 col-lg-12 mt-3">
                  <h5 class="mid_head_side">Description</h5>
                  <div class="bg_settings_parts d-flex">
                    {/* <span className="leftimg_gp">
                      <i class="fa fa-info-circle fa-3x"></i>
                    </span> */}
                    <h4 class="biga_price_fnt text-truncate text-wrap">
                      <ReactReadMoreReadLess
                        charLimit={220}
                        readMoreText={"Read more ▼"}
                        readLessText={"Read less ▲"}
                        readMoreClassName="read-more-less--more"
                        readLessClassName="read-more-less--less"
                      >
                        {orderData.description}
                      </ReactReadMoreReadLess>
                    </h4>
                  </div>
                </div>
              }
            </div>

              <br/> <br/> <br/>

                {!sellerComment &&
                orderData.status != 8 && orderData.status!=9 ? (
                <div class="text-center"> 
                  <img
                    src={sellerImage ? sellerImage : avatar}
                    onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                    alt="..."
                    width="65"
                    height="65"
                    class="rounded-circle"
                    data-pagespeed-url-hash="3993073969"
                    onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                  />
                  <h4 style={{margin:10}} class="user_name_align selleruser_name_align">{sellerName}</h4>
                  <h6 className="herupdt_cnt">Waiting for the Content Writer to Submit Delivery...</h6>
                </div>
              ) : (orderData.status != 8 && orderData.status!=9 &&
                <div class="text-center"> 
                   <img
                  src={buyerImage ? buyerImage : avatar}
                  onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                  alt="..."
                  width="65"
                  height="65"
                  class="rounded-circle"
                  data-pagespeed-url-hash="3993073969"
                  onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                />
               <h4 style={{margin:10}} class="user_name_align selleruser_name_align">{buyerName}</h4>
                  <h6 className="herupdt_cnt">Your turn to act</h6>
                </div>
              )}
    
            <div class="row m-0">
              {orderData.timerInitialState !== null &&
                orderData.timerInitialState !== undefined &&
                orderData.timerInitialState > 0 && (
                  <div key={orderData.deliverytimefortimer} class="pt-4 d-flex font-weight-bold justify-content-center timer-section mx-auto mt-4 mb-0">
                    <TimerControl
                      deadLineHours={orderData.deliverytimefortimer}
                      startDate={orderData.revisionstartdate}
                      currentDate={orderData.currentdatetime}
                      direction="backward"
                      sellerComment={sellerComment}
                    ></TimerControl>
                  </div>
                )
              }
            </div>
            <div class="pb-4 d-flex justify-content-center timerwords-section mx-auto mt-0 mb-4">
              <span class="timerwords">Days</span>
              <span class="timerwords">Hours</span>
              <span class="timerwords">Mins</span>
              <span class="timerwords">Seconds</span>
            </div>
            
            <div className="col-lg-12">
              <h5 className="herupdt_cnt">Here's your update!</h5>
            </div>

            {(orderData.status != 9 && !props.isTimerEnd && orderData.status != 8) && !props.lastHrTimer &&
              <div className="orange_box mb-3">
                <p> {orderData.buyermessage} </p>
              </div>
            }
            {orderData.status == 8 && props.isTimerEnd && !props.lastHrTimer &&
              <div className="grey_box mb-3">
                <p> {orderData.buyermessage} </p>
              </div>
            }
            {orderData.status == 7 && props.lastHrTimer && sellerComment && !props.isTimerEnd && 
              <div className="red_box mb-3">
                <p>Please act within the next 24 hours or your order will be automatically approved.</p>
              </div>
            }
            {(orderData.status == 7 && props.lastHrTimer && !sellerComment) &&
              <div className="orange_box mb-3">
                <p> {orderData.buyermessage} </p>
              </div>
            }
            {!sellerComment && props.isTimerEnd && orderData.status != 8 && orderData.status != 9 &&
              <div className="red_box mb-3">
                <p>Sorry, {sellerName} failed to deliver on time. You may message {sellerName} to request an update on the status of the delivery, or cancel the order.</p>
                <div class="d-flex justify-content-center">
                  <ContactSeller
                    receiverId={bloggerProfileData.user ? bloggerProfileData.user : ""}
                    receiverImage={bloggerProfileData.profilepic ? bloggerProfileData.profilepic : avatar}
                    receiverName={bloggerProfileData.username ? bloggerProfileData.username : "User"}
                    bloggerProfile={true}
                  />
                  <button
                    style={{ marginLeft: 25 }}
                    onClick={() => {
                      setConfirmDialog(true);
                      // cancelOrder(); 
                    }}
                    className="btn_ebony_clay"
                  > Cancel Order </button>
                </div>
              </div>
            }
            {orderData.status == 9 &&
              <div className="red_box mb-3">
                <p>Your order has officially been cancelled. You will be issued a refund from Stripe within 5-7 days.</p>
              </div>
            }
            {orderData.status == 8 && !orderData.isfeedbackcompleted &&
              <SubmitFeedback
                {...props}
                setModalContent={(e) => props.setModalContent(e)}
                orderRedirect={props.orderRedirect}
                orderList={(e) => props.orderList(e)}
                orderId={orderData.id}
                reloadMainScreen={() => {
                  props.reloadMainScreen();
                }}
              />
            }
            {orderData.status == 8 && sellerFeedback != null ? (
              <div class="mt-3">
                <FeedbackView data={sellerFeedback} />
              </div>
            ) : null}
            <div className="revision_sections card">
              {revisionReqAttachArray.length > 0 ? (
                revisionReqAttachArray.map((revision) => (
                  <div class="row m-0 repeat_revisions mb-4">
                    <div className="revision_set">
                      <h4 class="w-100 revision_heading">
                        {revision.revisionNumber == 0 ? "Order Requirements" : "Revision #" + revision.revisionNumber}
                      </h4>
                    </div>
                    <div class="d-block w-100 comment_sec_secs">
                      <div class="d-flex flex-wrap align-items-center mb-2">
                        <img
                          src={buyerImage ? buyerImage : avatar}
                          onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                          alt="..."
                          width="65"
                          height="65"
                          class="rounded-circle"
                          data-pagespeed-url-hash="3993073969"
                          onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                        />
                        <h4 style={{margin:10}} class="user_name_set selleruser_name_set">{buyerName}</h4>
                        <div class="date_time_mnt_set ml-3">
                          <small>
                            {revision.buyerObj.date !== null
                              ? revision.buyerObj.date
                              : ""}
                          </small>
                        </div>
                      </div>
                      <div class="comments_sectionset">
                        {revision.buyerObj.comment.split("\n").map((i) => {
                          return <p>{i}</p>;
                        })}
                      </div>
                      {revision.buyerObj.attachments !== null &&
                        revision.buyerObj.attachments !== undefined &&
                        revision.buyerObj.attachments.length > 0 ? (
                        <div className="attachemnts_Set">
                          <h5 class="mid_head_side pl-3">Attachments</h5>
                          <div class="file_name_attach">
                            {revision.buyerObj.attachments !== null &&
                              revision.buyerObj.attachments !== undefined &&
                              revision.buyerObj.attachments.length > 0 ? (
                              revision.buyerObj.attachments.map((value) => (
                                <div class="uploaded_imgs">
                                  <a
                                    href={value.link}
                                    target="_blank"
                                    class="upolade_filename"
                                  >
                                    {value.attachmentName !== "" ? value.attachmentName : value.name}{" "}
                                  </a>
                                  <a
                                    href={value.link}
                                    target="_blank"
                                    download={value.link}
                                    class="download_link_img"
                                  >
                                    <img src={icnDownload} class="ml-2 img-fluid" />
                                  </a>
                                </div>
                              ))
                            ) : (
                              <div class="not_found">
                                No attachments found.
                              </div>
                            )}
                          </div>
                          <div class="date_time_mnt_set">
                            <small>
                              {revision.buyerObj.date !== null
                                ? revision.buyerObj.date
                                : ""}
                            </small>
                          </div>
                        </div>
                      ) : null}
                      {revision.sellerObj.comment.length > 0 ? (
                        <div class="d-block w-100">
                          <div class="d-flex flex-wrap align-items-center mb-2">
                            <img
                              src={sellerImage ? sellerImage : avatar}
                              onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                              alt="..."
                              width="65"
                              height="65"
                              class="rounded-circle"
                              data-pagespeed-url-hash="3993073969"
                              onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                            />
                            <h4 style={{margin:10}} class="user_name_set selleruser_name_set">{sellerName}</h4>
                            <div class="date_time_mnt_set ml-3">
                              <small>
                                {revision.sellerObj.date !== null
                                  ? revision.sellerObj.date
                                  : ""}
                              </small>
                            </div>
                          </div>
                          <div class="comments_sectionset">
                            {/* <p>{revision.sellerObj.comment}</p> */}
                            {revision.sellerObj.comment.split("\n").map((i) => {
                              return <p>{i}</p>;
                            })}
                          </div>
                          {revision.sellerObj.attachments !== null &&
                            revision.sellerObj.attachments !== undefined &&
                            revision.sellerObj.attachments.length > 0 ? (
                            <div className="attachemnts_Set">
                              <h5 class="mid_head_side pl-3">Attachments</h5>
                              <div class="file_name_attach">
                                {revision.sellerObj.attachments !== null &&
                                  revision.sellerObj.attachments !== undefined &&
                                  revision.sellerObj.attachments.length > 0 ? (
                                  revision.sellerObj.attachments.map((value) => (
                                    <div class="uploaded_imgs float-left ml-2">
                                      <a
                                        href={value.link}
                                        target="_blank"
                                        class="upolade_filename"
                                      >
                                        {value.attachmentName !== "" ? value.attachmentName : value.name}{" "}
                                      </a>
                                      <a
                                        href={value.link}
                                        target="_blank"
                                        download={value.link}
                                        class="download_link_img"
                                      >
                                        <img src={icnDownload} class="ml-2" />
                                      </a>
                                    </div>
                                  ))
                                ) : (
                                  <div class="not_found">
                                    No attachments found.
                                  </div>
                                )}
                              </div>
                              <div class="date_time_mnt_set">
                                <small>
                                  {revision.sellerObj.date !== null
                                    ? revision.sellerObj.date
                                    : ""}
                                </small>
                              </div>
                            </div>
                          ) : null}
                        </div>
                      ) : null}
                    </div>
                  </div>
                ))
              ) : (
                <div className="norec_found">No revisions found.</div>
              )}
            </div>
          </div>

          {orderData.revision < orderData.max_revision   &&
            sellerComment &&
            orderData.status != 8 && (!props.isTimerEnd) ? (
            <div class="text-center w-100 pb-3">
              <div className="text-center">
                <button
                  style={{ marginRight: 20 }}
                  // href="#"
                  className="btn_light_green"
                  onClick={() => {
                    askForRevision();
                  }}
                >
                  I'm Not Ready Yet
                </button>
                <button
                  style={{ marginRight: 20 }}
                  onClick={() => {
                    setApproveDialog(true)
                    // approveOrder();
                  }}
                  className="btn_light_green"
                >
                  Yes, I Approve Delivery
                </button>
                {!orderData.isbuyerextension &&
                  <button
                    style={{ marginRight: 20 }}
                    onClick={() => {
                      setExtendDialog(true)
                      // orderExtension();
                    }}
                    className="btn_ebony_clay"
                  >
                    I need more time
                  </button>
                }
              </div>
            </div>
          ) : orderData.revision == orderData.max_revision &&
            sellerComment &&
            orderData.status != 8 && (!props.isTimerEnd) ? (
            <div class="text-center w-100 pb-3 d-flex mt-3 pb-4 justify-content-center">
              <div className="text-center" >
                <button
                  onClick={() => {
                    setApproveDialog(true)
                    // approveOrder();
                  }}
                  className="btn_light_green"
                >
                  Yes, I Approve Delivery
                </button>
              </div>
              <ContactSeller
                receiverId={sellerId ? sellerId : ""}
                receiverImage={sellerImage ? sellerImage : avatar}
                receiverName={sellerName ? sellerName : "User"}
                orderDetail={true}
              />
            </div>
          ) : null}
        </div>
      )}

    {confirmDialog && (
      <ConfirmationDialog
        open={confirmDialog}
        dialogTitle={"Cancel Order"}
        dialogContentText={`Are you sure you would like to cancel the order?`}
        cancelButtonText="No, I will contact seller"
        okButtonText={"Yes, please cancel my order"}
        onCancel={() => {
          setConfirmDialog(false);
        }}
        onClose={() => {
          setConfirmDialog(false);
        }}
        onOk={(e) => {
          setConfirmDialog(false);
          cancelOrder();
        }}
      />
    )}

    {approveDialog && (
      <ConfirmationDialog
        open={approveDialog}
        dialogTitle={"Approve Order"}
        dialogContentText={`Are you sure you would like to approve your order? All order approvals will be final.`}
        cancelButtonText="No, not yet"
        okButtonText={"Yes, I am sure"}
        onCancel={() => {
          setApproveDialog(false);
        }}
        onClose={() => {
          setApproveDialog(false);
        }}
        onOk={(e) => {
          setApproveDialog(false);
          approveOrder();
        }}
      />
    )}

    {extendDialog && (
      <ConfirmationDialog
        open={extendDialog}
        dialogTitle={"Extend Order"}
        dialogContentText={`Are you sure you need more time to review your order? An additional 24 hours will be added to your clock.`}
        cancelButtonText="No, I don’t need more time"
        okButtonText={"Yes, I am sure"}
        onCancel={() => {
          setExtendDialog(false);
        }}
        onClose={() => {
          setExtendDialog(false);
        }}
        onOk={(e) => {
          setExtendDialog(false);
          orderExtension();
        }}
      />
    )}
  </div>
  );
};


const mapStateToProps = (state) => {
  return {
    isTimerEnd: state.mainReducer.isTimerEnd,
    lastHrTimer: state.mainReducer.lastHrTimer
  }
}

export default connect(mapStateToProps, {
  getBloggerProfile,
})(UpdateDelivery);
