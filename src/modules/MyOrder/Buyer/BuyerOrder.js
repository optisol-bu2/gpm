import React from "react";
import MaterialTable from "material-table";
import moment from "moment";
import Button from "../../../components/core/Button";
import avatar from '../../../assets/images/avatar.png';
import { Popup } from 'semantic-ui-react';

const BuyerOrder = (props) => {
  const options = {
    headerStyle: {
      fontWeight: "bold",
    },
    searchFieldStyle: {
      underline: {
        "&:after": {
          borderBottomColor: "#ED3237",
        },
      },
    },
    pageSize: 25,
    actionsColumnIndex: -1,
    emptyRowsWhenPaging: false,
    draggable: false,
  };

  const columns = [
    {
      title: "ID",
      field: "invoiceid",
      width: "10%",
    },
    // {
    //   title: "Website URL",
    //   field: "websitelink",
    //   width: "20%",
    // },
    {
      title: "Date Order Placed",
      field: "startdate",
      width: "15%",
      render: ({ tableData: { id } }) => {
        return (
          orderTimeFormat(props.data[id].created_at)
        );
      },
    },
    {
      title: "Publisher",
      field: "publisher",
      width: "10%",
      render: ({ tableData: { id } }) => {
        return (
          <div>
            <div style={{ display: "inline-block" }} className="row">
              <img
                src={props.data[id].seller.image}
                onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                className="rounded-circle img-fluid"
                style={{ marginRight: "10px", width: 40, height: 40 }}
              />
              {props.data[id] && props.data[id].seller && (props.data[id].seller.bio != "" || props.data[id].seller.review !== "0.00") &&
                <Popup 
                  key={props.data[id]}
                  trigger={<label> {props.data[id].seller.name}</label>}>
                  <Popup.Header>
                    {props.data[id].seller.name}
                  </Popup.Header> 
                  <Popup.Content>
                    <div className="rating_star d-flex mt-1">
                      {props.data[id].seller.review !== 0 && Array.from(Array(Math.round(props.data[id].seller.review)), () => {
                        return (
                          <span className="fa fa-star star"></span>
                        )
                      })}                              
                    </div> 
                    {props.data[id].seller.bio}
                  </Popup.Content>
                </Popup>
              } 
              {props.data[id] && props.data[id].seller && (props.data[id].seller.bio == "" && props.data[id].seller.review == "0.00") &&
                <label> {props.data[id].seller.name}</label>
              }
            </div>
          </div>
        );
      },
    },
    {
      title: "Order Type",
      field: "servicetypename",
      render: ({ tableData: { id } }) => {
        return (
          props.data[id].servicetype == 1 ? "Service": props.data[id].servicetype == 2 ? "Content Writer" : "Custom"
        );
      },
      width: "10%",
    },
    {
      title: "Cost",
      field: "total",
      render: ({ tableData: { id } }) => {
        return (
          "$"+props.data[id].total
        );
      },
      width: "10%",
    },
    {
      title: "Actions",
      width: "15%",
      render: (row) => {
        return (
          <div className="view_btns" style={{ width: "100px" }}>
            <div style={{ display: "inline-block" }}>
              <Button
                autoFocus
                onClick={() => props.onEdit(row)}
                backgroundColor={"#37B877"}
                hoverBgColor={"#37B877"}
                // backgroundColor={"#999382"}
                // hoverBgColor={"#999382"}
                minWidth={"111px"}
                label={row.status == 6 ? "Start Order" : "View"}
                width={"10px"}
              />
            </div>
          </div>
        );
      },
    },
  ];

  const currentOrderColumns = [
    {
      title: "ID",
      field: "invoiceid",
      width: "10%",
    },
    // {
    //   title: "Website URL",
    //   field: "websitelink",
    //   width: "20%",
    // },
    {
      title: "Date Order Placed",
      field: "startdate",
      width: "15%",
      render: ({ tableData: { id } }) => {
        return (
          orderTimeFormat(props.data[id].created_at)
        );
      },
    },
    {
      title: "Publisher",
      field: "publisher",
      width: "10%",
      render: ({ tableData: { id } }) => {
        return (
          <div>
            <div style={{ display: "inline-block" }} className="row">
              <img
                src={props.data[id].seller.image}
                onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                className="rounded-circle img-fluid"
                style={{ marginRight: "10px", width: 40, height: 40 }}
              />
              {props.data[id] && props.data[id].seller && (props.data[id].seller.bio != "" || props.data[id].seller.review !== "0.00") &&
                <Popup 
                  key={props.data[id]}
                  trigger={<label> {props.data[id].seller.name}</label>}>
                  <Popup.Header>
                    {props.data[id].seller.name}
                  </Popup.Header> 
                  <Popup.Content>
                    <div className="rating_star d-flex mt-1">
                      {props.data[id].seller.review !== 0 && Array.from(Array(Math.round(props.data[id].seller.review)), () => {
                        return (
                          <span className="fa fa-star star"></span>
                        )
                      })}                              
                    </div> 
                    {props.data[id].seller.bio}
                  </Popup.Content>
                </Popup>
              } 
              {props.data[id] && props.data[id].seller && (props.data[id].seller.bio == "" && props.data[id].seller.review == "0.00") &&
                <label> {props.data[id].seller.name}</label>
              }
            </div>
          </div>
        );
      },
    },
    {
      title: "Order Type",
      field: "servicetypename",
      render: ({ tableData: { id } }) => {
        return (
          props.data[id].servicetype == 1 ? "Service": props.data[id].servicetype == 2 ? "Content Writer" : "Custom"
        );
      },
      width: "10%",
    },
    {
      title: "Cost",
      field: "total",
      render: ({ tableData: { id } }) => {
        return (
          "$"+props.data[id].total
        );
      },
      width: "10%",
    },
    {
      title: "Last Updated At",
      field: "updated_at",
      width: "15%",
      render: ({ tableData: { id } }) => {
        return (
          orderTimeFormat(props.data[id].updated_at)
        );
      },
    },
    {
      title: "Actions",
      width: "15%",
      render: (row) => {
        return (
          <div className="view_btns" style={{ width: "100px" }}>
            <div style={{ display: "inline-block" }}>
              <Button
                autoFocus
                onClick={() => props.onEdit(row)}
                backgroundColor={"#37B877"}
                hoverBgColor={"#37B877"}
                // backgroundColor={"#999382"}
                // hoverBgColor={"#999382"}
                minWidth={"111px"}
                label={row.status == 6 ? "Start Order" : "View"}
                width={"10px"}
              />
            </div>
          </div>
        );
      },
    },
  ];

  const completedOrderColumns = [
    {
      title: "ID",
      field: "invoiceid",
      width: "10%",
    },
    // {
    //   title: "Website URL",
    //   field: "websitelink",
    //   width: "20%",
    // },
    {
      title: "Date Order Placed",
      field: "startdate",
      width: "15%",
      render: ({ tableData: { id } }) => {
        return (
          orderTimeFormat(props.data[id].created_at)
        );
      },
    },
    {
      title: "Publisher",
      field: "publisher",
      width: "10%",
      render: ({ tableData: { id } }) => {
        return (
          <div>
            <div style={{ display: "inline-block" }} className="row">
              <img
                src={props.data[id].seller.image}
                onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                className="rounded-circle img-fluid"
                style={{ marginRight: "10px", width: 40, height: 40 }}
              />
              {props.data[id] && props.data[id].seller && (props.data[id].seller.bio != "" || props.data[id].seller.review !== "0.00") &&
                <Popup 
                  key={props.data[id]}
                  trigger={<label> {props.data[id].seller.name}</label>}>
                  <Popup.Header>
                    {props.data[id].seller.name}
                  </Popup.Header> 
                  <Popup.Content>
                    <div className="rating_star d-flex mt-1">
                      {props.data[id].seller.review !== 0 && Array.from(Array(Math.round(props.data[id].seller.review)), () => {
                        return (
                          <span className="fa fa-star star"></span>
                        )
                      })}                              
                    </div> 
                    {props.data[id].seller.bio}
                  </Popup.Content>
                </Popup>
              } 
              {props.data[id] && props.data[id].seller && (props.data[id].seller.bio == "" && props.data[id].seller.review == "0.00") &&
                <label> {props.data[id].seller.name}</label>
              }
            </div>
          </div>
        );
      },
    },
    {
      title: "Order Type",
      field: "servicetypename",
      render: ({ tableData: { id } }) => {
        return (
          props.data[id].servicetype == 1 ? "Service": props.data[id].servicetype == 2 ? "Content Writer" : "Custom"
        );
      },
      width: "10%",
    },
    {
      title: "Cost",
      field: "total",
      render: ({ tableData: { id } }) => {
        return (
          "$"+props.data[id].total
        );
      },
      width: "10%",
    },
    {
      title: "Download Documents",
      field: "attachmentfilename",
      render: ({ tableData: { id } }) => {
        if(props.data[id].attachmentfilename == "") {
          return ("-")
        } else {
          return (
            <div> 
              <a href={props.data[id].file_url} target="_blank" download={props.data[id].file_url} class="upolade_filename">
                <span className="fa fa-download"></span> 
              </a> &nbsp; 
              {props.data[id].attachmentfilename}
            </div>
          )
        }
      },
    },
    {
      title: "Actions",
      width: "15%",
      render: (row) => {
        return (
          <div className="view_btns" style={{ width: "100px" }}>
            <div style={{ display: "inline-block" }}>
              <Button
                autoFocus
                onClick={() => props.onEdit(row)}
                backgroundColor={"#37B877"}
                hoverBgColor={"#37B877"}
                // backgroundColor={"#999382"}
                // hoverBgColor={"#999382"}
                minWidth={"111px"}
                label={row.status == 6 ? "Start Order" : "View"}
                width={"10px"}
              />
            </div>
          </div>
        );
      },
    },
  ];

  /**
   * changes the given date to local date format
   * @param {*} dateTime
   * @returns
   */
  const orderTimeFormat = (dateTime) => {
    var utc = moment.utc(dateTime).toDate();
    var orderTime = moment(utc).local().format('MMM-DD-YYYY hh:mmA');
    return orderTime;
  }

  return (
    <div class="card-body order-tabs-navigation">
      <ul class="nav nav-tabs justify-content-between flex-column flex-md-row" data-tour="myOrders">
        <li class="nav-item flex-fill text-center">
          <a
            onClick={() => props.onTabChange(0)}
            class={props.tabType === 0 ? "nav-link active" : "nav-link"}
            data-toggle="tab"
            href="#current_order"
          >
            Current Orders
          </a>
        </li>
        <li class="nav-item flex-fill text-center">
          <a
            onClick={() => props.onTabChange(1)}
            class={props.tabType === 1 ? "nav-link active" : "nav-link"}
            data-toggle="tab"
            href="#completed_order"
          >
            Completed orders
          </a>
        </li>
        <li class="nav-item flex-fill text-center">
          <a
            onClick={() => props.onTabChange(2)}
            class={props.tabType === 2 ? "nav-link active" : "nav-link"}
            data-toggle="tab"
            href="#cancelled_order"
          >
            Cancelled Orders
          </a>
        </li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active grid_des orange_table myorders_table" id="current_order">
          <MaterialTable
            title={""}
            columns={currentOrderColumns}
            data={props.data}
            options={{
              ...options,
              selection: false,
              paging: false,
              search: false,               
            }}
          />
        </div>
        <div class="tab-pane fade  grid_des orange_table myorders_table" id="completed_order">
          <MaterialTable
            title={""}
            columns={completedOrderColumns}
            data={props.data}
            options={{
              ...options,
              selection: false,
              paging: false,
              search: false,
            }}
          />
        </div>
        <div class="tab-pane fade  grid_des orange_table myorders_table" id="cancelled_order">
          <MaterialTable
            title={""}
            columns={columns}
            data={props.data}
            options={{
              ...options,
              selection: false,
              paging: false,
              search: false,
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default BuyerOrder;
