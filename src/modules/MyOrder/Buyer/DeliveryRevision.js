import FileUpload from "../../../components/core/FileUpload";
import TimerControl from "../../../components/core/Timer";
import React, { useState, useEffect } from "react";
import { Button, Form } from "react-bootstrap";
import Loading from "../../../components/core/Loading";
import Modal from "@material-ui/core/Modal";
import PriceDetail from "../PriceDetail";
import { ListGroup } from "react-bootstrap";
import { IconButton, Tooltip } from "@material-ui/core";
import { Close } from "@material-ui/icons";
import { ToastAlert,PlaySound } from "../../../utils/sweetalert2";

const DeliveryRevision = (props) => {
  const [base64, setBase64] = useState(null);
  const [baseItemImage, setBaseItemImage] = useState(null);
  const [file, setFile] = useState(null);
  const [isImageDeleted, setIsImageDeleted] = useState(false);
  const [isImageCanceled, setIsImageCanceled] = useState(false);
  const [isFileError, setIsFileError] = useState(false);
  const [descriptionText, setDescriptionText] = useState("");
  const [charCount, setCharCount] = useState(0);
  const [isLoading, setLoading] = useState(null);
  const [fileList, setFileList] = useState([]);
  const [fileListHtml, setFileListHtml] = useState();
  const [orderDetail, setOrderDetail] = useState({});
  const [validated, setValidated] = useState(false);
  const [revision, setRevision] = useState("");
  const [disable, setDisabled ] = useState(false);

  useEffect(() => {
    if (props.orderId) {
      getOrderDetail();
    }
  }, [props.orderId]);

  useEffect(() => {
    if (props.orderRedirect) {
      getOrderDetail();
      props.orderList(false);
    }
  }, [props.orderRedirect]);

  useEffect(() => {
    if (file) {
      updateAttachment();
    }
  }, [file]);

  const onCloseModal = (event) => {
    setCharCount(0);
    setDescriptionText("");
    setBase64(null);
    setBaseItemImage(null);
    setFile(null);
    setIsImageCanceled(false);
    setIsFileError(false);
    props.onClose();
  }

  const handlesubmit = (event) => {
    if (descriptionText.length == 0) {
      setValidated(true);
      event.preventDefault();
      event.stopPropagation();
    } else {
      let param = {
        id: props.orderId,
      };
      setLoading(true);
      setDisabled(true);
      props.orderNotApproved(
        param,
        () => {
          const params = {
            comment: descriptionText,
            orderid: props.orderId,
            revision: revision,
          };
          props.sendOrderUpdate(
            params,
            () => {
              setLoading(false);
              setDisabled(false);
              props.setModalContent("Order successfully extended.");
              props.onSuccess();
            },
            (error) => {
              setLoading(false);
              setDisabled(false);
              ToastAlert('error', error.message);
              PlaySound()
              onCloseModal();
            }
          );
        },
        (error) => {
          setLoading(false);
          setDisabled(false);
          ToastAlert('error', error.message);
          PlaySound()
        }
      );
    }
  };

  const getAttachments = (revision) => {
    try {
      setLoading(true);
      props.getAttachments(
        props.orderId,
        revision,
        (response) => {
          setLoading(false);
          if (response.data !== null) {
            setDescriptionText(response.data.comment.value);
            var attachments = response.data.attachments;
            if (attachments.length > 0) {
              var list = fileList;
              for (let i = 0; i < attachments.length; i++) {
                var fileName = attachments[i].value.split("/")[
                  attachments[i].value.split("/").length - 1
                ];
                var id = attachments[i].id;
                var link = attachments[i].value;
                list.push({ name: fileName, id: id, link: link, attachmentName: attachments[i].attachmentfilename });
              }
              setFileList(list);
              setFileListHtml(loadFileList());
            }
          }
        },
        (error) => {
          setLoading(false);
          // ToastAlert('error', error.message);
        }
      );
    } catch (error) {}
  };

  const updateAttachment = () => {
    let formData = new FormData();
    formData.set("orderid", props.orderId);
    formData.append("document", file);
    formData.append("revision", revision);
    setLoading(true);

    props.updateAttachment(
      formData,
      (response) => {
        setLoading(false);

        if (file.name !== "") {
          var list = fileList;
          list.push({ name: file.name, id: response.data.id, attachmentName: file.name });
          setFileList(list);
          setFileListHtml(loadFileList());
        }
      },
      (error) => {
        setLoading(false);
      }
    );
  };

  const deleteAttachment = (attachmentId, index) => {
    setLoading(true);
    props.deleteAttachment(
      attachmentId,
      (response) => {
        setLoading(false);

        fileList.splice(index, 1);
        setFileList(fileList);
        setFileListHtml(loadFileList());
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  const loadFileList = () => {
    let items = [];
    try {
      for (let i = 0; i <= fileList.length; i++) {
        items.push(
          <ListGroup.Item>
            <div>
              <span>{fileList[i].attachmentName !== "" ? fileList[i].attachmentName : fileList[i].name}</span>
              <Tooltip title="Cancel">
                <IconButton
                  aria-label="cancel"
                  onClick={() => {
                    deleteAttachment(fileList[i].id, i);
                  }}
                >
                  <Close fontSize="small" />
                </IconButton>
              </Tooltip>
            </div>
          </ListGroup.Item>
        );
      }
    } catch (error) {}
    return items;
  };

  const getOrderDetail = () => {
    setLoading(true);
    props.getOrderDetail(
      props.orderId,
      (response) => {
        setLoading(false);
        if (response.data !== null) {
          setRevision(response.data.revision+1);
          setOrderDetail(response.data);
          getAttachments(response.data.revision+1);
        }
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  return (
    <div>
      <Loading isVisible={isLoading} />
      <Modal
        open={props.isVisible}
        disableBackdropClick
        style={{ overflow: "scroll" }}
        onClose={onCloseModal}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        <div class="modal_popup revision_popup" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="popup_title" id="exampleModalLabel" >Revision</h5>
              <button
                type="button"
                class="close"
                data-dismiss="modal"
                aria-label="Close"
                onClick={onCloseModal}
              >
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="modal-body">
                {orderDetail !== null && (
                  <div className="col-lg-8 col-xl-9 mt-4 float-left">
                    <div className="card">
                      <div className="card-body border-r-1">
                        <h5 className="text-muted font-weight-bold float-right">
                           Max Revisions:  {orderDetail ? orderDetail.max_revision : ""}
                        </h5>
                        <h5 className="text-dark font-weight-bold">
                          Deliver #{orderDetail ? orderDetail.revision+1 : ""} Revision
                        </h5>

                        <div class="py-4 d-flex font-weight-bold justify-content-center timer-section">
                          {orderDetail.timerInitialState !== null &&
                            orderDetail.timerInitialState !== undefined &&
                            orderDetail.timerInitialState > 0 && (
                              <div class="py-4 d-flex font-weight-bold justify-content-center timer-section">
                                <TimerControl
                                  deadLineHours={orderDetail.deliverytimefortimer}
                                  startDate={orderDetail.revisionstartdate}
                                  currentDate={orderDetail.currentdatetime}
                                  direction="backward"
                                ></TimerControl>
                                <span class="timerwords">Hours</span>
                              </div>
                            )}
                        </div>
                        <div className="form-group">
                          <h6 className="text-dark font-weight-bold">Comment</h6>
                          <Form
                            class="needs-validation7"
                            noValidate
                            validated={validated}
                          >
                            <Form.Group controlId="exampleForm.ControlTextarea1">
                              <Form.Control
                                as="textarea"
                                placeholder="Describe your update in details..."
                                value={descriptionText}
                                rows={8}
                                required
                                onChange={(e) => {
                                  if (e.target.value.length <= 2000) {
                                    setCharCount(e.target.value.length);
                                    setDescriptionText(e.target.value);
                                  }
                                }}
                              />
                              <Form.Control.Feedback type="invalid">
                                Please provide update details.
                              </Form.Control.Feedback>
                            </Form.Group>
                            <div class="d-flex p-2 justify-content-between border-bottom border-left border-right">
                              <p class="text-muted mb-0">{charCount}/2000</p>
                              <label for="upload_doc" className="mb-0">
                                <i className="fa fa-paperclip fa-lg text-muted link-cursor" title="Attach files"></i>
                                <FileUpload
                                  required={true}
                                  id="upload_doc"
                                  multiple={true}
                                  setBase64={setBase64}
                                  file={file}
                                  accept={"*/*"}
                                  isFileCanceled={isImageCanceled}
                                  isFileDeleted={isImageDeleted}
                                  setIsImageCanceled={setIsImageCanceled}
                                  setFile={setFile}
                                  setIsFileError={setIsFileError}
                                  size={20000}
                                  error={isFileError}
                                  errorMessage={"Please select file."}
                                />
                              </label>
                            </div>
                            <div>
                              {fileList.length > 0 && (
                                <ListGroup>{fileListHtml}</ListGroup>
                              )}
                            </div>
                            <div
                              style={{
                                display: "flex",
                                marginTop: 30,
                                width: "100%",
                                justifyContent: "center",
                                textAlign: "center",
                                alignItems: "center",
                              }}
                            >
                              <Button
                                style={{
                                  width: "20%",
                                  alignSelf: "center",
                                }}
                                className="btn_light_green effect effect-2"
                                disabled={disable}
                                onClick={handlesubmit}
                              >
                                Submit form
                              </Button>
                            </div>
                          </Form>
                        </div>
                        <div className="text-center">
                        </div>
                      </div>
                    </div>
                  </div>
                )}
                {orderDetail !== null && (
                  <div className="col-lg-4 col-xl-3 mt-4 float-right">
                    {orderDetail ? (
                      <PriceDetail orderDetail={orderDetail}></PriceDetail>
                    ) : null}
                  </div>
                )}
              </div>
          </div>
        </div>
      </Modal>
    </div>
  );
};

export default DeliveryRevision;
