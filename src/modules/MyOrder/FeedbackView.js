import React from "react";
import Rating from "@material-ui/lab/Rating";
import Box from "@material-ui/core/Box";
import moment from "moment";
import avatar from '../../assets/images/avatar.png';

const FeedbackView = (props) => {

  /**
   * changes the given date to local date format
   * @param {*} dateTime
   * @returns
   */
   const orderTimeFormat = (dateTime) => {
    var utc = moment.utc(dateTime).toDate();
    var orderTime = moment(utc).local().format('MMM-DD-YYYY hh:mm A');
    return orderTime;
  }

  return (
    <div className="card pt-0 pb-0">
    {props.data !== null && 
      <div class="card-body feedback_img">
        <div class="row">         
          <div class="col-lg-7">
            <div class="user d-flex flex-wrap flex-md-nowrap align-items-center">
              <div class="user_img">
                <img
                  src={props.data.userdetail ? props.data.userdetail.image : ""}
                  onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                  alt="..."
                  style={{ marginRight: 10 }}
                  class="rounded-circle img-fluid"
                />
              </div>
              <div class="user_content">
                <span class="user_name_set selleruser_name_set">{props.data.userdetail ? props.data.userdetail.name : ""}</span>
                <p class="date_fmt">{orderTimeFormat(props.data.created_at)}</p>
              </div>
            </div>
            <div class="bg_lightset">
              <p class="">
                {props.data.review ? props.data.review : ""}
              </p>
            </div>
          </div>
          <div class="col-lg-5">
            <div class="d-flex justify-content-between flex-column flex-md-row flex-wrap mt-2 align-content-start">
              <h6 class="feed_wordings">Communication</h6>
              <div class="rating_stars_colors">
                <Box component="fieldset" mb={3} borderColor="transparent">
                  <Rating
                    name="read-only"
                    value={props.data.communication ? props.data.communication : ""}
                    readOnly
                  />
                </Box>
              </div>
            </div>

            <div class="d-flex justify-content-between flex-column flex-md-row flex-wrap mt-2 align-content-start">
              <h6 class="feed_wordings">Service as Described</h6>
              <div class="rating_stars_colors">
                <Box component="fieldset" mb={3} borderColor="transparent">
                  <Rating
                    name="read-only"
                    value={props.data.service ? props.data.service : ""}
                    readOnly
                  />
                </Box>
              </div>
            </div>

            <div class="d-flex justify-content-between flex-column flex-md-row flex-wrap mt-2 align-content-start">
              <h6 class="feed_wordings">Buy Again or Recommend</h6>
              <div class="rating_stars_colors">
                <Box component="fieldset" mb={3} borderColor="transparent">
                  <Rating
                    name="read-only"
                    value={props.data.recommend ? props.data.recommend : ""}
                    readOnly
                  />
                </Box>
              </div>
            </div>
          </div>
        </div>
      </div>
    }
    </div>
  );
};

export default FeedbackView;
