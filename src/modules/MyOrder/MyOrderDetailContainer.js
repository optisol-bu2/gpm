import { connect } from "react-redux";
import MyOrderDetailParent from "./MyOrderDetailParent";
import {
  getOrderDetail,
  uploadAttachment,
  removeAttachment,
  startOrder,
  orderApproved,
  orderCancel,
  orderNotApproved,
  getAttachments,
  deleteAttachment,
  sendOrderUpdate,
  updateAttachment,
  submitFeedback,
  getFeedback,
  orderExtention,
  setEndTimer,
  last24Hours
} from "../../Actions/actionContainer";

const mapStateToProps = (state) => {
  return {
    serviceCategories: state.mainReducer.serviceCategories,
    isTimerEnd: state.mainReducer.isTimerEnd

  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getOrderDetail: (id, onSuccess, onFailure) =>
      dispatch(getOrderDetail(id, onSuccess, onFailure)),
    uploadAttachment: (params, onSuccess, onFailure) =>
      dispatch(uploadAttachment(params, onSuccess, onFailure)),
    removeAttachment: (id, onSuccess, onFailure) =>
      dispatch(removeAttachment(id, onSuccess, onFailure)),
    startOrder: (params, onSuccess, onFailure) =>
      dispatch(startOrder(params, onSuccess, onFailure)),
    orderApproved: (params, onSuccess, onFailure) =>
      dispatch(orderApproved(params, onSuccess, onFailure)),
    orderCancel: (params, onSuccess, onFailure) =>
      dispatch(orderCancel(params, onSuccess, onFailure)),
    orderNotApproved: (params, onSuccess, onFailure) =>
      dispatch(orderNotApproved(params, onSuccess, onFailure)),
    getAttachments: (order_id, revision, onSuccess, onFailure) =>
      dispatch(getAttachments(order_id, revision, onSuccess, onFailure)),
    deleteAttachment: (id, onSuccess, onFailure) =>
      dispatch(deleteAttachment(id, onSuccess, onFailure)),
    sendOrderUpdate: (params, onSuccess, onFailure) =>
      dispatch(sendOrderUpdate(params, onSuccess, onFailure)),
    updateAttachment: (params, onSuccess, onFailure) =>
      dispatch(updateAttachment(params, onSuccess, onFailure)),
    submitFeedback: (params, onSuccess, onFailure) =>
      dispatch(submitFeedback(params, onSuccess, onFailure)),
    getFeedback: (id, onSuccess, onFailure) =>
      dispatch(getFeedback(id, onSuccess, onFailure)),
    orderExtention: (params, onSuccess, onFailure) =>
      dispatch(orderExtention(params, onSuccess, onFailure)),
    setEndTimer: (params) =>
      dispatch(setEndTimer(params)),
    last24Hours: (params) => 
      dispatch(last24Hours(params)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyOrderDetailParent);
