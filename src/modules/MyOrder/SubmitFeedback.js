import React, { useState, useEffect } from "react";
import Rating from "@material-ui/lab/Rating";
import PriceDetail from "./PriceDetail";
import Loading from "../../components/core/Loading";
import { Button, Form } from "react-bootstrap";
import { ToastAlert,PlaySound } from "../../utils/sweetalert2";

const SubmitFeedback = (props) => {
  const [valuePoint1, setValuePoint1] = useState();
  const [valuePoint2, setValuePoint2] = useState();
  const [valuePoint3, setValuePoint3] = useState();
  const [charCount, setCharCount] = useState(0);
  const [isLoading, setLoading] = useState(null);
  const [orderDetail, setOrderDetail] = useState();
  const [validated, setValidated] = useState(false);
  const [descriptionText, setDescriptionText] = useState("");
  const [disable, setDisabled ] = useState(false);

  useEffect(() => {
    if (props.orderId)
    getOrderDetail();
  }, [props.orderId]);

  useEffect(() => {
    if (props.orderRedirect) {
      getOrderDetail();
      props.orderList(false);
    }
  }, [props.orderRedirect]);

  const getOrderDetail = () => {
    setLoading(true);
    props.getOrderDetail(
      props.orderId,
      (response) => {
        setLoading(false);
        if (response.data !== null) {
          setOrderDetail(response.data);
        }
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  const handlesubmit = (event) => {
    try {
      if (descriptionText.length == 0 ||
        valuePoint1 == null || valuePoint2 == null || valuePoint3 == null || 
        valuePoint1 == undefined || valuePoint2 == undefined || valuePoint3 == undefined) {
          if (valuePoint1 == null || valuePoint2 == null || valuePoint3 == null || 
            valuePoint1 == undefined || valuePoint2 == undefined || valuePoint3 == undefined) {
              ToastAlert('error', "Please give feedback stars");
              PlaySound()
          }
        setValidated(true);
        event.preventDefault();
        event.stopPropagation();
      } else {
        setLoading(true);
        setDisabled(true);
        let params = {
          communication: valuePoint1,
          service: valuePoint2,
          recommend: valuePoint3,
          review: descriptionText,
          orderid: props.orderId,
        };

        props.submitFeedback(
          params,
          (res) => {
            var feedback = parseFloat((valuePoint1+valuePoint2+valuePoint3)/3).toFixed(1).replace(/[.,]0$/, "")
            var name;
            if (props.userData.activeusertype === 1) {
              name = orderDetail.buyer.name
            } else if (props.userData.activeusertype === 2) {
              name = orderDetail.seller.name
            }
            setLoading(false);
            setDisabled(false);
            props.setModalContent("You have left "+name+" an average of "+feedback+" stars. The order is complete.");
            props.reloadMainScreen();
          },
          (error) => {
            ToastAlert('error', error.message);
            PlaySound()
            setLoading(false);
            setDisabled(false);
          }
        );
      }
    } catch (error) {
      ToastAlert('error', JSON.stringify(error));
      PlaySound()
      setLoading(false);
    }
  };

  return (
    <div>
      <Loading isVisible={isLoading} />
      <div className="row">
        <div class="col-lg-8 col-xl-9 mt-4">
          <div class="orderpage_details">
            <h5 class="mid_head_side ml-4">Feedback</h5>
            <p class="mid_head_side fontsixesmall ml-4">
              Share your experience with the community to help them make better decisions
            </p>

            <div class="d-flex justify-content-between flex-column flex-md-row flex-wrap mt-2 align-content-start ml-4">
              <h6 class="feed_wordings">Communication with Seller</h6>
              <div class="rating_stars_colors">
                <Rating
                  name="simple-controlled"
                  value={valuePoint1}
                  onChange={(event, newValue) => {
                    setValuePoint1(newValue);
                  }}
                />
                </div>
            </div>

            <div class="d-flex justify-content-between flex-column flex-md-row flex-wrap mt-2 align-content-start ml-4">
              <h6 class="feed_wordings">Service as Described</h6>
              <div class="rating_stars_colors">
                <Rating
                  name="simple-controlled1"
                  value={valuePoint2}
                  onChange={(event, newValue) => {
                    setValuePoint2(newValue);
                  }}
                />
              </div>
            </div>

            <div class="d-flex justify-content-between flex-column flex-md-row flex-wrap mt-2 align-content-start ml-4">
              <h6 class="feed_wordings">Buy Again or Recommend</h6>
              <div class="rating_stars_colors">
                <Rating
                  name="simple-controlled2"
                  value={valuePoint3}
                  onChange={(event, newValue) => {
                    setValuePoint3(newValue);
                  }}
                />
              </div>
            </div>

            <div class="form-group mt-5 ml-4">
              <Form
                class="needs-validation"
                noValidate
                validated={validated}
              >
                <Form.Group controlId="exampleForm.ControlTextarea1">
                  <Form.Control
                    as="textarea"
                    placeholder="Describe your experience..."
                    value={descriptionText}
                    rows={4}
                    required
                    onChange={(e) => {
                      if (e.target.value.length <= 300) {
                        setCharCount(e.target.value.length);
                        setDescriptionText(e.target.value);
                      }
                    }}
                    maxLength={300}
                  />
                  <Form.Control.Feedback type="invalid">
                    Please provide feedback.
                  </Form.Control.Feedback>
                </Form.Group>
                <div class="d-flex p-2 justify-content-between border-bottom border-left border-right">
                  <p class="text-muted mb-0">{charCount}/300</p>
                </div>
                <div
                  style={{
                    display: "flex",
                    marginTop: 30,
                    width: "100%",
                    justifyContent: "center",
                    textAlign: "center",
                    alignItems: "center",
                  }}
                >
                  <Button
                    style={{
                      alignSelf: "center",
                    }}
                    className="btn_light_green effect effect-2"
                    disabled={disable}
                    onClick={handlesubmit}
                  >
                    Submit Feedback
                  </Button>
                </div>
              </Form>
            </div>
          </div>
        </div>
      
        <div className="col-lg-4 col-xl-3 mt-4">
          {orderDetail ? (
            <PriceDetail orderDetail={orderDetail}></PriceDetail>
          ) : null}
        </div>
      </div>
    </div>
  );
};

export default SubmitFeedback;
