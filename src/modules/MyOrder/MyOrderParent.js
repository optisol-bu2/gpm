import React, { useState, useEffect, useRef } from "react";
import Loading from "../../components/core/Loading";
import SellerOrder from "./Seller/SellerOrder";
import BuyerOrder from "./Buyer/BuyerOrder";
import Pagination from "@material-ui/lab/Pagination";
import my_order_icon from "../../assets/images/menu-icons/my_orders_white.svg";
import { DatePicker } from 'antd';
import "antd/dist/antd.css";
import { ToastAlert } from "../../utils/sweetalert2";
import { Popover, OverlayTrigger } from 'react-bootstrap';
import moment from 'moment';
import * as dayjs from 'dayjs';

const { RangePicker } = DatePicker;

const MyOrderParent = (props) => {
  const [data, setData] = useState([]);
  const [isLoading, setLoading] = useState(null);
  const [totalPageNumber, setTotalPageNumber] = useState(1);
  const [pageIndex, setPageIndex] = useState(1);
  const [text, setText] = useState("");
  const [tabType, setTabType] = useState(0);
  const [minRange, setMinRange] = useState("");
  const [maxRange, setMaxRange] = useState(""); 
  const [isFilter, setFilter] = useState(false);
  const [isSearchFilter, setSearchFilter] = useState(false);
  const [searchText, setSearchText] = useState("");
  const [startDate, setStartDateValue] = useState(dayjs().format('YYYY-MM-DD'));
  const [endDate, setEndDateValue] = useState(dayjs().format('YYYY-MM-DD'));
  const [show, setShow] = useState(false);
  const [showPopover, setPopover] = useState(false);
  const dateFormat = 'YYYY-MM-DD';
  const wrapperRef = useRef(null);

  useEffect(() => {
    document.addEventListener("click", handleClickOutside, false);
    return () => {
      document.removeEventListener("click", handleClickOutside, false);
    };
  }, []);

  const handleClickOutside = event => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      setPopover(false);
      setShow(false);
    }
  };

  useEffect(() => {
    getData(pageIndex);
  }, [tabType]);

  const onGetDataSuccess = (response) => {
    try {
      if (response.data != null) {
        var data = response.data.records;
        setTotalPageNumber(
          Math.ceil(response.data.total/response.data.noofrecords)
        );
        setData(data);
        setLoading(false);
      } else {
        setData([]);
        setLoading(false);
      }
    } catch (error) {}
  };

  const onGetDataFailed = (error) => {
    setLoading(false);
    setTotalPageNumber(1);
    setData([]);
  };

  const getData = (val) => {
    var pageId = val === undefined ? 1 : val;
    setLoading(true);
    setData([]);

    switch (tabType) {
      case 0:
        props.getMyOrderDetails(
          pageId,
          (response) => {
            onGetDataSuccess(response);
          },
          (error) => onGetDataFailed(error)
        );
        break;
      case 1:
        props.getCompletedOrderData(
          pageId,
          (response) => {
            onGetDataSuccess(response);
          },
          (error) => onGetDataFailed(error)
        );
        break;
      case 2:
        props.getCancelOrderData(
          pageId,
          (response) => {
            onGetDataSuccess(response);
          },
          (error) => onGetDataFailed(error)
        );
        break;
    }
  };

  const onTextChange = (e) => {
    if (e.target.value.length >= 1) {
      callFilter(e.target.value);
    } else if (e.target.value.length == 0) {
      getData(1);
    }
  };

  const onFilterSuccess = (response) => {
    try {
      if (response.data != null) {
        setTotalPageNumber(Math.ceil(response.data.total/response.data.noofrecords));
        setData(response.data.records);
        setLoading(false);
      } else {
        setData([]);
        setLoading(false);
      }
    } catch (error) {}
  };

  const onFilterFailed = (error) => {
    setLoading(false);
    setTotalPageNumber(1);
    setData([]);
  };

  const callFilter = (val) => {
    setText(val);
    setLoading(true);
    var res = encodeURIComponent(val);
    switch (tabType) {
      case 0:
        props.filterOrderData(
          getParams(res),
          (response) => {
            onFilterSuccess(response);
          },
          (error) => {
            onFilterFailed(error);
          }
        );
        break;
      case 1:
        props.filterCompletedOrderData(
          getParams(res),
          (response) => {
            onFilterSuccess(response);
          },
          (error) => {
            onFilterFailed(error);
          }
        );
        break;
      case 2:
        props.filterCancelOrderData(
          getParams(res),
          (response) => {
            onFilterSuccess(response);
          },
          (error) => {
            onFilterFailed(error);
          }
        );
        break;
    }
  };

  const getParams = (serchText) => {
    return {
      pageindex: pageIndex,
      search: serchText,
    };
  };

  const handleOnPriceRangeChange = (value, type) => {
    if (type == "min") {
      setMinRange(value);
    } else if (type == "max") {
      setMaxRange(value);
    }
  };

  const handleChange = (value) => {
    if(value != null) {
      var startDate = value[0].format('YYYY-MM-DD');
      var endDate = value[1].format('YYYY-MM-DD');
      setStartDateValue(startDate);
      setEndDateValue(endDate);
    } else {
      setShow(false)
      getData(1)
    }
  };

  const getSearchParams = (searchValue,type) => {
    return {
      pageindex: pageIndex,
      search: searchValue,
      searchfield: type
    };
  };

  const callPriceAndDateFilter = (val) => {
    setLoading(true);
    if(val == 'price' && (minRange == "" && maxRange == "")) {
      getData(1)
    } else if(val == 'price' && (minRange == "" || maxRange == "")) {
      ToastAlert('error', "Please fill both Minimum and Maximum Cost"); 
    } else if (val == 'date' && (startDate == "" && endDate == "") ) {
      getData(1)
    } else if (val == 'date' && (startDate == "" || endDate == "") ) {
      ToastAlert('error', "Please Select both Start and End Date"); 
    } else {
      var searchParams;
      if(val == 'price') {
        setPopover(false);
        var searchValue = minRange + '-' + maxRange;
        searchParams = getSearchParams(searchValue, val)
      } else {
        setShow(false);
        var concatDate = startDate + "to" + endDate;
        searchParams = getSearchParams(concatDate, val)
      }
      switch (tabType) {
        case 0:
          props.SearchOrderData(
            searchParams,
            (response) => {
              onFilterSuccess(response);
            },
            (error) => {
              onFilterFailed(error);
            }
          );
          break;
        case 1:
          props.SearchCompletedOrderData(
            searchParams,
            (response) => {
              onFilterSuccess(response);
            },
            (error) => {
              onFilterFailed(error);
            }
          );
          break;
        case 2:
          props.SearchCancelOrderData(
            searchParams,
            (response) => {
              onFilterSuccess(response);
            },
            (error) => {
              onFilterFailed(error);
            }
          );
          break;
      }
    }  
  };

  const clearFilter = (e) => {
    getData(1)
    setSearchText("");
    setMinRange("");
    setMaxRange("");
    setFilter(false);
    setSearchFilter(false);
    setStartDateValue(dayjs().format('YYYY-MM-DD'));
    setEndDateValue(dayjs().format('YYYY-MM-DD'));
    // setPageIndex(1);
  };

  const popoverPrice = (
    <Popover id="popover-advance" onClick ={() => {setPopover(true);}}>
      <div className="row align-items-center price_filter_tag p-3" onClick ={(e) => {e.stopPropagation(); e.preventDefault(); e.nativeEvent.stopImmediatePropagation(); }} >
        <div className="price-range-sec col-lg-12 row m-0 p-0 mb-3 mt-2">
            <div class="col-sm-6 padleftzero">
              <div class="form-group mb-0">
                <label>Min</label>
                <div class="input-group search-box">                  
                  <input
                    type="number"
                    className="form-control border-0 p-1"
                    placeholder="Min Cost"
                    name="min"
                    min="0"
                    value={minRange}
                    onChange={e => handleOnPriceRangeChange(e.target.value, "min")}
                  />
                  <div class="input-group-append cont_bg">
                    <span class="input-group-text border-0">$</span>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-sm-6 padleftzero">
              <div class="form-group mb-0">
                <label>Max</label>  
                <div class="input-group search-box">                 
                  <input
                    type="number"
                    className="form-control border-0 p-1"
                    placeholder="Max Cost"
                    name="max"
                    min="0"
                    value={maxRange}
                    onChange={e => handleOnPriceRangeChange(e.target.value, "max")}
                  />           
                   <div class="input-group-append cont_bg">
                    <span class="input-group-text border-0">$</span>
                  </div>               
                </div>
              </div>
            </div>                      
          </div>
        <div className="col-sm-12 row m-0 pt-3 bortopside"> 
          <div className="col-sm-6 p-0"> 
              <button
                  type="button"
                  onClick={clearFilter}
                  className="btn_grey w-auto"
                >
                  Reset
                </button>  
              </div>
              <div className="col-sm-6 p-0 text-right"> 
              <button
                type="button"
                onClick={(e) => { 
                  e.currentTarget.blur();
                  setFilter(true);
                  callPriceAndDateFilter("price");
                }}
                className="btn_light_green"
              >Apply
              </button>                   
          </div>
        </div>
      </div>
    </Popover>
  );

  const popoverDate = (
   <Popover id="popover-basic" onClick ={() => {setShow(true)}}>
      <div className="float-right" onClick ={(e) => {e.stopPropagation(); e.preventDefault(); e.nativeEvent.stopImmediatePropagation();}}>
        <RangePicker 
          value={[moment(startDate, dateFormat), moment(endDate, dateFormat)]}
          format={dateFormat}
          onChange={handleChange}
          dropdownClassName="datepicker_set"
        />
          <div className="col-sm-12 row m-0 pt-3 bortopside"> 
            <div className="col-sm-6 p-0"> 
                <button
                    type="button"
                    onClick={clearFilter}
                    className="btn_grey w-auto"
                  >
                    Reset
                  </button>  
                </div>
                <div className="col-sm-6 p-0 text-right"> 
                <button
                  type="button"
                  onClick={(e) => { 
                    e.currentTarget.blur();
                    setFilter(true);
                    callPriceAndDateFilter("date");
                  }}
                  className="btn_light_green"
                >Apply
                </button>                   
            </div>
          </div>
        <br></br>
      </div> 
    </Popover>
  )

  return (
    <div class="page-wrapper">
        <div className="inner_wrapper">  
          <Loading isVisible={isLoading} />
          <div className="page_title row m-0 mb-4 pb-2">
              <div class="col-lg-1 col-md-2 col-3 p-0">
                  <span className="left_icon_title">
                    {/* <img src={my_order_icon} alt="" /> */}
                    <i class="fa fa-clipboard-list"></i>
                  </span>
              </div>
              <div class="col-lg-11 col-md-10 col-9 p-0 ">
                  <h2 className="right_title row m-0">My Orders</h2>
                  <span className="title_tagline m-0 row">All of your current, completed, and cancelled orders in one place.</span>
              </div>
          </div>
          <div class="grey_box_bg pb-3">
              <div className="content-wrapper">     
                <div className="row m-0 col-sm-12 justify-content-between pt-4">           
                  {props.userData.activeusertype === 1 ?   
                  <div className="col-lg-8 col-md-7 col-sm-6 col-12 mb-3">
                      <input
                        type="text"
                        className="form-control border-0 shadow-sm"
                        placeholder="Search by ID and Buyer. Enter keywords exactly as labeled in the order table."
                        name="search"
                        onChange={onTextChange}
                      />
                    </div> 
                    :   
                    <div className="col-lg-8 col-md-7 col-sm-6 col-12 mb-3">
                      <input
                        type="text"
                        className="form-control border-0 shadow-sm"
                        placeholder="Search by ID and Publisher. Enter keywords exactly as labeled in the order table."
                        name="search"
                        onChange={onTextChange}
                      />
                    </div>
                    }
                    <div className="col-lg-4 col-md-5 col-sm-6 col-12 filter_box_align justify-content-end btn_alignset mb-3"> 
                    <div ref={wrapperRef}>
                    <OverlayTrigger trigger="click" id="price" rootClose={true} show={showPopover} placement="bottom" overlay={popoverPrice}>
                        <button className="btn_light_green mr-3" onClick={() => {setShow(false); setPopover(!showPopover)} }
                          type="button" variant="success">Price Filter</button>
                      </OverlayTrigger>
                      <OverlayTrigger trigger="click" id="date" rootClose={true} show={show} placement="bottom" overlay={popoverDate}>
                      <button className="btn_light_green" onClick={() => {setPopover(false); setShow(!show)} }
                          type="button" variant="success">Date Filter</button>
                      </OverlayTrigger>
                    </div>
                    </div>
                  </div>
                <div class="col-12 mt-4">
                  <div class="card border-0">
                    {props.userData.activeusertype === 1 && (
                      <SellerOrder
                        {...props}
                        data={data}
                        tabType={tabType}
                        onTabChange={(id) => {
                          setTabType(id);
                        }}
                        onView={(data) => {
                          props.history.push("/myOrder/"+data.id)
                        }}
                      ></SellerOrder>
                    )}

                    {props.userData.activeusertype === 2 && (
                      <BuyerOrder
                        {...props}
                        data={data}
                        tabType={tabType}
                        onTabChange={(id) => {
                          setTabType(id);
                        }}
                        onEdit={(data) => {
                          props.history.push("/myOrder/"+data.id)
                        }}
                      >
                        {" "}
                      </BuyerOrder>
                    )}
                    <div
                      style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        height: "10vh",
                      }}
                    >
                      <Pagination
                        count={totalPageNumber}
                        onChange={(event, val) => {
                          if (pageIndex !== val) {
                            setPageIndex(val);
                            if (text !== "" && text.length >= 1) callFilter(text);
                            else getData(val);
                          }
                        }}
                        page={pageIndex}
                        variant="outlined"
                        color="primary"
                      />
                    </div>
                  </div>
                </div>
            </div>
          </div>
        </div>
      </div>
  );
};


export default MyOrderParent;
