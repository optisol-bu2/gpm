import { connect } from "react-redux";
import MyOrderParent from "./MyOrderParent";
import {
  getMyOrderDetails,
  filterOrderData,
  SearchOrderData,
  SearchCompletedOrderData,
  SearchCancelOrderData,
  getOrderDetail,
  uploadAttachment,
  removeAttachment,
  startOrder,
  filterCompletedOrderData,
  filterCancelOrderData,
  getCancelOrderData,
  getCompletedOrderData,
  orderApproved,
  orderCancel,
  orderNotApproved,
  getAttachments,
  deleteAttachment,
  sendOrderUpdate,
  updateAttachment,
  submitFeedback,
  getFeedback,
  orderExtention,
} from "../../Actions/actionContainer";

const mapStateToProps = (state) => {
  return {
    serviceCategories: state.mainReducer.serviceCategories,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getMyOrderDetails: (id, onSuccess, onFailure) =>
      dispatch(getMyOrderDetails(id, onSuccess, onFailure)),
    filterOrderData: (params, onSuccess, onFailure) =>
      dispatch(filterOrderData(params, onSuccess, onFailure)),
    SearchOrderData: (params, onSuccess, onFailure) =>
      dispatch(SearchOrderData(params, onSuccess, onFailure)),
    getOrderDetail: (id, onSuccess, onFailure) =>
      dispatch(getOrderDetail(id, onSuccess, onFailure)),
    uploadAttachment: (params, onSuccess, onFailure) =>
      dispatch(uploadAttachment(params, onSuccess, onFailure)),
    removeAttachment: (id, onSuccess, onFailure) =>
      dispatch(removeAttachment(id, onSuccess, onFailure)),
    startOrder: (params, onSuccess, onFailure) =>
      dispatch(startOrder(params, onSuccess, onFailure)),
    getCompletedOrderData: (id, onSuccess, onFailure) =>
      dispatch(getCompletedOrderData(id, onSuccess, onFailure)),
    filterCompletedOrderData: (params, onSuccess, onFailure) =>
      dispatch(filterCompletedOrderData(params, onSuccess, onFailure)),
    SearchCompletedOrderData: (params, onSuccess, onFailure) =>
      dispatch(SearchCompletedOrderData(params, onSuccess, onFailure)),
    getCancelOrderData: (id, onSuccess, onFailure) =>
      dispatch(getCancelOrderData(id, onSuccess, onFailure)),
    filterCancelOrderData: (params, onSuccess, onFailure) =>
      dispatch(filterCancelOrderData(params, onSuccess, onFailure)),
    SearchCancelOrderData: (params, onSuccess, onFailure) =>
      dispatch(SearchCancelOrderData(params, onSuccess, onFailure)),
    orderApproved: (params, onSuccess, onFailure) =>
      dispatch(orderApproved(params, onSuccess, onFailure)),
    orderCancel: (params, onSuccess, onFailure) =>
      dispatch(orderCancel(params, onSuccess, onFailure)),
    orderNotApproved: (params, onSuccess, onFailure) =>
      dispatch(orderNotApproved(params, onSuccess, onFailure)),
    getAttachments: (order_id, revision, onSuccess, onFailure) =>
      dispatch(getAttachments(order_id, revision, onSuccess, onFailure)),
    deleteAttachment: (id, onSuccess, onFailure) =>
      dispatch(deleteAttachment(id, onSuccess, onFailure)),
    sendOrderUpdate: (params, onSuccess, onFailure) =>
      dispatch(sendOrderUpdate(params, onSuccess, onFailure)),
    updateAttachment: (params, onSuccess, onFailure) =>
      dispatch(updateAttachment(params, onSuccess, onFailure)),
    submitFeedback: (params, onSuccess, onFailure) =>
      dispatch(submitFeedback(params, onSuccess, onFailure)),
    getFeedback: (id, onSuccess, onFailure) =>
      dispatch(getFeedback(id, onSuccess, onFailure)),
    orderExtention: (params, onSuccess, onFailure) =>
      dispatch(orderExtention(params, onSuccess, onFailure)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MyOrderParent);
