import React from "react";
import MaterialTable from "material-table";
import moment from "moment";
import Button from "../../../components/core/Button";
import avatar from '../../../assets/images/avatar.png';
import { Popup } from 'semantic-ui-react';

const SellerOrder = (props) => {
  const options = {
    headerStyle: {
      fontWeight: "bold",
    },
    searchFieldStyle: {
      underline: {
        "&:after": {
          borderBottomColor: "#ED3237",
        },
      },
    },
    pageSize: 25,
    actionsColumnIndex: -1,
    emptyRowsWhenPaging: false,
    draggable: false,
  };

  const columns = [
    {
      title: "ID",
      field: "invoiceid",
      width: "10%",
    },
    {
      title: "Buyer",
      field: "name",
      render: ({ tableData: { id } }) => {
        return (
          <div>
            <div style={{ display: "inline-block" }} className="row">
              <img
                src={props.data[id].buyer.image}
                onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                className="rounded-circle img-fluid"
                style={{ marginRight: "10px", width: 40, height: 40 }}
              />
              {props.data[id] && props.data[id].buyer && (props.data[id].buyer.bio != "" || props.data[id].buyer.review !== "0.00") &&
                <Popup 
                  key={props.data[id]}
                  trigger={<label> {props.data[id].buyer.name}</label>}>
                  <Popup.Header>
                    {props.data[id].buyer.name}
                  </Popup.Header>
                  <Popup.Content>
                    <div className="rating_star d-flex mt-1">
                      {props.data[id].buyer.review !== 0 && Array.from(Array(Math.round(props.data[id].buyer.review)), () => {
                        return (
                          <span className="fa fa-star star"></span>
                        )
                      })}                              
                    </div> 
                    {props.data[id].buyer.bio}
                  </Popup.Content>
                </Popup>
              } 
              {props.data[id] && props.data[id].buyer && (props.data[id].buyer.bio == "" && props.data[id].buyer.review == "0.00") &&
                <label> {props.data[id].buyer.name}</label>
              }
            </div>
          </div>
        );
      },
    },
    {
      title: "Due On",
      field: "duedate",
      render: ({ tableData: { id } }) => {
        if (props.data[id].duedate == null) {
          return ""
        } else {
          return (
            orderTimeFormat(props.data[id].duedate)
          );
        }
      },
    },
    {
      title: "Status",
      field: "statusname",
    },
    {
      title: "Order Type",
      field: "servicetypename",
      render: ({ tableData: { id } }) => {
        return (
          props.data[id].servicetype == 1 ? "Service": props.data[id].servicetype == 2 ? "Content Writer" : "Custom"
        );
      },
    },
    {
      title: "Cost",
      field: "total",
      render: ({ tableData: { id } }) => {
        return (
          "$"+props.data[id].total
        );
      },
    },
    {
      title: "Actions",
      render: (row) => {
        return (
          <div>
            <div className="view_btns" style={{ marginRight: "10px" }}>
              <Button
                autoFocus
                onClick={() => props.onView(row)}
                backgroundColor={"#37B877"}
                hoverBgColor={"#37B877"}
                label={"View"}
                width={"10px"}
              />
            </div>
          </div>
        );
      },
    },
  ];

  const currentOrderColumns = [
    {
      title: "ID",
      field: "invoiceid",
      width: "10%",
    },
    {
      title: "Buyer",
      field: "name",
      render: ({ tableData: { id } }) => {
        return (
          <div>
            <div style={{ display: "inline-block" }} className="row">
              <img
                src={props.data[id].buyer.image}
                onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                className="rounded-circle img-fluid"
                style={{ marginRight: "10px", width: 40, height: 40 }}
              />
              {props.data[id] && props.data[id].buyer && (props.data[id].buyer.bio != "" || props.data[id].buyer.review !== "0.00") &&
                <Popup 
                  key={props.data[id]}
                  trigger={<label> {props.data[id].buyer.name}</label>}>
                  <Popup.Header>
                    {props.data[id].buyer.name}
                  </Popup.Header>
                  <Popup.Content>
                    <div className="rating_star d-flex mt-1">
                      {props.data[id].buyer.review !== 0 && Array.from(Array(Math.round(props.data[id].buyer.review)), () => {
                        return (
                          <span className="fa fa-star star"></span>
                        )
                      })}                              
                    </div> 
                    {props.data[id].buyer.bio}
                  </Popup.Content>
                </Popup>
              } 
              {props.data[id] && props.data[id].buyer && (props.data[id].buyer.bio == "" && props.data[id].buyer.review == "0.00") &&
                <label> {props.data[id].buyer.name}</label>
              }
            </div>
          </div>
        );
      },
    },
    {
      title: "Due On",
      field: "duedate",
      render: ({ tableData: { id } }) => {
        if (props.data[id].duedate == null) {
          return ""
        } else {
          return (
            orderTimeFormat(props.data[id].duedate)
          );
        }
      },
    },
    {
      title: "Status",
      field: "statusname",
    },
    {
      title: "Order Type",
      field: "servicetypename",
      render: ({ tableData: { id } }) => {
        return (
          props.data[id].servicetype == 1 ? "Service": props.data[id].servicetype == 2 ? "Content Writer" : "Custom"
        );
      },
    },
    {
      title: "Cost",
      field: "total",
      render: ({ tableData: { id } }) => {
        return (
          "$"+props.data[id].total
        );
      },
    },
    {
      title: "Last Updated At",
      field: "updated_at",
      render: ({ tableData: { id } }) => {
        if (props.data[id].updated_at == null) {
          return ""
        } else {
          return (
            orderTimeFormat(props.data[id].updated_at)
          );
        }
      },
    },
    {
      title: "Actions",
      render: (row) => {
        return (
          <div>
            <div className="view_btns" style={{ marginRight: "10px" }}>
              <Button
                autoFocus
                onClick={() => props.onView(row)}
                backgroundColor={"#37B877"}
                hoverBgColor={"#37B877"}
                label={"View"}
                width={"10px"}
              />
            </div>
          </div>
        );
      },
    },
  ];

  const completedOrderColumns = [
    {
      title: "ID",
      field: "invoiceid",
      width: "10%",
    },
    {
      title: "Buyer",
      field: "name",
      render: ({ tableData: { id } }) => {
        return (
          <div>
            <div style={{ display: "inline-block" }} className="row">
              <img
                src={props.data[id].buyer.image}
                onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                className="rounded-circle img-fluid"
                style={{ marginRight: "10px", width: 40, height: 40 }}
              />
              {props.data[id] && props.data[id].buyer && (props.data[id].buyer.bio != "" || props.data[id].buyer.review !== "0.00") &&
                <Popup 
                  key={props.data[id]}
                  trigger={<label> {props.data[id].buyer.name}</label>}>
                  <Popup.Header>
                    {props.data[id].buyer.name}
                  </Popup.Header>
                  <Popup.Content>
                    <div className="rating_star d-flex mt-1">
                      {props.data[id].buyer.review !== 0 && Array.from(Array(Math.round(props.data[id].buyer.review)), () => {
                        return (
                          <span className="fa fa-star star"></span>
                        )
                      })}                              
                    </div> 
                    {props.data[id].buyer.bio}
                  </Popup.Content>
                </Popup>
              } 
              {props.data[id] && props.data[id].buyer && (props.data[id].buyer.bio == "" && props.data[id].buyer.review == "0.00") &&
                <label> {props.data[id].buyer.name}</label>
              }
            </div>
          </div>
        );
      },
    },
    {
      title: "Due On",
      field: "duedate",
      width: "11%",
      render: ({ tableData: { id } }) => {
        if (props.data[id].duedate == null) {
          return ""
        } else {
          return (
            orderTimeFormat(props.data[id].duedate)
          );
        }
      },
    },
    {
      title: "Status",
      field: "statusname",
    },
    {
      title: "Order Type",
      field: "servicetypename",
      render: ({ tableData: { id } }) => {
        return (
          props.data[id].servicetype == 1 ? "Service": props.data[id].servicetype == 2 ? "Content Writer" : "Custom"
        );
      },
    },
    {
      title: "Cost",
      field: "total",
      render: ({ tableData: { id } }) => {
        return (
          "$"+props.data[id].total
        );
      },
    },
    {
      title: "Download Documents",
      field: "attachmentfilename",
      render: ({ tableData: { id } }) => {
        if(props.data[id].attachmentfilename == "") {
          return ("-")
        } else {
          return (
            <div> 
              <a href={props.data[id].file_url} target="_blank" download={props.data[id].file_url} class="upolade_filename">
                <span className="fa fa-download"></span> 
              </a> &nbsp; 
              {props.data[id].attachmentfilename}
            </div>
          )
        }
      },
    },
    {
      title: "Actions",
      render: (row) => {
        return (
          <div>
            <div className="view_btns" style={{ marginRight: "10px" }}>
              <Button
                autoFocus
                onClick={() => props.onView(row)}
                backgroundColor={"#37B877"}
                hoverBgColor={"#37B877"}
                label={"View"}
                width={"10px"}
              />
            </div>
          </div>
        );
      },
    },
  ];


  /**
   * changes the given date to local date format
   * @param {*} dateTime
   * @returns
   */
  const orderTimeFormat = (dateTime) => {
    var utc = moment.utc(dateTime).toDate();
    var orderTime = moment(utc).local().format('MMM-DD-YYYY hh:mmA');
    return orderTime;
  }

  return (
    <div>
      <div class="card-body order-tabs-navigation">
        <ul class="nav nav-tabs justify-content-between flex-column flex-md-row">
          <li class="nav-item flex-fill text-center">
            <a
              onClick={() => props.onTabChange(0)}
              class={props.tabType === 0 ? "nav-link active" : "nav-link"}
              data-toggle="tab"
              href="#current_order"
            >
              Current Orders
            </a>
          </li>
          <li class="nav-item flex-fill text-center">
            <a
              onClick={() => props.onTabChange(1)}
              class={props.tabType === 1 ? "nav-link active" : "nav-link"}
              data-toggle="tab"
              href="#completed_order"
            >
              Completed orders
            </a>
          </li>
          <li class="nav-item flex-fill text-center">
            <a
              onClick={() => props.onTabChange(2)}
              class={props.tabType === 2 ? "nav-link active" : "nav-link"}
              data-toggle="tab"
              href="#cancelled_order"
            >
              Cancelled Orders
            </a>
          </li>
        </ul>
        <div class="tab-content">
        <div class="tab-pane active grid_des orange_table myorders_table" id="current_order">
            <MaterialTable
              title={""}
              columns={currentOrderColumns}
              data={props.data}
              options={{
                ...options,
                selection: false,
                paging: false,
                search: false,
              }}
            />
          </div>
          <div class="tab-pane fade grid_des orange_table myorders_table" id="completed_order">
            <MaterialTable
              title={""}
              columns={completedOrderColumns}
              data={props.data}
              options={{
                ...options,
                selection: false,
                paging: false,
                search: false,
              }}
            />
          </div>
          <div class="tab-pane fade  grid_des orange_table myorders_table" id="cancelled_order">
            <MaterialTable
              title={""}
              columns={columns}
              data={props.data}
              options={{
                ...options,
                selection: false,
                paging: false,
                search: false,
              }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default SellerOrder;
