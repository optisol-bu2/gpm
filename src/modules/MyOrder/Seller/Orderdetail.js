import React, { useState, useEffect } from "react";
import { Form } from "react-bootstrap";
import { Link } from "react-router-dom";
import Loading from "../../../components/core/Loading";
import moment from "moment";
import TimerControl from "../../../components/core/Timer";
import iconhours from "../../../assets/images/other-pages/esti-deli-icon.svg";
import iconnoofhours from "../../../assets/images/other-pages/no_of_hours.png";
import iconnoofwords from "../../../assets/images/other-pages/no_of_words.png";
import iconbacklink from "../../../assets/images/other-pages/backlink-icon.svg";
import icongplink from "../../../assets/images/other-pages/guest-post-icon.svg";
import icnDownload from "../../../assets/images/icn_download.svg";
import avatar from '../../../assets/images/avatar.png';
import Modal from "@material-ui/core/Modal";
import FileUpload from "../../../components/core/FileUpload";
import { Close } from "@material-ui/icons";
import { IconButton, Tooltip } from "@material-ui/core";
import { ListGroup } from "react-bootstrap";
import FeedbackView from "../FeedbackView";
import SubmitFeedback from "../SubmitFeedback";
import { ToastAlert, PlaySound } from "../../../utils/sweetalert2";
import my_order_icon from "../../../assets/images/menu-icons/my_orders_white.svg";
import ReactReadMoreReadLess from "react-read-more-read-less";
import AttachmentLoading from "../../../components/core/ChatLoading";
import ConfirmationDialog from "../../../components/core/ConfirmationDialog/ConfirmationDialog";
import { Popup } from 'semantic-ui-react';
import ContactBuyer from "../../../components/ContactBuyer/ContactBuyer";
import { getBloggerProfile } from "../../../Actions/bloggerAction";

const OrderDetail = (props) => {
  const [isLoading, setLoading] = useState(null);
  const [isAttachmentLoading, setAttachmentLoading] = useState(null);
  const [fileList, setFileList] = useState([]);
  const [fileListHtml, setFileListHtml] = useState();
  const [orderData, setOrderData] = useState({});
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [guestPostArray, setGuestPostArray] = useState([]);
  const [backLinkArray, setBackLinkArray] = useState([]);
  const [addElementToGuestPostArray, setAddElementToGuestPostArray] = useState(false);
  const [addElementToBackLinkArray, setAddElementToBackLinkArray] = useState(false);
  const [base64, setBase64] = useState(null);
  const [file, setFile] = useState(null);
  const [isImageDeleted, setIsImageDeleted] = useState(false);
  const [isImageCanceled, setIsImageCanceled] = useState(false);
  const [isFileError, setIsFileError] = useState(false);
  const [validated, setValidated] = useState(false);
  const [updateElementToGuestPostArray, setUpdateElementToGuestPostArray,] = useState(false);
  const [updateElementToBackLinkArray, setUpdateElementToBackLinkArray,] = useState(false);
  const [recordValue, setRecordValue] = useState();
  const [recordId, setRecordId] = useState();
  const [isLinkUpdate, setIsLinkUpdate] = useState(false);
  const [descriptionText, setDescriptionText] = useState("");
  const [charCount, setCharCount] = useState(0);
  const [revision, setRevision] = useState("");
  const [buyerName, setBuyerName] = useState("");
  const [sellerName, setSellerName] = useState("");
  const [buyerImage, setBuyerImage] = useState("");
  const [sellerImage, setSellerImage] = useState("");
  const [revisionReqAttachArray, setRevisionReqAttachArray] = useState([]);
  const [buyerComment, setBuyerComment] = useState(false);
  const [buyerFeedback, setBuyerFeedback] = useState(null);
  const [sellerComment, setSellerComment] = useState(false);
  const [isExtenstion, setExtension] = useState(false);
  const [isFileSize, setFileSize] = useState(0);
  const [extendDialog, setExtendDialog] = useState(false);

  useEffect(() => {
    props.setEndTimer(false)
    props.last24Hours(false)
  },[]);
  
  useEffect(() => {
    if (updateElementToGuestPostArray)
      updateElementToArray(recordValue, recordId, isLinkUpdate, 0);
  }, [updateElementToGuestPostArray]);

  useEffect(() => {
    if (updateElementToBackLinkArray)
      updateElementToArray(recordValue, recordId, isLinkUpdate, 1);
  }, [updateElementToBackLinkArray]);

  useEffect(() => {
    if (file) {
      updateAttachment();
    }
  }, [file]);

  useEffect(() => {
    if (isExtenstion) {
      getOrderDetail();
      getFeedback();
    }
  }, [isExtenstion]);

  useEffect(() => {
    if (props.orderId && props.userData.activeusertype === 1) {
      getOrderDetail();
      getFeedback();
    }
  }, [props.orderId]);

  useEffect(() => {
    if (props.orderRedirect && props.userData.activeusertype === 1) {
      getOrderDetail();
      getFeedback();
      props.orderList(false);
    }
  }, [props.orderRedirect]);

  useEffect(() => {
    if (addElementToGuestPostArray) addElementToArray(0);
  }, [addElementToGuestPostArray]);

  useEffect(() => {
    if (addElementToBackLinkArray) addElementToArray(1);
  }, [addElementToBackLinkArray]);

  const getFeedback = () => {
    try {
      props.getFeedback(
        props.orderId,
        (response) => {
          if (response.data !== null) {
            setBuyerFeedback(response.data.buyer ? response.data.buyer : null);
          }
        },
        () => { }
      );
    } catch (error) { }
  };

  const addElementToArray = (flag) => {
    if (guestPostArray.length > 0 && flag === 0) {
      var list = guestPostArray;
      list.push({ id: guestPostArray.length, key: "", value: "" });
      setGuestPostArray(list);
      setAddElementToGuestPostArray(false);
    }
    if (backLinkArray.length > 0 && flag === 1) {
      var list = backLinkArray;
      list.push({ id: backLinkArray.length, key: "", value: "" });
      setBackLinkArray(list);
      setAddElementToBackLinkArray(false);
    }
  };

  const updateElementToArray = (value, id, isLinkUpdate, flag) => {
    if (guestPostArray.length > 0 && flag === 0) {
      var list = guestPostArray;
      for (let i = 0; i < list.length; i++) {
        if (list[i].id === id) {
          isLinkUpdate === true
            ? (list[i].value = value)
            : (list[i].key = value);
        }
      }
      setGuestPostArray(list);
      setUpdateElementToGuestPostArray(false);
    }
    if (backLinkArray.length > 0 && flag === 1) {
      var list = backLinkArray;
      for (let i = 0; i < list.length; i++) {
        if (list[i].id === id) {
          isLinkUpdate === true
            ? (list[i].value = value)
            : (list[i].key = value);
        }
      }
      setBackLinkArray(list);
      setUpdateElementToBackLinkArray(false);
    }
  };

  /**
   * changes the given date to local date format
   * @param {*} date
   * @returns
   */
  const orderDateFormat = (date) => {
    var utc = moment.utc(date).toDate();
    var orderDate = moment(utc).local().format('MMM-DD-YYYY');
    return orderDate;
  }

  /**
   * changes the given date to local date format
   * @param {*} dateTime
   * @returns
   */
  const orderTimeFormat = (dateTime) => {
    var utc = moment.utc(dateTime).toDate();
    var orderTime = moment(utc).local().format('MMM-DD-YYYY hh:mm A');
    return orderTime;
  }

  const getOrderDetail = () => {
    setLoading(true);
    props.getOrderDetail(
      props.orderId,
      (response) => {
        setLoading(false);
        if (response.data !== null) {
          setRevision(response.data.revision);
          setBuyerImage(response.data.buyer.image);
          setBuyerName(response.data.buyer.name);
          setSellerName(response.data.seller.name);
          setSellerImage(response.data.seller.image);
          setBuyerComment(response.data.isbuyercommented);
          setSellerComment(response.data.issellercommented);
          response.data.timerInitialState =
            response.data.status != 8
              ? parseInt(response.data.deliverytime) * 3600000
              : 1000;

          if (response.data.comments.length > 0) {
            var list = [];
            for (let i = 0; i < response.data.comments.length; i++) {
              var buyerObj =
                response.data.comments[i].buyer !== null &&
                  response.data.comments[i].buyer !== undefined
                  ? createReqAttachObj(response.data.comments[i].buyer)
                  : { comment: "", date: "", attachments: [] };
              var sellerObj =
                response.data.comments[i].seller !== null &&
                  response.data.comments[i].seller !== undefined
                  ? createReqAttachObj(response.data.comments[i].seller)
                  : { comment: "", date: "", attachments: [] };
              var revision = response.data.comments[i].revision;
              list.push({
                buyerObj: buyerObj,
                sellerObj: sellerObj,
                revisionNumber: revision,
              });
            }
            setRevisionReqAttachArray(list);
          } else {
            setRevisionReqAttachArray({
              buyerObj: { comment: "", date: "", attachments: [] },
              sellerObj: { comment: "", date: "", attachments: [] },
            });
          }

          setOrderData(response.data);
          getAttachments(response.data.revision);
        }
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  const createReqAttachObj = (commentReqObj) => {
    var obj = { comment: "", date: "", attachments: [] };
    obj.comment =
      commentReqObj.comment !== undefined && commentReqObj.comment !== null
        ? commentReqObj.comment
        : "";
    obj.date =
      commentReqObj.date !== undefined && commentReqObj.date !== null
        ? orderTimeFormat(commentReqObj.date)
        : "";
    if (
      commentReqObj.attachments !== undefined &&
      commentReqObj.attachments !== null
    ) {
      var attachments = commentReqObj.attachments;
      if (attachments.length > 0) {
        var list = [];
        for (let i = 0; i < attachments.length; i++) {
          var fileName = attachments[i].name.split("/")[
            attachments[i].name.split("/").length - 1
          ];
          var link = attachments[i].document;
          list.push({ name: fileName, link: link, attachmentName: attachments[i].name });
        }
        obj.attachments = list;
      }
    }

    return obj;
  };

  const getAttachments = (revision) => {
    try {
      setLoading(true);
      props.getAttachments(
        props.orderId,
        revision,
        (response) => {
          setLoading(false);
          if (response.data !== null) {
            setDescriptionText(response.data.comment.value);
            var attachments = response.data.attachments;
            if (attachments.length > 0) {
              var list = fileList;
              for (let i = 0; i < attachments.length; i++) {
                var fileName = attachments[i].value.split("/")[
                  attachments[i].value.split("/").length - 1
                ];
                var id = attachments[i].id;
                var link = attachments[i].value;
                list.push({ name: fileName, id: id, link: link, attachmentName: attachments[i].name });
              }
              setFileList(list);
              setFileListHtml(loadFileList());
            }
          }
        },
        (error) => {
          setLoading(false);
        }
      );
    } catch (error) { }
  };

  const updateAttachment = () => {
    let formData = new FormData();
    let file_size = 0;
    formData.set("orderid", props.orderId);
    formData.append("document", file);
    formData.append("revision", revision);
    file_size = isFileSize + file.size;
    setFileSize(file_size)
    if (file_size >= 8388608) {
      ToastAlert("error", "File size exceeds maximum limit 8 MB.");
      PlaySound()
    }
    else {
      //setLoading(true);
      setAttachmentLoading(true);
      props.updateAttachment(
        formData,
        (response) => {
          setAttachmentLoading(false);

          if (file.name !== "") {
            var list = fileList;
            list.push({ name: file.name, id: response.data.id, attachmentName: file.name });
            setFileList(list);
            setFileListHtml(loadFileList());
          }
        },
        (error) => {
          setAttachmentLoading(false);
        }
      );
    }
  };

  const deleteAttachment = (attachmentId, index) => {
    setAttachmentLoading(true);
    props.deleteAttachment(
      attachmentId,
      (response) => {
        setAttachmentLoading(false);

        fileList.splice(index, 1);
        setFileList(fileList);
        setFileListHtml(loadFileList());
      },
      (error) => {
        setAttachmentLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  const orderExtension = () => {
    setLoading(true);
    let params = {
      orderid: props.orderId,
    };
    props.orderExtention(
      params,
      (response) => {
        setExtension(true);
        setLoading(false);
        ToastAlert('success', response.message);
        PlaySound()
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  };

  const loadFileList = () => {
    let items = [];
    try {
      for (let i = 0; i <= fileList.length; i++) {
        items.push(
          <ListGroup.Item>
            <div>
              <span>{fileList[i].attachmentName !== "" ? fileList[i].attachmentName : fileList[i].name}</span>
              <Tooltip title="Cancel">
                <IconButton
                  aria-label="cancel"
                  onClick={() => {
                    deleteAttachment(fileList[i].id, i);
                  }}
                >
                  <Close fontSize="small" />
                </IconButton>
              </Tooltip>
            </div>
          </ListGroup.Item>
        );
      }
    } catch (error) { }
    return items;
  };

  const handlesubmit = (e) => {
    if (!checkAllValidation()) {
      setValidated(true);
      e.preventDefault();
      e.stopPropagation();
    } else {
      const params = {
        comment: descriptionText,
        orderid: props.orderId,
        revision: revision,
      };
      setLoading(true);

      props.sendOrderUpdate(
        params,
        () => {
          closeSubmitModal();
          setLoading(false);
          props.setModalContent("Delivery successfully submitted.");
          props.reloadMainScreen();
        },
        () => {
          setLoading(false);
        }
      );
    }
  };

  const closeSubmitModal = () => {
    setShowUpdateModal(false);
    setValidated(false);
    setCharCount(0);
    setDescriptionText("");
    setBase64(null);
    setIsImageCanceled(false);
    setFile(null);
    setIsFileError(false);
  }

  const checkAllValidation = () => {
    let valid = true;
    descriptionText === undefined ||
      descriptionText === null ||
      descriptionText === ""
      ? (valid = false)
      : (valid = true);
    return valid;
  };

  const progress_complete = {
    width: "25%",
  };

  return (
    <div>
      <Loading isVisible={isLoading} />
      <div className="page_title row m-0 mb-4 pb-2">
        <div class="col-lg-1 col-md-2 col-3 p-0">
          <span className="left_icon_title"><img src={my_order_icon} alt="" /></span>
        </div>
        <div class="col-lg-11 col-md-10 col-9 p-0 ">
          <h2 className="right_title row m-0">Order Detail</h2>
          <span className="title_tagline m-0 row">
            <ol className="breadcrumb p-0 bg-none mb-0">
              <li className="breadcrumb-item">
                <Link to={{ pathname: "/sellerDashboard" }}>
                  <i className="fa fa-home"></i>
                </Link>
              </li>
              <li className="breadcrumb-item">
                <Link to={{ pathname: "/myOrder" }}>My Orders</Link>
              </li>
              <li className="breadcrumb-item active" aria-current="page">
                {orderData.invoiceid ? orderData.invoiceid : ""} Details
              </li>
            </ol>
          </span>
        </div>
      </div>
      {orderData !== null && isLoading === false && (
        <div class="grey_box_bg orderpage_details">
          <div class="card mt-3 ">
            <div class="card-body">
              <div class="d-flex flex-column flex-sm-row text-center text-sm-left justify-content-between flex-wrap align-items-center">
                <div className="col-lg-3">
                  <h5 class="order_id">
                    <span class="order_span">Order</span>{" "}
                    {orderData.invoiceid}
                  </h5>
                  <p class="orderp font-weight-bold">
                    {orderData.startdate == null ? "" : orderDateFormat(orderData.startdate)}
                  </p>
                </div>
                <div class="col-lg-4 progress_div">
                  <h6>{orderData.statusname}</h6>
                  <div class="progress">
                    <div
                      class={orderData.status == 9 ? "progress-bar bg-danger" : "progress-bar bg-success"}
                      role="progressbar"
                      style={{ width: (orderData.status == 8 || orderData.status == 9) ? "100%" : orderData.revision == 0 ? ((orderData.revision + (0.4 + (buyerComment && sellerComment ? 0.4 : 0))) / orderData.max_revision) * 100 + "%" : orderData.revision == orderData.max_revision ? ((orderData.revision - (0.4 + (buyerComment && sellerComment ? 0 : 0.4))) / orderData.max_revision) * 100 + "%" : ((orderData.revision + (buyerComment && sellerComment ? 0.5 : 0)) / orderData.max_revision) * 100 + "%" }}
                      aria-valuenow="25"
                      aria-valuemin="0"
                      aria-valuemax="100"
                    />
                  </div>
                </div>
                <div className="col-lg-3">
                  <h5 class="order_id text-center">
                    <span class="order_span">${orderData.total}</span>{" "}
                  </h5>
                  <p class="orderp font-weight-bold">Our Price</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class={orderData.servicetype == 3 ? "text-left  col-sm-6 col-lg-6" : "text-left  col-sm-6 col-lg-3"}>
                <h5 class="mid_head_side">Buyer</h5>
                <div class="bg_settings_parts user-profile d-flex">
                  <img
                    src={buyerImage ? buyerImage : avatar}
                    onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                    alt="..."
                    width="65"
                    height="65"
                    class="rounded-circle"
                    data-pagespeed-url-hash="3993073969"
                    onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                  />
                   {orderData && orderData.buyer &&
                    <Link
                      style={{ marginLeft: 10 }}
                      class="user-name text-dark mr-2 small"
                    >
                    <ContactBuyer
                      orderId={orderData.id}
                      buyerName={buyerName}
                      buyerBio={orderData.buyer.bio}
                      buyerReview={orderData.buyer.review}
                      buyerLastLoggingTime={orderData.buyer.lastlogin_at}
                      receiverId={orderData.buyer.id ? orderData.buyer.id : ""}
                      receiverImage={orderData.buyer.image ? orderData.buyer.image : avatar}
                      receiverName={orderData.buyer.name ? orderData.buyer.name : "User"}
                      bloggerProfile={true}
                    /> 
                    </Link>
                  }
                  {/* {orderData && orderData.buyer && (orderData.buyer.bio != "" || orderData.buyer.review !== "0.00") &&
                    <Popup 
                      key={orderData.id}
                      trigger={<a style={{ marginLeft: 10 }} class="user-name text-dark mr-2 small"> {buyerName} </a>}>
                      <Popup.Header>
                        {buyerName}
                      </Popup.Header>
                      <Popup.Content>
                        <div className="rating_star d-flex mt-1">
                          {orderData.buyer.review !== 0 && Array.from(Array(Math.round(orderData.buyer.review)), () => {
                            return (
                              <span className="fa fa-star star"></span>
                            )
                          })}                              
                        </div> 
                        {orderData.buyer.bio}
                      </Popup.Content>
                    </Popup>
                  }
                  {orderData && orderData.buyer && (orderData.buyer.bio == "" && orderData.buyer.review == "0.00") &&
                    <a style={{ marginLeft: 10 }} class="user-name text-dark mr-2 small"> {buyerName} </a> 
                  } */}
                </div>
              </div>
              {orderData.servicetype == 1 &&
                <div class="text-center col-sm-6 col-lg-3">
                  <h5 class="mid_head_side">Guest Post</h5>
                  <div class="bg_settings_parts d-flex">
                    <span className="leftimg_gp">
                      <img
                        src={icongplink}
                        alt=""
                        data-pagespeed-url-hash="1714040194"
                        onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                      />
                    </span>
                    <h4 class="big_price_fnt w-auto">{orderData.noofguestpost}</h4>
                  </div>
                </div>
              }
              {orderData.servicetype == 1 &&
                <div class="text-center col-sm-6 col-lg-3">
                  <h5 class="mid_head_side">Backlink</h5>
                  <div class="bg_settings_parts d-flex">
                    <span className="leftimg_gp">
                      <img
                        src={iconbacklink}
                        alt=""
                        data-pagespeed-url-hash="1714040194"
                        onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                      />
                    </span>
                    <h4 class="big_price_fnt w-auto">{orderData.noofbacklink}</h4>
                  </div>
                </div>
              }
              {orderData.servicetype == 2 &&
                <div class="text-center col-sm-6 col-lg-3">
                  <h5 class="mid_head_side">No. of Hours</h5>
                  <div class="bg_settings_parts d-flex">
                    <span className="leftimg_gp">
                      <img
                        src={iconnoofhours}
                        alt=""
                        data-pagespeed-url-hash="1714040194"
                        onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                      />
                    </span>
                    <h4 class="big_price_fnt w-auto">{orderData.noofhours}</h4>
                  </div>
                </div>
              }
              {orderData.servicetype == 2 &&
                <div class="text-center col-sm-6 col-lg-3">
                  <h5 class="mid_head_side">No. of Words</h5>
                  <div class="bg_settings_parts d-flex">
                    <span className="leftimg_gp">
                      <img
                        src={iconnoofwords}
                        alt=""
                        data-pagespeed-url-hash="1714040194"
                        onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                      />
                    </span>
                    <h4 class="big_price_fnt w-auto">{orderData.noofwords}</h4>
                  </div>
                </div>
              }
              <div class={orderData.servicetype == 3 ? "text-center  col-sm-6 col-lg-6" : "text-center  col-sm-6 col-lg-3"}>
                <h5 class="mid_head_side">Estimate Delivery</h5>
                <div class="bg_settings_parts d-flex">
                  <span className="leftimg_gp">
                    <img
                      src={iconhours}
                      alt=""
                      data-pagespeed-url-hash="3978192268"
                      onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                    />
                  </span>
                  <h4 class="biga_price_fnt">{orderData.deliverytime} {orderData.deliverytimein}</h4>
                </div>
              </div>
              {orderData.servicetype == 3 && orderData.description != "" &&
                <div class="text-center col-sm-6 col-lg-12 mt-3">
                  <h5 class="mid_head_side">Description</h5>
                  <div class="bg_settings_parts d-flex">
                    {/* <span className="leftimg_gp">
                      <i class="fa fa-info-circle fa-3x"></i>
                    </span> */}
                    <h4 class="biga_price_fnt text-truncate text-wrap">
                      <ReactReadMoreReadLess
                        charLimit={220}
                        readMoreText={"Read more ▼"}
                        readLessText={"Read less ▲"}
                        readMoreClassName="read-more-less--more"
                        readLessClassName="read-more-less--less"
                      >
                        {orderData.description}
                      </ReactReadMoreReadLess>
                    </h4>
                  </div>
                </div>
              }
            </div>
            <br/> <br/> <br/>
            {!(orderData.revision <= orderData.max_revision &&
                buyerComment &&
                !sellerComment) &&
                orderData.status != 8 && orderData.status!=9 ? (
                  <div class="text-center"> 
                    <img
                      src={buyerImage ? buyerImage : avatar}
                      onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                      alt="..."
                      width="65"
                      height="65"
                      class="rounded-circle"
                      data-pagespeed-url-hash="3993073969"
                      onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                    />
                    <h4 style={{margin:10}} class="user_name_align selleruser_name_align">{buyerName}</h4>
                    <h6 className="herupdt_cnt">Waiting for the Buyer to Accept Order or Start Revision...</h6>
                  </div>
              ) : (orderData.status != 8 && orderData.status!=9 &&
                <div class="text-center"> 
                <img
                  src={sellerImage ? sellerImage : avatar}
                  onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                  alt="..."
                  width="65"
                  height="65"
                  class="rounded-circle"
                  data-pagespeed-url-hash="3993073969"
                  onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                />
                <h4 style={{margin:10}} class="user_name_align selleruser_name_align">{sellerName}</h4>
                <h6 className="herupdt_cnt">Your turn to act</h6>
              </div>
              )}
            <div class="row m-0">
              {orderData.timerInitialState !== null &&
                orderData.timerInitialState !== undefined &&
                orderData.timerInitialState > 0 && (
                  <div key={orderData.deliverytimefortimer} class="pt-4 d-flex font-weight-bold justify-content-center timer-section mx-auto mt-4 mb-0">
                    <TimerControl
                      deadLineHours={orderData.deliverytimefortimer}
                      startDate={orderData.revisionstartdate}
                      currentDate={orderData.currentdatetime}
                      direction="backward"
                    ></TimerControl>
                  </div>
                )
              }
            </div>
            <div class="pb-4 d-flex justify-content-center timerwords-section mx-auto mt-0 mb-4">
              <span class="timerwords">Days</span>
              <span class="timerwords">Hours</span>
              <span class="timerwords">Mins</span>
              <span class="timerwords">Seconds</span>
            </div>
            {((orderData.status != 9 && !props.isTimerEnd) || (orderData.status == 8 && props.isTimerEnd)) &&
              <div className="col-lg-12">
                <h5 className="herupdt_cnt">Here's your update!</h5>
              </div>
            }
            {orderData.status != 9 && !props.isTimerEnd &&
              <div className="orange_box mb-3">
                <p> {orderData.sellermessage} </p>
              </div>
            }
            {orderData.status == 8 && props.isTimerEnd &&
              <div className="grey_box mb-3">
                <p> {orderData.sellermessage} </p>
              </div>
            }
            {orderData.status == 8 && !orderData.isfeedbackcompleted &&
              <SubmitFeedback 
                {...props}
                setModalContent={(e) => props.setModalContent(e)}
                orderRedirect={props.orderRedirect}
                orderList={(e) => props.orderList(e)}
                orderId={orderData.id}
                reloadMainScreen={() => {
                  props.reloadMainScreen();
                }}
              />
            }
            {orderData.status == 8 && buyerFeedback != null ? (
              <div class="mt-3">
                <FeedbackView data={buyerFeedback} />
              </div>
            ) : null}
            <div className="revision_sections card">
              {revisionReqAttachArray.length > 0 ? (
                revisionReqAttachArray.map((revision) => (
                  <div class="row m-0 repeat_revisions mb-4">
                    <div class="revision_set">
                      <h4 class="w-100 revision_heading">
                        {revision.revisionNumber == 0 ? "Order Requirements" : "Revision #" + revision.revisionNumber}
                      </h4>
                    </div>
                    <div class="d-block w-100 comment_sec_secs">
                      <div class="d-flex flex-wrap align-items-center mb-2">
                        <img
                          src={buyerImage ? buyerImage : avatar}
                          onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                          alt="..."
                          width="65"
                          height="65"
                          class="rounded-circle"
                          data-pagespeed-url-hash="3993073969"
                          onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                        />
                        <h4 style={{margin:10}} class="user_name_set selleruser_name_set">{buyerName}</h4>
                        <div class="date_time_mnt_set ml-3">
                          <small>
                            {revision.buyerObj.date !== null
                              ? revision.buyerObj.date
                              : ""}
                          </small>
                        </div>
                      </div>
                      <div class="comments_sectionset">
                        {revision.buyerObj.comment.split("\n").map((i) => {
                          return <p>{i}</p>;
                        })}
                      </div>
                      {revision.buyerObj.attachments !== null &&
                        revision.buyerObj.attachments !== undefined &&
                        revision.buyerObj.attachments.length > 0 ? (
                        <div className="attachemnts_Set">
                          <h5 class="mid_head_side pl-3">Attachments</h5>
                          <div class="file_name_attach">
                            {revision.buyerObj.attachments !== null &&
                              revision.buyerObj.attachments !== undefined &&
                              revision.buyerObj.attachments.length > 0 ? (
                              revision.buyerObj.attachments.map((value) => (
                                <div class="uploaded_imgs">
                                  <a
                                    href={value.link}
                                    target="_blank"
                                    class="upolade_filename"
                                  >
                                    {value.attachmentName !== "" ? value.attachmentName : value.name}{" "}
                                  </a>
                                  <a
                                    href={value.link}
                                    target="_blank"
                                    download={value.link}
                                    class="download_link_img"
                                  >
                                    <img src={icnDownload} class="ml-2 img-fluid" />
                                  </a>
                                </div>
                              ))
                            ) : (
                              <div class="not_found">
                                No attachments found.
                              </div>
                            )}
                          </div>
                          <div class="date_time_mnt_set">
                            <small>
                              {revision.buyerObj.date !== null
                                ? revision.buyerObj.date
                                : ""}
                            </small>
                          </div>
                        </div>
                      ) : null}
                      {revision.sellerObj.comment.length > 0 ? (
                        <div class="d-block w-100">
                          <div class="d-flex flex-wrap align-items-center mb-2">
                            <img
                              src={sellerImage ? sellerImage : avatar}
                              onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                              alt="..."
                              width="65"
                              height="65"
                              class="rounded-circle"
                              data-pagespeed-url-hash="3993073969"
                              onload="pagespeed.CriticalImages.checkImageForCriticality(this);"
                            />
                            <h4 style={{margin:10}} class="user_name_set selleruser_name_set">{sellerName}</h4>
                            <div class="date_time_mnt_set ml-3">
                              <small>
                                {revision.sellerObj.date !== null
                                  ? revision.sellerObj.date
                                  : ""}
                              </small>
                            </div>
                          </div>
                          <div class="comments_sectionset">
                            {revision.sellerObj.comment.split("\n").map((i) => {
                              return <p>{i}</p>;
                            })}
                          </div>
                          {revision.sellerObj.attachments !== null &&
                            revision.sellerObj.attachments !== undefined &&
                            revision.sellerObj.attachments.length > 0 ? (
                            <div className="attachemnts_Set">
                              <h5 class="mid_head_side pl-3">Attachments</h5>
                              <div class="file_name_attach">
                                {revision.sellerObj.attachments !== null &&
                                  revision.sellerObj.attachments !== undefined &&
                                  revision.sellerObj.attachments.length > 0 ? (
                                  revision.sellerObj.attachments.map((value) => (
                                    <div class="uploaded_imgs float-left ml-2">
                                      <a
                                        href={value.link}
                                        target="_blank"
                                        class="upolade_filename"
                                      >
                                        {value.attachmentName !== "" ? value.attachmentName : value.name}{" "}
                                      </a>
                                      <a
                                        href={value.link}
                                        target="_blank"
                                        download={value.link}
                                        class="download_link_img"
                                      >
                                        <img src={icnDownload} class="ml-2" />
                                      </a>
                                    </div>
                                  ))
                                ) : (
                                  <div class="not_found">
                                    No attachments found.
                                  </div>
                                )}
                              </div>
                              &nbsp;
                              &nbsp;
                              &nbsp;
                              <div class="date_time_mnt_set">
                                <small>
                                  {revision.sellerObj.date !== null
                                    ? revision.sellerObj.date
                                    : ""}
                                </small>
                              </div>
                            </div>
                          ) : null}
                        </div>
                      ) : null}
                    </div>
                  </div>
                ))
              ) : (
                <div className="norec_found">No revisions found.</div>
              )}
            </div>
          </div>
         
          {orderData.revision <= orderData.max_revision &&
            buyerComment &&
            !sellerComment && (!props.isTimerEnd || (props.isTimerEnd && orderData.status != 9)) && 
            orderData.status != 8 ? (
            <div class="text-center w-100 pb-3">
              <div className="text-center">
                <button
                  class="btn_light_green"
                  data-toggle="modal"
                  data-target="#send_update"
                  onClick={() => {
                    setShowUpdateModal(true);
                  }}
                >
                  Submit Delivery
                </button>
                {!orderData.issellerextension && (!props.isTimerEnd || (props.isTimerEnd && orderData.status != 9)) &&
                  <button
                    style={{ marginLeft: 20 }}
                    class="btn_ebony_clay"
                    data-toggle="modal"
                    data-target="#send_update"
                    onClick={() => {
                      setExtendDialog(true)
                      // orderExtension();
                    }}
                  >
                    I need more time
                  </button>
                }
              </div>
            </div>
          ) : null}
          <Modal
            open={showUpdateModal}
            disableBackdropClick
            style={{ overflow: "scroll" }}
            close={() => closeSubmitModal}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
          >
            <div class="modal-dialog modal_popup" role="document">
              <div class="modal-content">
                <AttachmentLoading isVisible={isAttachmentLoading} />
                <div class="modal-header">
                  <h5 class="popup_title" id="exampleModalLabel">
                    Submit Delivery
                  </h5>
                  <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                    onClick={() => {
                      setShowUpdateModal(false);
                      setValidated(false);
                      setCharCount(0);
                      setDescriptionText("");
                      setBase64(null);
                      setIsImageCanceled(false);
                      setFile(null);
                      setIsFileError(false);
                    }}
                  >
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">
                  <Form
                    id="myform"
                    noValidate
                    validated={validated}
                    onSubmit={handlesubmit}
                  >
                    <Form.Row>
                      <div class="form-group mt-3 w-100">
                        <Form.Group controlId="exampleForm.ControlTextarea1">
                          <Form.Control
                            as="textarea"
                            placeholder="Describe your update in details..."
                            value={descriptionText}
                            rows={8}
                            required
                            onChange={e => {
                              if (e.target.value.length <= 2000) {
                                setCharCount(e.target.value.length);
                                setDescriptionText(e.target.value)
                              }
                            }}
                            maxLength={2000}
                          />
                          <Form.Control.Feedback type="invalid">
                            Please provide update details.
                          </Form.Control.Feedback>
                        </Form.Group>
                        <div class="d-flex p-2 justify-content-between border-bottom border-left border-right">
                          <p class="text-muted mb-0">{charCount}/2000</p>
                          <label for="upload_doc" className="mb-0">
                            <i className="fa fa-paperclip fa-lg text-muted link-cursor" title="Attach files"></i>
                            <FileUpload
                              required={true}
                              id="upload_doc"
                              multiple={true}
                              setBase64={setBase64}
                              file={file}
                              accept={"*/*"}
                              isFileCanceled={isImageCanceled}
                              isFileDeleted={isImageDeleted}
                              setIsImageCanceled={setIsImageCanceled}
                              setFile={setFile}
                              setIsFileError={setIsFileError}
                              size={20000}
                              error={isFileError}
                              errorMessage={"Please select file."}
                            />
                          </label>
                        </div>
                        <div>
                          {fileList.length > 0 && (
                            <ListGroup>{fileListHtml}</ListGroup>
                          )}
                        </div>
                      </div>
                    </Form.Row>
                  </Form>
                </div>
                <div class="modal-footer justify-content-center">
                  <button
                    type="button"
                    class="btn_light_green effect effect-2"
                    onClick={handlesubmit}
                  >
                    Submit Delivery
                  </button>
                </div>
              </div>
            </div>
          </Modal>
        </div>
      )}

      {extendDialog && (
        <ConfirmationDialog
          open={extendDialog}
          dialogTitle={"Extend Order"}
          dialogContentText={`Are you sure you need more time to deliver your order? An additional 72 hours will be added to your clock.`}
          cancelButtonText="No, I don’t need more time"
          okButtonText={"Yes, I am sure"}
          onCancel={() => {
            setExtendDialog(false);
          }}
          onClose={() => {
            setExtendDialog(false);
          }}
          onOk={(e) => {
            setExtendDialog(false);
            orderExtension();
          }}
        />
      )}
    </div>
  );
};

export default OrderDetail;
