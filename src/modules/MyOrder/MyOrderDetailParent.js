import React, { useState, useEffect } from "react";
import Pusher from 'pusher-js';
import { PUSHER_KEY, PUSHER_CLUSTER } from "../../common/constants";
import Loading from "../../components/core/Loading";
import ConfirmationDialog from "../../components/core/ConfirmationDialog/ConfirmationDialog";
import SubmitRequest from "../MarketPlace/SubmitRequest";
import Orderdetail from "./Seller/Orderdetail";
import UpdateDelivery from "./Buyer/UpdateDelivery";
import DeliveryRevision from "./Buyer/DeliveryRevision";
import { ToastAlert,PlaySound } from "../../utils/sweetalert2";

const MyOrderDetailParent = (props) => {
  const [isLoading, setLoading] = useState(null);
  const [confirmationModal, setConfirmationModal] = useState(false);
  const [modalContent, setModalContent] = useState("");
  const [orderRedirect, setOrderRedirect] = useState(false);
  const [orderDetail, setOrderDetail] = useState({});
  const [isView, setOnViewClick] = useState(false);
  const [isFeedbackScreen, setFeedbackScreen] = useState(false);
  const [isDisplayDeliveryRevision, setDisplayDeliveryRevision] = useState(
    false
  );

  useEffect(() => {
    if (isFeedbackScreen)
    orderDetailRedirect(props.match.params.id);
  }, [isFeedbackScreen]);

  useEffect(() => {
    if (props.match.params.id !== undefined && props.match.params.id) {
      orderDetailRedirect(props.match.params.id);
    }
  }, [props.match.params.id]);

  useEffect(() => {
    let userId = props.userData.id
		const pusher = new Pusher(PUSHER_KEY, {
			cluster: PUSHER_CLUSTER,
			encrypted: true
		});
		const notificationChannel = pusher.subscribe('GPM-NOTIFICATION-' + userId);
		notificationChannel.bind('NOTIFICATION', data => {
      if (props.match.params.id !== undefined && props.match.params.id && props.match.params.id == data.message.metadata.id) {
        setConfirmationModal(false);
        redirect(props.match.params.id);
      }
		});
  }, []);

  const redirect = (id) => {
    orderDetailRedirect(id);
    setOrderRedirect(true);
  }

  /**
   * rediredts to order detail page
   * @param {*} id 
   */
  const orderDetailRedirect = (id) => {
    setLoading(true);
    props.getOrderDetail(
      id,
      (response) => {
        setOrderDetail(response.data);
        if (props.userData.activeusertype === 1 ) {
          setOnViewClick(true);
        } else if (props.userData.activeusertype === 2) {
          setOnViewClick(false);
        }
        setLoading(false);
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound()
      }
    );
  }

  return (
    <div>
      <Loading isVisible={isLoading} />
      {orderDetail !== null && isLoading === false &&
        <div className="page-wrapper min-vh-100">
          <Loading isVisible={isLoading} />
          {isView ? (
            (props.userData.activeusertype === 1 &&
              <Orderdetail
                reloadMainScreen={() => {
                  setConfirmationModal(true);
                }}
                setModalContent={setModalContent}
                orderRedirect={orderRedirect}
                orderList={setOrderRedirect}
                orderId={orderDetail.id}
                {...props}
              />
            )
          ) : orderDetail.status == 6 ? (
            <SubmitRequest
              setModalContent={setModalContent}
              orderRedirect={orderRedirect}
              orderList={setOrderRedirect}
              orderId={orderDetail.id}
              data={orderDetail}
              reloadMainScreen={() => {
                setConfirmationModal(true);
              }}
              {...props}
            />
          ) : (!orderDetail.issellercommented &&
              !orderDetail.isbuyercommented &&
              orderDetail.revision > 0 &&
              orderDetail.revision < orderDetail.max_revision) ||
              isDisplayDeliveryRevision ? (
              orderDetail.status == 8 && orderDetail.isfeedbackcompleted ? (
                props.userData.activeusertype === 2 &&
                <UpdateDelivery
                  reloadMainScreen={() => {
                    setConfirmationModal(true);
                  }}
                  reloadScreen={() => {
                    setFeedbackScreen(true);
                  }}
                  startRevision={() => {
                    setDisplayDeliveryRevision(true);
                  }}
                  setModalContent={setModalContent}
                  orderRedirect={orderRedirect}
                  orderList={setOrderRedirect}
                  orderId={orderDetail.id}
                  {...props}
                >
                </UpdateDelivery>
              ) : (
                props.userData.activeusertype === 2 &&
                <DeliveryRevision
                  isVisible={isDisplayDeliveryRevision}
                  onClose={() => {
                    setDisplayDeliveryRevision(false);
                  }}
                  onSuccess={() => {
                    setFeedbackScreen(false);
                    setConfirmationModal(true);
                    setDisplayDeliveryRevision(false);
                  }}
                  setModalContent={setModalContent}
                  orderRedirect={orderRedirect}
                  orderList={setOrderRedirect}
                  orderId={orderDetail.id}
                  {...props}
                />
              )
            ) : (
              <UpdateDelivery
                reloadMainScreen={() => {
                  setConfirmationModal(true);
                }}
                reloadScreen={() => {
                  setFeedbackScreen(true);
                }}
                startRevision={() => {
                  setDisplayDeliveryRevision(true);
                }}
                setModalContent={setModalContent}
                orderRedirect={orderRedirect}
                orderList={setOrderRedirect}
                orderId={orderDetail.id}
                {...props}
              >
              </UpdateDelivery>
            )
          }
        </div>
      }
      {confirmationModal && (
        <ConfirmationDialog
            open={confirmationModal}
            dialogTitle={"Confirm Action"}
            dialogContentText={modalContent}
            cancelButtonText="Go to Order List"
            okButtonText={"OK"}
            onCancel={() => {
              setConfirmationModal(false);
              props.history.push("/myOrder")
              
            }}
            onOk={(e) => {
              setConfirmationModal(false);
              redirect(orderDetail.id);
            }}
            onClose={(e) => {
              setConfirmationModal(false);
              redirect(orderDetail.id);
            }}
        />
      )}
    </div>
  );
};
export default MyOrderDetailParent;
