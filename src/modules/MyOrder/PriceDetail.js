import React from "react";
import moment from "moment";

function PriceDetail(props) {

  /**
   * changes the given date to local date format
   * @param {*} dateTime
   * @returns
   */
  const orderTimeFormat = (dateTime) => {
    var utc = moment.utc(dateTime).toDate();
    var orderTime = moment(utc).local().format('MMM-DD-YYYY hh:mm A');
    return orderTime;
  }

  return (
    <div className="card mr-4">
      <div className="card-body">
        <h5 className="font-weight-bold">Price Details</h5>
        <div className="d-flex justify-content-between">
          <p className="text-muted">Price:</p>
          <p className="text-dark font-weight-bold text-right">
            ${props.orderDetail ? props.orderDetail.actualprice : ""}
          </p>
        </div>
        <div className="d-flex justify-content-between">
          <p className="text-muted">Service Fee:</p>
          <p className="text-success font-weight-bold text-right">
            ${props.orderDetail ? props.orderDetail.servicefees : ""}
          </p>
        </div>
        <div className="d-flex justify-content-between">
          <p className="text-muted ">Delivery time:</p>
          <p className="text-dark font-weight-bold text-right">
            {props.orderDetail ? props.orderDetail.deliverytime + " " + props.orderDetail.deliverytimein : ""}
          </p>
        </div>
        <div className="d-flex justify-content-between">
          <p className="text-muted ">Status:</p>
          <p className="text-info font-weight-bold text-right">
            {props.orderDetail ? props.orderDetail.statusname : ""}
          </p>
        </div>
        <div className="d-flex justify-content-between">
          <p className="text-muted ">Order:</p>
          <p className="text-dark font-weight-bold text-right">
            {props.orderDetail ? props.orderDetail.invoiceid : ""}
          </p>
        </div>
        <div className="d-flex justify-content-between">
          <p className="text-muted mb-0">Order date:</p>
          <p className="text-dark font-weight-bold mb-0 text-right">
            {props.orderDetail ? orderTimeFormat(props.orderDetail.created_at) : ""}
          </p>
        </div>
      </div>
      <div className="card-body">
        <div className="d-flex justify-content-between">
          <p className="text-dark font-weight-bold mb-0">Total:</p>
          <p className="text-dark font-weight-bold mb-0 text-right">
            ${props.orderDetail ? props.orderDetail.total : ""}
          </p>
        </div>
      </div>
    </div>
  );
}

export default PriceDetail;
