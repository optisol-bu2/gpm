import React from "react";
import { connect } from "react-redux";
import { getAbout,getFaq } from "../../Actions/aboutAction";
import Loading from "../../components/core/Loading";
import aboutpg from "../../assets/images/other-pages/professional-content-writer-e1596394802404.jpeg";
import about_icon from "../../assets/images/dashboard-icons/titel-icon-aboutgpm.svg";

/**
 * About Component
 */
class About extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
            about: '',
            tab: '',
            page: '',
            isLoading: null,
            faq:[]
		};

	}

    componentDidMount() {
        this.changeTab('faq', 'faq');
	}

    /**
     * changes tab, page and loads data
     * @param {*} page 
     * @param {*} tab 
     */
    changeTab = (page, tab) => {
        this.setState({ page: page, tab: tab }, () => {
            if (this.state.page != "faq")
              this.getAbout(page);
            else
              this.getFaq();
        });
        
    }

    /**
     * gets about data
     */
    getAbout = (type) => {
        this.setState({ isLoading: true, about: '' });
        this.props.getAbout(type,
            (about) => {
                if (about.data) {
                    this.setState({ about: about.data, isLoading: false });
                }
            },
            (error) => {
                this.setState({ isLoading: false, about: '' });
            }
        ); 
    }

    /**
    * gets faq data
    */
    getFaq = () => {
        this.setState({ isLoading: true, faq: [] });
        this.props.getFaq(
            (response) => {
                if (response) {
                    this.setState({ faq: response.data, isLoading: false });
                }
            },
            (error) => {
                this.setState({ isLoading: false, faq: '' });
            }
        ); 
    }

    /**
     * main render
     * @returns 
     */
	render() {
        const { about, page, tab, isLoading, faq } = this.state;
		return (
            <div class="page-wrapper">
                <Loading isVisible={isLoading} />
                <div className="page_title row m-0 mb-4 pb-2">
                    <div class="col-lg-1 col-md-2 col-3 p-0">
                        <span className="left_icon_title">
                            {/* <img src={about_icon} alt="" /> */}
                            <i class="fa fa-question-circle"></i>
                        </span>
                    </div>
                    <div class="col-lg-11 col-md-10 col-9 p-0 d-flex align-items-center">
                        <h2 className="right_title row m-0">FAQ</h2>
                    </div>
                </div>
                <div class="grey_box_bg">
                    <div class="row m-0 mb-3">
                        {/* <div class="col-md-7 col-lg-7 col-xl-7 pl-5 pr-5 pt-4 pb-0 pad_sets_zero order-2 order-sm-1 order-md-1 order-lg-1"> */}
                        {/* <div class={tab == "faq" ? "col-md-12 col-lg-12 col-xl-12 pl-5 pr-5 pt-4 pb-0 pad_sets_zero" : "col-md-7 col-lg-7 col-xl-7 pl-5 pr-5 pt-4 pb-0 pad_sets_zero order-2 order-sm-1 order-md-1 order-lg-1"}> */}
                        <div class="col-md-12 col-lg-12 col-xl-12 pl-5 pr-5 pt-4 pb-0 pad_sets_zero">
                        {/* <div class="col-md-12 col-lg-12 col-xl-12 pl-5 pr-5 pt-4 pb-0 pad_sets_zero "> For FAQ */}
                            <div class="about_pg_menu mb-5 mt-3">
                                <div class="dropdown">
                                    <button class={tab == "faq" ? "btn_menu_item dropdown-toggle active" : "btn_menu_item dropdown-toggle"} type="button" id="faqmenu" onClick={() => { this.changeTab('faq', 'faq') }}>
                                        FAQ
                                    </button>
                                </div>
                                <div class="dropdown">
                                    <button class={tab == "about" ? "btn_menu_item dropdown-toggle active" : "btn_menu_item dropdown-toggle"} type="button" id="aboutmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        About
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="aboutmenu">
                                        <a className="dropdown-item" onClick={() => { this.changeTab('career', 'about') }}>About Us</a>
                                        <a className="dropdown-item" onClick={() => { this.changeTab('privacypolicy', 'about') }}>Privacy Policy</a>
                                        <a className="dropdown-item" onClick={() => { this.changeTab('termsofservice', 'about') }}>Terms of Service</a>
                                    </div>
                                </div>
                                <div class="dropdown">
                                    <button class={tab == "support" ? "btn_menu_item dropdown-toggle active" : "btn_menu_item dropdown-toggle"} type="button" id="supportmenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Support
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="supportmenu">
                                        <a className="dropdown-item" onClick={() => { this.props.history.push("/help") }}>Help &amp; Support</a>
                                        {/* <a className="dropdown-item" onClick={() => { this.changeTab('trustandsafety', 'support') }}>Trust &amp; Safety</a> */}
                                        <a className="dropdown-item" onClick={() => { this.changeTab('sellingongpm', 'support') }}>Selling on EngineScale</a>
                                        <a className="dropdown-item" onClick={() => { this.changeTab('buyingongpm', 'support') }}>Buying on EngineScale</a>
                                    </div>
                                </div>
                                <div class="dropdown">
                                    <button class={tab == "community" ? "btn_menu_item dropdown-toggle active" : "btn_menu_item dropdown-toggle"} type="button" id="communitymenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Community
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="communitymenu">
                                    <   a className="dropdown-item" onClick={() => { this.props.history.push("/blogs") }}>Blog</a>
                                        <a className="dropdown-item" onClick={() => { this.props.history.push("/invite") }}>Invite a Friend</a>
                                    </div>
                                </div>
                            </div>
                            <div class="btm_about_content">
                                <div className="tab-content" id="v-pills-tabContent">
                                    {page == "faq" &&
                                        <div>
                                            <div class="card_div">                   
                                                <h4 class="about_pg_title">FAQ</h4>   
                                                <div class="">                
                                                    {faq && faq.map(name => (  
                                                    <div class=""> 
                                                        <span class="mt-4 mb-4"> 
                                                            <b><strong>{name.question}</strong></b></span>
                                                        <a dangerouslySetInnerHTML={{ __html: name.answer}} /> 
                                                    </div>
                                                    ))}   
                                                </div>  
                                            </div>
                                        </div>
                                    }
                                    {page == "career" &&
                                        <div>
                                            <div class="card_div">                                            
                                                <h4 class="about_pg_title">About Us</h4>
                                                <div class="">
                                               
                                                    <p class="" dangerouslySetInnerHTML={{ __html: about}}></p>
                                                    </div>
                                                    </div>
                                            
                                        </div>
                                    }
                                    {page == "privacypolicy" &&
                                    <div>
                                        <div class="card_div">
                                            <h4 class="about_pg_title">Privacy Policy</h4>
                                            <div class=""><p class="" dangerouslySetInnerHTML={{ __html: about}}></p></div>
                                        </div>
                                    </div>
                                    }
                                    {page == "termsofservice" &&
                                    <div>
                                        <div class="card_div">
                                            <h4 class="about_pg_title">Terms of Service</h4>
                                            <div class=""><p class="" dangerouslySetInnerHTML={{ __html: about}}></p></div>
                                        </div>
                                    </div>
                                    }
                                    {/* {page == "helpandsupport" &&
                                    <div>
                                        <div class="card_div">
                                            <h4 class="about_pg_title">Help &amp; Support</h4>
                                            <div class="scrollbarsmooth"><p class="" dangerouslySetInnerHTML={{ __html: about}}></p></div>
                                        </div>
                                    </div>
                                    } */}
                                    {/* {page == "trustandsafety" &&
                                    <div>
                                        <div class="card_div">
                                            <h4 class="about_pg_title">Trust &amp; Safety</h4>
                                            <div class="scrollbarsmooth"><p class="" dangerouslySetInnerHTML={{ __html: about}}></p></div>
                                        </div>
                                    </div>
                                    } */}
                                    {page == "sellingongpm" &&
                                    <div>
                                        <div class="card_div">
                                            <h4 class="about_pg_title">Selling on EngineScale</h4>
                                            <div class=""><p class="" dangerouslySetInnerHTML={{ __html: about}}></p></div>
                                        </div>
                                    </div>
                                    }
                                    {page == "buyingongpm" &&
                                    <div>
                                        <div class="card_div">
                                            <h4 class="about_pg_title">Buying on EngineScale</h4>
                                            <div class=""><p class="" dangerouslySetInnerHTML={{ __html: about}}></p></div>
                                        </div>
                                    </div>
                                    }
                                    {/* {page == "blog" &&
                                    <div>
                                        <div class="card_div">
                                            <h4 class="about_pg_title">Blog</h4>
                                            <div class="scrollbarsmooth"><p class="">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est</p></div>
                                        </div>
                                    </div>
                                    } */}
                                    {/* {page == "invite_a_friend" &&
                                    <div>
                                        <div class="card_div">
                                            <h4 class="about_pg_title">Invite a Friend</h4>
                                            <div class="scrollbarsmooth"><p class="" dangerouslySetInnerHTML={{ __html: about}}></p></div>
                                        </div>
                                    </div>
                                    } */}
                                </div>                        
                            </div>
                        </div>
                        {/* <div class="col-md-5 col-lg-5 col-xl-5 p-0 order-1 order-sm-2 order-md-2 order-lg-2 aboutus_imgsets "> */}

                        {/* <div class={tab == "faq" ? "col-md-5 col-lg-5 col-xl-5 p-0 order-1 order-sm-2 order-md-2 order-lg-2 aboutus_imgsets d-none " : "col-md-5 col-lg-5 col-xl-5 p-0 order-1 order-sm-2 order-md-2 order-lg-2 aboutus_imgsets"}> */}

                        {/* d-none For FAQ*/}
                            {/* <img src={aboutpg} alt="" className="img-fluid bor_rad_set" /> */}
                        {/* </div>  */}
                    </div>
                </div>
			</div>
		);
	}
}

const mapStateToProps = ({ }) => {
	return {
	}
}


export default connect(mapStateToProps, {
    getAbout,getFaq
})(About);
