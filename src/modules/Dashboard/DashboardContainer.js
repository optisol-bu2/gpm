import { connect } from "react-redux";

import DashboardParent from "./DashboardParent";
import {
  getDashboard,
  getDataTotalSpent,
  getStatistics,
  analyticsDetails,
  updateAnalytics,
  getDataTrafficAnalytics,
  getDataDailyTraffic,
  setTotalOrganicValue,
} from "../../Actions/actionContainer";

const mapStateToProps = (state) => {
  return {
    clientId: state.loginReducer.clientId,
    viewId: state.loginReducer.viewId,
    organicValue: state.loginReducer.organicValue,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getDashboard: (onSuccess, onFailure) =>
      dispatch(getDashboard(onSuccess,onFailure)),
    getDataTotalSpent: (params, onSuccess, onFailure) =>
      dispatch(getDataTotalSpent(params, onSuccess, onFailure)),
    getStatistics: (id, onSuccess, onFailure) =>
      dispatch(getStatistics(id, onSuccess, onFailure)),
    analyticsDetails: (params, onSuccess, onFailure) =>
      dispatch(analyticsDetails(params, onSuccess, onFailure)),
    updateAnalytics: (data) => dispatch(updateAnalytics(data)),
    getDataTrafficAnalytics: (params, onSuccess, onFailure ) =>
      dispatch(getDataTrafficAnalytics(params, onSuccess, onFailure )),
    getDataDailyTraffic: (params, onSuccess, onFailure) =>
      dispatch(getDataDailyTraffic(params, onSuccess, onFailure)),
    setTotalOrganicValue: (params, onSuccess, onFailure) =>
      dispatch(setTotalOrganicValue(params, onSuccess, onFailure)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(DashboardParent);
