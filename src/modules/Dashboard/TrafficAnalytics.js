import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import moment from 'moment';
import organic_traffic_icon from "../../assets/images/dashboard-icons/organic_traffic_icon.svg";

const useStyles = makeStyles({
    table: {
        minWidth: 650,
    },
});


export default function TrafficAnalytics(props) {
    const [trafficAnalytics, setTrafficAnalytics] = useState(false);


    useEffect(() => {
        setTrafficAnalytics(props.trafficAnalytics);
    }, [props.trafficAnalytics]);

    /**
     * changes the given time to date format
     * @param {*} dateTime
     * @returns
     */
    const timeFormat = (dateTime) => {
        var utc = moment.utc(dateTime).toDate();
        var chatTime = moment(utc).local().format('MMM-DD-YYYY');
        return chatTime
    }

    const classes = useStyles();

    return (        
        <div class="traffic_analysis grid_bg">
            <div class="card-body">
                <h5 class="sub_title_dash"><span class="icon_span_bg"><i class="far fa-chart-bar"></i></span><a onClick={() => props.history.push("/myOrder")}>My Recent orders</a></h5>
                <div class="grid_des orange_table">
                    <TableContainer style={{ height: 265 }} component={Paper}>
                        <Table className="table flex-table"  >
                            <TableHead>
                                <TableRow>
                                    <TableCell component="th"><b>ID</b></TableCell>
                                    <TableCell component="th"><b>Date</b></TableCell>
                                    <TableCell component="th"><b>Type</b></TableCell>
                                    <TableCell component="th"><b>Cost</b></TableCell>
                                    <TableCell component="th"><b>Download Documents</b></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {trafficAnalytics && trafficAnalytics.length > 0 && trafficAnalytics.map((row) => (
                                    <TableRow key={row.id}>
                                        <TableCell scope="right"><strong><Link to={'/myOrder/'+row.id}>{row.invoiceid}</Link></strong></TableCell>
                                        <TableCell scope="right">{row.date ? row.date == "Ongoing" ? row.date : timeFormat(row.date) : ""}</TableCell>
                                        <TableCell scope="right">{row.type}</TableCell>
                                        <TableCell scope="right">${row.cost}</TableCell>
                                        <TableCell scope="right">
                                            {row.file_url == "" && 
                                                "-"
                                            }
                                            {row.file_url != "" && 
                                                <a href={row.file_url} target="_blank" download={row.file_url} class="upolade_filename">
                                                    <span className="fa fa-download"></span> 
                                                </a>
                                            } &nbsp; 
                                            {row.attachmentfilename}
                                        </TableCell>
                                    </TableRow>
                                ))}
                                {trafficAnalytics && trafficAnalytics.length === 0 && 
                                    <TableRow>
                                        <TableCell scope="center"></TableCell>
                                        <TableCell scope="center"></TableCell>
                                        <TableCell scope="center">No Record Found</TableCell>
                                    </TableRow>
                                }
                            </TableBody>
                        </Table>
                    </TableContainer>
                </div>
            </div>
        </div>        
    )
}