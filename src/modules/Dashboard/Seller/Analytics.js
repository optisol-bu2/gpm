import React, { useState, useEffect } from "react";
import { Bar } from 'react-chartjs-2';
import organic_traffic_icon from "../../../assets/images/dashboard-icons/organic_traffic_icon.svg";

require ('../../../components/core/RoundedBars')

export default function Analytics(props) {
    const [labelData, setLabelData] = useState([]);
    const [currentData, setCurrentData] = useState([]);
    const [earnedData, setEarnedData] = useState([]);
    const [completedData, setCompletedData] = useState([]);

    const data = {
        labels: labelData,
        datasets: [
            {
                label: 'Current',
                data: currentData,
                backgroundColor:'#636d78',
                pointStyle: 'rectRounded',
            },
            {
                label: 'Earned',
                data: earnedData,
                backgroundColor:'#e25e4a',
                pointStyle: 'rectRounded',
            },
            {
                label: 'Completed',
                data: completedData,
                backgroundColor:'#f2c66f',
                pointStyle: 'rectRounded',
            }
        ],
    }

    const options = {
        legend: {
            labels: {
                usePointStyle: true,
            },
        },
        cornerRadius: 40,
        maintainAspectRatio : false,
        scales: {
            yAxes: [
                {
                    ticks: {
                        beginAtZero: true,
                    },
                },
            ],
        },
        datasets: {
            bar: {
                barPercentage: 0.2,
            }
        },
    }

    useEffect(() => {
        if (labelData && labelData.length !== 0) {
            var completedData = [];
            var currentData = [];
            var earnedData = [];
            if (props.analytics.completed && props.analytics.completed.length !== 0) {
                labelData.forEach((label) => {
                    var completed = null;
                    props.analytics.completed.forEach((completedData) => {
                        if (label == Object.keys(completedData)[0])
                        completed = Object.values(completedData)[0];
                    });
                    completedData.push(completed);
                })
            }
            if (props.analytics.current && props.analytics.current.length !== 0) {
                labelData.forEach((label) => {
                    var current = null;
                    props.analytics.current.forEach((currentData) => {
                        if (label == Object.keys(currentData)[0])
                        current = Object.values(currentData)[0];
                    });
                    currentData.push(current);
                })
            }
            if (props.analytics.earned && props.analytics.earned.length !== 0) {
                labelData.forEach((label) => {
                    var earned = null;
                    props.analytics.earned.forEach((earnedData) => {
                        if (label == Object.keys(earnedData)[0])
                        earned = Object.values(earnedData)[0];
                    });
                    earnedData.push(earned);
                })
            }
            setCompletedData(completedData);
            setCurrentData(currentData);
            setEarnedData(earnedData);
        }
    }, [labelData])

    useEffect(() => {
        if (props.analytics) {
            var labelData = [];
            if (props.analytics.completed && props.analytics.completed.length !== 0) {
                props.analytics.completed.forEach((completed) => {
                    if (!labelData.includes(Object.keys(completed)[0]))
                    labelData.push(Object.keys(completed)[0]);
                });
            }
            if (props.analytics.current && props.analytics.current.length !== 0) {
                props.analytics.current.forEach((current) => {
                    if (!labelData.includes(Object.keys(current)[0]))
                    labelData.push(Object.keys(current)[0]);
                });
            }
            if (props.analytics.earned && props.analytics.earned.length !== 0) {
                props.analytics.earned.forEach((earned) => {
                    if (!labelData.includes(Object.keys(earned)[0]))
                    labelData.push(Object.keys(earned)[0]);
                });
            }
            if (labelData.length !== 0) {
                labelData.sort();
                setLabelData(labelData)
            }
        }
    }, [props.analytics]);

    return (
        <div class="traffic_analysis grid_bg_white">
            <div class="card-body">
                <h5 class="sub_title_dash col-lg-12 p-0 mb-3"><span class="icon_span_bg"><i class="far fa-chart-bar"></i></span>Analytics</h5>
                <div className="analytics_data_trf">
                    <div className='header'></div>
                    {labelData && labelData.length !== 0 &&
                        <div class="chart-container" style={{position: "relative", height:"350px", width:"934px"}}>
                            <Bar data={data} options={options} />
                        </div>
                    }
                    {(!labelData || labelData.length == 0) &&
                        <h5 className="norec_found">No Data Found</h5>
                    }
                </div>
            </div>
        </div>
    )
}