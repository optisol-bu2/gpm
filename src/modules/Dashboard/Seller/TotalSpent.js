import React from "react";
import pink_hand from "../../../assets/images/dashboard-icons/pink_hand.svg";
import graph_line1 from "../../../assets/images/dashboard-icons/graph_lines1.svg";
import about from "../../../assets/images/menu-icons/about-gpm-white.svg";
import { Popup } from 'semantic-ui-react'

export default function TotalSpent(props) {

    return (
    <div class="card dash-box1 mx-md-auto mb-3">
        <div class="card-body p-3"> 
            <div className="row m-0">
                <div className="col-lg-7 col-md-8 p-0">
                    <span class="d-block mb-4"><img src={pink_hand} alt="" className="img-fluid"  /></span>
                    <h5 className="payment_count">${props.totalSpent}</h5>
                    <span className="clear_pay_text">Earned</span>
                </div>
                <div className="col-lg-5 col-md-4 p-0">
                    <span class="mb-5 d-block w-100 text-right popup-image">
                        <Popup
                            trigger={<i class="fa fa-info-circle colr_white"></i>}
                            // trigger={<img src={about} alt="" className="img-fluid link-cursor"/>}
                        >
                            <Popup.Header>
                            Earned
                            </Popup.Header>
                            <Popup.Content>
                            {"Total amount earned is $"+props.totalSpent}
                            </Popup.Content>
                        </Popup>
                    </span>
                    <span class="pt-2 d-block w-100"><img src={graph_line1} alt="" className="img-fluid" width="100px" /></span>
                </div>
            </div>                    
        </div>
    </div>                          
    )
}