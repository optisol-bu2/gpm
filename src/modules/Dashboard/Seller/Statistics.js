import React from "react";
import statistics_icon from "../../../assets/images/dashboard-icons/statistics_icon.svg";
import graph_line2 from "../../../assets/images/dashboard-icons/graph_lines2.png";
import current_order from "../../../assets/images/dashboard-icons/current_order_icon.svg";
import complete_order from "../../../assets/images/dashboard-icons/complete_order_icon.png";
import earn_order from "../../../assets/images/dashboard-icons/earn.png";
import moment from 'moment';
import about from "../../../assets/images/menu-icons/about-gpm-white.svg";
import { Popup } from 'semantic-ui-react'

export default function Statistics(props) {

    return (
        <div class="card dash-box2">
            <div class="card-body">
                <div class="row m-0 mb-3">
                    <h5 class="sub_title_dash col-lg-4 pl-2"><span class="icon_span_bg"><i class="fas fa-chart-line"></i></span>Statistics</h5>
                    <div class="col-lg-8 col-md-12 col-sm-12 p-0">
                        <div class="row m-0">
                            <div class="col-lg-12 col-md-12 col-sm-12 mb-0">
                                <img class="img-fluid" src={graph_line2} alt="" />
                            </div>
                        </div>                       
                    </div>                      
                </div>
                <div class="row m-0">                                    
                    <div class="col-lg-4 col-md-4 col-sm-6 pl-0">
                        <div class="media media_1">
                            <span class="d-flex mr-3 "><img class="img-fluid" src={current_order} alt=""  /></span>
                            <div class="media-body">
                                <span class="float-right popup-image">
                                    <Popup
                                        trigger={<i class="fa fa-info-circle colr_white"></i>}
                                        // trigger={<img src={about} alt="" className="img-fluid link-cursor mb-2" width="20" height="20"/>}
                                    >
                                        <Popup.Header>
                                        Current Orders
                                        </Popup.Header>
                                        <Popup.Content>
                                        {"You have "+(props.statistics.currentorders != undefined ? props.statistics.currentorders : 0)+ " current orders"}
                                        </Popup.Content>
                                    </Popup>
                                </span>
                                <h5 class="count_statics">{props.statistics.currentorders}</h5>
                                <p class="cont_statis">Current Orders</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 pl-0">
                        <div class="media media_2">
                            <span class="d-flex mr-3"><img class="img-fluid" src={earn_order} alt=""  /></span>
                            <div class="media-body">
                                <span class="float-right popup-image">
                                    <Popup
                                        trigger={<i class="fa fa-info-circle colr_white"></i>}
                                        // trigger={<img src={about} alt="" className="img-fluid link-cursor mb-2" width="20" height="20"/>}
                                    >
                                        <Popup.Header>
                                        Earned in {moment().subtract(1, "month").startOf("month").format('MMMM')}
                                        </Popup.Header>
                                        <Popup.Content>
                                        {"You have earned $"+(props.statistics.earnlastmonth != undefined ? props.statistics.earnlastmonth : 0)+ " in "+moment().subtract(1, "month").startOf("month").format('MMMM')}
                                        </Popup.Content>
                                    </Popup>
                                </span>
                                <h5 class="count_statics">${props.statistics.earnlastmonth}</h5>
                                <p class="cont_statis">Earned in {moment().subtract(1, "month").startOf("month").format('MMMM')}</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 pl-0">
                        <div class="media media_3">
                            <span class="d-flex mr-3"><img class="img-fluid" src={complete_order} alt="" /></span>
                            <div class="media-body">
                                <span class="float-right popup-image">
                                    <Popup
                                        trigger={<i class="fa fa-info-circle colr_white"></i>}
                                        // trigger={<img src={about} alt="" className="img-fluid link-cursor mt-2" width="20" height="20"/>}
                                    >
                                        <Popup.Header>
                                        Completed Orders
                                        </Popup.Header>
                                        <Popup.Content>
                                        {"You have completed "+(props.statistics.completeorders != undefined ? props.statistics.completeorders : 0)+" orders"}
                                        </Popup.Content>
                                    </Popup>
                                </span>
                                <h5 class="count_statics">{props.statistics.completeorders}</h5>
                                <p class="cont_statis">Completed Orders</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}