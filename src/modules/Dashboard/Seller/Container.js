import { connect } from "react-redux";
import SellerDashboard from "./SellerDashboard";
import { getDashboard, getDataTotalSpent, getStatistics, getDataTrafficAnalytics, getDataDailyTraffic } from "../../../Actions/actionContainer";

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = dispatch => {
  return {
    getDashboard: (onSuccess, onFailure) =>
      dispatch(getDashboard(onSuccess,onFailure)),
    getDataTotalSpent: (params, onSuccess, onFailure) =>
      dispatch(getDataTotalSpent(params, onSuccess, onFailure)),
    getStatistics: (id, onSuccess, onFailure) =>
      dispatch(getStatistics(id, onSuccess, onFailure)),
    getDataTrafficAnalytics: (params, onSuccess, onFailure ) =>
      dispatch(getDataTrafficAnalytics(params, onSuccess, onFailure )),
    getDataDailyTraffic: (params, onSuccess, onFailure) =>
      dispatch(getDataDailyTraffic(params, onSuccess, onFailure)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(SellerDashboard);
