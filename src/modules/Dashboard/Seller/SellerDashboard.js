import React, { useState, useEffect } from "react";
import TotalSpent from "./TotalSpent";
import Statistics from "./Statistics";
import Analytics from "./Analytics";
import { ToastAlert,PlaySound } from "../../../utils/sweetalert2";
import Loading from "../../../components/core/Loading";
import dashboard from "../../../assets/images/menu-icons/dashboard-white.svg";

export default function SellerDashboard(props) {
  const [loading, setLoading] = useState(null);
  const [totalSpent, setTotalSpent] = useState(0);
  const [statistics, setStatistics] = useState({});
  const [analytics, setAnalytics] = useState({});


  useEffect(() => {
    getDashboard();
  }, []);

  const getDashboard = () => {
    setLoading(true);
    props.getDashboard(
      (response) => {
        if (response.data) {
          setTotalSpent(response.data.clearpayments)
          setStatistics(response.data.statistics)
          setAnalytics(response.data.graph)
        }
        setLoading(false);
      },
      (error) => {
        setLoading(false);
        ToastAlert('error', error.message);
        PlaySound();
      }
    );
  };

  return (
    <div className="page-wrapper row m-0 mb-3">
      <div className="container m-0">
        <div className="page_title row m-0 w-100 mb-3">
          <div class="col-lg-1 col-md-2 col-3 p-0">
            <span className="left_icon_title">
              {/* <img src={dashboard} alt="" /> */}
              <i class="fa fa-dashboard"></i>
            </span>
          </div>
          <div class="col-lg-11 col-md-10 col-3 p-0">
              <h2 className="right_title row m-0"> Content Writer Dashboard </h2>
              <span className="title_tagline m-0 row">Welcome {props.userData ? props.userData.username : ""}</span>
          </div>
        </div>
        <div class="row w-100 justify-content-between " data-tour="sellerDashboard">
          <div class="col-lg-4 col-xl-3 col-md-12 col-sm-12">
            <TotalSpent
              totalSpent={totalSpent}
              {...props}
            />
          </div>
          <div class="col-lg-8 col-xl-9 col-md-12 col-sm-12">
            <Statistics
              statistics={statistics}
              {...props}
            />
          </div>
        </div>
        <div class="row w-100 m-0">
          <div class="col-12 p-0 mt-4">
            <Analytics
              analytics={analytics}
              {...props}
            />
          </div>
        </div>
        <Loading isVisible={loading}></Loading>
      </div>
    </div>
  );
};