import React, { useState, useEffect } from "react";
import statistics_icon from "../../assets/images/dashboard-icons/statistics_icon.svg";
import graph_line2 from "../../assets/images/dashboard-icons/graph_lines2.png";
import current_order from "../../assets/images/dashboard-icons/current_order_icon.svg";
import complete_order from "../../assets/images/dashboard-icons/complete_order_icon.png";
import earn_order from "../../assets/images/dashboard-icons/earn.png";
import Select from 'react-select';
import { ToastAlert,PlaySound } from "../../utils/sweetalert2";
import about from "../../assets/images/menu-icons/about-gpm-white.svg";
import { Popup } from 'semantic-ui-react';

export default function Statistics(props) {
    const [statistics, setStatistics] = useState({});
    const [filterValue, setFilterValue] = useState({});
    const [filterOptions, setFilterOptions] = useState([
        { label: "Last 7 days" , value: "7"}, { label: "Last 30 days" , value: "30"},
        { label: "Last 6 Months" , value: "180"}, { label: "All Time" , value: ""},
    ]);

    useEffect(() => {
        setFilterValue(filterOptions[0])
        getStatistics(7);
    }, [props.statistics]);


    /**
     * gets statistics data
     * @param {*} days 
     */
    const getStatistics = (days) => {
        props.getStatistics(days,
            (response) => {
                if (response.data) {
                    setStatistics(response.data.statistics);
                }
            },
            (error) => {
                ToastAlert('error', error.message);
                PlaySound();
            }
        );
    };


    return (
        <div class="card dash-box2">
            <div class="card-body">
                <div class="row m-0 mb-3">
                    <h5 class="sub_title_dash col-lg-4 pl-2"><span class="icon_span_bg"><i class="fas fa-chart-line"></i></span>Statistics</h5>
                    <div class="col-lg-8 col-md-12 col-sm-12 p-0">
                        <div class="row m-0">
                            <div class="col-lg-7 col-md-7 col-sm-12 p-0 mb-3">
                                <img class="img-fluid" src={graph_line2} alt="" />
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-5 p-0">
                                <div class="form-group selectpicker_form">
                                    <Select
                                        className="basic-single-select"
                                        isSearchable={false}
                                        onChange={(selected) => {
                                            setFilterValue(selected);
                                            getStatistics(selected.value);
                                        }}
                                        classNamePrefix={"select_dropdown"}
                                        value={filterValue}
                                        options={filterOptions}
                                    />
                                </div>
                            </div>
                        </div>                       
                    </div>                      
                </div>
                <div class="row justify-content-center m-0">                                    
                    <div class="col-lg-4 col-md-4 col-sm-6 pl-2">
                        <div class="media media_1">
                            <span class="d-flex mr-3 "><img class="img-fluid" src={current_order} alt=""  /></span>
                            <div class="media-body">
                                <span class="float-right popup-image">
                                    <Popup
                                       trigger={<i class="fa fa-info-circle colr_white"></i>}
                                    //    trigger={<img src={about} alt="" className="img-fluid link-cursor mb-2" width="20" height="20"/>}
                                    >
                                        <Popup.Header>
                                        Content Writing Orders
                                        </Popup.Header>
                                        <Popup.Content>
                                        {"You have "+(statistics.contentwritingorderscount != undefined ? statistics.contentwritingorderscount : 0)+ " content writing orders"}
                                        </Popup.Content>
                                    </Popup>
                                </span>
                                <h5 class="count_statics">{statistics.contentwritingorderscount}</h5>
                                <p class="cont_statis">Content Writing Orders</p>
                            </div>
                        </div>
                    </div>
                    {/* <div class="col-lg-4 col-md-4 col-sm-6 pl-2">
                        <div class="media media_2">
                            <span class="d-flex mr-3 "><img class="img-fluid" src={complete_order} alt=""  /></span>
                            <div class="media-body">
                                <span class="float-right popup-image">
                                    <Popup
                                        trigger={<img src={about} alt="" className="img-fluid link-cursor mb-2" width="20" height="20"/>}
                                    >
                                        <Popup.Header>
                                        Outreach Orders
                                        </Popup.Header>
                                        <Popup.Content>
                                        {"You have "+(statistics.outreachorderscount != undefined ? statistics.outreachorderscount : 0)+ " outreach orders"}
                                        </Popup.Content>
                                    </Popup>
                                </span>
                                <h5 class="count_statics">{statistics.outreachorderscount}</h5>
                                <p class="cont_statis">Outreach Orders</p>
                            </div>
                        </div>
                    </div>
                     */}
                    <div class="col-lg-4 col-md-4 col-sm-6 pl-2">
                        <div class="media media_3">
                            <span class="d-flex mr-3"><img class="img-fluid" src={earn_order} alt="" /></span>
                            <div class="media-body">
                                <span class="float-right popup-image">
                                    <Popup
                                        trigger={<i class="fa fa-info-circle colr_white posreltop"></i>}
                                        // trigger={<img src={about} alt="" className="img-fluid link-cursor mb-4" width="20" height="20"/>}
                                    >
                                        <Popup.Header>
                                        Organic Traffic
                                        </Popup.Header>
                                        <Popup.Content>
                                        {"You have " + props.organicValue+" organic traffic"}
                                        </Popup.Content>
                                    </Popup>
                                </span>
                                <h5 class="count_statics">{props.organicValue}</h5>
                                <p class="cont_statis">Organic Traffic</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}