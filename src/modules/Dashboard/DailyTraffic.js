import * as React from "react";
import { useEffect, useState } from "react";
import { useAnalyticsApi } from "react-use-analytics-api";
import { AuthorizeButton, SignOutButton } from "react-analytics-charts";
import { DatePicker } from 'antd';
import moment from 'moment';
import Modal from "@material-ui/core/Modal";
import { Line } from 'react-chartjs-2';
import { ToastAlert,PlaySound } from "../../utils/sweetalert2";
import Loading from "../../components/core/Loading";
import ViewProgressDialoge from "../../components/core/ViewProgressDialoge";
import GraphLoading from "../../components/core/ChatLoading";
import "antd/dist/antd.css";
import * as dayjs from 'dayjs';
import daily_traffic_icon from "../../assets/images/dashboard-icons/daily_traffic_icon.svg";

const { RangePicker } = DatePicker;

const DailyTraffic = (props) => {
    const [loading, setLoading] = useState(null);
    const [analyticsLoading, setAnalyticsLoading] = useState(false);
    const [isGraphLoading, setGraphLoading] = useState(false);
    const [noData, setNoData] = useState(false);
    const [startDateValue, setStartDateValue] = useState(dayjs().subtract(7, 'day').format('YYYY-MM-DD'));
    const [endDateValue, setEndDateValue] = useState(dayjs().format('YYYY-MM-DD'));
    const [dimensionsValue, setDimensionValue] = useState("ga:date")
    const [selectType, setSelectType] = useState('');
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [googleClientId, setGoogleClientId] = useState('');
    const [googleViewId, setGoogleViewId] = useState('');
    const [labelData, setLabelData] = useState([]);
    const [sessionData, setSessionData] = useState([]);
    const dateFormat = 'YYYY-MM-DD';
    const clientId = props.clientId;
    const viewId = props.viewId;
    const { ready, gapi, authorized } = useAnalyticsApi();

    const options = {
        tooltips: {
            callbacks: {
               title: function(value, data) {
                    var label;
                    if (selectType == 'Hour') {
                        label = dayjs(value[0].xLabel).format('D MMM, YYYY hh:mm A')
                    } else if (selectType == 'Day') {
                        label = dayjs(value[0].xLabel).format('D MMM, YYYY')
                    } else if (selectType == 'Week') {
                        var year = value[0].xLabel.slice(0, 4)
                        var week = value[0].xLabel.slice(4, 6)
                        label = "Week "+week+", "+year
                    } else if (selectType == 'Month') {
                        label = dayjs(value[0].xLabel).format('MMM YYYY')
                    }
                    return label;
               }
            }
        },
        scales: {
            yAxes: [
                {
                    display: true,
                    gridLines: {
                        display      : true,
                        color        : '#e8e8e8',
                        zeroLineColor: 'transparent',
                    },
                    lineWidth: 0.5,
                    ticks: {
                        beginAtZero: true,
                        autoSkip: true,
                        maxTicksLimit: 4,
                    },
                }
            ],
            xAxes: [
                {
                    display: true,
                    gridLines: {
                        display: false,
                    },
                    ticks: {
                        maxRotation: 0,
                        autoskip: true,
                        autoSkipPadding: 50,
                        maxTicksLimit: 8,
                        callback: function(value, index, values) {
                            var label;
                            if (selectType == 'Hour') {
                                label = dayjs(value).format('D MMM')
                            } else if (selectType == 'Day') {
                                label = dayjs(value).format('D MMM')
                            } else if (selectType == 'Week') {
                                var year = value.slice(0, 4)
                                var week = value.slice(4, 6)
                                label = "Week "+week+", "+year
                            } else if (selectType == 'Month') {
                                label = dayjs(value).format('MMM YYYY')
                            }
                            return label;
                        }
                    }
                },
            ],
        },
    }
    
    
    const LineData = {
        labels: labelData,
        datasets: [
          {
            label: 'Sessions',
            fill: true,
            lineTension: 0.1,
            backgroundColor: '#eafaf1',
            borderColor: '#f2c66f',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: '#32ca75',
            pointBackgroundColor: '#32ca75',
            pointBorderWidth: 2,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: '#eafaf1',
            pointHoverBorderColor: '#32ca75',
            pointHoverBorderWidth: 3,
            pointRadius: 3,
            data: sessionData
          }
        ]
      };

    useEffect(() => {
        if (gapi !== undefined) {
            gapi.analytics.auth.on('signOut', function() {
                var data = {
                    clientId: null,
                    viewId: null
                }
                props.updateAnalytics(data);
                window.location.reload();
                props.setTotalOrganicValue('0');
            });
        }
    }, [gapi])

    /**
     * checks graph data api fot errors
     */
    const checkGraph = () => {
        setGraphLoading(true);
        var report = new gapi.analytics.report.Data({
            query: {
                ids: 'ga:'+viewId,
                "start-date": startDateValue,
                "end-date": endDateValue,
                metrics: "ga:sessions",
                dimensions: dimensionsValue,
            }
        });
        report.on('error', function(response) {
            props.setTotalOrganicValue("0");
            setGraphLoading(false);
            setAnalyticsLoading(false);
            setNoData(true);
            setLabelData([])
            setSessionData([])
            ToastAlert('error', "No Data Found.");
            PlaySound();
        });
        report.on('success', function(response) {
            setNoData(false);
            if (response.totalsForAllResults && response.totalsForAllResults['ga:sessions'])
                props.setTotalOrganicValue(response.totalsForAllResults['ga:sessions']);
            if (response.rows) {
                var labelData = [];
                var sessionData = [];
                response.rows.forEach((row) => {
                    labelData.push(row[0])
                    sessionData.push(row[1])
                })
                setLabelData(labelData)
                setSessionData(sessionData)
                setGraphLoading(false);
                setAnalyticsLoading(false);
            }
        });
        report.execute();
    }
    
    const dateHandleChange = (date, stringDate) => {
        setStartDateValue(stringDate.length > 0 ? stringDate[0] ? stringDate[0] : dayjs().subtract(7, 'day').format('YYYY-MM-DD') : dayjs().subtract(7, 'day').format('YYYY-MM-DD'));
        setEndDateValue(stringDate.length > 1 ? stringDate[1] ? stringDate[1] : dayjs().format('YYYY-MM-DD') : dayjs().format('YYYY-MM-DD'))
    }

    useEffect(() => {
        if (authorized) {
            if (selectType === '')
            setSelectType('Day')
            setAnalyticsLoading(true);
            setNoData(false);
        } else {
            setAnalyticsLoading(false);
            setNoData(true);
        }
    }, [authorized])

    useEffect(() => {
        if (selectType)
        checkGraph();
    }, [selectType])

    useEffect(() => {
        if (selectType && startDateValue)
        checkGraph();
    }, [startDateValue])

    useEffect(() => {
        if (selectType && endDateValue)
        checkGraph();
    }, [endDateValue ])

    const getCurrentGoogleData = (type) => {
        switch (type) {
            case 'Hour':
                setDimensionValue("ga:dateHour");
                setSelectType('Hour');
                break;
            case 'Day':
                setDimensionValue("ga:date");
                setSelectType('Day');
                break;
            case 'Week':
                setDimensionValue("ga:yearWeek");
                setSelectType('Week');
                break;
            case 'Month':
                setDimensionValue("ga:yearMonth");
                setSelectType('Month');
                break;
        }
    }

    /**
     * closes the modal
     */
    const closeModal = () => {
        setIsModalOpen(false);
        setGoogleClientId('');
        setGoogleViewId('');
    }

    const analyticsDetails = () => {
        if (googleClientId && googleViewId) {
            let params = {
                clientid: googleClientId,
                viewid: googleViewId,
            }
            setLoading(true);
            props.analyticsDetails(params,
                (response) => {
                    setLoading(false);
                    if (response.data) {
                        closeModal();
                        var data = {
                            clientId: response.data.analytics_clientid,
                            viewId: response.data.analytics_viewid
                        }
                        props.updateAnalytics(data);
                        window.location.reload();
                        props.setTotalOrganicValue('0');
                    }
                },
                (error) => {
                    setLoading(false);
                    ToastAlert('error', error.message);
                    PlaySound();
                }
            );
        }
    }

    return (
        <div class="traffic_analysis grid_bg" data-tour="dashboard-ga">
            <Loading isVisible={loading}></Loading>           
            <div class="card-body">
                <div class="row m-0 mb-3">
                    <h5 class="sub_title_dash col-lg-6 p-0"><span class="icon_span_bg"><i class="fas fa-chart-area"></i></span>Daily Traffic</h5>
                    <div class="col-lg-6 p-0 text-right">
                        <button class="btn_green effect effect-2 float-right" onClick={(e) => {e.currentTarget.blur(); setIsModalOpen(true)}}>Connect</button>
                    </div>
                </div>
                <div class="row m-0">
                    <div class="col-lg-6 col-md-12 col-sm-12 mb-3 col-12 p-0">
                    {(!analyticsLoading && noData) ? "":
                        <div className="float-left">
                            <RangePicker
                                value={[moment(startDateValue, dateFormat), moment(endDateValue, dateFormat)]}
                                format={dateFormat}
                                onChange={dateHandleChange}
                            />
                        </div> }
                    </div>
                    <div class="col-lg-6 col-md-12 col-sm-12 mb-3 col-12 p-0">                       
                        <div className="float-right">
                        {(!analyticsLoading && noData) ? "":
                            <div className="daily-trafic-view" role="group" aria-label="Basic example">
                                <button type="button" className={selectType === 'Hour' ? "btn switch_btn active-tab" : 'switch_btn'} onClick={() => getCurrentGoogleData('Hour')}>Hourly</button>
                                <button type="button" className={selectType === 'Day' ? "btn switch_btn active-tab" : 'switch_btn'} onClick={() => getCurrentGoogleData('Day')}>Day</button>
                                <button type="button" className={selectType === 'Week' ? "btn switch_btn active-tab" : 'switch_btn'} onClick={() => getCurrentGoogleData('Week')}>Week</button>
                                <button type="button" className={selectType === 'Month' ? "btn switch_btn active-tab" : 'switch_btn'} onClick={() => getCurrentGoogleData('Month')}>Month</button>
                            </div>}
                        </div>
                    </div>
                </div>
                <div class="row m-0">   
                    {(clientId == null || clientId == "" || viewId == null || viewId == "") &&
                        // <h4 className="graph-maintainance">Please connect to view sessions graph here</h4>
                        <h4 className="graph-maintainance sub_title_dash">Please connect to your  Google Analytics here to view the organic traffic of your website</h4>
                    }
                    {(clientId !== null && clientId !== "" && viewId !== null && viewId !== "") &&
                    <div className="lineChart mt-4 col-lg-12 p-0">
                        {analyticsLoading &&
                            <div className="loaderStyle">
                                <ViewProgressDialoge />
                            </div>
                        }
                        {ready && gapi !== undefined && clientId !== undefined && clientId !== null && clientId !== "" &&
                            <AuthorizeButton
                                gapi={gapi}
                                authOptions={{ clientId }}
                            />
                        }
                        {ready && authorized && (
                            <div>
                            <SignOutButton gapi={gapi} />
                            </div>
                        )}
                        {(!analyticsLoading && noData) &&
                            <h3 className="graph-maintainance">No Data Found.You should see the organic search traffic grow as you publish more blog posts with EngineScale. Learn how to use EngineScale.</h3>
                        }
                        {labelData.length > 0 && sessionData.length > 0 &&
                            <div>
                                <GraphLoading isVisible={isGraphLoading} />
                                <Line
                                    data={LineData}
                                    width={300}
                                    height={100}
                                    options={options}
                                />
                            </div>
                        }
                    </div>
                }
                <Modal
                    open={isModalOpen}
                    disableBackdropClick
                    onClose={closeModal}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description">
                    <div class="modal-dialog modal_popup" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="popup_title" id="exampleModalLabel">Enter Analytics Details</h5>
                                <button
                                    type="button"
                                    class="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                    onClick={closeModal}
                                >
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body form_fields_white_bg">
                                <div class="flex-fill mb-3 mx-3">
                                    <input type="text" name="message" placeholder="Client ID" class="input_field_w" value={googleClientId} onChange={(e) => {setGoogleClientId(e.target.value)}} />
                                </div>
                                <div class="flex-fill mb-3 mx-3">
                                    <input type="text" name="message" placeholder="View ID" class="input_field_w" value={googleViewId} onChange={(e) => {setGoogleViewId(e.target.value)}} />
                                </div>
                                <ul className="analytics-list pl-3">
                                    EngineScale allows you to connect to your Google Analytics by entering your Client ID and View ID to see the performance of your website grow as you publish more blog posts.
                                </ul>
                                <ul className="analytics-list pl-3">
                                    Please see the following instructions:
                                </ul>
                                <ul className="analytics-list pl-3">
                                    <ul className="analytics-list pl-0 font-weight-bold">
                                    Client ID
                                    </ul>
                                    <li className="analytics-list ml-4">
                                        {"Go to Google API Console > Credentials > OAuth client ID > Copy Client ID"}
                                    </li>
                                </ul>
                                <ul className="analytics-list pl-3">
                                   <ul className="analytics-list pl-0 font-weight-bold">
                                    View ID
                                    </ul>
                                    <li className="analytics-list ml-4">
                                        {"Go to Google Analytics > Admin > View > View Settings > Basic Settings > Copy View ID"}
                                    </li>
                                </ul>
                            </div>
                            <div class="modal-footer">
                                <button class="btn_green" disabled={!googleClientId || !googleViewId} onClick={(e) => {e.currentTarget.blur(); analyticsDetails();}}>Submit</button>
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        </div>
    </div>
    )

}

export default DailyTraffic