import React from "react";
import { connect } from "react-redux";
import Loading from "../../components/core/Loading";
import ChatLoading from "../../components/core/ChatLoading";
import { ToastAlert, PlaySound } from '../../utils/sweetalert2';
import Pusher from 'pusher-js';
import { PUSHER_KEY, PUSHER_CLUSTER } from "../../common/constants";
import InfiniteScroll from 'react-infinite-scroll-component';
import inbox from "../../assets/images/dashboard-icons/inbox-icon.svg";
import moment from 'moment';
import {
	sendMessage,
	getChats,
	getChatMessages,
	searchChatList,
	updateMessages,
    updateChatMessages,
    sendAttachment,
    deleteMessage,
    removeMessage,
    rejectCustomOrder,
    customOrderDetail,
    changeMessage,
} from "../../Actions/inboxAction";
import { addCartCustom } from "../../Actions/actionContainer";
import avatar from "../../assets/images/avatar.png";
import defaultImage from "../../assets/images/placeholder.jpeg";
import { Popup } from 'semantic-ui-react';

/**
 * Inbox Component
 */
class Inbox extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			message: "",
			receiverId: "",
			receiverImage: "",
			receiverName: "",
            receiverReview: "",
            receiverLastLoggingTime: "",
            receiverBio: "",
			searchText: "",
			hasMore: true,
			pageIndex: 1,
            postFile: [],
			isLoading: null,
			isChatLoading: false,
            searchMessageText: "",
            searchArray: [],
            searchIds: [],
            searchUp: '',
            searchDown: '',
            searchId: '',
            file_size:0,
		};
	}

	componentDidMount() {
        this.mounted = true;
		let userId = this.props.userData.id
		const pusher = new Pusher(PUSHER_KEY, {
			cluster: PUSHER_CLUSTER,
			encrypted: true
		});
		const channel = pusher.subscribe('GPM-' + userId);
		channel.bind('PERSONAL_MESSAGES', data => {
            if(data.message.sendfrom == this.state.receiverId) {
                this.props.updateMessages(data.message);
                this.props.getChats();
                if(this.mounted) {
                    this.changeChat(this.state.receiverId, true);
                }
            } else {
                this.props.getChats((chats) => {
                    if(this.mounted) {
                        if (chats && chats.data.length != 0) {
                            this.changeChat(this.state.receiverId, true);
                        }
                    }
                })
            }

		});
		const deleteChannel = pusher.subscribe('GPM-DELETE-MESSAGE-' + userId);
		deleteChannel.bind('DELETE_PERSONAL_MESSAGES', data => {
            if (data.message[0].sendfrom == this.state.receiverId || data.message[0].sendto == this.state.receiverId)
            this.props.removeMessage(data.message[0]);
		});
		const rejectChannel = pusher.subscribe('GPM-REJECT-CUSTOM-ORDER-' + userId);
		rejectChannel.bind('REJECT_CUSTOM_ORDERS', data => {
            if (data.message.sendfrom == this.state.receiverId || data.message.sendto == this.state.receiverId)
            this.props.changeMessage(data.message);
		});
		const deleteCustomChannel = pusher.subscribe('GPM-DELETE-CUSTOM-ORDER-' + userId);
		deleteCustomChannel.bind('DELETE_CUSTOM_ORDERS', data => {
            if (data.message.sendfrom == this.state.receiverId || data.message.sendto == this.state.receiverId)
            this.props.changeMessage(data.message);
		});
		const startCustomChannel = pusher.subscribe('GPM-START-CUSTOM-ORDER-' + userId);
		startCustomChannel.bind('START_CUSTOM_ORDERS', data => {
            if (data.message.sendfrom == this.state.receiverId || data.message.sendto == this.state.receiverId)
            this.props.changeMessage(data.message);
		});
        const completeCustomChannel = pusher.subscribe('GPM-ORDER-COMPLETED-' + userId);
		completeCustomChannel.bind('ORDER_COMPLETED', data => {
            if (data.message.sendfrom == this.state.receiverId || data.message.sendto == this.state.receiverId)
            this.props.changeMessage(data.message);
		});
        this.getChats();
	}

    componentWillUnmount(){
        this.mounted = false;
    }

    /**
     * send message function for both text and file types
     */
	sendMessage = () => {
        if(this.state.message) {
            let params = {
                "message": this.state.message,
                "to": this.state.receiverId
            }
            this.props.sendMessage(params, (message) => {
                this.checkSearch();
            });
            this.props.getChats();
            this.setState({ message: "" });
        }
        if(this.state.postFile && this.state.postFile.length > 0) {
            this.state.postFile.forEach(file => {
                let postData = new FormData();
                postData.append("attachment", file);
                postData.append("to", this.state.receiverId);
                this.props.sendAttachment(postData);
            })
            this.props.getChats();
            this.setState({ postFile: [] });
        }

	}

    /**
     * handles on enter to send message
     * @param {*} e 
     */
    onEnterPress = e => {
        if (e.keyCode === 13) {
          this.sendMessage();
        }
    };

    /**
     * function to get chat list for the user
     */
	getChats = () => {
        this.setState({ isLoading: true });
		this.props.getChats(
            (chats) => {
                if(this.mounted) {
                    if (chats && chats.data.length != 0) {
                        this.setState({ receiverId: chats.data[0].userid, 
                            receiverImage: chats.data[0].image ,
                            receiverName: chats.data[0].name,
                            receiverBio: chats.data[0].bio,
                            receiverReview: chats.data[0].review,
                            receiverLastLoggingTime: chats.data[0].lastlogin_at
                        }, () => {
                            this.changeChat(chats.data[0].userid)
                        });
                    }
                }
                this.setState({ isLoading: false });
		    },
            (error) => {
                this.setState({ isLoading: false });
                ToastAlert("error", error.message);
                PlaySound();
            }
        )
	}

    /**
     * searches user matching searchText in chat list
     */
	searchChat = () => {
        var params = {
            searchText: this.state.searchText,
        };
		this.props.searchChatList(params);
	}

    /**
     * gets messages for the selected chat
     * @param {*} receiverId
     */
	changeChat = (receiverId, load) => {
        if (load === undefined) {
            this.setState({
                isChatLoading: true,
                searchArray: [],
                searchIds: [],
                searchUp: '',
                searchId: '',
                searchDown: '',
                searchMessageText: '',
                message: '',
                postFile: [],
            })
        }
        this.setState({ receiverId: receiverId.toString() })
        var params = {
            receiverId: receiverId,
            pageIndex: 1,
        };
		this.props.getChatMessages(params,
            (chats) => {
                this.props.getChats();
                this.setState({ isChatLoading: false });
                if (this.props.chatList.length != 0) {
                    this.props.chatList.forEach(chat => {
                        if (chat.userid == receiverId) {
                            this.setState({ 
                                receiverImage: chat.image ? chat.image : avatar, 
                                receiverName: chat.name,
                                receiverReview: chat.review,
                                receiverLastLoggingTime: chat.lastlogin_at,
                                receiverBio: chat.bio,
                                pageIndex: 1, hasMore: true });
                        }
                        if(this.props.chatHistoryDetails.total <= 10 ) {
                            this.setState({  hasMore: false });
                        }
                    })
                }
                if (load !== undefined) {
                    this.checkSearch();
                }
            },
            (error) => {
                this.setState({ isChatLoading: false });
                ToastAlert("error", error.message)
                PlaySound()
            }
        )
	}

    /**
     * gets more messages for a particular chat
     */
    fetchMoreData = () => {
        const { receiverId } = this.state;
        let maxpage = Math.ceil(this.props.chatHistoryDetails.total/this.props.chatHistoryDetails.loadafter)
        if (this.state.pageIndex < maxpage) {
            this.setState({ pageIndex: this.state.pageIndex+1 }, () => {
                var params = {
                    receiverId: receiverId,
                    pageIndex: this.state.pageIndex,
                };
                this.props.updateChatMessages(params, (messages) => {
                    this.checkSearch();
                });
            });
        } else {
            this.setState({ hasMore: false });
        }

    }

    /**
     * handles changes in the event and updates message state
     * @param {*} e
     */
	onChangeText = (e) => {
		this.setState({ [e.target.name]: e.target.value });
	};

    /**
     * handles changes in the event and updates searchText state
     * @param {*} e
     */
	onChangeSearchText = (e) => {
		if (e.target.value.length >= 3) {
			this.setState({ [e.target.name]: e.target.value }, () => {
				this.searchChat();
			});
		} else {
			this.setState({ [e.target.name]: e.target.value }, () => {
                if (!this.state.searchText) {
                    this.props.getChats();
                    this.changeChat(this.state.receiverId);
                }
            });
		}
	};

    /**
     * checks whether search is on or not
     */
    checkSearch = () => {
        if (this.state.searchMessageText !== '') {
            var search = {
                target: {
                    value: this.state.searchMessageText,
                    name: "searchMessageText"
                }
            }
            this.onChangeSearchMessageText(search);
        }
    }

    /**
     * scrolls to the element
     */
     scrollToNode = (node) => {
        node.scrollIntoView({ behavior: 'smooth' });
    }

    /**
     * handles changes in the event and updates searchMessageText state
     * @param {*} e
     */
	onChangeSearchMessageText = (e) => {
        const { chatMessages } = this.props;
		if (e.target.value) {
			this.setState({ [e.target.name]: e.target.value }, () => {
                var searchArray = [];
                var searchIds = [];
                if (chatMessages && chatMessages.length != 0) {
                    chatMessages.forEach(chatMessage => {
                        if (chatMessage.message != null && chatMessage.message != "") {
                            if (chatMessage.message.toLowerCase().includes(this.state.searchMessageText.toLowerCase())) {
                                searchArray.push(chatMessage.node);
                                searchIds.push(chatMessage.id);
                            }
                        }
                    });
                    this.setState({
                        searchArray: searchArray,
                        searchIds: searchIds,
                        searchUp: (searchIds && searchIds.length !== 0) ? 1 : '',
                        searchId: (searchIds && searchIds.length !== 0) ? searchIds[1] : '',
                        searchDown: (searchIds && searchIds.length !== 0) ? 1 : '',
                    }, () => {
                        this.scrollToMessage(1);
                    });
                    
                }
			});
		} else {
			this.setState({
                [e.target.name]: e.target.value,
                searchArray: [],
                searchIds: [],
                searchUp: '',
                searchId: '',
                searchDown: '',
            });
		}
	};

    /**
     * scrolls to the searched message
     * @param {*} scrollOption 
     */
    scrollToMessage = (scrollOption) => {
        const { searchArray, searchIds, searchUp, searchDown } = this.state;
        if (scrollOption == 0 && searchUp !== '') {
            if (searchUp === searchArray.length - 1) {
                this.setState({ 
                    searchUp: 0,
                    searchDown: 0,
                    searchId: searchIds[0]
                }, () => {
                    this.scrollToNode(searchArray[this.state.searchUp])
                });
            } else {
                this.setState({
                    searchUp: this.state.searchUp + 1,
                    searchDown: this.state.searchUp + 1,
                    searchId: searchIds[this.state.searchUp + 1]
                }, () => {
                    this.scrollToNode(searchArray[this.state.searchUp])
                });
            }
        } else if (scrollOption == 1 && searchDown !== '') {
            if (searchDown === 0) {
                this.setState({
                    searchDown: searchArray.length - 1,
                    searchUp: searchArray.length - 1,
                    searchId: searchIds[searchArray.length - 1]
                }, () => {
                    this.scrollToNode(searchArray[this.state.searchDown])
                });
            } else {
                this.setState({
                    searchDown: this.state.searchDown - 1,
                    searchUp: this.state.searchDown - 1,
                    searchId: searchIds[this.state.searchDown - 1]
                }, () => {
                    this.scrollToNode(searchArray[this.state.searchDown])
                });
            }
        }
    }

    /**
     * assigns file in the postFile state
     * @param {*} e
     */
    onFileChange = (e) => {
        if (e.target.files && e.target.files.length != 0) {
            this.state.file_size = e.target.files[0].size + this.state.file_size;
            if (e.target.files.length <= 5) {
                if ( this.state.file_size >= 8388608) {
                    this.setState({
                        postFile: []
                    })
                    ToastAlert("error", "File size exceeds maximum limit 8 MB.");
                    PlaySound()
                }
                else{
                    this.setState({
                        postFile: [ ...e.target.files]
                    })
                    let button = document.getElementById("message-send");
                    button.focus();
                }
            } else {
                this.setState({
                    postFile: []
                })
                ToastAlert("error", "You can only upload a maximum of 5 files at a time")
                PlaySound()
            }
        }
    };

    /**
     * removes file from the postFile state
     * @param {*} e
     */
    deleteFile = (e) => {
        const files = this.state.postFile.filter((item, index) => index !== e);
        this.setState({
            postFile: files
        })
    };

    /**
     * changes the given time to chat time format
     * @param {*} dateTime
     * @returns
     */
    chatTimeFormat = (dateTime) => {
        if(!dateTime) return "" ; 
        var utc = moment.utc(dateTime).toDate();
        var utcDate = moment.utc(dateTime).format('YYYY-MM-DD');
        var todayDate = moment.utc().format('YYYY-MM-DD');
        if (utcDate === todayDate) {
            var chatTime = moment(utc).local().format('hh:mm A');
        } else {
            var chatTime = moment(utc).local().format('MMM DD, YYYY hh:mm A');
        } 
        return chatTime
    }

    chatTimeFormatForLastLogin = (dateTime) => {
        if(!dateTime) return "" ; 
        var utc = moment.utc(dateTime).toDate();
        var utcDate = moment.utc(dateTime).format('YYYY-MM-DD');
        var todayDate = moment.utc().format('YYYY-MM-DD');
        var utcTime = moment(utc).local().format("DD-MM-YYYY HH:mm:ss");
        var todayTime = moment().local().format("DD-MM-YYYY HH:mm:ss");
        var ms = moment(todayTime,"DD/MM/YYYY HH:mm:ss").diff(moment(utcTime,"DD/MM/YYYY HH:mm:ss"));
        var duration = moment.duration(ms);
        if (utcDate === todayDate) {
            var getMinutes = parseInt(duration.as('minutes'));
            var chatTime = getMinutes + " minutes ago"; 
            if(parseInt(getMinutes) == 1) {
                var chatTime = getMinutes + " minute ago";
            }
            if(parseInt(getMinutes) >= 60) {
                var getHours = parseInt(duration.as('hours'));
                var chatTime = getHours + " hours ago"; 
                if(parseInt(getHours) == 1) {
                    var chatTime = getHours + " hour ago";
                }
            }
        } else {
            var getDays = parseInt(duration.as('days'));
            var chatTime = getDays + " days ago";
            if(parseInt(getDays) == 1) {
                var chatTime = getDays + " day ago";
            }
        } 
        return chatTime
    }

    /**
     * adds custom order to cart
     */
    addCartCustom = (id) => {
        this.setState({ isLoading: true });
        let params = {
            customorder: id,
        };
        this.props.addCartCustom(params,
            (customorder) => {
                this.setState({ isLoading: false });
                PlaySound()
                this.props.history.push("/cartdetails");       
            },
            (error) => {
                this.setState({ isLoading: false });
                ToastAlert("error", error.message);
                PlaySound()
            }
        )
    }

    /**
     * deletes message
     * @param {*} id 
     */
    deleteMessage = (id) => {
        this.setState({ isLoading: true });
        this.props.deleteMessage(id,
            () => {
                this.setState({ isLoading: false });
            },
            (error) => {
                this.setState({ isLoading: false });
                ToastAlert("error", error.message);
                PlaySound()
            }
        )
    }

    /**
     * rejects custom order
     * @param {*} id 
     */
    rejectCustomOrder = (id) => {
        this.setState({ isLoading: true });
        let params = {
            id : id
        }
        this.props.rejectCustomOrder(params,
            (reject) => {
                this.props.changeMessage(reject.data);
                this.setState({ isLoading: false });
            },
            (error) => {
                this.setState({ isLoading: false });
                ToastAlert("error", error.message);
                PlaySound()
            }
        )
    }

    /**
     * renders chat list
     * @returns
     */
    renderChatList () {
        const { receiverId, searchText } = this.state;
		const { chatList } = this.props;
        return (
            <div class="user-list border-right">
                <div class="d-flex p-3 align-items-center">
                    <div class="user-info mr-3">
                        <img 
                            src={this.props.userData ? this.props.userData.profilepic ? this.props.userData.profilepic : avatar : avatar} 
                            onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                            class="rounded-circle"
                            alt=""
                        />
                        <span class="user-status on"></span>
                    </div>
                    <div class="form-group mb-0 flex-fill">
                        <div className="search_bar_div">
                            <input type="text" name="searchText" class="form-control user-search-box" placeholder="Search..." value={searchText} onChange={this.onChangeSearchText} />
                            <span className="fa fa-search search_icon"></span>
                        </div>
                    </div>
                </div>
                <p class="inbox_title px-3">Inbox</p>
                {chatList.length == 0 &&
                    <div class="d-flex px-3 py-2 user-list-item align-items-center">
                        <p class="font-weight-bold mb-0 user-name text-break text-break">No Chats Found</p>
                    </div>
                }
                <div className="chat_ht_scroll scrollbarsmooth">
                    {chatList.length != 0 && chatList.map(chat => {
                        return (
                            <div class={"d-flex px-3 py-2 user-list-item align-items-center " + ((chat.userid == receiverId) ? "active" : "")} onClick={() => { this.changeChat(chat.userid) }}>
                                <div class="user-info mr-3">
                                    <img 
                                        src={chat.image ? chat.image : avatar}
                                        onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                                        class="rounded-circle"
                                        alt=""
                                    />
                                    <span class="user-status on"></span>
                                </div>
                                <div class="flex-fill max_width_80">
                                    <div class="d-block count_design">
                                        <p class="font-weight-bold mb-0 user-name text-truncate">{chat.name}</p>
                                        <span class="on-date small timeFont-Right">{this.chatTimeFormat(chat.lastmessage.created_at)}</span>
                                        <span className="badge">
                                            {chat.unread != 0 ? chat.unread : ""}
                                        </span>
                                    </div>
                                    {/* <p class="mb-0 user-bio text-truncate" >{((chat.lastmessage.message == null || chat.lastmessage.message == "") && chat.lastmessage.attachment == null)? "Custom Order" : ((chat.lastmessage.message == null || chat.lastmessage.message == "") && chat.lastmessage.attachment != null) ? "File" : chat.lastmessage.message}</p> */}
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>
        )
    }

    /**
     * renders chat messages
     * @returns
     */
    renderChatMessages () {
        const { receiverId, receiverImage, searchIds, searchId } = this.state;
		const { chatMessages } = this.props;
        return (
            <div class="min-100 d-flex flex-wrap align-content-end">
                {chatMessages && chatMessages.length != 0 && chatMessages.map(chat => {
                    if (chat.sendfrom == receiverId) {
                        return (
                            <div class="user-chat user-response">
                                <div className="d-flex align-items-start">
                                    <div class="user-info">
                                        <img 
                                            src={receiverImage}
                                            onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                                            class="rounded-circle"
                                            alt=""
                                        />
                                    </div>  
                                    <div class="user_msgs">                            
                                        {(chat.message != null && chat.message != "") &&
                                        <div class="card p-2 shadow-sm leftchatbox">
                                            <span className="fa fa-caret-left left_iconset"></span>
                                            <p id={chat.id} ref={(node) => chat.node = node} class={(searchIds && searchIds.length !== 0) ? searchIds.includes(chat.id) ? (searchId == chat.id) ? "mb-0 background-blue" : "mb-0 background-yellow" : "mb-0": "mb-0"}>{chat.message}</p>
                                        </div>
                                        }
                                        {(chat.message == null || chat.message == "") && chat.document != null &&
                                        <div class="card p-2 shadow-sm">
                                            <span className="fa fa-caret-left left_iconset"></span>
                                            <span>
                                                {chat.document.match(/.(jpg|jpeg|png|gif|svg|pdf|mp4|webm|ogv)$/i) &&
                                                    <a data-fancybox="files" href={chat.document}>
                                                        {chat.document.match(/.(jpg|jpeg|png|gif|svg)$/i) &&
                                                            <img src={chat.document} alt="" height="auto" width="300"/>
                                                        }
                                                        {chat.document.match(/.(pdf)$/i) &&
                                                            <i class="fa fa-file-pdf-o fa-3x white-icon" aria-hidden="true" title={chat.name !== "" ? chat.name : chat.document.split("/uploads/message/").pop()}></i>
                                                        }
                                                        {chat.document.match(/.(mp4|webm|ogv)$/i) &&
                                                            <i class="fa fa-file-video-o fa-3x white-icon" aria-hidden="true" title={chat.name !== "" ? chat.name : chat.document.split("/uploads/message/").pop()}></i>
                                                        }
                                                    </a>
                                                }
                                                {!chat.document.match(/.(jpg|jpeg|png|gif|svg|pdf|mp4|webm|ogv)$/i) &&
                                                    <a href={chat.document} target="_blank">
                                                        <i class="fa fa-file-text-o fa-3x white-icon" aria-hidden="true" title={chat.name !== "" ? chat.name : chat.document.split("/uploads/message/").pop()}></i>
                                                    </a>
                                                }
                                            </span>
                                        </div>
                                        }
                                        {(chat.message == null || chat.message == "") && chat.document == null && chat.customorderid.isdelete != 1 &&
                                            <span></span>
                                        }
                                        {(chat.message == null || chat.message == "") && chat.document == null && chat.customorderid.isdelete != 1 &&
                                            <div className="custom_order_left" id={chat.id}>
                                                <h5 class="bg_light_gry_set"><span className="custom_head"><strong>Custom Order</strong></span><span class="mb-0 float-right"><strong>${chat.customorderid.price}</strong></span></h5>
                                                <div class="body_cutome_order">
                                                    <div class="estimate_delvd_days row m-0 mb-3">
                                                        <div class="img_cusord col-lg-8 p-0">
                                                            <a data-fancybox="custom-order-files" href={chat.customorderid.servicepic}>
                                                                <img                                                                   
                                                                    src={chat.customorderid.servicepic}
                                                                    onError={(e)=>{e.target.onerror = null; e.target.src=defaultImage}}
                                                                    className="card-img-top"
                                                                    alt="..."
                                                                />
                                                            </a>
                                                        </div>
                                                        <div class="est_delvery col-lg-4 p-0"><p>Estimated Delivery</p><span class="days_count"><strong>{chat.customorderid.estimatedelivery.name}</strong></span></div>
                                                    </div> 
                                                    <p class="mb-0"><span class="mb-0">{chat.customorderid.description}</span></p>
                                                </div>                                             
                                                <div class="footer_cutome_order">
                                                    {chat.customorderid.reject == 0 && chat.customorderid.isstarted == false && this.props.userData.activeusertype == 2 && 
                                                        <button class="btn btn-outline-danger rounded link-cursor float-left" type="button" onClick={() => { this.rejectCustomOrder(chat.customorderid.id) }}>Reject</button>
                                                    }
                                                    {chat.customorderid.reject == 0 && chat.customorderid.isstarted == false && this.props.userData.activeusertype == 2 &&
                                                        <button class="btn_light_green float-right effect effect-2" type="button" onClick={() => { this.addCartCustom(chat.customorderid.id) }}>Add to Cart</button>
                                                    }
                                                    {chat.customorderid.reject != 0 && 
                                                        <span className="text-muted float-right">You rejected this order</span>
                                                    }
                                                    {chat.customorderid.isstarted == true && chat.customorderid.orderstatus==7 &&
                                                        <span class="text-muted float-right">You started this order</span>
                                                    }
                                                    {chat.customorderid.isstarted == true && chat.customorderid.orderstatus==8 &&
                                                        <span class="text-muted float-right"><strong>You completed this order</strong></span>
                                                    }
                                                </div>
                                            </div>
                                        }
                                        {(chat.message == null || chat.message == "") && chat.document == null && chat.customorderid.isdelete == 1 &&
                                            <div class="card p-2 shadow-sm leftchatbox">
                                                <span className="fa fa-caret-left left_iconset"></span>
                                                <p id={chat.id} class="mb-0"><strong>Custom Order Deleted</strong></p>
                                            </div>
                                        }
                                        <div class="d-block text-left text_time_set txt_clr_green">{this.chatTimeFormat(chat.created_at)}</div>                    
                                    </div>
                                </div>
                            </div>
                        );
                    } else {
                        return (
                            <div class="user-chat user-reply d-flex">
                                <div className="d-flex align-items-start">
                                    <div class="user-info ">
                                        <img
                                            src={this.props.userData ? this.props.userData.profilepic ? this.props.userData.profilepic : avatar : avatar}
                                            onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                                            class="rounded-circle"
                                            alt=""
                                            />
                                    </div>          
                                    <div class="user_msgs">                      
                                        {(chat.message != null && chat.message != "") &&
                                        <div class="card p-2 shadow-sm leftchatbox">
                                            <span className="fa fa-caret-right left_iconset"></span>
                                            <p id={chat.id} ref={(node) => chat.node = node} class={(searchIds && searchIds.length !== 0) ? searchIds.includes(chat.id) ? (searchId == chat.id) ? "mb-0 background-blue" : "mb-0 background-yellow" : "mb-0": "mb-0"}>{chat.message}</p>
                                        </div>
                                        }
                                        {(chat.message == null || chat.message == "") && chat.document != null &&
                                        <div class="card p-2 shadow-sm">
                                            <span className="fa fa-caret-right left_iconset"></span>
                                            <span>
                                                {chat.document.match(/.(jpg|jpeg|png|gif|svg|pdf|mp4|webm|ogv)$/i) &&
                                                    <a data-fancybox="files" href={chat.document}>
                                                        {chat.document.match(/.(jpg|jpeg|png|gif|svg)$/i) &&
                                                            <img src={chat.document} alt="" height="auto" width="300"/>
                                                        }
                                                        {chat.document.match(/.(pdf)$/i) &&
                                                            <i class="fa fa-file-pdf-o fa-3x white-icon" aria-hidden="true" title={chat.name !== "" ? chat.name : chat.document.split("/uploads/message/").pop()}></i>
                                                        }
                                                        {chat.document.match(/.(mp4|webm|ogv)$/i) &&
                                                            <i class="fa fa-file-video-o fa-3x white-icon" aria-hidden="true" title={chat.name !== "" ? chat.name : chat.document.split("/uploads/message/").pop()}></i>
                                                        }
                                                    </a>
                                                }
                                                {!chat.document.match(/.(jpg|jpeg|png|gif|svg|pdf|mp4|webm|ogv)$/i) &&                                                   
                                                    <a href={chat.document} target="_blank">
                                                        <i class="fa fa-file-text-o fa-3x white-icon" aria-hidden="true" title={chat.name !== "" ? chat.name : chat.document.split("/uploads/message/").pop()}></i>
                                                    </a>                                                   
                                                }
                                            </span>
                                        </div>
                                        }
                                        {(chat.message == null || chat.message == "") && chat.document == null && chat.customorderid.isdelete != 1 &&
                                            <span></span>
                                        }
                                        {(chat.message == null || chat.message == "") && chat.document == null && chat.customorderid.isdelete != 1 &&
                                            <div class="custom_order_right" id={chat.id}>
                                                <h5 class="bg_light_gry_set"><span className="custom_head"><strong>Custom Order</strong></span><span class="mb-0 float-right"><strong>${chat.customorderid.price}</strong></span></h5>
                                                <div class="body_cutome_order">
                                                    <div class="estimate_delvd_days row m-0 mb-3">
                                                        <div class="img_cusord col-lg-8 p-0">
                                                            <a data-fancybox="custom-order-files" href={chat.customorderid.servicepic}>
                                                                <img                                                                
                                                                    src={chat.customorderid.servicepic}
                                                                    onError={(e)=>{e.target.onerror = null; e.target.src=defaultImage}}
                                                                    className="img-fluid"
                                                                    alt="..."
                                                                />
                                                            </a>
                                                        </div>
                                                        <div class="est_delvery col-lg-4 p-0"><p>Estimated Delivery</p><span class="days_count">{chat.customorderid.estimatedelivery.name}</span>
                                                        </div>
                                                    </div>
                                                    <p class="mb-0"><span class="mb-0">{chat.customorderid.description}</span></p>
                                                </div>
                                                <div class="footer_cutome_order">
                                                    {chat.customorderid.reject != 0 && 
                                                        <span class="text-muted float-right"><strong>Your order has been rejected</strong></span>
                                                    }
                                                    {chat.customorderid.isstarted == true && chat.customorderid.orderstatus==7 &&
                                                        <span class="text-muted float-right"><strong>Your order has been started</strong></span>
                                                    }
                                                    {chat.customorderid.isstarted == true && chat.customorderid.orderstatus==8 &&
                                                        <span class="text-muted float-right"><strong>Your order has been completed</strong></span>
                                                    }
                                                    {chat.customorderid.isstarted == false && chat.customorderid.reject == 0 && this.props.userData.activeusertype == 1 &&
                                                        <button class="btn btn-outline-danger rounded link-cursor float-right" type="button" onClick={() => { this.deleteMessage(chat.id) }}>Delete</button>
                                                    }
                                                     {chat.customorderid.isstarted == false && chat.customorderid.reject == 0 && this.props.userData.activeusertype == 2 &&
                                                        <span class="text-muted float-right"><strong>View Only</strong></span>
                                                    }
                                                </div>
                                            </div>
                                        }
                                        {(chat.message == null || chat.message == "") && chat.document == null && chat.customorderid.isdelete == 1 &&
                                            <div class="card p-2 shadow-sm leftchatbox">
                                                <span className="fa fa-caret-right left_iconset"></span>
                                                <p id={chat.id} class="mb-0"><strong>Custom Order Deleted</strong></p>
                                            </div>
                                        }
                                        <div class="d-block text-right text_time_set txt_clr_grey">{this.chatTimeFormat(chat.created_at)}</div>                       
                                    </div>                                
                                </div>
                            </div> 
                        );
                    }
                })}
            </div>
        )
    }

    /**
     * renders file preview
     * @returns
     */
    renderFilePreview () {
        const { postFile } = this.state;
        return (
            <div className="w-100">
                {postFile.length > 0 &&
                    <div className="list-group">
                    {postFile.map((item, index) => {
                        return (
                        <div className="list-group-item " key={item}>
                            <span>{item.name}</span> &nbsp;
                            <button type="button" className="close_icon" onClick={() => { this.deleteFile(index) }}>x</button>
                        </div>
                        );
                        
                    }                   
                    )}
                    </div>
                }
            </div>
        )
    }

    /**
     * main render
     * @returns
     */
	render() {
		const { message, receiverImage, receiverName, receiverBio, receiverLastLoggingTime, receiverReview, hasMore, postFile, searchMessageText, searchArray } = this.state;
		const { chatList, chatMessages } = this.props;
		return (
			<div class="page-wrapper">
                <Loading isVisible={this.state.isLoading} />
                <div className="page_title row m-0">
                    <div class="col-lg-1 col-md-2 col-3 p-0">
                        <span className="left_icon_title">
                            {/* <img src={inbox} alt="" /> */}
                            <i class="fa fa-inbox"></i>
                        </span>
                    </div>
                    <div class="col-lg-11 col-md-10 col-9 p-0">
                        <h2 className="right_title row m-0">Inbox</h2>
                        {this.props.userData.activeusertype == 2 &&
                            <span className="title_tagline m-0 row">Send a message to any buyer or content writer about their service.</span>
                        }
                        {this.props.userData.activeusertype == 1 &&
                            <span className="title_tagline m-0 row">Send a message to your customers about their requirements to provide the highest quality service.</span>
                        }
                    </div>
                </div>                
				<div class="content-wrapper inbox_page" data-tour="inbox">
					<div class="card border mt-3 inbox_div_pg">
						<div class="d-flex disp_blk">
							{this.renderChatList()}
							{chatList && chatList.length != 0 &&
                                <div class="flex-fill chat-section-wrapper chat-position-relative">
                                    <ChatLoading isVisible={this.state.isChatLoading} />
                                    {chatMessages && chatMessages.length != 0 &&
                                        <div>
                                            <div class="chat-section-header d-flex align-items-center shadow-sm p-2">
                                                <div class="user-info mr-3">
                                                    <img
                                                        src={receiverImage}
                                                        onError={(e)=>{e.target.onerror = null; e.target.src=avatar}}
                                                        class="rounded-circle"
                                                        alt=""
                                                        />
                                                    <span class="user-status on"></span>
                                                </div>                                                 
                                                <div class=" justify-content-between flex-wrap">
                                                    {chatMessages && chatMessages.length != 0 && (receiverReview !== "0.00" || receiverBio != "") &&
                                                        <Popup 
                                                            key={45}
                                                            trigger={ 
                                                                <div class="order_details_seller">
                                                                    <p class="font-weight-bold mb-0 user-name text-break text-break">{receiverName}</p>
                                                                    {receiverLastLoggingTime != "" &&
                                                                        <span class="on-date small timeFont-Right">Last Login: {this.chatTimeFormatForLastLogin(receiverLastLoggingTime)}</span>
                                                                    } 
                                                                    {receiverLastLoggingTime == "" &&
                                                                        <span class="on-date small timeFont-Right">Last Login: Hasn’t logged in</span>
                                                                    }
                                                                </div>
                                                            } 
                                                        >
                                                            <Popup.Header>
                                                                {receiverName}
                                                            </Popup.Header>
                                                            <Popup.Content>
                                                                <div className="rating_star d-flex mt-1">
                                                                {receiverReview !== 0 && Array.from(Array(Math.round(receiverReview)), () => {
                                                                    return (
                                                                    <span className="fa fa-star star"></span>
                                                                    )
                                                                })}                              
                                                                </div> 
                                                                {receiverBio}
                                                            </Popup.Content>
                                                        </Popup>
                                                    }   
                                                    {chatMessages && chatMessages.length != 0 && (receiverReview == "0.00" && receiverBio == "") &&
                                                        <div class="order_details_seller">
                                                            <p class="font-weight-bold mb-0 user-name text-break text-break">{receiverName}</p>
                                                            {receiverLastLoggingTime != "" &&
                                                                <span class="on-date small timeFont-Right">Last Login: {this.chatTimeFormatForLastLogin(receiverLastLoggingTime)}</span>
                                                            } 
                                                            {receiverLastLoggingTime == "" &&
                                                                <span class="on-date small timeFont-Right">Last Login: Hasn’t logged in</span>
                                                            }
                                                        </div>                                                    
                                                    }
                                                </div>
                                            </div> 
                                            <div id="scrollableDiv" class="bg-grey scrollbarsmooth chatScroll">
                                                <InfiniteScroll
                                                    dataLength={chatMessages.length}
                                                    next={this.fetchMoreData}
                                                    hasMore={hasMore}
                                                    style={{ display: 'flex', flexDirection: 'column-reverse' }}
                                                    inverse={true}
                                                    scrollableTarget="scrollableDiv"
                                                    // endMessage={<strong><p class="text-success text-center px-3 shadow-sm">Beginning of the conversation</p></strong>}
                                                    loader={<strong><p class="text-success text-center px-3"><span className="fas fa-spinner fa-pulse"></span></p></strong>}
                                                    >
                                                    {this.renderChatMessages()}
                                                </InfiniteScroll>
                                            </div>
                                        </div>
                                    }
                                    
									<div class="chat-section-footer">
                                        <div class="form-group flex-fill mb-0">
											<textarea  name="message" id="message-send" placeholder="Type your message here..." class="form-control" value={message} onKeyDown={message || postFile.length !== 0 ? this.onEnterPress : ''} onChange={this.onChangeText} />
										</div>
                                        <div class="row file_updoaded_dets">
                                            {this.renderFilePreview()}
                                        </div>
                                        <div className="row ">
                                            <div className="col-lg-10 col-7 d-flex align-items-center">
                                                <label for="file-upload" class="mb-0 pb-0 mr-2 link-cursor"><span className="fa fa-paperclip"></span></label>
                                                <input type="file" multiple hidden={true} id="file-upload" disabled={postFile.length >= 5} onChange={e => {this.onFileChange(e)}} />                                            
                                            </div>
                                            <div className="col-lg-2 col-5 text-right">
                                                <button class="btn_light_green" disabled={!message && postFile.length == 0} onClick={this.sendMessage}>Send</button>
                                            </div>
                                        </div>
                                       
									</div>
								</div>
							}
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = ({ loginReducer, inboxReducer }) => {
	return {
        userData: loginReducer.userData,
		chatList: inboxReducer.chatList,
		chatMessages: inboxReducer.chatMessages,
		chatHistoryDetails: inboxReducer.chatHistoryDetails,
	}
}


export default connect(mapStateToProps, {
	sendMessage,
	getChats,
	getChatMessages,
	searchChatList,
	updateMessages,
    updateChatMessages,
    sendAttachment,
    addCartCustom,
    deleteMessage,
    removeMessage,
    rejectCustomOrder,
    customOrderDetail,
    changeMessage,
})(Inbox);
