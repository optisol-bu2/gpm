import React from "react";
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import Modal from "@material-ui/core/Modal";
import moment from 'moment';
import ChatLoading from "../../components/core/ChatLoading";
import {
    sendMessage,
    sendAttachment
} from "../../Actions/inboxAction";
import successImage from "../../assets/images/icn_done.svg";
import { PlaySound, ToastAlert } from '../../utils/sweetalert2';
import avatar from '../../assets/images/avatar.png';
import { Popup } from 'semantic-ui-react';

/**
 * ContactBuyer Component
 */
class ContactBuyer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            message: "",
            receiverId: this.props.receiverId,
            receiverName: "",
            receiverImage: "",
            isModalOpen: false,
            isMessageSent: false,
            fileUploadCount: 0,
            postFile: [],
            localTime: this.localTimeFormat(),
            isChatLoading: false,
        };

    }

    componentDidMount() {
        this.interval = setInterval(
            () => this.setLocalTime(),
            1000
        );
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    /**
     * sets local time
     */
    setLocalTime = () => {
        this.setState({ localTime: this.localTimeFormat() });
    }

    /**
     * changes the given time to chat time format
     * @param {*} dateTime
     * @returns
     */ 
    chatTimeFormat = (dateTime) => {
        if(!dateTime) return "" ; 
        var utc = moment.utc(dateTime).toDate();
        var utcDate = moment.utc(dateTime).format('YYYY-MM-DD');
        var todayDate = moment.utc().format('YYYY-MM-DD');
        var utcTime = moment(utc).local().format("DD-MM-YYYY HH:mm:ss");
        var todayTime = moment().local().format("DD-MM-YYYY HH:mm:ss");
        var ms = moment(todayTime,"DD/MM/YYYY HH:mm:ss").diff(moment(utcTime,"DD/MM/YYYY HH:mm:ss"));
        var duration = moment.duration(ms);
        if (utcDate === todayDate) {
            var getMinutes = parseInt(duration.as('minutes'));
            var chatTime = getMinutes + " minutes ago"; 
            if(parseInt(getMinutes) == 1) {
                var chatTime = getMinutes + " minute ago";
            }
            if(parseInt(getMinutes) >= 60) {
                var getHours = parseInt(duration.as('hours'));
                var chatTime = getHours + " hours ago"; 
                if(parseInt(getHours) == 1) {
                    var chatTime = getHours + " hour ago";
                }
            }
        } else {
            var getDays = parseInt(duration.as('days'));
            var chatTime = getDays + " days ago";
            if(parseInt(getDays) == 1) {
                var chatTime = getDays + " day ago";
            }
        } 
        return chatTime
    }

    /**
     * send message function for both text and file types
     */
    sendMessage = () => {
        if (this.state.message) {
            let params = {
                "message": this.state.message,
                "to": this.state.receiverId
            }
            this.setState({ isChatLoading: true });
            this.props.sendMessage(params, (message) => {
                if (this.state.postFile && this.state.postFile.length > 0) {
                    this.setState({ isChatLoading: false, message: "" });
                } else {
                    this.setState({ isChatLoading: false, message: "", isMessageSent: true });
                }
            });

        }
        if (this.state.postFile && this.state.postFile.length > 0) {
            this.state.postFile.forEach(file => {
                let postData = new FormData();
                postData.append("attachment", file);
                postData.append("to", this.state.receiverId);
                this.setState({ isChatLoading: true });
                this.props.sendAttachment(postData, (attachment) => {
                    this.setState({
                        fileUploadCount: this.state.fileUploadCount + 1,
                    }, () => {
                        if (this.state.postFile.length != 0 && this.state.fileUploadCount == this.state.postFile.length) {
                            this.setState({
                                isChatLoading: false,
                                postFile: "",
                                fileUploadCount: 0,
                                isMessageSent: true
                            });
                        }
                    });
                });
            })
        }
    }

    /**
     * handles on enter to send message
     * @param {*} e 
     */
    onEnterPress = e => {
        if (e.keyCode === 13) {
            this.sendMessage();
        }
    };

    /**
     * handles changes in the event and updates message state
     * @param {*} e 
     */
    onChangeText = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    };

    /**
     * opens the modal
     */
    openModal = () => {
        this.setState({
            isModalOpen: true,
            receiverId: this.props.receiverId.toString(),
            receiverImage: this.props.receiverImage,
            receiverName: this.props.receiverName,
            isMessageSent: false
        })
    }

    /**
     * closes the modal
     */
    closeModal = () => {
        this.setState({
            isModalOpen: false,
            receiverId: "",
            receiverImage: "",
            receiverName: "",
            message: "",
            postFile: "",
            fileUploadCount: 0,
            isMessageSent: false
        })
    }

    /**
     * assigns file in the postFile state
     * @param {*} e 
     */
    onFileChange = (e) => {
        if (e.target.files && e.target.files.length != 0) {
            if (e.target.files.length <= 5) {
                this.setState({
                    postFile: [...e.target.files]
                })
                let button = document.getElementById("message-send");
                button.focus();
            } else {
                this.setState({
                    postFile: []
                })
                ToastAlert("error", "You can only upload a maximum of 5 files at a time")
                PlaySound()
            }
        }
    };

    /**
     * removes file from the postFile state
     * @param {*} e 
     */
    deleteFile = (e) => {
        const files = this.state.postFile.filter((item, index) => index !== e);
        this.setState({
            postFile: files
        })
    };

    /**
     * changes the given time to chat time format
     * @param {*} dateTime 
     * @returns
     */
    localTimeFormat = () => {
        var utc = moment.utc().toDate();
        var chatTime = moment(utc).local().format('ddd hh:mm A');
        return chatTime
    }

    /**
     * renders file preview
     * @returns 
     */
    renderFilePreview() {
        const { postFile } = this.state;
        return (
            <div className="w-100">
                {postFile.length > 0 &&
                    <div className="list-group">
                        {postFile.map((item, index) => {
                            return (
                                <div className="list-group-item " key={item}>
                                    <span>{item.name}</span> &nbsp;
                                    <button type="button" className="close_icon" onClick={() => { this.deleteFile(index) }}>x</button>
                                </div>
                            );

                        }
                        )}
                    </div>
                }
            </div>
        )
    }

    /**
     * main render
     * @returns 
     */
    render() {
        const { message, receiverImage, receiverName, postFile, isMessageSent, isModalOpen, localTime, isChatLoading } = this.state;
        return (
            <div class={this.props.orderDetail !== undefined ? "" : "d-flex align-items-center"}>
                {!this.props.buyerName &&
                    <button
                        class={this.props.bloggerProfile !== undefined ? "btn_light_green pl-2 pr-2 pt-1 pb-1" : this.props.orderDetail !== undefined ? "btn_light_green ml-2" : "btn_light_green pl-2 pr-2"}
                        // btn_light_green pt-2 pb-2 pl-3 pr-3 mr-3
                        disabled={(this.props.userData ? this.props.userData.id : "") === this.props.receiverId}
                        onClick={(e) => { e.currentTarget.blur(); this.openModal() }}
                    >
                        {this.props.bloggerProfile !== undefined && <i class="fa fa-comments">&nbsp;</i>}
                        {this.props.bloggerProfile !== undefined ? "Message" : "Contact Seller"}
                    </button>
                }

                {this.props.buyerName && (this.props.buyerBio != "" || this.props.buyerReview !== "0.00") &&
                    <Popup 
                      key={this.props.orderId}
                      trigger={
                        <div class="order_details_seller">
                        <a disabled={(this.props.userData ? this.props.userData.id : "") === this.props.receiverId} onClick={(e) => { e.currentTarget.blur(); this.openModal() }}>
                            {this.props.buyerName}
                        </a><br/>
                        {this.props.buyerLastLoggingTime != "" &&
                            <span class="on-date date-size timeFont-Right">Last Login: {this.chatTimeFormat(this.props.buyerLastLoggingTime)}</span>
                        }
                        {this.props.buyerLastLoggingTime == "" &&
                            <span class="on-date date-size timeFont-Right">Last Login: Hasn’t logged in</span>
                        }
                        </div>}  
                    >
                      <Popup.Header>
                        {this.props.buyerName}
                      </Popup.Header>
                      <Popup.Content>
                        <div className="rating_star d-flex mt-1">
                          {this.props.buyerReview !== 0 && Array.from(Array(Math.round(this.props.buyerReview)), () => {
                            return (
                              <span className="fa fa-star star"></span>
                            )
                          })}                              
                        </div> 
                        {this.props.buyerBio}
                      </Popup.Content>
                    </Popup>
                }
                {this.props.buyerName && (this.props.buyerBio == "" && this.props.buyerReview == "0.00") &&
                    <div class="order_details_seller">
                        <a disabled={(this.props.userData ? this.props.userData.id : "") === this.props.receiverId} onClick={(e) => { e.currentTarget.blur(); this.openModal() }} >{this.props.buyerName}</a>
                        <br/>
                        {this.props.buyerLastLoggingTime != "" &&
                            <span class="on-date date-size timeFont-Right">Last Login: {this.chatTimeFormat(this.props.buyerLastLoggingTime)}</span>
                        }
                        {this.props.buyerLastLoggingTime == "" &&
                            <span class="on-date date-size timeFont-Right">Last Login: Hasn’t logged in</span>
                        }
                    </div>
                }
                <Modal
                    open={isModalOpen}
                    className="modal_popup"
                    disableBackdropClick
                    onClose={this.closeModal}
                    aria-labelledby="simple-modal-title"
                    aria-describedby="simple-modal-description">
                    {!isMessageSent ?
                        (
                            <div class="modal-dialog modal_popup mediumpopup sendamsgsize_fnt contentseleler_sendmsg" role="document">
                                <div class="modal-content">
                                    <ChatLoading isVisible={isChatLoading} />
                                    <div class="modal-header">
                                        <h5 class="popup_title" id="exampleModalLabel">Send a Message</h5>
                                        <button
                                            type="button"
                                            class="close"
                                            data-dismiss="modal"
                                            aria-label="Close"
                                            onClick={this.closeModal}
                                        >
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body overflow-auto">
                                        <div class="d-flex px-3 py-2 user-list-item align-items-center h-45">
                                            <div class="user-info mr-3">
                                                <img
                                                    src={receiverImage}
                                                    onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                                                    class="rounded-circle"
                                                    alt=""
                                                />
                                                <span class="user-status on"></span>
                                            </div>
                                            <div class="flex-fill">
                                                <div class="d-flex justify-content-between flex-wrap">
                                                    <p class="font-weight-bold mb-0 user-name text-break text-break">{receiverName}</p>
                                                </div>
                                                <p class="mb-0 user-bio small" >Local Time: <strong>{localTime}</strong></p>
                                                <p class="mb-0 user-bio small" >Avg. response time : <strong>1 Hrs</strong></p>
                                                {this.props.orderDetail !== undefined &&
                                                    <p class="mb-0 text-muted small float-right"><br />If you are not happy with the buyer's work you may contact <a href="mailto: support@enginescale.com" target="_blank">support@enginescale.com</a>.</p>
                                                }
                                            </div>
                                        </div>
                                        <div className="row file_updoaded_dets m-0">
                                            {this.renderFilePreview()}
                                        </div>
                                        <div class="chat-section-footer d-flex align-items-center p-3 form_fields_white_bg">
                                            <div class="input-group align-items-center">
                                                <div class="input-group-prepend">
                                                    <label for="file-upload" class="mt-2 link-cursor"><span className="fa fa-paperclip"></span></label>
                                                    <input type="file" multiple hidden={true} id="file-upload" disabled={postFile.length === 5} onChange={e => { this.onFileChange(e) }} />
                                                </div>
                                                <div class="form-group flex-fill mb-0 mx-3 textarea_set">
                                                    <textarea placeholder="Enter your message..." class="form-control" name="message" value={message} onChange={this.onChangeText} ></textarea>
                                                </div>
                                                <div class="input-group-append">
                                                    <div>
                                                        <button class="btn_light_green" disabled={!message && postFile.length == 0} onClick={this.sendMessage}>Send</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        ) : (
                            <div class="modal-dialog modal_popup" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="popup_title" id="exampleModalLabel">Send a Message</h5>
                                        <button
                                            type="button"
                                            class="close"
                                            data-dismiss="modal"
                                            aria-label="Close"
                                            onClick={this.closeModal}
                                        >
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div className="modal-body justify-content-center text-center">
                                        <img src={successImage} class="rounded-circle" style={{ "margin": "0 auto" }} height="80" width="80" alt="" />
                                        <br /><br />
                                        <h4 className="text-center">Message Sent!</h4>
                                    </div>
                                    <br />
                                    <div class="modal-footer justify-content-center hoverbtn_link">
                                        <Link className="btn_light_green" to={{ pathname: "/inbox" }}>View your message</Link>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = ({ loginReducer }) => {
    return {
        userData: loginReducer.userData,
    }
}


export default connect(mapStateToProps, {
    sendMessage,
    sendAttachment
})(ContactBuyer);
