import React from "react";
import { Link } from "react-router-dom";
import Pusher from 'pusher-js';
import { PUSHER_KEY, PUSHER_CLUSTER } from "../../../common/constants";
import order1 from "../../../assets/images/menu-icons/my_orders_white.svg";
import inbox1 from "../../../assets/images/menu-icons/inbox-white.svg";
import switchUser from "../../../assets/images/topheader/switch_user_icon.svg";
import avatar from "../../../assets/images/avatar.png";
import { connect } from "react-redux";
import Button from "../../../components/core/Button";
import ConfirmationDialog from "../../../components/core/ConfirmationDialog/ConfirmationDialog";
import { getServiceCategories, getServiceEstimatedDelivey, removeCartItem } from "../../../Actions/actionContainer";
import { getHeaderInfo, readNotification, readAllNotification } from "../../../Actions/inboxAction";
import { forceLogOut } from "../../../Actions/actionContainer";
import { PlaySound, ToastAlert } from "../../../utils/sweetalert2";
import like from "../../../assets/music/notification.mp3";

const likeAudio = new Audio(like);
class TopHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      messageCount: 0,
      notificationCount: 0,
      notificationData: [],
      cartData: [],
      cartDataCount: 0,
      openNotification: false,
      isOpen: false,
      openCart: false,
      isCartOpen: false,
      confirmDialog: false,
      removeCart: {}
    };
    this.notificationRef = React.createRef();
    this.cartRef = React.createRef();
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside);
    document.addEventListener('mousedown', this.handleCartClickOutside);
    let userId = this.props.userData.id
    const pusher = new Pusher(PUSHER_KEY, {
      cluster: PUSHER_CLUSTER,
      encrypted: true
    });
    const notificationChannel = pusher.subscribe('GPM-NOTIFICATION-' + userId);
    notificationChannel.bind('NOTIFICATION', data => {
      this.getHeaderInfo();
      // this.playSound(likeAudio);
      PlaySound();
    });
    const favoriteChannel = pusher.subscribe('GPM-FAVORITE-' + userId);
    favoriteChannel.bind('FAVORITE', data => {
      this.getHeaderInfo();
      // this.playSound(likeAudio);
      PlaySound();

		});
    const messageReadChannel = pusher.subscribe('GPM-MESSAGE-' + userId);
    messageReadChannel.bind('PERSONAL_MESSAGE_NOTIFICATION', data => {
      this.getHeaderInfo();
      // this.playSound(likeAudio);
      // PlaySound();

		});
    const cartChannel = pusher.subscribe('GPM-CART-NOTIFICATION-' + userId);
    cartChannel.bind('CART_NOTIFICATION', data => {
      this.getHeaderInfo();
      // this.playSound(likeAudio);
      PlaySound();

    });
    const userActivateChannel = pusher.subscribe('GPM-USER-STATE-CHANGE-' + userId);
    userActivateChannel.bind('USER_STATE_CHANGE', data => {
      this.props.forceLogOut();
    });

    this.props.getServiceCategories();
    this.props.getServiceEstimatedDelivey();
    this.getHeaderInfo();
  }

  componentDidUpdate(prevPorps) {
    if (prevPorps.isLogin !== this.props.isLogin) {
      this.props.notificationsAction();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.headerInfo) {
      this.setState({
        messageCount: nextProps.headerInfo.unread_message_count,
        notificationCount: nextProps.headerInfo.unread_notification_count,
        notificationData: nextProps.headerInfo.notifications,
        cartData: nextProps.headerInfo.cart_notifications ? nextProps.headerInfo.cart_notifications : [],
        cartDataCount: nextProps.headerInfo.cart_notifications ? nextProps.headerInfo.cart_notifications.length : 0,
      });
    }
  }

  /**
   * plays notification sound
   */
  // playSound = audioFile => {
  //   audioFile.play();
  // }

  /**
   * opens notification dropdown
   */
  onNotification = () => {
    // this.readAllNotification();
    if (!this.state.openNotification && this.state.isOpen) {
      this.setState({ openNotification: false });
    } else if (!this.state.openNotification) {
      this.setState({ openNotification: true });
    }
  }

  /**
   * reads all notifications
   */
  readAllNotification = () => {
    this.props.readAllNotification(
      notification => {
        this.getHeaderInfo();
      },
      error => {
        ToastAlert("error", error.message);
        // this.playSound(likeAudio);
        PlaySound();
      }
    );
  }

  /**
   * handles notification click
   * @param {*} id 
   */
  onClickNotification = (id) => {
    this.props.readNotification(id,
      notification => {
        this.getHeaderInfo();
      },
      error => {
        ToastAlert("error", error.message);
        // this.playSound(likeAudio);
        PlaySound();
      }
    );
  }

  /**
   * closes notification dropdown when clicked outside
   * @param {*} event 
   */
  handleClickOutside = (event) => {
    this.setState({ isOpen: false });
    if (this.notificationRef && !this.notificationRef.current.contains(event.target)) {
      this.setState({ openNotification: false, isOpen: true });
    }
  }

  /**
   * opens cart dropdown
   */
  onCart = () => {
    if (!this.state.openCart && this.state.isCartOpen) {
      this.setState({ openCart: false });
    } else if (!this.state.openCart) {
      this.setState({ openCart: true });
    }
  }

  /**
   * closes cart dropdown when clicked outside
   * @param {*} event 
   */
  handleCartClickOutside = (event) => {
    this.setState({ isCartOpen: false });
    if (this.cartRef && !this.cartRef.current.contains(event.target)) {
      this.setState({ openCart: false, isCartOpen: true });
    }
  }

  /**
   * gets header info for the user
   */
  getHeaderInfo = () => {
    this.props.getHeaderInfo();
  }
  checkBoxChange = (e) => {
    // this.setState({checked:!this.state.checked})
  };

  render() {
    const { messageCount, notificationCount, openNotification, openCart, confirmDialog, removeCart, notificationData, cartData, cartDataCount } = this.state;
    return (
      <div className="topbar-header">
        <nav className="navbar top-navbar navbar-expand-md">
          <div className="navbar-header">
            <button
              className="navbar-toggler ml-2"
              type="button"
              data-toggle="collapse"
              data-target="#left-sidebar"
              aria-controls="left-sidebar"
              aria-expanded="false"
              aria-label="Toggle navigation"
              id="navtoggler"
            >
              <i className="fa fa-bars" aria-hidden="true"></i>
            </button>
          </div>
          <div className="navbar-collapse collapse" id="navbarSupportedContent">
            <div className="box-wrap float-right">
              <ul className="navbar-nav right_icons">
                <li className="nav-item-right" data-tour="guide" onClick={this.props.openTour}>
                  <Link className="nav-link-right" to={{ pathname: this.props.activeusertype === 2 ? "/dashboard" : "/sellerDashboard" }}>
                    <span className="fas fa-info"></span>
                  </Link>
                </li>
                <li className="nav-item-right" data-tour="switchUser">
                  <a
                    href="#!"
                    className="nav-link-right"
                    data-toggle="modal"
                    data-backdrop="static"
                    data-target="#exampleModal"
                    title={this.props.activeusertype === 1
                      ? "Switch to Buyer"
                      : "Switch to Content Writer"}
                  >
                    <img src={switchUser}></img>
                  </a>
                </li>
                <li className="nav-item-right">
                  <Link className="nav-link-right notify_symbol" to={{ pathname: "/inbox" }}>
                    <span className="far fa-comments"></span>
                    {messageCount != 0 &&
                    <span className="badge badge-primary rounded-circle">
                      {messageCount != 0 ? messageCount : ""}
                    </span>
                    }                    
                  </Link>
                </li>
                <li className="nav-item-right position-relative">
                  <a onClick={this.onNotification} className="nav-link-right notification notify_symbol bell link-cursor">
                    <span className="fa fa-bell-o"></span>
                    {notificationCount != 0 &&
                      <span className="badge badge-primary rounded-circle">
                        {notificationCount != 0 ? notificationCount : ""}
                      </span>
                    }
                  </a>
                  {openNotification &&
                    <div ref={this.notificationRef} class={this.props.activeusertype === 1 ? "notification-seller-section" : "notification-section"}>
                      <div id="triangle-up"></div>
                      <h3 class="notify_head">Notifications {notificationCount == 0 ? <span className="notification-tick-mark"></span> : <a class="all_read_click" onClick={this.readAllNotification} >Mark all as read</a>}</h3>
                      <ul class="notification-menu scrollbarsmooth" id="notification-menu">
                        {notificationData && notificationData.length != 0 && notificationData.map(notification => {
                          return (
                            <li
                              id={notification.id}
                              className={notification.isread == 0 ? "link-cursor unread-notification" : "link-cursor"}
                              onClick={() => {
                                if (notification.isread == 0) {
                                  this.onClickNotification(notification.id)
                                }
                                this.setState({ openNotification: false });
                                if (notification.type == 0 && notification.metadata.usertype === this.props.activeusertype) {
                                  this.props.history.push("/myOrder/" + notification.metadata.id);
                                } else if (notification.type == 1) {
                                  this.props.history.push("/inbox");
                                } else if (notification.type == 2 && this.props.activeusertype === 2 && notification.metadata.usertype === this.props.activeusertype) {
                                  this.props.history.push("/favorite");
                                }
                              }}
                            >
                              {notification.type == 0 &&
                                <div class="row m-0">
                                  <div class="col-lg-2 p-0">
                                    <span class="img_bg_green">
                                      <img class="avatar" src={order1} alt="" />
                                    </span>
                                  </div>
                                  <div class="col-lg-10 p-0 right_cont_noti">
                                    <h3 class="d-block">Order Notification</h3>
                                    <p class="d-block">{notification.message}</p>
                                  </div>
                                </div>
                              }
                              {notification.type == 1 &&
                                <div class="row m-0">
                                  <div class="col-lg-2 p-0">
                                    <span class="img_bg_green">
                                      <img class="avatar" src={inbox1} alt="" />
                                    </span>
                                  </div>
                                  <div class="col-lg-10 p-0 right_cont_noti">
                                    <h3 class="d-block">Message Notification</h3>
                                    <p class="d-block">{notification.message}</p>
                                  </div>
                                </div>
                              }
                              {notification.type == 2 &&
                                <div class="row m-0">
                                  <div class="col-lg-2 p-0">
                                    <span class="img_bg_green">
                                      <span class="fa fa-heart-o"></span>
                                    </span>
                                  </div>
                                  <div class="col-lg-10 p-0 right_cont_noti">
                                    <h3 class="d-block">Favorite Notification</h3>
                                    <p class="d-block">{notification.message}</p>
                                  </div>
                                </div>
                              }
                              {notification.type == 4 &&
                                <div class="row m-0">
                                  <div class="col-lg-2 p-0">
                                    <span class="img_bg_green">
                                      <img class="avatar" src={order1} alt="" />
                                    </span>
                                  </div>
                                  <div class="col-lg-10 p-0 right_cont_noti">
                                    <h3 class="d-block">Custom Order Reject</h3>
                                    <p class="d-block">{notification.message}</p>
                                  </div>
                                </div>
                              }
                            </li>
                          )
                        })}
                        {notificationData && notificationData.length == 0 &&
                          <li>
                            <div class="row m-0">
                              <div class="col-lg-12 p-0 right_cont_noti">
                                <h3 className="d-block text-center">No notifications found</h3>
                              </div>
                            </div>
                          </li>
                        }
                      </ul>
                    </div>
                  }
                </li>
                {this.props.activeusertype === 2 &&
                  <li className="nav-item-right">
                    <Link className="nav-link-right" to={{ pathname: "/favorite" }}>
                      {this.props.location.pathname.includes('/favorite') ?
                        <span className="fa fa-heart heartred"></span> :
                        <span className="fa fa-heart-o"></span>
                      }
                    </Link>
                  </li>
                }
                {/* <li className="nav-item-right">
                  <a className="nav-link-right link-cursor">
                  <span className="far fa-moon"></span>
                  </a>
                </li>                             */}
                {this.props.activeusertype === 2 &&
                  <li className="nav-item-right position-relative">
                    <a onClick={this.onCart} class="nav-link-right notification notify_symbol link-cursor">
                      <span className="fa fa-shopping-cart"></span>
                      {cartDataCount != 0 &&
                        <span className="badge badge-primary rounded-circle">
                          {cartDataCount}
                        </span>

                      }
                    </a>
                    {openCart &&
                      <div ref={this.cartRef} class="cart-section">
                        <div id="triangle-up"></div>
                        <h3 class="cart_head">Cart <a class="go_to_cart" onClick={() => { this.setState({ openCart: false }); this.props.history.push("/cartdetails") }} >Go to Cart</a></h3>
                        <ul class="cart-menu scrollbarsmooth" id="cart-menu">
                          {cartData && cartData.length != 0 && cartData.map(cart => {
                            if (cart.servicetype == 1) {
                              return (
                                <li>
                                  <div class="row m-0">
                                    <div class="col-lg-2 p-0">
                                      <span class="img_bg_green">
                                        <span className="fa fa-shopping-cart"></span>
                                      </span>
                                    </div>
                                    <div class="col-lg-8 p-0 right_cont_cart">
                                      <h3 class="d-block"> Service: {cart.title} by {cart.seller} has been added to Cart</h3>
                                      <p class="d-block">Price: ${cart.price} Estimated Delivery: {cart.estimatedelivery.name}</p>
                                    </div>
                                    <div class="col-lg-2 p-0 right_cont_cart">
                                      <Button
                                        label={"Remove"}
                                        width={"50px"}
                                        customClass={"button"}
                                        backgroundColor={"rgb(255 255 255)"}
                                        textColor={"rgb(230, 0, 0)"}
                                        top={"15px"}
                                        hoverBgColor={"#fff"}
                                        onClick={(e) => {
                                          this.setState({ confirmDialog: true, removeCart: cart });
                                        }}
                                      />
                                    </div>
                                  </div>
                                </li>
                              )
                            } else if (cart.servicetype == 2) {
                              return (
                                <li>
                                  <div class="row m-0">
                                    <div class="col-lg-2 p-0">
                                      <span class="img_bg_green">
                                        <span className="fa fa-shopping-cart"></span>
                                      </span>
                                    </div>
                                    <div class="col-lg-8 p-0 right_cont_cart">
                                      <h3 class="d-block"> Content Writer: {cart.seller} has been added to Cart</h3>
                                      <p class="d-block">Price: ${cart.price} Estimated Delivery: {cart.estimateddelivery.name}</p>
                                    </div>
                                    <div class="col-lg-2 p-0 right_cont_cart">
                                      <Button
                                        label={"Remove"}
                                        width={"50px"}
                                        customClass={"button"}
                                        backgroundColor={"rgb(255 255 255)"}
                                        textColor={"rgb(230, 0, 0)"}
                                        top={"15px"}
                                        hoverBgColor={"#fff"}
                                        onClick={(e) => {
                                          this.setState({ confirmDialog: true, removeCart: cart });
                                        }}
                                      />
                                    </div>
                                  </div>
                                </li>
                              )
                            } else if (cart.servicetype == 3) {
                              return (
                                <li>
                                  <div class="row m-0">
                                    <div class="col-lg-2 p-0">
                                      <span class="img_bg_green">
                                        <span className="fa fa-shopping-cart"></span>
                                      </span>
                                    </div>
                                    <div class="col-lg-8 p-0 right_cont_cart">
                                      <h3 class="d-block"> Custom Order by {cart.seller} has been added to Cart</h3>
                                      <p class="d-block">Price: ${cart.price} Estimated Delivery: {cart.estimatedelivery.name}</p>
                                    </div>
                                    <div class="col-lg-2 p-0 right_cont_cart">
                                      <Button
                                        label={"Remove"}
                                        width={"50px"}
                                        customClass={"button"}
                                        backgroundColor={"rgb(255 255 255)"}
                                        textColor={"rgb(230, 0, 0)"}
                                        top={"15px"}
                                        hoverBgColor={"#fff"}
                                        onClick={(e) => {
                                          this.setState({ confirmDialog: true, removeCart: cart });
                                        }}
                                      />
                                    </div>
                                  </div>
                                </li>
                              )
                            }
                          })}
                          {openCart && cartData && cartData.length == 0 &&
                            <li>
                              <div class="row m-0">
                                <div class="col-lg-12 p-0 right_cont_cart">
                                  <h3 className="d-block text-center">Nothing in Cart</h3>
                                </div>
                              </div>
                            </li>
                          }
                        </ul>
                      </div>
                    }
                  </li>
                }
                <li>
                  <div className="user-profile d-none">
                    <Link to="/profile/details">
                      <h4 className="mr-2 d-none d-lg-inline-block">
                        <span className="user-name">
                          {this.props.userData
                            ? this.props.userData.username
                            : ""}
                        </span>
                        <span className="status">Available</span>
                      </h4>
                      <img
                        src={
                          this.props.userData
                            ? (this.props.userData.profilepic ? this.props.userData.profilepic : avatar)
                            : avatar
                        }
                        alt=""
                        className="user-photo-block"
                      />
                    </Link>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        {confirmDialog && (
          <ConfirmationDialog
            open={confirmDialog}
            dialogTitle={"Remove"}
            dialogContentText={`Are you sure you want to remove the data?`}
            cancelButtonText="Cancel"
            okButtonText={"Remove"}
            onCancel={() => {
              this.setState({ confirmDialog: false, removeCart: {} });
            }}
            onClose={() => {
              this.setState({ confirmDialog: false, removeCart: {} });
            }}
            onOk={(e) => {
              this.setState({ confirmDialog: false });
              this.props.removeCartItem(
                removeCart.id,
                (response) => {
                  this.setState({ removeCart: {} });
                },
                (error) => {
                  ToastAlert('error', error.message)
                  // this.playSound(likeAudio);
                  PlaySound();
                }
              );
            }}
          />
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    activeusertype: state.loginReducer.userData.activeusertype,
    headerInfo: state.inboxReducer.headerInfo
  };
};

export default connect(mapStateToProps, {
  getHeaderInfo,
  readNotification,
  readAllNotification,
  getServiceCategories,
  getServiceEstimatedDelivey,
  removeCartItem,
  forceLogOut
})(TopHeader);
