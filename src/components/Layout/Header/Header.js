import React from "react";
import TopHeader from "./TopHeader";
export default class Header extends React.Component {
  render() {
    return (
      <div>
        <TopHeader {...this.props} />
      </div>
    );
  }
}
