import React from "react";
import { Link } from "react-router-dom";
import Pusher from 'pusher-js';
import { PUSHER_KEY, PUSHER_CLUSTER } from "../../../common/constants";
import { connect } from "react-redux";

import logoImage from "../../../assets/images/landing-page/rocketreach-logo.png";
import dashboard from "../../../assets/images/menu-icons/dashboard-white.svg";
import order from "../../../assets/images/menu-icons/my_orders_white.svg";
import service from "../../../assets/images/menu-icons/service-white.svg";
import inbox from "../../../assets/images/menu-icons/inbox-white.svg";
import blogger from "../../../assets/images/menu-icons/blogger-white.svg";
import about from "../../../assets/images/menu-icons/about-gpm-white.svg";
import blog from "../../../assets/images/menu-icons/blog-white.svg";
import invite from "../../../assets/images/menu-icons/invite-a-friend-white.svg";
import help from "../../../assets/images/menu-icons/help-support-white.svg";
import logout from "../../../assets/images/menu-icons/logout-white.svg";
import avatar from "../../../assets/images/avatar.png";
import { forceLogOut } from "../../../Actions/actionContainer";
import Footer from '../Footer/Footer';

class NavBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      orderNotification: false,
      inboxNotification: false,
    }
  }
  componentDidMount() {
    let userId = this.props.userData.id

    const pusher = new Pusher(PUSHER_KEY, {
      cluster: PUSHER_CLUSTER,
      encrypted: true
    });
    const notificationChannel = pusher.subscribe('GPM-NOTIFICATION-' + userId);
    notificationChannel.bind('NOTIFICATION', data => {
      this.setState({ orderNotification: true })
    });
    const channel = pusher.subscribe('GPM-' + userId);
    channel.bind('PERSONAL_MESSAGES', data => {
      this.setState({ inboxNotification: true })
    })
  }
  /**
   * collapses navbar
   */
  collapseNavBar = () => {
    let aria_expanded = document.getElementById('navtoggler').getAttribute('aria-expanded');
    if (aria_expanded) {
      let collapse = document.getElementById('navtoggler');
      collapse.click();
    }
  }

  render() {
    const { orderNotification, inboxNotification } = this.state;
    return (
      <aside className="left-sidebar" id="left-sidebar">
        <nav className="navbar">
          <div className="navbar-header">
            <div className="navbar-brand">
              <a href="/dashboard">
                <img className="img-fluid" src={logoImage} alt="" />
              </a>
            </div>
          </div>
          <div className="user_profile_set mb-3">
            <div className="user-profile">
              <div className="profile_link">
                <span className="profile_img">
                  <Link to="/profile/details" className="usernamelink"
                    onClick={() => {
                      this.props.history.push("/profile/details");
                      let button = document.getElementById("v-profile-tab");
                      button.click();
                    }}
                  >
                    <img
                      src={this.props.userData ? (this.props.userData.profilepic ? this.props.userData.profilepic : avatar) : avatar}
                      onError={(e) => { e.target.onerror = null; e.target.src = avatar }}
                      alt=""
                      className="user-photo-block"
                    /><span className={this.props.userData ? (this.props.userData.onlinestatus ? "onoff_status fa fa-circle online_check" : "onoff_status fa fa-circle offline_check") : "onoff_status fa fa-circle online_check"}></span>
                  </Link>
                  {/* <span className="onoff_status fa fa-circle offline_check"></span> */}
                </span>
                <h4 className="mr-2 d-none d-block">
                  <Link to="/profile/details" className="usernamelink"
                    onClick={() => {
                      this.props.history.push("/profile/details");
                      let button = document.getElementById("v-profile-tab");
                      button.click();
                    }}
                  >
                    <span className="user-name">
                      {this.props.userData
                        ? this.props.userData.username
                        : ""}
                    </span>
                  </Link>
                  <span className="status">{this.props.userData ? (this.props.userData.onlinestatus ? "Available" : "Offline") : "Available"}</span> <br/>
                  <span className="usertype_status">{this.props.userData ? (this.props.userData.activeusertype == 1 ? "Content Writer" : "Customer") : "Content Writer"}</span>
                </h4>
              </div>
            </div>
          </div>
          <div className="scroll-sidebar">
            <div className="sidebar-nav">
              <ul>
                <li className="sidebar-item">
                  <a
                    className={this.props.location.pathname.includes('/sellerDashboard') ? "sidebar-link active" : "sidebar-link"}
                    onClick={() => { this.props.history.push("/sellerDashboard") }}
                  >
                    <span className="icon_img">
                      {/* <img src={dashboard} alt="" /> */}
                      <i class="fa fa-dashboard"></i>
                    </span>
                    Dashboard
                  </a>
                </li>
                <li className="sidebar-item">
                  <a
                    className={this.props.location.pathname.includes('/myOrder') ? "sidebar-link active" : "sidebar-link"}
                    onClick={() => { this.props.history.push("/myOrder"); this.setState({ orderNotification: false }) }}
                  >
                    <span className="icon_img">
                      {/* <img src={order} alt="" /> */}
                      <i class="fa fa-clipboard-list"></i>
                    </span>
                    Orders {orderNotification && <i className="fa fa-exclamation-circle required-red"></i>}
                  </a>
                </li>
                <li className="sidebar-item">
                  <a
                    className={this.props.location.pathname.includes('/contentWriter') ? "sidebar-link active d-flex" : "sidebar-link d-flex"}
                    onClick={() => { this.props.history.push("/contentWriter") }}
                  >
                    <span className="icon_img">
                      {/* <img src={blogger} alt="" /> */}
                      <i class="fas fa-blog"></i>
                    </span>
                    Become Blogger
                  </a>
                </li>
                {/* <li className="sidebar-item">
                  <a
                    className={this.props.location.pathname.includes('/services') ? "sidebar-link active" : "sidebar-link"}
                    onClick={() => { this.props.history.push("/services") }}
                  >
                   <span className="icon_img"><img src={service} alt="" /></span>Your Services
                  </a>
                </li> */}
                <li className="sidebar-item">
                  <a
                    className={this.props.location.pathname.includes('/inbox') ? "sidebar-link active" : "sidebar-link"}
                    onClick={() => { this.props.history.push("/inbox"); this.setState({ inboxNotification: false }) }}
                  >
                    <span className="icon_img">
                      {/* <img src={inbox} alt="" /> */}
                      <i class="fas fa-inbox"></i>
                    </span>
                    Inbox {inboxNotification && <i className="fa fa-exclamation-circle required-red"></i>}
                  </a>
                </li>
                <li className="sidebar-item">
                  <a
                    className={this.props.location.pathname.includes('/about') ? "sidebar-link active" : "sidebar-link"}
                    onClick={() => { this.props.history.push("/about") }}
                  >
                    <span className="icon_img">
                      {/* <img src={about} alt="" /> */}
                      <i class="fas fa-question-circle"></i>
                    </span>
                    FAQ
                  </a>
                </li>
                <li className="sidebar-item">
                  <a
                    className={this.props.location.pathname.includes('/blogs') ? "sidebar-link active" : "sidebar-link"}
                    onClick={() => { this.props.history.push("/blogs") }}
                  >
                    <span className="icon_img">
                      {/* <img src={blog} alt="" /> */}
                      <i class="fab fa-blogger-b"></i>
                    </span>
                    EngineScale Blog
                  </a>
                </li>
                <li className="sidebar-item">
                  <a
                    className={this.props.location.pathname.includes('/invite') ? "sidebar-link active" : "sidebar-link"}
                    onClick={() => { this.props.history.push("/invite") }}
                  >
                    <span className="icon_img">
                      {/* <img src={invite} alt="" /> */}
                      <i class="fas fa-user-plus"></i>
                    </span>
                    Invite a Friend
                  </a>
                </li>
                <li className="sidebar-item">
                  <a
                    className={this.props.location.pathname.includes('/help') ? "sidebar-link active" : "sidebar-link"}
                    onClick={() => { this.props.history.push("/help") }}
                  >
                    <span className="icon_img">
                      {/* <img src={help} alt="" /> */}
                      <i class="fas fa-headset"></i>
                    </span>
                    Help & Support
                  </a>
                </li>
                <li className="sidebar-item">
                  <Link
                    to="/"
                    className="sidebar-link"
                    onClick={() => {
                      this.props.forceLogOut();
                      // window.location.reload();
                    }}
                  >
                    <span className="icon_img">
                      {/* <img src={logout} alt="" /> */}
                      <i class="fas fa-sign-out-alt"></i>
                    </span>
                    Logout
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <Footer />
      </aside>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    forceLogOut: () => dispatch(forceLogOut()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);;
