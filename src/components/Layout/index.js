import React from "react";
import { connect } from "react-redux";
import Header from "./Header/Header";
import BuyerNavBar from "./BuyerNavbar/NavBar";
import SellerNavBar from "./SellerNavBar/NavBar";
import TermsNdCondition from "../custom/TermsNdCondition";
import Tour from 'reactour'
import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock'
import { switchUser, reLoginAction } from "../../Actions/actionContainer";

class CommonLayout extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      checked: false,
      goToStep: 0,
      isGuideOpen: props.userData.isfirstimelogin,
    };
    this.steps = this.props.userData.activeusertype === 2 ? 
      [
        {
          selector: '[data-tour="dashboard"]',
          content: 'The dashboard displays the total $ amount spent as well as analytics for placed orders and organic traffic.',
        },
        {
          selector: '[data-tour="dashboard-ga"]',
          content: 'You can connect to Google Analytics to see the growth of the organic traffic of your website.',
        },
        {
          selector: '[data-tour="content-writer"]',
          content: 'Place an order with any content writer to write blogs to grow your organic traffic.',
          position: [20, 100],
        },
        {
          selector: '[data-tour="myOrders"]',
          content: 'Click here to see all of your current, completed, and cancelled orders.',
        },
        {
          selector: '[data-tour="inbox"]',
          content: 'View all of your messages with any content writer in the marketplace.',
        },
        {
          selector: '[data-tour="switchUser"]',
          content: 'Switch between buyer and seller in our marketplace anytime.',
        },
        {
          selector: '[data-tour="guide"]',
          content: 'Click to go through the onboarding process again anytime.',
        },
      ] 
      :
      [
        {
          selector: '[data-tour="sellerDashboard"]',
          content: 'The dashboard displays the total $ amount earned as well as analytics for current and completed orders',
        },
        {
          selector: '[data-tour="blogger"]',
          content: 'Become a Blogger or content writer to help businesses grow their organic search traffic.',
          position: [20, 100],
        },
        {
          selector: '[data-tour="service"]',
          content: 'Create a custom order for a specific user or create a service as an outreach specialist to help content gain visibility.',
          position: [20, 100],
        },
        {
          selector: '[data-tour="inbox"]',
          content: 'Communicate with buyers who have placed an order for your service.',
        },
        {
          selector: '[data-tour="switchUser"]',
          content: 'Switch between buyer and seller in our marketplace anytime.',
        },
        {
          selector: '[data-tour="guide"]',
          content: 'Click to go through the onboarding process again anytime.',
        },
      ];
  }
  

  disableBody = target => disableBodyScroll(target)
  enableBody = target => enableBodyScroll(target)

  render() {
    return (
      <div>
        <section id="main-container">          
          {this.props.userData.activeusertype === 2 ? (
            <BuyerNavBar {...this.props} />
          ) : (
            <SellerNavBar {...this.props} />
          )}
          <section id="main-content">
            <Header openTour={() => this.setState({ isGuideOpen: true })} {...this.props} />
            {React.cloneElement(this.props.children, { ...this.props })}
          </section>
        </section>    
        <TermsNdCondition {...this.props}/>
        <>
          <Tour
            steps={this.steps}
            isOpen={this.state.isGuideOpen}
            disableInteraction
            goToStep={this.state.goToStep}
            getCurrentStep={currentStep => {
              var step = currentStep+1;
              if (this.props.userData.activeusertype === 2) {
                if (step == 1) {
                  if (!this.props.location.pathname.includes('/dashboard')) {
                    this.props.history.push("/dashboard")
                    this.setState({ goToStep : 0 });
                  }
                } else if (step == 2) {
                  if (!this.props.location.pathname.includes('/dashboard')) {
                    this.props.history.push("/dashboard")
                    this.setState({ goToStep : 1 });
                  }
                } else if (step == 3) {
                  if (!this.props.location.pathname.includes('/contentWriter')) {
                    this.props.history.push("/contentWriter");
                    this.setState({ goToStep : 2 });
                  }
                } else if (step == 4) {
                  if (!this.props.location.pathname.includes('/myOrder')) {
                    this.props.history.push("/myOrder");
                    this.setState({ goToStep : 3 });
                  }
                } else if (step == 5) {
                  if (!this.props.location.pathname.includes('/inbox')) {
                    this.props.history.push("/inbox");
                    this.setState({ goToStep : 4 });
                  }
                }
              } else if (this.props.userData.activeusertype === 1) {
                if (step == 1) {
                  if (!this.props.location.pathname.includes('/sellerDashboard')) {
                    this.props.history.push("/sellerDashboard")
                    this.setState({ goToStep : 0 });
                  }
                } else if (step == 2) {
                  if (!this.props.location.pathname.includes('/contentWriter')) {
                    this.props.history.push("/contentWriter");
                    this.setState({ goToStep : 1 });
                  }
                } else if (step == 3) {
                  if (!this.props.location.pathname.includes('/services')) {
                    this.props.history.push("/services");
                    this.setState({ goToStep : 2 });
                  }
                } else if (step == 4) {
                  if (!this.props.location.pathname.includes('/inbox')) {
                    this.props.history.push("/inbox");
                    this.setState({ goToStep : 3 });
                  }
                } else if (step == 4) {
                  if (!this.props.location.pathname.includes('/inbox')) {
                    this.props.history.push("/inbox");
                    this.setState({ goToStep : 3 });
                  }
                }
              }
            }}
            startAt={0}
            rounded={5}
            accentColor={"#37B877"}
            lastStepNextButton={<button className="btn_light_green">All Set!</button>}
            onRequestClose={() => {
              // if (this.props.userData.isfirstimelogin == true) {
              //   this.props.reLoginAction();
              // }
              if (this.props.userData.activeusertype === 1){
                this.props.history.push("/contentWriter");
              }
              if (this.props.userData.activeusertype === 2){
                this.props.history.push("/dashboard");
              }
              this.setState({ isGuideOpen: false })
            }}
            onAfterOpen={this.disableBody}
            onBeforeClose={this.enableBody}
            closeWithMask={false}
            update={this.state.goToStep}
            updateDelay={1500}
          />
        </>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { userData: state.loginReducer.userData };
};

const mapDispatchToProps = (dispatch) => {
  return {
    switchUser: (onSuccess, onFailure) =>
      dispatch(switchUser(onSuccess, onFailure)),
    reLoginAction: (onSuccess, onFailure) =>
      dispatch(reLoginAction(onSuccess, onFailure)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CommonLayout);