import React from 'react';
class Footer extends React.Component {

  render() {
    return (
      <footer>
        <div className="inner_footer">                
          <div className="col-md-12 copy-right text-center">
              <p>&copy; 2021 <span>EngineScale</span> All rights Reserved.</p>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;