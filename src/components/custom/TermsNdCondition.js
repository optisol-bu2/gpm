import React, { useState } from "react";
import { ToastAlert,PlaySound } from "../../utils/sweetalert2";
import Loading from "../../components/core/Loading";

const TermsNdCondition = (props) => {
  const [checked, setChecked] = useState(false);
  const [isLoading, setLoading] = useState(null);

  const onSubmit = (e) => {
    if (!checked) {
      return( 
      ToastAlert("error", "Please accept terms and condition."),
      PlaySound()
      );
    } else {
      setLoading(true);
      props.switchUser(
        () => {
          setLoading(false);
          if (props.userData.activeusertype === 2) {
            props.history.push("/sellerDashboard");
          } else {
            props.history.push("/dashboard");
            window.location.reload();
          }
        },
        () => setLoading(false)
      );
      setChecked(false);
      let button = document.getElementById("closemodal");
      button.click();
    }
  };
  return (
    <div
      className="modal modal_popup"
      id="exampleModal"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <Loading isVisible={isLoading} />
      <div className="modal-dialog modal_popup modal_dialog_settings modal-dialog-scrollable" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="popup_title" id="exampleModalLabel">
              {props.userData.activeusertype === 1 ? "Become a Buyer" : "Become a Content Writer"}
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
              onClick={() => setChecked(false)}
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          {props.userData.activeusertype == 2 &&
            <div className="modal-body scrollbarsmooth terms_conditions_page">            
              <p>
                At EngineScale, you can earn money as a content writer by becoming a content writer or guestpost outreach specialist. content writers help businesses and websites grow their oganic traffic through content writing and outreach through guest posts.
              </p>
              <ul>
                <li>
                  <p>
                    A content writer can switch to become a buyer, and a buyer can switch to become a content writer with the same email login and credentials.
                  </p>
                </li>
                <li>
                  <p>
                    When you sign up as a content writer, you must fill out all of the necessary billing information to connect to your appropriate Stripe account before creating any service.
                  </p>
                </li>
                <li>
                  <p>
                    All of your services may be viewed in the marketplace. The more highly rated your service, the more customers and visiblity you gain, and the more money you earn.
                  </p>
                </li>
                <li>
                  <p>
                    As a content writer, if you are obligated to deliver the highest quality service to the buyers, otherwise your review ratings may be impacted.
                  </p>
                </li>
              </ul>
              <div className="custom-control custom-checkbox pl-0">
                  <input
                    type="checkbox"
                    className="custom-control-input"
                    id="customCheckTerms"
                    checked={checked}
                    onChange={() => setChecked(!checked)}
                  />
                  <label
                    style={{ marginLeft: 20 }}
                    className="custom-control-label text-dark"
                    htmlFor="customCheckTerms"
                  >
                    Accept all terms & conditions
                  </label>
                </div>
            </div>
          }
          {props.userData.activeusertype == 1 && 
            <div className="modal-body scrollbarsmooth terms_conditions_page">            
              <p>
                At EngineScale, you can grow the organic traffic of your website by hiring content writers to create amazing content through blog posts and conducting outreach by publishing those blog posts on other high traffic websites related to your niche.
              </p>
              <ul>
                <li>
                  <p>
                    A buyer can switch to become a content writer, and a content writer can switch to become a buyer with the same email login and credentials.
                  </p>
                </li>
                <li>
                  <p>
                    As a buyer, you are allotted up to 3 revisions for any work offered by the content writer. The revisions will allow you to iterate on the services provided by the content writers to meet your requirements and satisfaction.
                  </p>
                </li>
                <li>
                  <p>
                    Be sure to message and communicate with the the content writer before placing your order and provide crystal clear requirements to the content writer. All order submissions are final!
                  </p>
                </li>
                <li>
                  <p>
                    The more blog posts you produce on your website, and the more visibility you gain from publishing guest posts on other related websites in your niche, the greater your organic traffic.
                  </p>
                </li>
              </ul>
              <div className="custom-control custom-checkbox pl-0">
                <input
                  type="checkbox"
                  className="custom-control-input"
                  id="customCheckTerms"
                  checked={checked}
                  onChange={() => setChecked(!checked)}
                />
                <label
                  style={{ marginLeft: 20 }}
                  className="custom-control-label text-dark"
                  htmlFor="customCheckTerms"
                >
                  Accept all terms & conditions
                </label>
              </div>
            </div>
          }
          <div className="modal-footer justify-content-between">
            <button type="button" 
              className="btn_grey w-auto"
              data-dismiss="modal"
              aria-label="Close"
              onClick={() => setChecked(false)}
            >
              Back
            </button>
            <button
              type="button"
              className="btn_light_green effect effect-2"
              onClick={onSubmit}
            >
              Continue
            </button>
            <button
              type="button"
              id="closemodal"
              className="btn_light_green"
              data-dismiss="modal"
              hidden
            ></button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TermsNdCondition;
