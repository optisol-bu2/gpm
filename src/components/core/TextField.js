import React, { useState, useEffect } from "react";
import { FormHelperText, TextField } from "@material-ui/core";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";

/**
 * textField control with mandatory props
 * @param id
 * @param {string} value
 * @param {boolean} error
 * @param {boolean} required
 * @param {boolean} multiline
 * @param {int} rows
 * @param {boolean} fullWidth
 * @param {boolean} numeric
 * @param {string} defaultvalue
 * @param {boolean} isAutoFocus
 * @param {string} errorMessage
 * @param {string} label
 * @param {string} placeholder
 * @param options
 * @param disabled
 * @callback onChange
 * @param {boolean} isInline
 * @param {string} type // VISIT : https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input#Form_%3Cinput%3E_types
 *  When float value is there then type="decimal"
 */

const useStyles = makeStyles({
  root: {
    "& .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
      borderColor: "#BFDEFF",
    },
    "&:hover .MuiOutlinedInput-root .MuiOutlinedInput-notchedOutline": {
      borderColor: "none",
    },
    "& .MuiOutlinedInput-root.Mui-focused .MuiOutlinedInput-notchedOutline": {
      borderColor: "#BFDEFF",
      // borderColor: "#17a2b8",
      //borderColor: "#88dbf2",
      border: "3px solid",
    },
  },
});

const TextInput = ({ ...props }) => {
  const {
    id,
    value: valueFromProps,
    error: isError = false,
    required = false,
    multiline = false,
    rows = 1,
    options,
    fullWidth = true,
    numeric = false,
    type = "text",
    defaultvalue,
    onChange,
    onKeyUp,
    isAutoFocus: autoFocusFromProps = false,
    errorMessage: errorMessageFromProps = "This Field is Required",
    label,
    variant = "outlined",
    placeholder = label,
    isInline = false,
    disabled = false,
    maxLength,
    margin = "dense",
  } = props;
  const classes = useStyles();
  const [value, setValue] = useState(valueFromProps || "");
  const [autoFocus, setAutoFocus] = useState(autoFocusFromProps);
  const [errorMessage, setErrorMessage] = useState(errorMessageFromProps);
  const [error, setError] = useState(isError);
  const [isChanged, setIsChanged] = useState(false);

  useEffect(() => {
    setIsChanged(false);
    setValue(valueFromProps);
  }, [valueFromProps]);

  useEffect(() => {
    setErrorMessage(errorMessageFromProps);
  }, [errorMessageFromProps]);

  function TextChange(event) {
    let newValue = event.target.value;
    if (newValue.length <= maxLength) setValue(newValue);
    setError(false);
    if (isInline) {
      if (newValue !== "") {
        setIsChanged(true);
      } else {
        setIsChanged(false);
      }
    }
    setErrorMessage("");
    setAutoFocus(true);
  }

  const mobile = new RegExp(/^\d*$/);
  const email = new RegExp(
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  );
  const password = new RegExp(
    /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/
  );

  function regexCheck(id) {
    if (id === "mobile") {
      if (!(mobile.test(value) && value.length === 10)) {
        setErrorMessage("Mobile number should be 10 digits");
        setError(true);
        props.setIsMobileRegexValid(false);
      } else {
        props.setIsMobileRegexValid(true);
      }
    } else if (id === "email") {
      if (!email.test(value)) {
        setErrorMessage("Please Enter Valid Email");
        setError(true);
        props.setIsEmailRegexValid(false);
      } else {
        props.setIsEmailRegexValid(true);
      }
    } else if (id === "password") {
      if (!password.test(value)) {
        setErrorMessage("Please Enter Valid password");
        setError(true);
      }
    } else props.setIsInvoiceNoRegexValid(true);
  }

  function blurEvent() {
    if (!value && required && value !== undefined) {
      setError(true);
      setErrorMessage(errorMessageFromProps);
    }
    if (value && value !== undefined) {
      if (options && options.type) {
        regexCheck(options.type);
      } else {
        setError(false);
        setErrorMessage("");
      }
    }
  }

  return (
    <div>
      <TextField
        className={classes.root}
        variant={variant}
        disabled={disabled}
        error={error}
        required={required}
        autoFocus={false}
        type={type}
        id={id || value}
        multiline={multiline}
        rows={rows}
        defaultValue={defaultvalue}
        value={value}
        fullWidth={fullWidth}
        margin={margin}
        style={{ borderColor: "green" }}
        onChange={(e) => {
          TextChange(e);
          onChange && onChange(e);
        }}
        onKeyUp={(e) => {
          onKeyUp && onKeyUp(e);
        }}
        onBlur={() => {
          blurEvent();
        }}
        onKeyDown={(e) => {
          if (type && type === "decimal") {
            const value = e.key;
            const invalidChars = ["-", "+", "e", "E"];
            const isDotContain = (e.target.value.match(/[.]/g) || []).length;
            const isNotDecimal = /^[0-9]*(\.[0-9]*)?$/.test(value);
            if (
              e.keyCode !== 8 &&
              (invalidChars.includes(value) ||
                isNotDecimal === false ||
                (isDotContain === 1 && value === ".") ||
                isDotContain > 1)
            ) {
              e.preventDefault();
            }
          }
        }}
        onPaste={(e) => {
          if (type && type === "decimal") {
            const value = e.clipboardData.getData("Text");
            const invalidChars = ["-", "+", "e", "E"];
            const isNotDecimal = /^[0-9](\.[0-9]*)?$/.test(value);
            if (invalidChars.includes(value) || isNotDecimal === false) {
              e.preventDefault();
            }
          }
        }}
        placeholder={placeholder}
        onInput={(e) => {
          if (numeric) e.target.value = e.target.value.replace(/[^0-9]/g, "");
        }}
        title={value}
      />
      {(error || isError) && !isInline && (
        <FormHelperText id="component-error-text">
          <div style={{ color: "red" }}>{errorMessage}</div>
        </FormHelperText>
      )}
    </div>
  );
};

TextInput.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default TextInput;
