import React from "react";

const ChatLoader = (props) => {

  return (
    <div className={(props.isVisible)?'site-loader':''}>
        <div className={(props.isVisible)?'loading-spinner':''}>

        </div>
    </div>
  );

};

export default ChatLoader;
