import React, { useState, useEffect } from "react";
import { InputLabel, FormControl } from "@material-ui/core";

/**
 * File upload control
 *
 */
const UploadButtons = (props) => {
  const {
    id = "",
    multiple = false,
    label = "File",
    required = false,
    accept = "pif/*",
    isFileDeleted = false,
    isFileCanceled = false,
    size = 500,
    error: isError = false,
    errorMessage: errorMessageFromProps = "This Field is Required",
    isFileReset: isFileResetFromProps = false,
  } = props;
  const [selectedFile, setSelectedFile] = useState("");
  const [errorMessage, setErrorMessage] = useState(errorMessageFromProps);
  const [error, setError] = useState(isError);
  const [isChanged, setIsChanged] = useState(isFileResetFromProps);

  const colors = {
    primaryColor: "#ED3237", // primary blue
    defaultWhite: "white",
    defaultRed: "#ED3237",
  };

  const restrictedFileType = [
    "EXE",
    "PIF",
    "APPLICATION",
    "GADGET",
    "MSI",
    "MSP",
    "COM",
    "SCR",
    "HTA",
    "CPL",
    "MSC",
    "JAR",
    "BAT",
    "CMD",
    "VB",
    "VBS",
    "VBE",
    "JS",
    "JSE",
    "WS",
    "WSF",
    "WSC",
    "WSH",
    "PS1",
    "PS1XML",
    "PS2",
    "PS2XML",
    "PSC1",
    "PSC2",
    "MSH",
    "MSH1",
    "MSH2",
    "MSHXML",
    "MSH1XML",
    "MSH2XML",
    "SCF",
    "LNK",
    "INF",
    "REG",
  ];

  useEffect(() => {
    if (
      isFileDeleted === true ||
      isFileCanceled === true ||
      (isFileResetFromProps === true && isChanged === false)
    ) {
      document.getElementById(`icon-button-file${id}`).value = null;
      setError(isError);
      setErrorMessage(errorMessageFromProps);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isFileDeleted, isFileCanceled, isFileResetFromProps, isChanged]);

  useEffect(() => {
    setErrorMessage(errorMessageFromProps);
  }, [errorMessageFromProps]);

  const checkFiletype = (file) => {
    let isValid = true;
    if (file) {
      let fileName = file.name;
      let typeString = fileName.split(".");
      const extension = typeString[typeString.length - 1];
      restrictedFileType.forEach((invalidType) => {
        if (extension.toUpperCase() === invalidType) {
          isValid = false;
          setErrorMessage("Invalid file type.");
          props.setIsFileError(true);
          return isValid;
        }
      });

      let fileTypeData = file.type.split("/");
      const fileType = fileTypeData[fileTypeData.length - 1];
      restrictedFileType.forEach((invalidType) => {
        if (fileType.toUpperCase() === invalidType) {
          isValid = false;
          setErrorMessage("Invalid file type.");
          props.setIsFileError(true);
          return isValid;
        }
      });

      let fileSize = file.size / 1 / 1024; //In kb
      if (fileSize >= size) {
        isValid = false;
        setErrorMessage("File size exceed.");
        props.setIsFileError(true);
        setError(true);
        return isValid;
      }
    }
    return isValid;
  };

  // on file select event
  const onChange = (event, isFromProps = false) => {
    let isValid;
    if (isFromProps) {
      setSelectedFile(null);
    } else {
      setSelectedFile(selectedFile);
      setIsChanged(true);
      for (let i = 0; i < event.target.files.length; i++) {
        let file = event.target.files[i];
        if (file) {
          isValid = checkFiletype(file);
          if (isValid) {
            props.setIsFileError(false);
            setSelectedFile(file.name);
            let fileReader = new FileReader();
            fileReader.readAsDataURL(file);

            fileReader.onload = (e) => {
              const fileData = e.target.result;

              var dur = new Audio(fileReader.result);
              dur.onloadedmetadata = function () {
                props.setDuration(dur.duration);
              };

              //API call to check file type (fileData as req param)
              if (
                fileData.split(",")[1] !== "" &&
                fileData.split(",")[1] !== undefined
              ) {
                props.setBase64(fileData);
                props.setIsImageCanceled(false);
                props.setFile(file);
                props.setFileName(file.name);
                setError(false);
              } else {
                setErrorMessage("Invalid file type.");
                setError(true);
                props.setIsFileError(true);
                setIsChanged(false);
                return;
              }
            };
          } else {
            setIsChanged(false);
            setError(true);
            return;
          }
        } else {
          setIsChanged(false);
          setError(true);
          setErrorMessage(errorMessageFromProps);
          return;
        }
      }
    }
  };
  return (
    <FormControl className="form-group" required={required}>
      {/* <InputLabel shrink htmlFor="bootstrap-input" required={required}>
        {label}
        <span style={{ color: colors.defaultRed, fontSize: 10 }}>
          {" "}
          Max {size/1024 > 1 ? size/1024 : size} {size/1024 > 1 ? 'MB' : 'KB'}
        </span>
      </InputLabel> */}
      <span
        style={{
          border: "1px solid #AFAFAF",
          width: "100%",
          borderRadius: 5,
          overflow: "hidden",
        }}
      >
        <input
          accept={accept}
          id={id}
          type="file"
          multiple={multiple}
          onChange={onChange}
          hidden
        />
      </span>

      {((isError && !isChanged) || error) && (
        <span style={{ color: colors.defaultRed }}>{errorMessage}</span>
      )}
    </FormControl>
  );
};
export default UploadButtons;
