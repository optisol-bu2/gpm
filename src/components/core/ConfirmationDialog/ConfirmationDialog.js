import React from "react";
import Modal from "@material-ui/core/Modal";

export default function ConfirmationDialog({
  open = false,
  onCancel,
  onOk,
  onClose,
  dialogTitle,
  dialogContentText,
  cancelButtonText = "Cancel",
  okButtonText = "OK",
}) {
  return (
    <Modal
      open={open}
      disableBackdropClick
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description">
      <div class="modal-dialog modal_popup" role="document">
          <div class="modal-content">
              <div class="modal-header">
                  <h5 class="popup_title" id="confirmModalLabel">{dialogTitle}</h5>
                  <button
                    type="button"
                    class="close"
                    data-dismiss="modal"
                    aria-label="Close"
                    onClick={onClose}
                  >
                    <span aria-hidden="true">×</span>
                  </button>
              </div>
              <div className="modal-body">
                <p>{dialogContentText}</p>
              </div>
              <div class="modal-footer">
                <button class="btn_ebony_clay" onClick={onCancel}>{cancelButtonText}</button>
                <button class="btn_green" onClick={onOk}>{okButtonText}</button>
              </div>
          </div>
      </div>
    </Modal>
  );
}
