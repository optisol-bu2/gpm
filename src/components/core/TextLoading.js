import React from "react";
import Modal from "@material-ui/core/Modal";

const TextLoader = (props) => {
  const body = (
    <div
      style={{
        position: "absolute",
        left: "50%",
        top: "50%",
        transform: "translate(-50%, -50%)",
        textAlign: "center",
      }}
    >
      <div className="fa-3x">
        <i className="fas fa-circle-notch fa-spin"></i>
      </div>
      <div>
        <span style={{
          color: "white",
          fontSize: "20px",
          fontWeight: 600,
        }}>{props.loadingText}
        </span>
      </div>
    </div>
  );

  return (
    <div>
      <Modal
        open={props.isVisible}
        disableBackdropClick
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
};

export default TextLoader;
