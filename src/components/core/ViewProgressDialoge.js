import React from "react";
import { makeStyles, Theme, createStyles } from "@material-ui/core/styles";
import CircularProgress from "@material-ui/core/CircularProgress";

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: "flex",
      "& > * + *": {
        marginLeft: theme.spacing(2),
      },
    },
  })
);

export default function InsideViewProgressDialoge(props) {
  const {
    size = 50,
    color = "primary" /* 'primary' | 'secondary' | 'inherit' */,
  } = props;

  const classes = useStyles();
  return (
    <div>
      <div className={classes.root}>
        <CircularProgress color={color} size={size} />
      </div>
    </div>
  );
}
