import React, { useState } from "react";
import Button from "@material-ui/core/Button";
import Icon from "@material-ui/core/Icon";

/**
 * Button control with mandatory props
 * @param id
 * @param {string} variant   ["contained" || "outlined"]
 * @param {string} size  [ "small" || "medium" || "large"]
 * @param {boolean} disabled
 * @param {string} textColor [colorCode(hax)]
 * @param {string} label
 * @param {string} textTransform  ["capitalize" || "uppercase" || "lowercase"]
 * @param {string} borderColor [colorCode(hax)]
 * @param {string} backgroundColor [colorCode(hax)]
 * @param {link} href
 * @callback onClick
 */

const ButtonControl = props => {
  const {
    id,
    variant = "contained",
    size = "medium",
    disabled = false,
    onClick,
    textColor = "white",
    backgroundColor = "Red",
    hoverBgColor = "Black",
    label = "Button ",
    textTransform = "capitalize",
    top = "",
    href,
    customClass = "button",
    borderRadius = "50px",
    width = "100px",
    minWidth = "64px",
    svgRight = "",
    svgLeft = "",
    outLined = false,
    disableElevation = false
  } = props;

  const [color, setColor] = useState(backgroundColor);

  const svgRightIcon = (
    <Icon>
      <img alt="Alternative Text" src={svgRight} />
    </Icon>
  );

  const svgLeftIcon = (
    <Icon>
      <img alt="Alternative Text" src={svgLeft} />
    </Icon>
  );

  return (
    <div>
      <Button
        id={id}
        variant={variant} //"contained" || "outlined"
        size={size} // "small" // "medium" // "large"
        style={{
          width: width,
          color: textColor,
          backgroundColor: color,
          textTransform: textTransform, // "capitalize" || "uppercase" || "lowercase"
          borderRadius: borderRadius,
          top: top,
          "min-width": minWidth
        }}
        disabled={disabled}
        variant={outLined ? "outlined" : ""}
        onClick={e => onClick && onClick(e)}
        autoHighlight={true}
        onMouseEnter={e => setColor(hoverBgColor)}
        onMouseLeave={e => setColor(backgroundColor)}
        startIcon={svgRight !== "" ? svgRightIcon : ""}
        endIcon={svgLeft !== "" ? svgLeftIcon : ""}
        href={href && href}
        className={customClass}
        disableElevation={disableElevation}
      >
        <b> {label}</b>
      </Button>
    </div>
  );
};
export default ButtonControl;
