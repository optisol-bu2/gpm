import React from "react";
import { GoogleLogin } from "react-google-login";
import { GOOGLE_KEY } from "../../common/constants";

const GoogleLoginBtn = (props) => {
  const { onSuccess, onFailure } = props;
  return (
    <GoogleLogin
      clientId={GOOGLE_KEY}
      buttonText={
        <p className="text-center flex-fill mb-0">Continue with Google</p>
      }
      className="btn btn-outline-secondary rounded btn-block"
      onSuccess={onSuccess}
      onFailure={onFailure}
      cookiePolicy={"single_host_origin"}
    />
  );
};

export default GoogleLoginBtn;
