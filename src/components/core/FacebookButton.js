import React from "react";
import FacebookLogin from "react-facebook-login";
import { FACEBOOK_KEY } from "../../common/constants";
import facebookImage from "../../assets/images/facebook.svg";
const FacebookButton = props => {
  const { responseFacebook } = props;
  return (
    <FacebookLogin
      appId={FACEBOOK_KEY}
      fields="name,email,picture"
      textButton={
        <p className="text-center flex-fill mb-0">Continue with Facebook</p>
      }
      callback={responseFacebook}
      cssClass="btn btn-outline-secondary rounded btn-block  facebook d-flex justify-content-between"
      icon={<img width="18" src={facebookImage} alt="..." />}
    />
  );
};

export default FacebookButton;
