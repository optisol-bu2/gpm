import React from "react";
import NoPageImg from "../../assets/images/404.png";
import OopsImg from "../../assets/images/landing-page/oops.png";
import logoImg from "../../assets/images/landing-page/rocketreach-logo_bg.png";

const PageNotFound = (props) => {
  return (
    <div>
      <div class="container">
          <div class="row mb-3 logobgpgfound">
           <div class="col-sm-4 text-center mt-3"><img src={logoImg} alt="" class="img-fluid" /></div>
          </div>
      </div>
      <section class="pagenotfound pb-5">
        <div class="container">
          <div class="row align-items-center">
            <div class="col-sm-4 text-center">
              <img src={NoPageImg} alt="" class="img-fluid" />
            </div>
            <div class="col-sm-8 text-center">
              <img src={OopsImg} alt="" class="img-fluid" />
              <p class="pg404page">404 - page not found</p>
              <p class="text-center lastlinepgfnd">
                This page you are requested could not found. Please click this 
                </p>
                <p class="text-center">
                <a href="javascript:void(0)" 
                onClick={() => {
                  props.history.goBack();
                }} 
                class="btn_light_green pageerror_link">Go Back</a>
              </p> 
            </div>
          </div>
        </div>
      </section>  
    </div>
  );
};

export default PageNotFound;
