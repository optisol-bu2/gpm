import React, { useRef, useEffect } from 'react'
import LoadingBar from 'react-top-loading-bar'

const Loader = (props) => {
  const loadingRef = useRef(null)

  useEffect(() => {
    if (props.isVisible == true) {
      loadingRef.current.continuousStart();
    } else if (props.isVisible == false) {
      loadingRef.current.complete();
    }
  }, [props.isVisible])

  return (
    <div>
      <LoadingBar color='#12da75' ref={loadingRef} />
    </div>
  );
};

export default Loader;
