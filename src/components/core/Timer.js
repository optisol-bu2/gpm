import React, { useState } from "react";
import Timer from "react-compound-timer";
import * as moment from "moment";
import { connect } from "react-redux";
import { setEndTimer, last24Hours } from "../../Actions/actionContainer";

const TimerControl = (props) => {
  
  const {
    //initialTime,
    deadLineHours,
    currentDate,
    startDate,
    direction,
    sellerComment
  } = props;

  function getInitialTime () {
    var now = moment(currentDate); // todays date
    var end = moment(startDate); // start date
    var duration = moment.duration(now.diff(end));
    var initialTime = (deadLineHours - duration.asHours()) * 3600000;
    var timerValue= initialTime > 0 ? deadLineHours == 0 ? 0 : initialTime : 0;
    // timerValue = 0;
    if(timerValue <= 86400000 && timerValue != 0 && sellerComment) {
      props.last24Hours(true)
    }
    if(timerValue == 0) {
      props.setEndTimer(true)
    }
    return timerValue;
  }

  function splitTime(num) {
    var number = num,
      output = [],
      sNumber = number.toString();
    for (var i = 0, len = sNumber.length; i < len; i += 1) {
      output.push(+sNumber.charAt(i));
    }
    return output;
  }

  return (
    <Timer initialTime={getInitialTime()} direction={direction} 
      checkpoints={[
        { time: 0, callback: () => props.setEndTimer(true) },
        { time: 86400000, callback: () => { if(sellerComment) props.last24Hours(true)} }
      ]}>
      {/* <Timer  initialTime={0* 1000} direction={direction} checkpoints={[{time: 0, callback: () => props.setEndTimer(true)}]}>   */}
      {() => (
        <React.Fragment>
          <span style={{ color: props.lastHrTimer ? "#e74e5b" : "#46586D"}}>
            <Timer.Days
              formatValue={(value) =>
                `${value < 10 ? `0` : splitTime(value)[0]}`
              }
            />{" "}
          </span>
          <span style={{ color: props.lastHrTimer ? "#e74e5b" : "#46586D"}}>
            <Timer.Days
              formatValue={(value) =>
                `${value < 10 ? `${value}` : splitTime(value)[1]}`
              }
            />{" "}
          </span>
          <p class="text-mine_clr" style={{ color: props.lastHrTimer ? "#e74e5b" : "#37B877" }}>:</p>
          <span style={{ color: props.lastHrTimer ? "#e74e5b" : "#46586D"}}>
            <Timer.Hours
              formatValue={(value) =>
                `${value < 10 ? `0` : splitTime(value)[0]}`
              }
            />{" "}
          </span>
          <span style={{ color: props.lastHrTimer ? "#e74e5b" : "#46586D"}}>
            <Timer.Hours
              formatValue={(value) =>
                `${value < 10 ? `${value}` : splitTime(value)[1]}`
              }
            />{" "}
          </span>
          <p class="text-mine_clr" style={{ color: props.lastHrTimer ? "#e74e5b" : "#37B877" }}>:</p>
          <span style={{ color: props.lastHrTimer ? "#e74e5b" : "#46586D"}}>
            <Timer.Minutes
              formatValue={(value) =>
                `${value < 10 ? `0` : splitTime(value)[0]}`
              }
            />{" "}
          </span>
          <span style={{ color: props.lastHrTimer ? "#e74e5b" : "#46586D"}}>
            <Timer.Minutes
              formatValue={(value) =>
                `${value < 10 ? `${value}` : splitTime(value)[1]}`
              }
            />{" "}
          </span>
          <p class="text-mine_clr" style={{ color: props.lastHrTimer ? "#e74e5b" : "#37B877" }}>:</p>
          <span style={{ color: props.lastHrTimer ? "#e74e5b" : "#46586D"}}>
            <Timer.Seconds
              formatValue={(value) =>
                `${value < 10 ? `0` : splitTime(value)[0]}`
              }
            />{" "}
          </span>
          <span style={{ color: props.lastHrTimer ? "#e74e5b" : "#46586D"}}>
            <Timer.Seconds
              formatValue={(value) =>
                `${value < 10 ? `${value}` : splitTime(value)[1]}`
              }
            />{" "}
          </span>
        </React.Fragment>
      )}
    </Timer>
  );
};

const mapStateToProps = (state) => {
  return {
    lastHrTimer: state.mainReducer.lastHrTimer,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    setEndTimer: (param) => dispatch(setEndTimer(param)),
    last24Hours: (param) => dispatch(last24Hours(param)),
  };
};

export default connect(mapStateToProps,mapDispatchToProps)
(TimerControl);
