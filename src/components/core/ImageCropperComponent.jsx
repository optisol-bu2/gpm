import React from "react";
import Modal from "@material-ui/core/Modal";
import { connect } from "react-redux";
import Cropper from 'react-cropper';
import 'cropperjs/dist/cropper.css';

class ImageCropperComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            src: null,
            isModalOpen: false,
        }
        this.cropperRef = React.createRef();
    }

    /**
     * sets initial image crop screen
     */
    _setCropBoxData() {
        var w = this.props.width;
        var h = this.props.height;
        this.cropperRef.cropper.setCropBoxData({ width: w, height: h });
    }

    /**
     * crops the image
     */
    _crop = () => {
        const imageElement = this.cropperRef?.current;
        const cropper = imageElement?.cropper;
        const canvas = cropper.getCroppedCanvas();
        canvas.toBlob(blob => {
            if (!blob) {
                return;
            }
            window.URL.revokeObjectURL(this.fileUrl);
            this.fileUrl = window.URL.createObjectURL(blob);
            this.setState({ blobFile: blob })
        }, "image/png");

        this.setState({ croppedImageUrl: canvas.toDataURL("image/png") });
    };

    /**
     * closes the modal
     */
    closeModel = () => {
        var data = { croppedImageUrl: '', blobFile: '' };
        this.props.setCroppedImage(data);
        this.props.onClose();
    }

    /**
     * cancel crop image
     */
    cancelCrop = () => {
        this.setState({ src: null })
        this.closeModel();
    }

    /**
     * saves cropped image
     */
    saveCrop = () => {
        const { croppedImageUrl, blobFile } = this.state;
        var data = { croppedImageUrl, blobFile };
        this.props.setCroppedImage(data);
        this.props.onClose();
    }

    render() {
        return (
            <div>
                <Modal
                    open={this.props.isOpen}
                    style={{ overflow: "scroll" }}
                    disableBackdropClick
                    onClose={this.closeModel}
                >
                    <div className="modal-dialog modal_popup modal-lg" role="document">
                        <div className="modal-content">
                            <div class="modal-header">
                                <h5 class="popup_title" id="exampleModalLabel">Crop Image</h5>
                                <button
                                    type="button"
                                    class="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                    onClick={this.closeModel}
                                >
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <div>
                                    <Cropper
                                        ref={this.cropperRef}
                                        src={this.props.src}
                                        autoCropArea={0}
                                        style={{ height: '500px' }}
                                        aspectRatio={this.props.aspectRatio ? this.props.aspectRatio : 'NAN'}
                                        guides={false}
                                        crop={this._crop}
                                        minCropBoxWidth={400}
                                        minCropBoxHeight={100}
                                        minContainerWidth={this.props.width}
                                        minContainerHeight={this.props.height}
                                        ready={this._setCropBoxData}
                                        dragMode={'move'}
                                        viewMode={1}
                                    />

                                </div>
                                <div className="modal-footer">
                                    <div className="row">
                                        <div className="col-md-12 text-right mt-3">
                                            <button
                                                onClick={this.cancelCrop}
                                                className="btn_grey w-auto mr-3"
                                            >Cancel </button>
                                            <button
                                                className="btn_green"
                                                onClick={this.saveCrop}
                                            >Crop and Save </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {};
};

const mapDispatchToProps = {
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ImageCropperComponent);