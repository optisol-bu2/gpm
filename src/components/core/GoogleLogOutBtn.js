import React from "react";
import { GoogleLogout } from "react-google-login";
import { GOOGLE_KEY } from "../../common/constants";

const GoogleLogOutBtn = props => {
  const { logout } = props;
  return (
    <GoogleLogout
      clientId={GOOGLE_KEY}
      buttonText="Logout"
      onLogoutSuccess={logout}
    ></GoogleLogout>
  );
};

export default GoogleLogOutBtn;
