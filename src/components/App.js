import React from "react";
import AppRouter from "../routers/RTContainer";

export const App = (props) => {
  return (
    <div>
      <AppRouter {...props} />
    </div>
  );
};
