import React from "react";
import ReactDOM from "react-dom";
import { App } from "../src/components/App";
import "bootstrap/dist/css/bootstrap.min.css";
import "@fortawesome/fontawesome-free/css/fontawesome.min.css";
import "@fortawesome/fontawesome-free/css/solid.min.css";
import "@fortawesome/fontawesome-free/css/regular.min.css";
import "@fortawesome/fontawesome-free/css/v4-shims.min.css";
import "../src/assets/sass/styles.scss";
import "../src/assets/css/bootstrap.min.css";
import "../src/assets/css/custom.css";
import "../src/assets/css/swiper.min.css";


import ParentErrorBoundary from "./ParentErrorBoundary";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import configureStore from "./store/configureStore";
const { store, persistor } = configureStore();

/**
 * This file renders our components inside the root div in index.html
 */
ReactDOM.render(
  <Provider store={store}>
    {/* Persisting store when the page is refreshed/redirected */}
    <PersistGate loading={null} persistor={persistor}>
      {/* Wrapping the main component inside error boundary to handle all
    application errors */}
      <ParentErrorBoundary>
        <App />
      </ParentErrorBoundary>
    </PersistGate>
  </Provider>,
  document.getElementById("root")
);

// serviceWorker.unregister();
