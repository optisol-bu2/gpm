# EngineScale 
The engine behind scaling the organic traffic.
At EngineScale, we provide a marketplace of highly vetted, talented content writers ready to craft incredible blog posts and articles to drive the organic search traffic.

## Version 
react - 16.13.1

## Installation 
```bash
npm install
```

## To Start Server:
```bash
npm start
```

## To Visit App:
Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.
The page will reload if you make edits.
You will also see any lint errors in the console.

----------------------
### Folder Structure
----------------------
## build 

```bash
npm run build
```

Builds the app for production to this `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

------------------------------------------------------------------------------------------------------------
## node_modules 
contains all the dependencies that are needed for an initial working react app.

------------------------------------------------------------------------------------------------------------
## public 
root folder that gets served up as our react app. It includes files such as, 

`favicon.ico`
icon file that is used in index.html as favicon.

`index.html`
template file which is served up when we run start script to launch our app.

`manifest.json` 
It’s used to define our app, mostly contains metadata. Used by mobile phones to add web app to the home-screen of a device. Part of PWA.

`robots.txt`
defines rules for spiders, crawlers and scrapers for accessing our app.

`Logo.png` 
logo which can be used in manifest file with dimensions 192*192 px and 512*512 px.

------------------------------------------------------------------------------------------------------------

## src 
it’s our react app source code folder i.e. containing components, tests, css files etc.

------------------------------------------------------------------------------------------------------------
# Actions
Actions are a plain JavaScript object that contains information. 
They are the only source of information for the store. 
They have a type field that tells what kind of action to perform and all other fields contain information or data.
Each action created in the application has an intention to fetch various information from the server using the API, 

`actionType.js`
constants that contain the string value that identifies the type of action uniquely in the application.

------------------------------------------------------------------------------------------------------------
# assets 
This folder contains all the media assets, such as images, videos, css, sass, webfonts, etc.

------------------------------------------------------------------------------------------------------------
# common 
`constants.js`
This file contains all the global constants used across the application such as baseurls, api keys, etc.

------------------------------------------------------------------------------------------------------------
# components
The component is the basic building block of any application. Thus the components created in this application are designed generically so that they can be re-usable throughout the application.
Two types of Components used in this application are,

1) Functional Component
=> Functional because they are basically functions
=> Stateless because they do not hold and/or manage state
=> Presentational because all they do is output UI elements

Example File : "components/core/Timer.js"

2) Class Component  
Use a class component if you:
*need to manage local state,
*need to add lifecycle methods to our component,
*need to add logic for event handlers.

Example File : "components/ContactSeller/ContactSeller.jsx"

------------------------------------------------------------------------------------------------------------
# modules 

`Container.js` - Containers are the primary components and the entry point from which we call child components. Basically, container components are called smart components because each container component is connected to Redux and consumes the global data coming from the store.

Example: MarketPlace/Master 
 
MarketPlaceParent.js - parent component 

Container.js - container of MarketPlaceParent component

------------------------------------------------------------------------------------------------------------
# Reducers 
Reducers are normal functions used to manage a whole application’s state. 
They use the name of the action to be generated along with the action data.
Based on the action data, the state object can be manipulated using different switch cases.

It uses action.type and based on the type the switch statement identifies, triggers an action. 
If there are no matching conditions, then the default section will be triggered.

------------------------------------------------------------------------------------------------------------
# routers
standard library for routing in React. It enables the navigation among views of various components in a React Application, allows changing the browser URL, and keeps the UI in sync with the URL.

------------------------------------------------------------------------------------------------------------
# sagas 
Saga is a redux middleware library used to allow a Redux store to interact with resources outside of itself asynchronously. This includes making HTTP requests to external services, accessing browser storage, and executing I/O operations. 

------------------------------------------------------------------------------------------------------------
# store 
A store holds the whole state tree of our application.
Store configuration done by combining Reducers, Sagas and Middleware.
`configureStore.js`
configureStore() - method that simplifies the store setup process. 
configureStore() wraps around the Redux library’s createStore() method and combineReducers() method and handles most of the store setup for us automatically.

------------------------------------------------------------------------------------------------------------
# utils
place where you can place small snippets which you can use throughout the application. 
Small functions to build bigger things with.

------------------------------------------------------------------------------------------------------------
`index.js`

The package ReactDOM renders our application (specifically the App component and every component within it)by attaching it to a HTML element with an id value of 'root'.
<Provider store={store}> around entire <App> component allows other components to talk to the store.

`ParentErrorBoundary.js`

To handle all errors occured in application and show fallback UI insted of crashing.
Error won't show up in production mode, it's just a development tool duplicating the normal browser console.
 
`serviceWorker.js`

Service worker for pre-caching the scripts files of our react app thereby improving performance.

------------------------------------------------------------------------------------------------------------

# .env.local
Local overrides. This file is loaded for all environments except test.
an entry for .env.local should be added to the revision control ignore list (e.g. .gitignore) as it should not be checked in.

# .gitignore
This file specifies intentionally untracked files that Git should ignore

# package.json
This file contains various metadata that is relevant to our project. It specifies the dependencies being used in the project which helps npm setup same environment on different machine for our project.



